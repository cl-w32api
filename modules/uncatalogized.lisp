
(cl:in-package w32apimod)

(define-w32api-module uncatalogized :uncatalogized)

(cl:in-package cl-w32api.module.uncatalogized)

(defconstant-exported WINVER #x0400)

(defconstant-exported _WIN32_WINNT #x0400)

(defconstant-exported MAX_PATH 260)

(defconstant-exported STRICT 1)

(defconstant-exported SEVERITY_SUCCESS 0)

(defconstant-exported SEVERITY_ERROR 1)

(defconstant-exported FACILITY_WINDOWS 8)

(defconstant-exported FACILITY_STORAGE 3)

(defconstant-exported FACILITY_RPC 1)

(defconstant-exported FACILITY_WIN32 7)

(defconstant-exported FACILITY_CONTROL 10)

(defconstant-exported FACILITY_NULL 0)

(defconstant-exported FACILITY_ITF 4)

(defconstant-exported FACILITY_DISPATCH 2)

(defconstant-exported FACILITY_NT_BIT #x10000000)

(defconstant-exported __NEWLIB_H__ 1)

(defconstant-exported _NEWLIB_VERSION "1.13.0")

(defconstant-exported _WANT_IO_LONG_LONG 1)

(defconstant-exported _WANT_IO_LONG_DOUBLE 1)

(defconstant-exported _WANT_IO_POS_ARGS 1)

(defconstant-exported _MB_CAPABLE 1)

(defconstant-exported _MB_LEN_MAX 8)

(defconstant-exported _HAVE_ARRAY_ALIASING 1)

(defconstant-exported __INT_MAX__ 2147483647)

(defconstant-exported __LONG_MAX__ 2147483647)

(defconstant-exported __RAND_MAX #x7fffffff)

(defconstant-exported __WCHAR_MAX__ #xffff)
































(defconstant-exported ANSI_NULL 0)

(defconstant-exported ADDRESS_TAG_BIT #x80000000)

(defconstant-exported ACE_OBJECT_TYPE_PRESENT #x00000001)

(defconstant-exported ACE_INHERITED_OBJECT_TYPE_PRESENT #x00000002)

(defconstant-exported APPLICATION_ERROR_MASK #x20000000)

(defconstant-exported ERROR_SEVERITY_SUCCESS #x00000000)

(defconstant-exported ERROR_SEVERITY_INFORMATIONAL #x40000000)

(defconstant-exported ERROR_SEVERITY_WARNING #x80000000)

(defconstant-exported ERROR_SEVERITY_ERROR #xC0000000)

(defconstant-exported COMPRESSION_FORMAT_NONE #x0000)

(defconstant-exported COMPRESSION_FORMAT_DEFAULT #x0001)

(defconstant-exported COMPRESSION_FORMAT_LZNT1 #x0002)

(defconstant-exported COMPRESSION_ENGINE_STANDARD #x0000)

(defconstant-exported COMPRESSION_ENGINE_MAXIMUM #x0100)

(defconstant-exported COMPRESSION_ENGINE_HIBER #x0200)

(defconstant-exported ACCESS_ALLOWED_ACE_TYPE #x0)

(defconstant-exported ACCESS_DENIED_ACE_TYPE #x1)

(defconstant-exported SYSTEM_AUDIT_ACE_TYPE #x2)

(defconstant-exported SYSTEM_ALARM_ACE_TYPE #x3)

(defconstant-exported ANYSIZE_ARRAY 1)

(defconstant-exported OBJECT_INHERIT_ACE 1)

(defconstant-exported CONTAINER_INHERIT_ACE 2)

(defconstant-exported NO_PROPAGATE_INHERIT_ACE 4)

(defconstant-exported INHERIT_ONLY_ACE 8)

(defconstant-exported VALID_INHERIT_FLAGS 16)

(defconstant-exported SUCCESSFUL_ACCESS_ACE_FLAG 64)

(defconstant-exported FAILED_ACCESS_ACE_FLAG 128)

(defconstant-exported DELETE #x00010000)

(defconstant-exported READ_CONTROL #x20000)

(defconstant-exported WRITE_DAC #x40000)

(defconstant-exported WRITE_OWNER #x80000)

(defconstant-exported SYNCHRONIZE #x100000)

(defconstant-exported STANDARD_RIGHTS_REQUIRED #xF0000)

(defconstant-exported STANDARD_RIGHTS_READ #x20000)

(defconstant-exported STANDARD_RIGHTS_WRITE #x20000)

(defconstant-exported STANDARD_RIGHTS_EXECUTE #x20000)

(defconstant-exported STANDARD_RIGHTS_ALL #x1F0000)

(defconstant-exported SPECIFIC_RIGHTS_ALL #xFFFF)

(defconstant-exported ACCESS_SYSTEM_SECURITY #x1000000)

(defconstant-exported MAXIMUM_ALLOWED #x2000000)

(defconstant-exported GENERIC_READ #x80000000)

(defconstant-exported GENERIC_WRITE #x40000000)

(defconstant-exported GENERIC_EXECUTE #x20000000)

(defconstant-exported GENERIC_ALL #x10000000)

(defconstant-exported FILE_LIST_DIRECTORY #x00000001)

(defconstant-exported FILE_READ_DATA #x00000001)

(defconstant-exported FILE_ADD_FILE #x00000002)

(defconstant-exported FILE_WRITE_DATA #x00000002)

(defconstant-exported FILE_ADD_SUBDIRECTORY #x00000004)

(defconstant-exported FILE_APPEND_DATA #x00000004)

(defconstant-exported FILE_CREATE_PIPE_INSTANCE #x00000004)

(defconstant-exported FILE_READ_EA #x00000008)

(defconstant-exported FILE_READ_PROPERTIES #x00000008)

(defconstant-exported FILE_WRITE_EA #x00000010)

(defconstant-exported FILE_WRITE_PROPERTIES #x00000010)

(defconstant-exported FILE_EXECUTE #x00000020)

(defconstant-exported FILE_TRAVERSE #x00000020)

(defconstant-exported FILE_DELETE_CHILD #x00000040)

(defconstant-exported FILE_READ_ATTRIBUTES #x00000080)

(defconstant-exported FILE_WRITE_ATTRIBUTES #x00000100)

(defconstant-exported FILE_SHARE_READ #x00000001)

(defconstant-exported FILE_SHARE_WRITE #x00000002)

(defconstant-exported FILE_SHARE_DELETE #x00000004)

(defconstant-exported FILE_SHARE_VALID_FLAGS #x00000007)

(defconstant-exported FILE_ATTRIBUTE_READONLY #x00000001)

(defconstant-exported FILE_ATTRIBUTE_HIDDEN #x00000002)

(defconstant-exported FILE_ATTRIBUTE_SYSTEM #x00000004)

(defconstant-exported FILE_ATTRIBUTE_DIRECTORY #x00000010)

(defconstant-exported FILE_ATTRIBUTE_ARCHIVE #x00000020)

(defconstant-exported FILE_ATTRIBUTE_DEVICE #x00000040)

(defconstant-exported FILE_ATTRIBUTE_NORMAL #x00000080)

(defconstant-exported FILE_ATTRIBUTE_TEMPORARY #x00000100)

(defconstant-exported FILE_ATTRIBUTE_SPARSE_FILE #x00000200)

(defconstant-exported FILE_ATTRIBUTE_REPARSE_POINT #x00000400)

(defconstant-exported FILE_ATTRIBUTE_COMPRESSED #x00000800)

(defconstant-exported FILE_ATTRIBUTE_OFFLINE #x00001000)

(defconstant-exported FILE_ATTRIBUTE_NOT_CONTENT_INDEXED #x00002000)

(defconstant-exported FILE_ATTRIBUTE_ENCRYPTED #x00004000)

(defconstant-exported FILE_ATTRIBUTE_VALID_FLAGS #x00007fb7)

(defconstant-exported FILE_ATTRIBUTE_VALID_SET_FLAGS #x000031a7)

(defconstant-exported FILE_COPY_STRUCTURED_STORAGE #x00000041)

(defconstant-exported FILE_STRUCTURED_STORAGE #x00000441)

(defconstant-exported FILE_VALID_OPTION_FLAGS #x00ffffff)

(defconstant-exported FILE_VALID_PIPE_OPTION_FLAGS #x00000032)

(defconstant-exported FILE_VALID_MAILSLOT_OPTION_FLAGS #x00000032)

(defconstant-exported FILE_VALID_SET_FLAGS #x00000036)

(defconstant-exported FILE_SUPERSEDE #x00000000)

(defconstant-exported FILE_OPEN #x00000001)

(defconstant-exported FILE_CREATE #x00000002)

(defconstant-exported FILE_OPEN_IF #x00000003)

(defconstant-exported FILE_OVERWRITE #x00000004)

(defconstant-exported FILE_OVERWRITE_IF #x00000005)

(defconstant-exported FILE_MAXIMUM_DISPOSITION #x00000005)

(defconstant-exported FILE_DIRECTORY_FILE #x00000001)

(defconstant-exported FILE_WRITE_THROUGH #x00000002)

(defconstant-exported FILE_SEQUENTIAL_ONLY #x00000004)

(defconstant-exported FILE_NO_INTERMEDIATE_BUFFERING #x00000008)

(defconstant-exported FILE_SYNCHRONOUS_IO_ALERT #x00000010)

(defconstant-exported FILE_SYNCHRONOUS_IO_NONALERT #x00000020)

(defconstant-exported FILE_NON_DIRECTORY_FILE #x00000040)

(defconstant-exported FILE_CREATE_TREE_CONNECTION #x00000080)

(defconstant-exported FILE_COMPLETE_IF_OPLOCKED #x00000100)

(defconstant-exported FILE_NO_EA_KNOWLEDGE #x00000200)

(defconstant-exported FILE_OPEN_FOR_RECOVERY #x00000400)

(defconstant-exported FILE_RANDOM_ACCESS #x00000800)

(defconstant-exported FILE_DELETE_ON_CLOSE #x00001000)

(defconstant-exported FILE_OPEN_BY_FILE_ID #x00002000)

(defconstant-exported FILE_OPEN_FOR_BACKUP_INTENT #x00004000)

(defconstant-exported FILE_NO_COMPRESSION #x00008000)

(defconstant-exported FILE_RESERVE_OPFILTER #x00100000)

(defconstant-exported FILE_OPEN_REPARSE_POINT #x00200000)

(defconstant-exported FILE_OPEN_NO_RECALL #x00400000)

(defconstant-exported FILE_OPEN_FOR_FREE_SPACE_QUERY #x00800000)

(defconstant-exported FILE_ALL_ACCESS (cl:logior #xF0000 #x100000 #x1FF))

(defconstant-exported FILE_GENERIC_EXECUTE (cl:logior #x20000 #x00000080 #x00000020 #x100000))

(defconstant-exported FILE_GENERIC_READ (cl:logior #x20000 #x00000001 #x00000080 #x00000008 #x100000))

(defconstant-exported FILE_GENERIC_WRITE (cl:logior #x20000 #x00000002 #x00000100 #x00000010 #x00000004 #x100000))

(defconstant-exported FILE_NOTIFY_CHANGE_FILE_NAME #x00000001)

(defconstant-exported FILE_NOTIFY_CHANGE_DIR_NAME #x00000002)

(defconstant-exported FILE_NOTIFY_CHANGE_NAME #x00000003)

(defconstant-exported FILE_NOTIFY_CHANGE_ATTRIBUTES #x00000004)

(defconstant-exported FILE_NOTIFY_CHANGE_SIZE #x00000008)

(defconstant-exported FILE_NOTIFY_CHANGE_LAST_WRITE #x00000010)

(defconstant-exported FILE_NOTIFY_CHANGE_LAST_ACCESS #x00000020)

(defconstant-exported FILE_NOTIFY_CHANGE_CREATION #x00000040)

(defconstant-exported FILE_NOTIFY_CHANGE_EA #x00000080)

(defconstant-exported FILE_NOTIFY_CHANGE_SECURITY #x00000100)

(defconstant-exported FILE_NOTIFY_CHANGE_STREAM_NAME #x00000200)

(defconstant-exported FILE_NOTIFY_CHANGE_STREAM_SIZE #x00000400)

(defconstant-exported FILE_NOTIFY_CHANGE_STREAM_WRITE #x00000800)

(defconstant-exported FILE_NOTIFY_VALID_MASK #x00000fff)

(defconstant-exported FILE_CASE_SENSITIVE_SEARCH #x00000001)

(defconstant-exported FILE_CASE_PRESERVED_NAMES #x00000002)

(defconstant-exported FILE_UNICODE_ON_DISK #x00000004)

(defconstant-exported FILE_PERSISTENT_ACLS #x00000008)

(defconstant-exported FILE_FILE_COMPRESSION #x00000010)

(defconstant-exported FILE_VOLUME_QUOTAS #x00000020)

(defconstant-exported FILE_SUPPORTS_SPARSE_FILES #x00000040)

(defconstant-exported FILE_SUPPORTS_REPARSE_POINTS #x00000080)

(defconstant-exported FILE_SUPPORTS_REMOTE_STORAGE #x00000100)

(defconstant-exported FS_LFN_APIS #x00004000)

(defconstant-exported FILE_VOLUME_IS_COMPRESSED #x00008000)

(defconstant-exported FILE_SUPPORTS_OBJECT_IDS #x00010000)

(defconstant-exported FILE_SUPPORTS_ENCRYPTION #x00020000)

(defconstant-exported FILE_NAMED_STREAMS #x00040000)

(defconstant-exported IO_COMPLETION_QUERY_STATE #x0001)

(defconstant-exported IO_COMPLETION_MODIFY_STATE #x0002)

(defconstant-exported IO_COMPLETION_ALL_ACCESS (cl:logior #xF0000 #x100000 #x3))

(defconstant-exported DUPLICATE_CLOSE_SOURCE #x00000001)

(defconstant-exported DUPLICATE_SAME_ACCESS #x00000002)

(defconstant-exported DUPLICATE_SAME_ATTRIBUTES #x00000004)

(defconstant-exported PROCESS_TERMINATE 1)

(defconstant-exported PROCESS_CREATE_THREAD 2)

(defconstant-exported PROCESS_SET_SESSIONID 4)

(defconstant-exported PROCESS_VM_OPERATION 8)

(defconstant-exported PROCESS_VM_READ 16)

(defconstant-exported PROCESS_VM_WRITE 32)

(defconstant-exported PROCESS_DUP_HANDLE 64)

(defconstant-exported PROCESS_CREATE_PROCESS 128)

(defconstant-exported PROCESS_SET_QUOTA 256)

(defconstant-exported PROCESS_SET_INFORMATION 512)

(defconstant-exported PROCESS_QUERY_INFORMATION 1024)

(defconstant-exported PROCESS_ALL_ACCESS (cl:logior #xF0000 #x100000 #xFFF))

(defconstant-exported THREAD_TERMINATE 1)

(defconstant-exported THREAD_SUSPEND_RESUME 2)

(defconstant-exported THREAD_GET_CONTEXT 8)

(defconstant-exported THREAD_SET_CONTEXT 16)

(defconstant-exported THREAD_SET_INFORMATION 32)

(defconstant-exported THREAD_QUERY_INFORMATION 64)

(defconstant-exported THREAD_SET_THREAD_TOKEN 128)

(defconstant-exported THREAD_IMPERSONATE 256)

(defconstant-exported THREAD_DIRECT_IMPERSONATION #x200)

(defconstant-exported THREAD_ALL_ACCESS (cl:logior #xF0000 #x100000 #x3FF))

(defconstant-exported THREAD_BASE_PRIORITY_LOWRT 15)

(defconstant-exported THREAD_BASE_PRIORITY_MAX 2)

(defconstant-exported THREAD_BASE_PRIORITY_MIN -2)

(defconstant-exported THREAD_BASE_PRIORITY_IDLE -15)

(defconstant-exported EXCEPTION_NONCONTINUABLE 1)

(defconstant-exported EXCEPTION_MAXIMUM_PARAMETERS 15)

(defconstant-exported MUTANT_QUERY_STATE #x0001)

(defconstant-exported MUTANT_ALL_ACCESS (cl:logior #xF0000 #x100000 #x0001))

(defconstant-exported TIMER_QUERY_STATE #x0001)

(defconstant-exported TIMER_MODIFY_STATE #x0002)

(defconstant-exported TIMER_ALL_ACCESS (cl:logior #xF0000 #x100000 #x0001 #x0002))

(defconstant-exported SECURITY_NULL_RID 0)

(defconstant-exported SECURITY_WORLD_RID #x00000000)

(defconstant-exported SECURITY_LOCAL_RID 0)

(defconstant-exported SECURITY_CREATOR_OWNER_RID 0)

(defconstant-exported SECURITY_CREATOR_GROUP_RID 1)

(defconstant-exported SECURITY_DIALUP_RID 1)

(defconstant-exported SECURITY_NETWORK_RID 2)

(defconstant-exported SECURITY_BATCH_RID 3)

(defconstant-exported SECURITY_INTERACTIVE_RID 4)

(defconstant-exported SECURITY_LOGON_IDS_RID 5)

(defconstant-exported SECURITY_SERVICE_RID 6)

(defconstant-exported SECURITY_LOCAL_SYSTEM_RID 18)

(defconstant-exported SECURITY_BUILTIN_DOMAIN_RID 32)

(defconstant-exported SECURITY_PRINCIPAL_SELF_RID 10)

(defconstant-exported SECURITY_CREATOR_OWNER_SERVER_RID #x2)

(defconstant-exported SECURITY_CREATOR_GROUP_SERVER_RID #x3)

(defconstant-exported SECURITY_LOGON_IDS_RID_COUNT #x3)

(defconstant-exported SECURITY_ANONYMOUS_LOGON_RID #x7)

(defconstant-exported SECURITY_PROXY_RID #x8)

(defconstant-exported SECURITY_ENTERPRISE_CONTROLLERS_RID #x9)

(defconstant-exported SECURITY_SERVER_LOGON_RID #x9)

(defconstant-exported SECURITY_AUTHENTICATED_USER_RID #xB)

(defconstant-exported SECURITY_RESTRICTED_CODE_RID #xC)

(defconstant-exported SECURITY_NT_NON_UNIQUE_RID #x15)

(defconstant-exported SID_REVISION 1)

(defconstant-exported DOMAIN_USER_RID_ADMIN #x1F4)

(defconstant-exported DOMAIN_USER_RID_GUEST #x1F5)

(defconstant-exported DOMAIN_GROUP_RID_ADMINS #x200)

(defconstant-exported DOMAIN_GROUP_RID_USERS #x201)

(defconstant-exported DOMAIN_ALIAS_RID_ADMINS #x220)

(defconstant-exported DOMAIN_ALIAS_RID_USERS #x221)

(defconstant-exported DOMAIN_ALIAS_RID_GUESTS #x222)

(defconstant-exported DOMAIN_ALIAS_RID_POWER_USERS #x223)

(defconstant-exported DOMAIN_ALIAS_RID_ACCOUNT_OPS #x224)

(defconstant-exported DOMAIN_ALIAS_RID_SYSTEM_OPS #x225)

(defconstant-exported DOMAIN_ALIAS_RID_PRINT_OPS #x226)

(defconstant-exported DOMAIN_ALIAS_RID_BACKUP_OPS #x227)

(defconstant-exported DOMAIN_ALIAS_RID_REPLICATOR #x228)

(defconstant-exported SE_CREATE_TOKEN_NAME "SeCreateTokenPrivilege")

(defconstant-exported SE_ASSIGNPRIMARYTOKEN_NAME "SeAssignPrimaryTokenPrivilege")

(defconstant-exported SE_LOCK_MEMORY_NAME "SeLockMemoryPrivilege")

(defconstant-exported SE_INCREASE_QUOTA_NAME "SeIncreaseQuotaPrivilege")

(defconstant-exported SE_UNSOLICITED_INPUT_NAME "SeUnsolicitedInputPrivilege")

(defconstant-exported SE_MACHINE_ACCOUNT_NAME "SeMachineAccountPrivilege")

(defconstant-exported SE_TCB_NAME "SeTcbPrivilege")

(defconstant-exported SE_SECURITY_NAME "SeSecurityPrivilege")

(defconstant-exported SE_TAKE_OWNERSHIP_NAME "SeTakeOwnershipPrivilege")

(defconstant-exported SE_LOAD_DRIVER_NAME "SeLoadDriverPrivilege")

(defconstant-exported SE_SYSTEM_PROFILE_NAME "SeSystemProfilePrivilege")

(defconstant-exported SE_SYSTEMTIME_NAME "SeSystemtimePrivilege")

(defconstant-exported SE_PROF_SINGLE_PROCESS_NAME "SeProfileSingleProcessPrivilege")

(defconstant-exported SE_INC_BASE_PRIORITY_NAME "SeIncreaseBasePriorityPrivilege")

(defconstant-exported SE_CREATE_PAGEFILE_NAME "SeCreatePagefilePrivilege")

(defconstant-exported SE_CREATE_PERMANENT_NAME "SeCreatePermanentPrivilege")

(defconstant-exported SE_BACKUP_NAME "SeBackupPrivilege")

(defconstant-exported SE_RESTORE_NAME "SeRestorePrivilege")

(defconstant-exported SE_SHUTDOWN_NAME "SeShutdownPrivilege")

(defconstant-exported SE_DEBUG_NAME "SeDebugPrivilege")

(defconstant-exported SE_AUDIT_NAME "SeAuditPrivilege")

(defconstant-exported SE_SYSTEM_ENVIRONMENT_NAME "SeSystemEnvironmentPrivilege")

(defconstant-exported SE_CHANGE_NOTIFY_NAME "SeChangeNotifyPrivilege")

(defconstant-exported SE_REMOTE_SHUTDOWN_NAME "SeRemoteShutdownPrivilege")

(defconstant-exported SE_CREATE_GLOBAL_NAME "SeCreateGlobalPrivilege")

(defconstant-exported SE_UNDOCK_NAME "SeUndockPrivilege")

(defconstant-exported SE_MANAGE_VOLUME_NAME "SeManageVolumePrivilege")

(defconstant-exported SE_IMPERSONATE_NAME "SeImpersonatePrivilege")

(defconstant-exported SE_ENABLE_DELEGATION_NAME "SeEnableDelegationPrivilege")

(defconstant-exported SE_SYNC_AGENT_NAME "SeSyncAgentPrivilege")

(defconstant-exported SE_GROUP_MANDATORY 1)

(defconstant-exported SE_GROUP_ENABLED_BY_DEFAULT 2)

(defconstant-exported SE_GROUP_ENABLED 4)

(defconstant-exported SE_GROUP_OWNER 8)

(defconstant-exported SE_GROUP_USE_FOR_DENY_ONLY 16)

(defconstant-exported SE_GROUP_LOGON_ID 3221225472)

(defconstant-exported SE_GROUP_RESOURCE 536870912)

(defconstant-exported LANG_NEUTRAL #x00)

(defconstant-exported LANG_ARABIC #x01)

(defconstant-exported LANG_BULGARIAN #x02)

(defconstant-exported LANG_CATALAN #x03)

(defconstant-exported LANG_CHINESE #x04)

(defconstant-exported LANG_CZECH #x05)

(defconstant-exported LANG_DANISH #x06)

(defconstant-exported LANG_GERMAN #x07)

(defconstant-exported LANG_GREEK #x08)

(defconstant-exported LANG_ENGLISH #x09)

(defconstant-exported LANG_SPANISH #x0a)

(defconstant-exported LANG_FINNISH #x0b)

(defconstant-exported LANG_FRENCH #x0c)

(defconstant-exported LANG_HEBREW #x0d)

(defconstant-exported LANG_HUNGARIAN #x0e)

(defconstant-exported LANG_ICELANDIC #x0f)

(defconstant-exported LANG_ITALIAN #x10)

(defconstant-exported LANG_JAPANESE #x11)

(defconstant-exported LANG_KOREAN #x12)

(defconstant-exported LANG_DUTCH #x13)

(defconstant-exported LANG_NORWEGIAN #x14)

(defconstant-exported LANG_POLISH #x15)

(defconstant-exported LANG_PORTUGUESE #x16)

(defconstant-exported LANG_ROMANIAN #x18)

(defconstant-exported LANG_RUSSIAN #x19)

(defconstant-exported LANG_CROATIAN #x1a)

(defconstant-exported LANG_SERBIAN #x1a)

(defconstant-exported LANG_SLOVAK #x1b)

(defconstant-exported LANG_ALBANIAN #x1c)

(defconstant-exported LANG_SWEDISH #x1d)

(defconstant-exported LANG_THAI #x1e)

(defconstant-exported LANG_TURKISH #x1f)

(defconstant-exported LANG_URDU #x20)

(defconstant-exported LANG_INDONESIAN #x21)

(defconstant-exported LANG_UKRAINIAN #x22)

(defconstant-exported LANG_BELARUSIAN #x23)

(defconstant-exported LANG_SLOVENIAN #x24)

(defconstant-exported LANG_ESTONIAN #x25)

(defconstant-exported LANG_LATVIAN #x26)

(defconstant-exported LANG_LITHUANIAN #x27)

(defconstant-exported LANG_FARSI #x29)

(defconstant-exported LANG_VIETNAMESE #x2a)

(defconstant-exported LANG_ARMENIAN #x2b)

(defconstant-exported LANG_AZERI #x2c)

(defconstant-exported LANG_BASQUE #x2d)

(defconstant-exported LANG_MACEDONIAN #x2f)

(defconstant-exported LANG_AFRIKAANS #x36)

(defconstant-exported LANG_GEORGIAN #x37)

(defconstant-exported LANG_FAEROESE #x38)

(defconstant-exported LANG_HINDI #x39)

(defconstant-exported LANG_MALAY #x3e)

(defconstant-exported LANG_KAZAK #x3f)

(defconstant-exported LANG_KYRGYZ #x40)

(defconstant-exported LANG_SWAHILI #x41)

(defconstant-exported LANG_UZBEK #x43)

(defconstant-exported LANG_TATAR #x44)

(defconstant-exported LANG_BENGALI #x45)

(defconstant-exported LANG_PUNJABI #x46)

(defconstant-exported LANG_GUJARATI #x47)

(defconstant-exported LANG_ORIYA #x48)

(defconstant-exported LANG_TAMIL #x49)

(defconstant-exported LANG_TELUGU #x4a)

(defconstant-exported LANG_KANNADA #x4b)

(defconstant-exported LANG_MALAYALAM #x4c)

(defconstant-exported LANG_ASSAMESE #x4d)

(defconstant-exported LANG_MARATHI #x4e)

(defconstant-exported LANG_SANSKRIT #x4f)

(defconstant-exported LANG_MONGOLIAN #x50)

(defconstant-exported LANG_GALICIAN #x56)

(defconstant-exported LANG_KONKANI #x57)

(defconstant-exported LANG_MANIPURI #x58)

(defconstant-exported LANG_SINDHI #x59)

(defconstant-exported LANG_SYRIAC #x5a)

(defconstant-exported LANG_KASHMIRI #x60)

(defconstant-exported LANG_NEPALI #x61)

(defconstant-exported LANG_DIVEHI #x65)

(defconstant-exported LANG_INVARIANT #x7f)

(defconstant-exported SUBLANG_NEUTRAL #x00)

(defconstant-exported SUBLANG_DEFAULT #x01)

(defconstant-exported SUBLANG_SYS_DEFAULT #x02)

(defconstant-exported SUBLANG_ARABIC_SAUDI_ARABIA #x01)

(defconstant-exported SUBLANG_ARABIC_IRAQ #x02)

(defconstant-exported SUBLANG_ARABIC_EGYPT #x03)

(defconstant-exported SUBLANG_ARABIC_LIBYA #x04)

(defconstant-exported SUBLANG_ARABIC_ALGERIA #x05)

(defconstant-exported SUBLANG_ARABIC_MOROCCO #x06)

(defconstant-exported SUBLANG_ARABIC_TUNISIA #x07)

(defconstant-exported SUBLANG_ARABIC_OMAN #x08)

(defconstant-exported SUBLANG_ARABIC_YEMEN #x09)

(defconstant-exported SUBLANG_ARABIC_SYRIA #x0a)

(defconstant-exported SUBLANG_ARABIC_JORDAN #x0b)

(defconstant-exported SUBLANG_ARABIC_LEBANON #x0c)

(defconstant-exported SUBLANG_ARABIC_KUWAIT #x0d)

(defconstant-exported SUBLANG_ARABIC_UAE #x0e)

(defconstant-exported SUBLANG_ARABIC_BAHRAIN #x0f)

(defconstant-exported SUBLANG_ARABIC_QATAR #x10)

(defconstant-exported SUBLANG_AZERI_LATIN #x01)

(defconstant-exported SUBLANG_AZERI_CYRILLIC #x02)

(defconstant-exported SUBLANG_CHINESE_TRADITIONAL #x01)

(defconstant-exported SUBLANG_CHINESE_SIMPLIFIED #x02)

(defconstant-exported SUBLANG_CHINESE_HONGKONG #x03)

(defconstant-exported SUBLANG_CHINESE_SINGAPORE #x04)

(defconstant-exported SUBLANG_CHINESE_MACAU #x05)

(defconstant-exported SUBLANG_DUTCH #x01)

(defconstant-exported SUBLANG_DUTCH_BELGIAN #x02)

(defconstant-exported SUBLANG_ENGLISH_US #x01)

(defconstant-exported SUBLANG_ENGLISH_UK #x02)

(defconstant-exported SUBLANG_ENGLISH_AUS #x03)

(defconstant-exported SUBLANG_ENGLISH_CAN #x04)

(defconstant-exported SUBLANG_ENGLISH_NZ #x05)

(defconstant-exported SUBLANG_ENGLISH_EIRE #x06)

(defconstant-exported SUBLANG_ENGLISH_SOUTH_AFRICA #x07)

(defconstant-exported SUBLANG_ENGLISH_JAMAICA #x08)

(defconstant-exported SUBLANG_ENGLISH_CARIBBEAN #x09)

(defconstant-exported SUBLANG_ENGLISH_BELIZE #x0a)

(defconstant-exported SUBLANG_ENGLISH_TRINIDAD #x0b)

(defconstant-exported SUBLANG_ENGLISH_ZIMBABWE #x0c)

(defconstant-exported SUBLANG_ENGLISH_PHILIPPINES #x0d)

(defconstant-exported SUBLANG_FRENCH #x01)

(defconstant-exported SUBLANG_FRENCH_BELGIAN #x02)

(defconstant-exported SUBLANG_FRENCH_CANADIAN #x03)

(defconstant-exported SUBLANG_FRENCH_SWISS #x04)

(defconstant-exported SUBLANG_FRENCH_LUXEMBOURG #x05)

(defconstant-exported SUBLANG_FRENCH_MONACO #x06)

(defconstant-exported SUBLANG_GERMAN #x01)

(defconstant-exported SUBLANG_GERMAN_SWISS #x02)

(defconstant-exported SUBLANG_GERMAN_AUSTRIAN #x03)

(defconstant-exported SUBLANG_GERMAN_LUXEMBOURG #x04)

(defconstant-exported SUBLANG_GERMAN_LIECHTENSTEIN #x05)

(defconstant-exported SUBLANG_ITALIAN #x01)

(defconstant-exported SUBLANG_ITALIAN_SWISS #x02)

(defconstant-exported SUBLANG_KASHMIRI_INDIA #x02)

(defconstant-exported SUBLANG_KASHMIRI_SASIA #x02)

(defconstant-exported SUBLANG_KOREAN #x01)

(defconstant-exported SUBLANG_LITHUANIAN #x01)

(defconstant-exported SUBLANG_MALAY_MALAYSIA #x01)

(defconstant-exported SUBLANG_MALAY_BRUNEI_DARUSSALAM #x02)

(defconstant-exported SUBLANG_NEPALI_INDIA #x02)

(defconstant-exported SUBLANG_NORWEGIAN_BOKMAL #x01)

(defconstant-exported SUBLANG_NORWEGIAN_NYNORSK #x02)

(defconstant-exported SUBLANG_PORTUGUESE_BRAZILIAN #x01)

(defconstant-exported SUBLANG_PORTUGUESE #x02)

(defconstant-exported SUBLANG_SERBIAN_LATIN #x02)

(defconstant-exported SUBLANG_SERBIAN_CYRILLIC #x03)

(defconstant-exported SUBLANG_SPANISH #x01)

(defconstant-exported SUBLANG_SPANISH_MEXICAN #x02)

(defconstant-exported SUBLANG_SPANISH_MODERN #x03)

(defconstant-exported SUBLANG_SPANISH_GUATEMALA #x04)

(defconstant-exported SUBLANG_SPANISH_COSTA_RICA #x05)

(defconstant-exported SUBLANG_SPANISH_PANAMA #x06)

(defconstant-exported SUBLANG_SPANISH_DOMINICAN_REPUBLIC #x07)

(defconstant-exported SUBLANG_SPANISH_VENEZUELA #x08)

(defconstant-exported SUBLANG_SPANISH_COLOMBIA #x09)

(defconstant-exported SUBLANG_SPANISH_PERU #x0a)

(defconstant-exported SUBLANG_SPANISH_ARGENTINA #x0b)

(defconstant-exported SUBLANG_SPANISH_ECUADOR #x0c)

(defconstant-exported SUBLANG_SPANISH_CHILE #x0d)

(defconstant-exported SUBLANG_SPANISH_URUGUAY #x0e)

(defconstant-exported SUBLANG_SPANISH_PARAGUAY #x0f)

(defconstant-exported SUBLANG_SPANISH_BOLIVIA #x10)

(defconstant-exported SUBLANG_SPANISH_EL_SALVADOR #x11)

(defconstant-exported SUBLANG_SPANISH_HONDURAS #x12)

(defconstant-exported SUBLANG_SPANISH_NICARAGUA #x13)

(defconstant-exported SUBLANG_SPANISH_PUERTO_RICO #x14)

(defconstant-exported SUBLANG_SWEDISH #x01)

(defconstant-exported SUBLANG_SWEDISH_FINLAND #x02)

(defconstant-exported SUBLANG_URDU_PAKISTAN #x01)

(defconstant-exported SUBLANG_URDU_INDIA #x02)

(defconstant-exported SUBLANG_UZBEK_LATIN #x01)

(defconstant-exported SUBLANG_UZBEK_CYRILLIC #x02)

(defconstant-exported NLS_VALID_LOCALE_MASK 1048575)

(defconstant-exported SORT_DEFAULT 0)

(defconstant-exported SORT_JAPANESE_XJIS 0)

(defconstant-exported SORT_JAPANESE_UNICODE 1)

(defconstant-exported SORT_CHINESE_BIG5 0)

(defconstant-exported SORT_CHINESE_PRCP 0)

(defconstant-exported SORT_CHINESE_UNICODE 1)

(defconstant-exported SORT_CHINESE_PRC 2)

(defconstant-exported SORT_CHINESE_BOPOMOFO 3)

(defconstant-exported SORT_KOREAN_KSC 0)

(defconstant-exported SORT_KOREAN_UNICODE 1)

(defconstant-exported SORT_GERMAN_PHONE_BOOK 1)

(defconstant-exported SORT_HUNGARIAN_DEFAULT 0)

(defconstant-exported SORT_HUNGARIAN_TECHNICAL 1)

(defconstant-exported SORT_GEORGIAN_TRADITIONAL 0)

(defconstant-exported SORT_GEORGIAN_MODERN 1)

(defconstant-exported ACL_REVISION 2)

(defconstant-exported ACL_REVISION_DS 4)

(defconstant-exported ACL_REVISION1 1)

(defconstant-exported ACL_REVISION2 2)

(defconstant-exported ACL_REVISION3 3)

(defconstant-exported ACL_REVISION4 4)

(defconstant-exported MIN_ACL_REVISION 2)

(defconstant-exported MAX_ACL_REVISION 4)

(defconstant-exported MINCHAR #x80)

(defconstant-exported MAXCHAR #x7f)

(defconstant-exported MINSHORT #x8000)

(defconstant-exported MAXSHORT #x7fff)

(defconstant-exported MINLONG #x80000000)

(defconstant-exported MAXLONG #x7fffffff)

(defconstant-exported MAXBYTE #xff)

(defconstant-exported MAXWORD #xffff)

(defconstant-exported MAXDWORD #xffffffff)

(defconstant-exported PROCESSOR_INTEL_386 386)

(defconstant-exported PROCESSOR_INTEL_486 486)

(defconstant-exported PROCESSOR_INTEL_PENTIUM 586)

(defconstant-exported PROCESSOR_MIPS_R4000 4000)

(defconstant-exported PROCESSOR_ALPHA_21064 21064)

(defconstant-exported PROCESSOR_INTEL_IA64 2200)

(defconstant-exported PROCESSOR_ARCHITECTURE_INTEL 0)

(defconstant-exported PROCESSOR_ARCHITECTURE_MIPS 1)

(defconstant-exported PROCESSOR_ARCHITECTURE_ALPHA 2)

(defconstant-exported PROCESSOR_ARCHITECTURE_PPC 3)

(defconstant-exported PROCESSOR_ARCHITECTURE_SHX 4)

(defconstant-exported PROCESSOR_ARCHITECTURE_ARM 5)

(defconstant-exported PROCESSOR_ARCHITECTURE_IA64 6)

(defconstant-exported PROCESSOR_ARCHITECTURE_ALPHA64 7)

(defconstant-exported PROCESSOR_ARCHITECTURE_MSIL 8)

(defconstant-exported PROCESSOR_ARCHITECTURE_AMD64 9)

(defconstant-exported PROCESSOR_ARCHITECTURE_IA32_ON_WIN64 10)

(defconstant-exported PROCESSOR_ARCHITECTURE_UNKNOWN #xFFFF)

(defconstant-exported PF_FLOATING_POINT_PRECISION_ERRATA 0)

(defconstant-exported PF_FLOATING_POINT_EMULATED 1)

(defconstant-exported PF_COMPARE_EXCHANGE_DOUBLE 2)

(defconstant-exported PF_MMX_INSTRUCTIONS_AVAILABLE 3)

(defconstant-exported PF_PPC_MOVEMEM_64BIT_OK 4)

(defconstant-exported PF_ALPHA_BYTE_INSTRUCTIONS 5)

(defconstant-exported PF_XMMI_INSTRUCTIONS_AVAILABLE 6)

(defconstant-exported PF_3DNOW_INSTRUCTIONS_AVAILABLE 7)

(defconstant-exported PF_RDTSC_INSTRUCTION_AVAILABLE 8)

(defconstant-exported PF_PAE_ENABLED 9)

(defconstant-exported PF_XMMI64_INSTRUCTIONS_AVAILABLE 10)

(defconstant-exported FILE_ACTION_ADDED #x00000001)

(defconstant-exported FILE_ACTION_REMOVED #x00000002)

(defconstant-exported FILE_ACTION_MODIFIED #x00000003)

(defconstant-exported FILE_ACTION_RENAMED_OLD_NAME #x00000004)

(defconstant-exported FILE_ACTION_RENAMED_NEW_NAME #x00000005)

(defconstant-exported FILE_ACTION_ADDED_STREAM #x00000006)

(defconstant-exported FILE_ACTION_REMOVED_STREAM #x00000007)

(defconstant-exported FILE_ACTION_MODIFIED_STREAM #x00000008)

(defconstant-exported FILE_ACTION_REMOVED_BY_DELETE #x00000009)

(defconstant-exported FILE_ACTION_ID_NOT_TUNNELLED #x0000000A)

(defconstant-exported FILE_ACTION_TUNNELLED_ID_COLLISION #x0000000B)

(defconstant-exported HEAP_NO_SERIALIZE 1)

(defconstant-exported HEAP_GROWABLE 2)

(defconstant-exported HEAP_GENERATE_EXCEPTIONS 4)

(defconstant-exported HEAP_ZERO_MEMORY 8)

(defconstant-exported HEAP_REALLOC_IN_PLACE_ONLY 16)

(defconstant-exported HEAP_TAIL_CHECKING_ENABLED 32)

(defconstant-exported HEAP_FREE_CHECKING_ENABLED 64)

(defconstant-exported HEAP_DISABLE_COALESCE_ON_FREE 128)

(defconstant-exported HEAP_CREATE_ALIGN_16 #x0000)

(defconstant-exported HEAP_CREATE_ENABLE_TRACING #x20000)

(defconstant-exported HEAP_MAXIMUM_TAG #xFFF)

(defconstant-exported HEAP_PSEUDO_TAG_FLAG #x8000)

(defconstant-exported HEAP_TAG_SHIFT 16)

(defconstant-exported KEY_QUERY_VALUE 1)

(defconstant-exported KEY_SET_VALUE 2)

(defconstant-exported KEY_CREATE_SUB_KEY 4)

(defconstant-exported KEY_ENUMERATE_SUB_KEYS 8)

(defconstant-exported KEY_NOTIFY 16)

(defconstant-exported KEY_CREATE_LINK 32)

(defconstant-exported KEY_WRITE #x20006)

(defconstant-exported KEY_EXECUTE #x20019)

(defconstant-exported KEY_READ #x20019)

(defconstant-exported KEY_ALL_ACCESS #xf003f)

(defconstant-exported REG_WHOLE_HIVE_VOLATILE 1)

(defconstant-exported REG_REFRESH_HIVE 2)

(defconstant-exported REG_NO_LAZY_FLUSH 4)

(defconstant-exported REG_OPTION_RESERVED 0)

(defconstant-exported REG_OPTION_NON_VOLATILE 0)

(defconstant-exported REG_OPTION_VOLATILE 1)

(defconstant-exported REG_OPTION_CREATE_LINK 2)

(defconstant-exported REG_OPTION_BACKUP_RESTORE 4)

(defconstant-exported REG_OPTION_OPEN_LINK 8)

(defconstant-exported REG_LEGAL_OPTION 15)

(defconstant-exported OWNER_SECURITY_INFORMATION 1)

(defconstant-exported GROUP_SECURITY_INFORMATION 2)

(defconstant-exported DACL_SECURITY_INFORMATION 4)

(defconstant-exported SACL_SECURITY_INFORMATION 8)

(defconstant-exported MAXIMUM_PROCESSORS 32)

(defconstant-exported PAGE_NOACCESS #x0001)

(defconstant-exported PAGE_READONLY #x0002)

(defconstant-exported PAGE_READWRITE #x0004)

(defconstant-exported PAGE_WRITECOPY #x0008)

(defconstant-exported PAGE_EXECUTE #x0010)

(defconstant-exported PAGE_EXECUTE_READ #x0020)

(defconstant-exported PAGE_EXECUTE_READWRITE #x0040)

(defconstant-exported PAGE_EXECUTE_WRITECOPY #x0080)

(defconstant-exported PAGE_GUARD #x0100)

(defconstant-exported PAGE_NOCACHE #x0200)

(defconstant-exported MEM_COMMIT #x1000)

(defconstant-exported MEM_RESERVE #x2000)

(defconstant-exported MEM_DECOMMIT #x4000)

(defconstant-exported MEM_RELEASE #x8000)

(defconstant-exported MEM_FREE #x10000)

(defconstant-exported MEM_PRIVATE #x20000)

(defconstant-exported MEM_MAPPED #x40000)

(defconstant-exported MEM_RESET #x80000)

(defconstant-exported MEM_TOP_DOWN #x100000)

(defconstant-exported MEM_WRITE_WATCH #x200000)

(defconstant-exported MEM_PHYSICAL #x400000)

(defconstant-exported MEM_4MB_PAGES #x80000000)

(defconstant-exported SEC_BASED #x00200000)

(defconstant-exported SEC_NO_CHANGE #x00400000)

(defconstant-exported SEC_FILE #x00800000)

(defconstant-exported SEC_IMAGE #x01000000)

(defconstant-exported SEC_VLM #x02000000)

(defconstant-exported SEC_RESERVE #x04000000)

(defconstant-exported SEC_COMMIT #x08000000)

(defconstant-exported SEC_NOCACHE #x10000000)

(defconstant-exported SECTION_EXTEND_SIZE 16)

(defconstant-exported SECTION_MAP_READ 4)

(defconstant-exported SECTION_MAP_WRITE 2)

(defconstant-exported SECTION_QUERY 1)

(defconstant-exported SECTION_MAP_EXECUTE 8)

(defconstant-exported SECTION_ALL_ACCESS #xf001f)

(defconstant-exported MESSAGE_RESOURCE_UNICODE 1)

(defconstant-exported RTL_CRITSECT_TYPE 0)

(defconstant-exported RTL_RESOURCE_TYPE 1)

(defconstant-exported IMAGE_SIZEOF_FILE_HEADER 20)

(defconstant-exported IMAGE_FILE_RELOCS_STRIPPED 1)

(defconstant-exported IMAGE_FILE_EXECUTABLE_IMAGE 2)

(defconstant-exported IMAGE_FILE_LINE_NUMS_STRIPPED 4)

(defconstant-exported IMAGE_FILE_LOCAL_SYMS_STRIPPED 8)

(defconstant-exported IMAGE_FILE_AGGRESIVE_WS_TRIM 16)

(defconstant-exported IMAGE_FILE_LARGE_ADDRESS_AWARE 32)

(defconstant-exported IMAGE_FILE_BYTES_REVERSED_LO 128)

(defconstant-exported IMAGE_FILE_32BIT_MACHINE 256)

(defconstant-exported IMAGE_FILE_DEBUG_STRIPPED 512)

(defconstant-exported IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP 1024)

(defconstant-exported IMAGE_FILE_NET_RUN_FROM_SWAP 2048)

(defconstant-exported IMAGE_FILE_SYSTEM 4096)

(defconstant-exported IMAGE_FILE_DLL 8192)

(defconstant-exported IMAGE_FILE_UP_SYSTEM_ONLY 16384)

(defconstant-exported IMAGE_FILE_BYTES_REVERSED_HI 32768)

(defconstant-exported IMAGE_FILE_MACHINE_UNKNOWN 0)

(defconstant-exported IMAGE_FILE_MACHINE_I386 332)

(defconstant-exported IMAGE_FILE_MACHINE_R3000 354)

(defconstant-exported IMAGE_FILE_MACHINE_R4000 358)

(defconstant-exported IMAGE_FILE_MACHINE_R10000 360)

(defconstant-exported IMAGE_FILE_MACHINE_ALPHA 388)

(defconstant-exported IMAGE_FILE_MACHINE_POWERPC 496)

(defconstant-exported IMAGE_DOS_SIGNATURE #x5A4D)

(defconstant-exported IMAGE_OS2_SIGNATURE #x454E)

(defconstant-exported IMAGE_OS2_SIGNATURE_LE #x454C)

(defconstant-exported IMAGE_VXD_SIGNATURE #x454C)

(defconstant-exported IMAGE_NT_SIGNATURE #x00004550)

(defconstant-exported IMAGE_NT_OPTIONAL_HDR_MAGIC #x10b)

(defconstant-exported IMAGE_ROM_OPTIONAL_HDR_MAGIC #x107)

(defconstant-exported IMAGE_SEPARATE_DEBUG_SIGNATURE #x4944)

(defconstant-exported IMAGE_NUMBEROF_DIRECTORY_ENTRIES 16)

(defconstant-exported IMAGE_SIZEOF_ROM_OPTIONAL_HEADER 56)

(defconstant-exported IMAGE_SIZEOF_STD_OPTIONAL_HEADER 28)

(defconstant-exported IMAGE_SIZEOF_NT_OPTIONAL_HEADER 224)

(defconstant-exported IMAGE_SIZEOF_SHORT_NAME 8)

(defconstant-exported IMAGE_SIZEOF_SECTION_HEADER 40)

(defconstant-exported IMAGE_SIZEOF_SYMBOL 18)

(defconstant-exported IMAGE_SIZEOF_AUX_SYMBOL 18)

(defconstant-exported IMAGE_SIZEOF_RELOCATION 10)

(defconstant-exported IMAGE_SIZEOF_BASE_RELOCATION 8)

(defconstant-exported IMAGE_SIZEOF_LINENUMBER 6)

(defconstant-exported IMAGE_SIZEOF_ARCHIVE_MEMBER_HDR 60)

(defconstant-exported SIZEOF_RFPO_DATA 16)

(defconstant-exported IMAGE_SUBSYSTEM_UNKNOWN 0)

(defconstant-exported IMAGE_SUBSYSTEM_NATIVE 1)

(defconstant-exported IMAGE_SUBSYSTEM_WINDOWS_GUI 2)

(defconstant-exported IMAGE_SUBSYSTEM_WINDOWS_CUI 3)

(defconstant-exported IMAGE_SUBSYSTEM_OS2_CUI 5)

(defconstant-exported IMAGE_SUBSYSTEM_POSIX_CUI 7)

(defconstant-exported IMAGE_SUBSYSTEM_XBOX 14)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_EXPORT 0)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_IMPORT 1)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_RESOURCE 2)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_EXCEPTION 3)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_SECURITY 4)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_BASERELOC 5)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_DEBUG 6)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_COPYRIGHT 7)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_GLOBALPTR 8)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_TLS 9)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG 10)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT 11)

(defconstant-exported IMAGE_DIRECTORY_ENTRY_IAT 12)

(defconstant-exported IMAGE_SCN_TYPE_NO_PAD 8)

(defconstant-exported IMAGE_SCN_CNT_CODE 32)

(defconstant-exported IMAGE_SCN_CNT_INITIALIZED_DATA 64)

(defconstant-exported IMAGE_SCN_CNT_UNINITIALIZED_DATA 128)

(defconstant-exported IMAGE_SCN_LNK_OTHER 256)

(defconstant-exported IMAGE_SCN_LNK_INFO 512)

(defconstant-exported IMAGE_SCN_LNK_REMOVE 2048)

(defconstant-exported IMAGE_SCN_LNK_COMDAT 4096)

(defconstant-exported IMAGE_SCN_MEM_FARDATA #x8000)

(defconstant-exported IMAGE_SCN_MEM_PURGEABLE #x20000)

(defconstant-exported IMAGE_SCN_MEM_16BIT #x20000)

(defconstant-exported IMAGE_SCN_MEM_LOCKED #x40000)

(defconstant-exported IMAGE_SCN_MEM_PRELOAD #x80000)

(defconstant-exported IMAGE_SCN_ALIGN_1BYTES #x100000)

(defconstant-exported IMAGE_SCN_ALIGN_2BYTES #x200000)

(defconstant-exported IMAGE_SCN_ALIGN_4BYTES #x300000)

(defconstant-exported IMAGE_SCN_ALIGN_8BYTES #x400000)

(defconstant-exported IMAGE_SCN_ALIGN_16BYTES #x500000)

(defconstant-exported IMAGE_SCN_ALIGN_32BYTES #x600000)

(defconstant-exported IMAGE_SCN_ALIGN_64BYTES #x700000)

(defconstant-exported IMAGE_SCN_LNK_NRELOC_OVFL #x1000000)

(defconstant-exported IMAGE_SCN_MEM_DISCARDABLE #x2000000)

(defconstant-exported IMAGE_SCN_MEM_NOT_CACHED #x4000000)

(defconstant-exported IMAGE_SCN_MEM_NOT_PAGED #x8000000)

(defconstant-exported IMAGE_SCN_MEM_SHARED #x10000000)

(defconstant-exported IMAGE_SCN_MEM_EXECUTE #x20000000)

(defconstant-exported IMAGE_SCN_MEM_READ #x40000000)

(defconstant-exported IMAGE_SCN_MEM_WRITE #x80000000)

(defconstant-exported IMAGE_SYM_UNDEFINED 0)

(defconstant-exported IMAGE_SYM_ABSOLUTE -1)

(defconstant-exported IMAGE_SYM_DEBUG -2)

(defconstant-exported IMAGE_SYM_TYPE_NULL 0)

(defconstant-exported IMAGE_SYM_TYPE_VOID 1)

(defconstant-exported IMAGE_SYM_TYPE_CHAR 2)

(defconstant-exported IMAGE_SYM_TYPE_SHORT 3)

(defconstant-exported IMAGE_SYM_TYPE_INT 4)

(defconstant-exported IMAGE_SYM_TYPE_LONG 5)

(defconstant-exported IMAGE_SYM_TYPE_FLOAT 6)

(defconstant-exported IMAGE_SYM_TYPE_DOUBLE 7)

(defconstant-exported IMAGE_SYM_TYPE_STRUCT 8)

(defconstant-exported IMAGE_SYM_TYPE_UNION 9)

(defconstant-exported IMAGE_SYM_TYPE_ENUM 10)

(defconstant-exported IMAGE_SYM_TYPE_MOE 11)

(defconstant-exported IMAGE_SYM_TYPE_BYTE 12)

(defconstant-exported IMAGE_SYM_TYPE_WORD 13)

(defconstant-exported IMAGE_SYM_TYPE_UINT 14)

(defconstant-exported IMAGE_SYM_TYPE_DWORD 15)

(defconstant-exported IMAGE_SYM_TYPE_PCODE 32768)

(defconstant-exported IMAGE_SYM_DTYPE_NULL 0)

(defconstant-exported IMAGE_SYM_DTYPE_POINTER 1)

(defconstant-exported IMAGE_SYM_DTYPE_FUNCTION 2)

(defconstant-exported IMAGE_SYM_DTYPE_ARRAY 3)

(defconstant-exported IMAGE_SYM_CLASS_END_OF_FUNCTION -1)

(defconstant-exported IMAGE_SYM_CLASS_NULL 0)

(defconstant-exported IMAGE_SYM_CLASS_AUTOMATIC 1)

(defconstant-exported IMAGE_SYM_CLASS_EXTERNAL 2)

(defconstant-exported IMAGE_SYM_CLASS_STATIC 3)

(defconstant-exported IMAGE_SYM_CLASS_REGISTER 4)

(defconstant-exported IMAGE_SYM_CLASS_EXTERNAL_DEF 5)

(defconstant-exported IMAGE_SYM_CLASS_LABEL 6)

(defconstant-exported IMAGE_SYM_CLASS_UNDEFINED_LABEL 7)

(defconstant-exported IMAGE_SYM_CLASS_MEMBER_OF_STRUCT 8)

(defconstant-exported IMAGE_SYM_CLASS_ARGUMENT 9)

(defconstant-exported IMAGE_SYM_CLASS_STRUCT_TAG 10)

(defconstant-exported IMAGE_SYM_CLASS_MEMBER_OF_UNION 11)

(defconstant-exported IMAGE_SYM_CLASS_UNION_TAG 12)

(defconstant-exported IMAGE_SYM_CLASS_TYPE_DEFINITION 13)

(defconstant-exported IMAGE_SYM_CLASS_UNDEFINED_STATIC 14)

(defconstant-exported IMAGE_SYM_CLASS_ENUM_TAG 15)

(defconstant-exported IMAGE_SYM_CLASS_MEMBER_OF_ENUM 16)

(defconstant-exported IMAGE_SYM_CLASS_REGISTER_PARAM 17)

(defconstant-exported IMAGE_SYM_CLASS_BIT_FIELD 18)

(defconstant-exported IMAGE_SYM_CLASS_FAR_EXTERNAL 68)

(defconstant-exported IMAGE_SYM_CLASS_BLOCK 100)

(defconstant-exported IMAGE_SYM_CLASS_FUNCTION 101)

(defconstant-exported IMAGE_SYM_CLASS_END_OF_STRUCT 102)

(defconstant-exported IMAGE_SYM_CLASS_FILE 103)

(defconstant-exported IMAGE_SYM_CLASS_SECTION 104)

(defconstant-exported IMAGE_SYM_CLASS_WEAK_EXTERNAL 105)

(defconstant-exported IMAGE_COMDAT_SELECT_NODUPLICATES 1)

(defconstant-exported IMAGE_COMDAT_SELECT_ANY 2)

(defconstant-exported IMAGE_COMDAT_SELECT_SAME_SIZE 3)

(defconstant-exported IMAGE_COMDAT_SELECT_EXACT_MATCH 4)

(defconstant-exported IMAGE_COMDAT_SELECT_ASSOCIATIVE 5)

(defconstant-exported IMAGE_COMDAT_SELECT_LARGEST 6)

(defconstant-exported IMAGE_COMDAT_SELECT_NEWEST 7)

(defconstant-exported IMAGE_WEAK_EXTERN_SEARCH_NOLIBRARY 1)

(defconstant-exported IMAGE_WEAK_EXTERN_SEARCH_LIBRARY 2)

(defconstant-exported IMAGE_WEAK_EXTERN_SEARCH_ALIAS 3)

(defconstant-exported IMAGE_REL_I386_ABSOLUTE 0)

(defconstant-exported IMAGE_REL_I386_DIR16 1)

(defconstant-exported IMAGE_REL_I386_REL16 2)

(defconstant-exported IMAGE_REL_I386_DIR32 6)

(defconstant-exported IMAGE_REL_I386_DIR32NB 7)

(defconstant-exported IMAGE_REL_I386_SEG12 9)

(defconstant-exported IMAGE_REL_I386_SECTION 10)

(defconstant-exported IMAGE_REL_I386_SECREL 11)

(defconstant-exported IMAGE_REL_I386_REL32 20)

(defconstant-exported IMAGE_REL_MIPS_ABSOLUTE 0)

(defconstant-exported IMAGE_REL_MIPS_REFHALF 1)

(defconstant-exported IMAGE_REL_MIPS_REFWORD 2)

(defconstant-exported IMAGE_REL_MIPS_JMPADDR 3)

(defconstant-exported IMAGE_REL_MIPS_REFHI 4)

(defconstant-exported IMAGE_REL_MIPS_REFLO 5)

(defconstant-exported IMAGE_REL_MIPS_GPREL 6)

(defconstant-exported IMAGE_REL_MIPS_LITERAL 7)

(defconstant-exported IMAGE_REL_MIPS_SECTION 10)

(defconstant-exported IMAGE_REL_MIPS_SECREL 11)

(defconstant-exported IMAGE_REL_MIPS_SECRELLO 12)

(defconstant-exported IMAGE_REL_MIPS_SECRELHI 13)

(defconstant-exported IMAGE_REL_MIPS_REFWORDNB 34)

(defconstant-exported IMAGE_REL_MIPS_PAIR 35)

(defconstant-exported IMAGE_REL_ALPHA_ABSOLUTE 0)

(defconstant-exported IMAGE_REL_ALPHA_REFLONG 1)

(defconstant-exported IMAGE_REL_ALPHA_REFQUAD 2)

(defconstant-exported IMAGE_REL_ALPHA_GPREL32 3)

(defconstant-exported IMAGE_REL_ALPHA_LITERAL 4)

(defconstant-exported IMAGE_REL_ALPHA_LITUSE 5)

(defconstant-exported IMAGE_REL_ALPHA_GPDISP 6)

(defconstant-exported IMAGE_REL_ALPHA_BRADDR 7)

(defconstant-exported IMAGE_REL_ALPHA_HINT 8)

(defconstant-exported IMAGE_REL_ALPHA_INLINE_REFLONG 9)

(defconstant-exported IMAGE_REL_ALPHA_REFHI 10)

(defconstant-exported IMAGE_REL_ALPHA_REFLO 11)

(defconstant-exported IMAGE_REL_ALPHA_PAIR 12)

(defconstant-exported IMAGE_REL_ALPHA_MATCH 13)

(defconstant-exported IMAGE_REL_ALPHA_SECTION 14)

(defconstant-exported IMAGE_REL_ALPHA_SECREL 15)

(defconstant-exported IMAGE_REL_ALPHA_REFLONGNB 16)

(defconstant-exported IMAGE_REL_ALPHA_SECRELLO 17)

(defconstant-exported IMAGE_REL_ALPHA_SECRELHI 18)

(defconstant-exported IMAGE_REL_PPC_ABSOLUTE 0)

(defconstant-exported IMAGE_REL_PPC_ADDR64 1)

(defconstant-exported IMAGE_REL_PPC_ADDR32 2)

(defconstant-exported IMAGE_REL_PPC_ADDR24 3)

(defconstant-exported IMAGE_REL_PPC_ADDR16 4)

(defconstant-exported IMAGE_REL_PPC_ADDR14 5)

(defconstant-exported IMAGE_REL_PPC_REL24 6)

(defconstant-exported IMAGE_REL_PPC_REL14 7)

(defconstant-exported IMAGE_REL_PPC_TOCREL16 8)

(defconstant-exported IMAGE_REL_PPC_TOCREL14 9)

(defconstant-exported IMAGE_REL_PPC_ADDR32NB 10)

(defconstant-exported IMAGE_REL_PPC_SECREL 11)

(defconstant-exported IMAGE_REL_PPC_SECTION 12)

(defconstant-exported IMAGE_REL_PPC_IFGLUE 13)

(defconstant-exported IMAGE_REL_PPC_IMGLUE 14)

(defconstant-exported IMAGE_REL_PPC_SECREL16 15)

(defconstant-exported IMAGE_REL_PPC_REFHI 16)

(defconstant-exported IMAGE_REL_PPC_REFLO 17)

(defconstant-exported IMAGE_REL_PPC_PAIR 18)

(defconstant-exported IMAGE_REL_PPC_TYPEMASK 255)

(defconstant-exported IMAGE_REL_PPC_NEG 256)

(defconstant-exported IMAGE_REL_PPC_BRTAKEN 512)

(defconstant-exported IMAGE_REL_PPC_BRNTAKEN 1024)

(defconstant-exported IMAGE_REL_PPC_TOCDEFN 2048)

(defconstant-exported IMAGE_REL_BASED_ABSOLUTE 0)

(defconstant-exported IMAGE_REL_BASED_HIGH 1)

(defconstant-exported IMAGE_REL_BASED_LOW 2)

(defconstant-exported IMAGE_REL_BASED_HIGHLOW 3)

(defconstant-exported IMAGE_REL_BASED_HIGHADJ 4)

(defconstant-exported IMAGE_REL_BASED_MIPS_JMPADDR 5)

(defconstant-exported IMAGE_ARCHIVE_START_SIZE 8)

(defconstant-exported IMAGE_ARCHIVE_START "!<arch>\n")

(defconstant-exported IMAGE_ARCHIVE_END "`\n")

(defconstant-exported IMAGE_ARCHIVE_PAD "\n")

(defconstant-exported IMAGE_ARCHIVE_LINKER_MEMBER "/               ")

(defconstant-exported IMAGE_ARCHIVE_LONGNAMES_MEMBER "//              ")

(defconstant-exported IMAGE_ORDINAL_FLAG32 #x80000000)

(defconstant-exported IMAGE_ORDINAL_FLAG64 #x8000000000000000)

(defconstant-exported IMAGE_ORDINAL_FLAG #x80000000)

(defconstant-exported IMAGE_RESOURCE_NAME_IS_STRING #x80000000)

(defconstant-exported IMAGE_RESOURCE_DATA_IS_DIRECTORY #x80000000)

(defconstant-exported IMAGE_DEBUG_TYPE_UNKNOWN 0)

(defconstant-exported IMAGE_DEBUG_TYPE_COFF 1)

(defconstant-exported IMAGE_DEBUG_TYPE_CODEVIEW 2)

(defconstant-exported IMAGE_DEBUG_TYPE_FPO 3)

(defconstant-exported IMAGE_DEBUG_TYPE_MISC 4)

(defconstant-exported IMAGE_DEBUG_TYPE_EXCEPTION 5)

(defconstant-exported IMAGE_DEBUG_TYPE_FIXUP 6)

(defconstant-exported IMAGE_DEBUG_TYPE_OMAP_TO_SRC 7)

(defconstant-exported IMAGE_DEBUG_TYPE_OMAP_FROM_SRC 8)

(defconstant-exported FRAME_FPO 0)

(defconstant-exported FRAME_TRAP 1)

(defconstant-exported FRAME_TSS 2)

(defconstant-exported FRAME_NONFPO 3)

(defconstant-exported IMAGE_DEBUG_MISC_EXENAME 1)

(defconstant-exported N_BTMASK #x000F)

(defconstant-exported N_TMASK #x0030)

(defconstant-exported N_TMASK1 #x00C0)

(defconstant-exported N_TMASK2 #x00F0)

(defconstant-exported N_BTSHFT 4)

(defconstant-exported N_TSHIFT 2)

(defconstant-exported IS_TEXT_UNICODE_ASCII16 1)

(defconstant-exported IS_TEXT_UNICODE_REVERSE_ASCII16 16)

(defconstant-exported IS_TEXT_UNICODE_STATISTICS 2)

(defconstant-exported IS_TEXT_UNICODE_REVERSE_STATISTICS 32)

(defconstant-exported IS_TEXT_UNICODE_CONTROLS 4)

(defconstant-exported IS_TEXT_UNICODE_REVERSE_CONTROLS 64)

(defconstant-exported IS_TEXT_UNICODE_SIGNATURE 8)

(defconstant-exported IS_TEXT_UNICODE_REVERSE_SIGNATURE 128)

(defconstant-exported IS_TEXT_UNICODE_ILLEGAL_CHARS 256)

(defconstant-exported IS_TEXT_UNICODE_ODD_LENGTH 512)

(defconstant-exported IS_TEXT_UNICODE_NULL_BYTES 4096)

(defconstant-exported IS_TEXT_UNICODE_UNICODE_MASK 15)

(defconstant-exported IS_TEXT_UNICODE_REVERSE_MASK 240)

(defconstant-exported IS_TEXT_UNICODE_NOT_UNICODE_MASK 3840)

(defconstant-exported IS_TEXT_UNICODE_NOT_ASCII_MASK 61440)

(defconstant-exported SERVICE_KERNEL_DRIVER 1)

(defconstant-exported SERVICE_FILE_SYSTEM_DRIVER 2)

(defconstant-exported SERVICE_ADAPTER 4)

(defconstant-exported SERVICE_RECOGNIZER_DRIVER 8)

(defconstant-exported SERVICE_DRIVER (cl:logior 1 2 8))

(defconstant-exported SERVICE_WIN32_OWN_PROCESS 16)

(defconstant-exported SERVICE_WIN32_SHARE_PROCESS 32)

(defconstant-exported SERVICE_WIN32 (cl:logior 16 32))

(defconstant-exported SERVICE_INTERACTIVE_PROCESS 256)

(defconstant-exported SERVICE_TYPE_ALL (cl:logior SERVICE_WIN32 SERVICE_ADAPTER SERVICE_DRIVER SERVICE_INTERACTIVE_PROCESS))

(defconstant-exported SERVICE_BOOT_START 0)

(defconstant-exported SERVICE_SYSTEM_START 1)

(defconstant-exported SERVICE_AUTO_START 2)

(defconstant-exported SERVICE_DEMAND_START 3)

(defconstant-exported SERVICE_DISABLED 4)

(defconstant-exported SERVICE_ERROR_IGNORE 0)

(defconstant-exported SERVICE_ERROR_NORMAL 1)

(defconstant-exported SERVICE_ERROR_SEVERE 2)

(defconstant-exported SERVICE_ERROR_CRITICAL 3)

(defconstant-exported SE_OWNER_DEFAULTED 1)

(defconstant-exported SE_GROUP_DEFAULTED 2)

(defconstant-exported SE_DACL_PRESENT 4)

(defconstant-exported SE_DACL_DEFAULTED 8)

(defconstant-exported SE_SACL_PRESENT 16)

(defconstant-exported SE_SACL_DEFAULTED 32)

(defconstant-exported SE_DACL_AUTO_INHERIT_REQ 256)

(defconstant-exported SE_SACL_AUTO_INHERIT_REQ 512)

(defconstant-exported SE_DACL_AUTO_INHERITED 1024)

(defconstant-exported SE_SACL_AUTO_INHERITED 2048)

(defconstant-exported SE_DACL_PROTECTED 4096)

(defconstant-exported SE_SACL_PROTECTED 8192)

(defconstant-exported SE_SELF_RELATIVE #x8000)

(defconstant-exported SECURITY_DESCRIPTOR_MIN_LENGTH 20)

(defconstant-exported SECURITY_DESCRIPTOR_REVISION 1)

(defconstant-exported SECURITY_DESCRIPTOR_REVISION1 1)

(defconstant-exported SE_PRIVILEGE_ENABLED_BY_DEFAULT 1)

(defconstant-exported SE_PRIVILEGE_ENABLED 2)

(defconstant-exported SE_PRIVILEGE_USED_FOR_ACCESS #x80000000)

(defconstant-exported PRIVILEGE_SET_ALL_NECESSARY 1)

(defconstant-exported SECURITY_DYNAMIC_TRACKING 1)

(defconstant-exported SECURITY_STATIC_TRACKING 0)

(defconstant-exported TOKEN_ASSIGN_PRIMARY #x0001)

(defconstant-exported TOKEN_DUPLICATE #x0002)

(defconstant-exported TOKEN_IMPERSONATE #x0004)

(defconstant-exported TOKEN_QUERY #x0008)

(defconstant-exported TOKEN_QUERY_SOURCE #x0010)

(defconstant-exported TOKEN_ADJUST_PRIVILEGES #x0020)

(defconstant-exported TOKEN_ADJUST_GROUPS #x0040)

(defconstant-exported TOKEN_ADJUST_DEFAULT #x0080)

(defconstant-exported TOKEN_ALL_ACCESS (cl:logior #xF0000 #x0001 #x0002 #x0004 #x0008 #x0010 #x0020 #x0040 #x0080))

(defconstant-exported TOKEN_READ (cl:logior #x20000 #x0008))

(defconstant-exported TOKEN_WRITE (cl:logior #x20000 #x0020 #x0040 #x0080))

(defconstant-exported TOKEN_EXECUTE #x20000)

(defconstant-exported TOKEN_SOURCE_LENGTH 8)

(defconstant-exported DLL_PROCESS_DETACH 0)

(defconstant-exported DLL_PROCESS_ATTACH 1)

(defconstant-exported DLL_THREAD_ATTACH 2)

(defconstant-exported DLL_THREAD_DETACH 3)

(defconstant-exported DBG_CONTINUE #x10002)

(defconstant-exported DBG_TERMINATE_THREAD #x40010003)

(defconstant-exported DBG_TERMINATE_PROCESS #x40010004)

(defconstant-exported DBG_CONTROL_C #x40010005)

(defconstant-exported DBG_CONTROL_BREAK #x40010008)

(defconstant-exported DBG_EXCEPTION_NOT_HANDLED #x80010001)

(defconstant-exported TAPE_ABSOLUTE_POSITION 0)

(defconstant-exported TAPE_LOGICAL_POSITION 1)

(defconstant-exported TAPE_PSEUDO_LOGICAL_POSITION 2)

(defconstant-exported TAPE_REWIND 0)

(defconstant-exported TAPE_ABSOLUTE_BLOCK 1)

(defconstant-exported TAPE_LOGICAL_BLOCK 2)

(defconstant-exported TAPE_PSEUDO_LOGICAL_BLOCK 3)

(defconstant-exported TAPE_SPACE_END_OF_DATA 4)

(defconstant-exported TAPE_SPACE_RELATIVE_BLOCKS 5)

(defconstant-exported TAPE_SPACE_FILEMARKS 6)

(defconstant-exported TAPE_SPACE_SEQUENTIAL_FMKS 7)

(defconstant-exported TAPE_SPACE_SETMARKS 8)

(defconstant-exported TAPE_SPACE_SEQUENTIAL_SMKS 9)

(defconstant-exported TAPE_DRIVE_FIXED 1)

(defconstant-exported TAPE_DRIVE_SELECT 2)

(defconstant-exported TAPE_DRIVE_INITIATOR 4)

(defconstant-exported TAPE_DRIVE_ERASE_SHORT 16)

(defconstant-exported TAPE_DRIVE_ERASE_LONG 32)

(defconstant-exported TAPE_DRIVE_ERASE_BOP_ONLY 64)

(defconstant-exported TAPE_DRIVE_ERASE_IMMEDIATE 128)

(defconstant-exported TAPE_DRIVE_TAPE_CAPACITY 256)

(defconstant-exported TAPE_DRIVE_TAPE_REMAINING 512)

(defconstant-exported TAPE_DRIVE_FIXED_BLOCK 1024)

(defconstant-exported TAPE_DRIVE_VARIABLE_BLOCK 2048)

(defconstant-exported TAPE_DRIVE_WRITE_PROTECT 4096)

(defconstant-exported TAPE_DRIVE_EOT_WZ_SIZE 8192)

(defconstant-exported TAPE_DRIVE_ECC #x10000)

(defconstant-exported TAPE_DRIVE_COMPRESSION #x20000)

(defconstant-exported TAPE_DRIVE_PADDING #x40000)

(defconstant-exported TAPE_DRIVE_REPORT_SMKS #x80000)

(defconstant-exported TAPE_DRIVE_GET_ABSOLUTE_BLK #x100000)

(defconstant-exported TAPE_DRIVE_GET_LOGICAL_BLK #x200000)

(defconstant-exported TAPE_DRIVE_SET_EOT_WZ_SIZE #x400000)

(defconstant-exported TAPE_DRIVE_EJECT_MEDIA #x1000000)

(defconstant-exported TAPE_DRIVE_CLEAN_REQUESTS #x2000000)

(defconstant-exported TAPE_DRIVE_SET_CMP_BOP_ONLY #x4000000)

(defconstant-exported TAPE_DRIVE_RESERVED_BIT #x80000000)

(defconstant-exported TAPE_DRIVE_LOAD_UNLOAD #x80000001)

(defconstant-exported TAPE_DRIVE_TENSION #x80000002)

(defconstant-exported TAPE_DRIVE_LOCK_UNLOCK #x80000004)

(defconstant-exported TAPE_DRIVE_REWIND_IMMEDIATE #x80000008)

(defconstant-exported TAPE_DRIVE_SET_BLOCK_SIZE #x80000010)

(defconstant-exported TAPE_DRIVE_LOAD_UNLD_IMMED #x80000020)

(defconstant-exported TAPE_DRIVE_TENSION_IMMED #x80000040)

(defconstant-exported TAPE_DRIVE_LOCK_UNLK_IMMED #x80000080)

(defconstant-exported TAPE_DRIVE_SET_ECC #x80000100)

(defconstant-exported TAPE_DRIVE_SET_COMPRESSION #x80000200)

(defconstant-exported TAPE_DRIVE_SET_PADDING #x80000400)

(defconstant-exported TAPE_DRIVE_SET_REPORT_SMKS #x80000800)

(defconstant-exported TAPE_DRIVE_ABSOLUTE_BLK #x80001000)

(defconstant-exported TAPE_DRIVE_ABS_BLK_IMMED #x80002000)

(defconstant-exported TAPE_DRIVE_LOGICAL_BLK #x80004000)

(defconstant-exported TAPE_DRIVE_LOG_BLK_IMMED #x80008000)

(defconstant-exported TAPE_DRIVE_END_OF_DATA #x80010000)

(defconstant-exported TAPE_DRIVE_RELATIVE_BLKS #x80020000)

(defconstant-exported TAPE_DRIVE_FILEMARKS #x80040000)

(defconstant-exported TAPE_DRIVE_SEQUENTIAL_FMKS #x80080000)

(defconstant-exported TAPE_DRIVE_SETMARKS #x80100000)

(defconstant-exported TAPE_DRIVE_SEQUENTIAL_SMKS #x80200000)

(defconstant-exported TAPE_DRIVE_REVERSE_POSITION #x80400000)

(defconstant-exported TAPE_DRIVE_SPACE_IMMEDIATE #x80800000)

(defconstant-exported TAPE_DRIVE_WRITE_SETMARKS #x81000000)

(defconstant-exported TAPE_DRIVE_WRITE_FILEMARKS #x82000000)

(defconstant-exported TAPE_DRIVE_WRITE_SHORT_FMKS #x84000000)

(defconstant-exported TAPE_DRIVE_WRITE_LONG_FMKS #x88000000)

(defconstant-exported TAPE_DRIVE_WRITE_MARK_IMMED #x90000000)

(defconstant-exported TAPE_DRIVE_FORMAT #xA0000000)

(defconstant-exported TAPE_DRIVE_FORMAT_IMMEDIATE #xC0000000)

(defconstant-exported TAPE_DRIVE_HIGH_FEATURES #x80000000)

(defconstant-exported TAPE_FIXED_PARTITIONS 0)

(defconstant-exported TAPE_INITIATOR_PARTITIONS 2)

(defconstant-exported TAPE_SELECT_PARTITIONS 1)

(defconstant-exported TAPE_FILEMARKS 1)

(defconstant-exported TAPE_LONG_FILEMARKS 3)

(defconstant-exported TAPE_SETMARKS 0)

(defconstant-exported TAPE_SHORT_FILEMARKS 2)

(defconstant-exported TAPE_ERASE_LONG 1)

(defconstant-exported TAPE_ERASE_SHORT 0)

(defconstant-exported TAPE_LOAD 0)

(defconstant-exported TAPE_UNLOAD 1)

(defconstant-exported TAPE_TENSION 2)

(defconstant-exported TAPE_LOCK 3)

(defconstant-exported TAPE_UNLOCK 4)

(defconstant-exported TAPE_FORMAT 5)

(defconstant-exported VER_PLATFORM_WIN32s 0)

(defconstant-exported VER_PLATFORM_WIN32_WINDOWS 1)

(defconstant-exported VER_PLATFORM_WIN32_NT 2)

(defconstant-exported VER_NT_WORKSTATION 1)

(defconstant-exported VER_NT_DOMAIN_CONTROLLER 2)

(defconstant-exported VER_NT_SERVER 3)

(defconstant-exported VER_SUITE_SMALLBUSINESS 1)

(defconstant-exported VER_SUITE_ENTERPRISE 2)

(defconstant-exported VER_SUITE_BACKOFFICE 4)

(defconstant-exported VER_SUITE_TERMINAL 16)

(defconstant-exported VER_SUITE_SMALLBUSINESS_RESTRICTED 32)

(defconstant-exported VER_SUITE_DATACENTER 128)

(defconstant-exported VER_SUITE_PERSONAL 512)

(defconstant-exported VER_SUITE_BLADE 1024)

(defconstant-exported WT_EXECUTEDEFAULT #x00000000)

(defconstant-exported WT_EXECUTEINIOTHREAD #x00000001)

(defconstant-exported WT_EXECUTEINWAITTHREAD #x00000004)

(defconstant-exported WT_EXECUTEONLYONCE #x00000008)

(defconstant-exported WT_EXECUTELONGFUNCTION #x00000010)

(defconstant-exported WT_EXECUTEINTIMERTHREAD #x00000020)

(defconstant-exported WT_EXECUTEINPERSISTENTTHREAD #x00000080)

(defconstant-exported WT_TRANSFER_IMPERSONATION #x00000100)

(defconstant-exported TLS_MINIMUM_AVAILABLE 64)

(defconstant-exported MAXIMUM_REPARSE_DATA_BUFFER_SIZE 16384)

(defconstant-exported IO_REPARSE_TAG_RESERVED_ZERO 0)

(defconstant-exported IO_REPARSE_TAG_RESERVED_ONE 1)

(defconstant-exported IO_REPARSE_TAG_RESERVED_RANGE 1)

(defconstant-exported IO_REPARSE_TAG_VALID_VALUES #xE000FFFF)

(defconstant-exported IO_REPARSE_TAG_SYMBOLIC_LINK 0)

(defconstant-exported IO_REPARSE_TAG_MOUNT_POINT #xA0000003)





(defcstructex-exported GUID
        (Data1 :unsigned-long)
        (Data2 :unsigned-short)
        (Data3 :unsigned-short)
        (Data4 :pointer))







(defcstructex-exported GENERIC_MAPPING
        (GenericRead :unsigned-long)
        (GenericWrite :unsigned-long)
        (GenericExecute :unsigned-long)
        (GenericAll :unsigned-long))





(defcstructex-exported ACE_HEADER
        (AceType :unsigned-char)
        (AceFlags :unsigned-char)
        (AceSize :unsigned-short))





(defcstructex-exported ACCESS_ALLOWED_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (SidStart :unsigned-long))





(defcstructex-exported ACCESS_DENIED_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (SidStart :unsigned-long))





(defcstructex-exported SYSTEM_AUDIT_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (SidStart :unsigned-long))





(defcstructex-exported SYSTEM_ALARM_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (SidStart :unsigned-long))





(defcstructex-exported ACCESS_ALLOWED_OBJECT_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (Flags :unsigned-long)
        (ObjectType GUID)
        (InheritedObjectType GUID)
        (SidStart :unsigned-long))





(defcstructex-exported ACCESS_DENIED_OBJECT_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (Flags :unsigned-long)
        (ObjectType GUID)
        (InheritedObjectType GUID)
        (SidStart :unsigned-long))





(defcstructex-exported SYSTEM_AUDIT_OBJECT_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (Flags :unsigned-long)
        (ObjectType GUID)
        (InheritedObjectType GUID)
        (SidStart :unsigned-long))





(defcstructex-exported SYSTEM_ALARM_OBJECT_ACE
        (Header ACE_HEADER)
        (Mask :unsigned-long)
        (Flags :unsigned-long)
        (ObjectType GUID)
        (InheritedObjectType GUID)
        (SidStart :unsigned-long))





(defcstructex-exported ACL
        (AclRevision :unsigned-char)
        (Sbz1 :unsigned-char)
        (AclSize :unsigned-short)
        (AceCount :unsigned-short)
        (Sbz2 :unsigned-short))





(defcstructex-exported ACL_REVISION_INFORMATION
        (AclRevision :unsigned-long))



(defcstructex-exported ACL_SIZE_INFORMATION
        (AceCount :unsigned-long)
        (AclBytesInUse :unsigned-long)
        (AclBytesFree :unsigned-long))



(defconstant-exported SIZE_OF_80387_REGISTERS 80)

(defconstant-exported CONTEXT_i386 #x10000)

(defconstant-exported CONTEXT_i486 #x10000)

(defconstant-exported CONTEXT_CONTROL (cl:logior #x10000 #x00000001))

(defconstant-exported CONTEXT_INTEGER (cl:logior #x10000 #x00000002))

(defconstant-exported CONTEXT_SEGMENTS (cl:logior #x10000 #x00000004))

(defconstant-exported CONTEXT_FLOATING_POINT (cl:logior #x10000 #x00000008))

(defconstant-exported CONTEXT_DEBUG_REGISTERS (cl:logior #x10000 #x00000010))

(defconstant-exported CONTEXT_EXTENDED_REGISTERS (cl:logior #x10000 #x00000020))

(defconstant-exported CONTEXT_FULL (cl:logior #x10000 #x00000001 #x10000 #x00000002 #x10000 #x00000004))

(defconstant-exported MAXIMUM_SUPPORTED_EXTENSION 512)

(defcstructex-exported FLOATING_SAVE_AREA
        (ControlWord :unsigned-long)
        (StatusWord :unsigned-long)
        (TagWord :unsigned-long)
        (ErrorOffset :unsigned-long)
        (ErrorSelector :unsigned-long)
        (DataOffset :unsigned-long)
        (DataSelector :unsigned-long)
        (RegisterArea :pointer)
        (Cr0NpxState :unsigned-long))



(defcstructex-exported CONTEXT
        (ContextFlags :unsigned-long)
        (Dr0 :unsigned-long)
        (Dr1 :unsigned-long)
        (Dr2 :unsigned-long)
        (Dr3 :unsigned-long)
        (Dr6 :unsigned-long)
        (Dr7 :unsigned-long)
        (FloatSave FLOATING_SAVE_AREA)
        (SegGs :unsigned-long)
        (SegFs :unsigned-long)
        (SegEs :unsigned-long)
        (SegDs :unsigned-long)
        (Edi :unsigned-long)
        (Esi :unsigned-long)
        (Ebx :unsigned-long)
        (Edx :unsigned-long)
        (Ecx :unsigned-long)
        (Eax :unsigned-long)
        (Ebp :unsigned-long)
        (Eip :unsigned-long)
        (SegCs :unsigned-long)
        (EFlags :unsigned-long)
        (Esp :unsigned-long)
        (SegSs :unsigned-long)
        (ExtendedRegisters :pointer))







(defcstructex-exported EXCEPTION_RECORD
        (ExceptionCode :unsigned-long)
        (ExceptionFlags :unsigned-long)
        (ExceptionRecord :pointer)
        (ExceptionAddress :pointer)
        (NumberParameters :unsigned-long)
        (ExceptionInformation :pointer))







(defcstructex-exported EXCEPTION_POINTERS
        (ExceptionRecord :pointer)
        (ContextRecord :pointer))







(cffi:defcunion LARGE_INTEGER
        (QuadPart :long-long)
        (u :pointer))





(defcstructex-exported LARGE_INTEGER_u
        (LowPart :unsigned-long)
        (HighPart :int32))

(cffi:defcunion ULARGE_INTEGER
        (QuadPart :unsigned-long-long)
        (u :pointer))





(defcstructex-exported ULARGE_INTEGER_u
        (LowPart :unsigned-long)
        (HighPart :unsigned-long))

(defcstructex-exported LUID
        (LowPart :unsigned-long)
        (HighPart :int32))





(defcstructex-exported LUID_AND_ATTRIBUTES
        (Luid LUID)
        (Attributes :unsigned-long))









(defcstructex-exported PRIVILEGE_SET
        (PrivilegeCount :unsigned-long)
        (Control :unsigned-long)
        (Privilege :pointer))





(defcstructex-exported SECURITY_ATTRIBUTES
        (nLength :unsigned-long)
        (lpSecurityDescriptor :pointer)
        (bInheritHandle :int))







(cffi:defcenum SECURITY_IMPERSONATION_LEVEL
        :SecurityAnonymous
        :SecurityIdentification
        :SecurityImpersonation
        :SecurityDelegation)







(defcstructex-exported SECURITY_QUALITY_OF_SERVICE
        (Length :unsigned-long)
        (ImpersonationLevel SECURITY_IMPERSONATION_LEVEL)
        (ContextTrackingMode :unsigned-char)
        (EffectiveOnly :unsigned-char))







(defcstructex-exported SE_IMPERSONATION_STATE
        (Token :pointer)
        (CopyOnOpen :unsigned-char)
        (EffectiveOnly :unsigned-char)
        (Level SECURITY_IMPERSONATION_LEVEL))





(defcstructex-exported SID_IDENTIFIER_AUTHORITY
        (Value :pointer))









(defcstructex-exported SID
        (Revision :unsigned-char)
        (SubAuthorityCount :unsigned-char)
        (IdentifierAuthority SID_IDENTIFIER_AUTHORITY)
        (SubAuthority :pointer))





(defcstructex-exported SID_AND_ATTRIBUTES
        (Sid :pointer)
        (Attributes :unsigned-long))









(defcstructex-exported TOKEN_SOURCE
        (SourceName :pointer)
        (SourceIdentifier LUID))





(defcstructex-exported TOKEN_CONTROL
        (TokenId LUID)
        (AuthenticationId LUID)
        (ModifiedId LUID)
        (TokenSource TOKEN_SOURCE))





(defcstructex-exported TOKEN_DEFAULT_DACL
        (DefaultDacl :pointer))





(defcstructex-exported TOKEN_GROUPS
        (GroupCount :unsigned-long)
        (Groups :pointer))







(defcstructex-exported TOKEN_OWNER
        (Owner :pointer))





(defcstructex-exported TOKEN_PRIMARY_GROUP
        (PrimaryGroup :pointer))





(defcstructex-exported TOKEN_PRIVILEGES
        (PrivilegeCount :unsigned-long)
        (Privileges :pointer))







(cffi:defcenum TOKEN_TYPE
        (:TokenPrimary #.1)
        :TokenImpersonation)



(defcstructex-exported TOKEN_STATISTICS
        (TokenId LUID)
        (AuthenticationId LUID)
        (ExpirationTime LARGE_INTEGER)
        (TokenType TOKEN_TYPE)
        (ImpersonationLevel SECURITY_IMPERSONATION_LEVEL)
        (DynamicCharged :unsigned-long)
        (DynamicAvailable :unsigned-long)
        (GroupCount :unsigned-long)
        (PrivilegeCount :unsigned-long)
        (ModifiedId LUID))





(defcstructex-exported TOKEN_USER
        (User SID_AND_ATTRIBUTES))













(defcstructex-exported SECURITY_DESCRIPTOR
        (Revision :unsigned-char)
        (Sbz1 :unsigned-char)
        (Control :unsigned-short)
        (Owner :pointer)
        (Group :pointer)
        (Sacl :pointer)
        (Dacl :pointer))







(cffi:defcenum TOKEN_INFORMATION_CLASS
        (:TokenUser #.1)
        :TokenGroups
        :TokenPrivileges
        :TokenOwner
        :TokenPrimaryGroup
        :TokenDefaultDacl
        :TokenSource
        :TokenType
        :TokenImpersonationLevel
        :TokenStatistics
        :TokenRestrictedSids
        :TokenSessionId
        :TokenGroupsAndPrivileges
        :TokenSessionReference
        :TokenSandBoxInert
        :TokenAuditPolicy
        :TokenOrigin)

(cffi:defcenum SID_NAME_USE
        (:SidTypeUser #.1)
        :SidTypeGroup
        :SidTypeDomain
        :SidTypeAlias
        :SidTypeWellKnownGroup
        :SidTypeDeletedAccount
        :SidTypeInvalid
        :SidTypeUnknown
        :SidTypeComputer)



(defcstructex-exported QUOTA_LIMITS
        (PagedPoolLimit :unsigned-long)
        (NonPagedPoolLimit :unsigned-long)
        (MinimumWorkingSetSize :unsigned-long)
        (MaximumWorkingSetSize :unsigned-long)
        (PagefileLimit :unsigned-long)
        (TimeLimit LARGE_INTEGER))





(defcstructex-exported IO_COUNTERS
        (ReadOperationCount :unsigned-long-long)
        (WriteOperationCount :unsigned-long-long)
        (OtherOperationCount :unsigned-long-long)
        (ReadTransferCount :unsigned-long-long)
        (WriteTransferCount :unsigned-long-long)
        (OtherTransferCount :unsigned-long-long))





(defcstructex-exported FILE_NOTIFY_INFORMATION
        (NextEntryOffset :unsigned-long)
        (Action :unsigned-long)
        (FileNameLength :unsigned-long)
        (FileName :pointer))





(defcstructex-exported TAPE_ERASE
        (Type :unsigned-long)
        (Immediate :unsigned-char))





(defcstructex-exported TAPE_GET_DRIVE_PARAMETERS
        (ECC :unsigned-char)
        (Compression :unsigned-char)
        (DataPadding :unsigned-char)
        (ReportSetmarks :unsigned-char)
        (DefaultBlockSize :unsigned-long)
        (MaximumBlockSize :unsigned-long)
        (MinimumBlockSize :unsigned-long)
        (MaximumPartitionCount :unsigned-long)
        (FeaturesLow :unsigned-long)
        (FeaturesHigh :unsigned-long)
        (EOTWarningZoneSize :unsigned-long))





(defcstructex-exported TAPE_GET_MEDIA_PARAMETERS
        (Capacity LARGE_INTEGER)
        (Remaining LARGE_INTEGER)
        (BlockSize :unsigned-long)
        (PartitionCount :unsigned-long)
        (WriteProtected :unsigned-char))





(defcstructex-exported TAPE_GET_POSITION
        (Type :unsigned-long)
        (Partition :unsigned-long)
        (OffsetLow :unsigned-long)
        (OffsetHigh :unsigned-long))





(defcstructex-exported TAPE_PREPARE
        (Operation :unsigned-long)
        (Immediate :unsigned-char))





(defcstructex-exported TAPE_SET_DRIVE_PARAMETERS
        (ECC :unsigned-char)
        (Compression :unsigned-char)
        (DataPadding :unsigned-char)
        (ReportSetmarks :unsigned-char)
        (EOTWarningZoneSize :unsigned-long))





(defcstructex-exported TAPE_SET_MEDIA_PARAMETERS
        (BlockSize :unsigned-long))





(defcstructex-exported TAPE_SET_POSITION
        (Method :unsigned-long)
        (Partition :unsigned-long)
        (Offset LARGE_INTEGER)
        (Immediate :unsigned-char))





(defcstructex-exported TAPE_WRITE_MARKS
        (Type :unsigned-long)
        (Count :unsigned-long)
        (Immediate :unsigned-char))





(defcstructex-exported TAPE_CREATE_PARTITION
        (Method :unsigned-long)
        (Count :unsigned-long)
        (Size :unsigned-long))





(defcstructex-exported MEMORY_BASIC_INFORMATION
        (BaseAddress :pointer)
        (AllocationBase :pointer)
        (AllocationProtect :unsigned-long)
        (RegionSize :unsigned-long)
        (State :unsigned-long)
        (Protect :unsigned-long)
        (Type :unsigned-long))





(defcstructex-exported MESSAGE_RESOURCE_ENTRY
        (Length :unsigned-short)
        (Flags :unsigned-short)
        (Text :pointer))





(defcstructex-exported MESSAGE_RESOURCE_BLOCK
        (LowId :unsigned-long)
        (HighId :unsigned-long)
        (OffsetToEntries :unsigned-long))





(defcstructex-exported MESSAGE_RESOURCE_DATA
        (NumberOfBlocks :unsigned-long)
        (Blocks :pointer))





(defcstructex-exported LIST_ENTRY
        (Flink :pointer)
        (Blink :pointer))





(defcstructex-exported SINGLE_LIST_ENTRY
        (Next :pointer))





(cffi:defcunion SLIST_HEADER
        (Alignment :unsigned-long-long)
        (s :pointer))





(defcstructex-exported SLIST_HEADER_s
        (Next SINGLE_LIST_ENTRY)
        (Depth :unsigned-short)
        (Sequence :unsigned-short))

(defcstructex-exported RTL_CRITICAL_SECTION_DEBUG
        (Type :unsigned-short)
        (CreatorBackTraceIndex :unsigned-short)
        (CriticalSection :pointer)
        (ProcessLocksList LIST_ENTRY)
        (EntryCount :unsigned-long)
        (ContentionCount :unsigned-long)
        (Spare :pointer))





(defcstructex-exported RTL_CRITICAL_SECTION
        (DebugInfo :pointer)
        (LockCount :int32)
        (RecursionCount :int32)
        (OwningThread :pointer)
        (LockSemaphore :pointer)
        (Reserved :unsigned-long))





(defcstructex-exported EVENTLOGRECORD
        (Length :unsigned-long)
        (Reserved :unsigned-long)
        (RecordNumber :unsigned-long)
        (TimeGenerated :unsigned-long)
        (TimeWritten :unsigned-long)
        (EventID :unsigned-long)
        (EventType :unsigned-short)
        (NumStrings :unsigned-short)
        (EventCategory :unsigned-short)
        (ReservedFlags :unsigned-short)
        (ClosingRecordNumber :unsigned-long)
        (StringOffset :unsigned-long)
        (UserSidLength :unsigned-long)
        (UserSidOffset :unsigned-long)
        (DataLength :unsigned-long)
        (DataOffset :unsigned-long))





(defcstructex-exported OSVERSIONINFOA
        (dwOSVersionInfoSize :unsigned-long)
        (dwMajorVersion :unsigned-long)
        (dwMinorVersion :unsigned-long)
        (dwBuildNumber :unsigned-long)
        (dwPlatformId :unsigned-long)
        (szCSDVersion :pointer))







(defcstructex-exported OSVERSIONINFOW
        (dwOSVersionInfoSize :unsigned-long)
        (dwMajorVersion :unsigned-long)
        (dwMinorVersion :unsigned-long)
        (dwBuildNumber :unsigned-long)
        (dwPlatformId :unsigned-long)
        (szCSDVersion :pointer))







(defcstructex-exported OSVERSIONINFOEXA
        (dwOSVersionInfoSize :unsigned-long)
        (dwMajorVersion :unsigned-long)
        (dwMinorVersion :unsigned-long)
        (dwBuildNumber :unsigned-long)
        (dwPlatformId :unsigned-long)
        (szCSDVersion :pointer)
        (wServicePackMajor :unsigned-short)
        (wServicePackMinor :unsigned-short)
        (wSuiteMask :unsigned-short)
        (wProductType :unsigned-char)
        (wReserved :unsigned-char))







(defcstructex-exported OSVERSIONINFOEXW
        (dwOSVersionInfoSize :unsigned-long)
        (dwMajorVersion :unsigned-long)
        (dwMinorVersion :unsigned-long)
        (dwBuildNumber :unsigned-long)
        (dwPlatformId :unsigned-long)
        (szCSDVersion :pointer)
        (wServicePackMajor :unsigned-short)
        (wServicePackMinor :unsigned-short)
        (wSuiteMask :unsigned-short)
        (wProductType :unsigned-char)
        (wReserved :unsigned-char))







(defcstructex-exported IMAGE_VXD_HEADER
        (e32_magic :unsigned-short)
        (e32_border :unsigned-char)
        (e32_worder :unsigned-char)
        (e32_level :unsigned-long)
        (e32_cpu :unsigned-short)
        (e32_os :unsigned-short)
        (e32_ver :unsigned-long)
        (e32_mflags :unsigned-long)
        (e32_mpages :unsigned-long)
        (e32_startobj :unsigned-long)
        (e32_eip :unsigned-long)
        (e32_stackobj :unsigned-long)
        (e32_esp :unsigned-long)
        (e32_pagesize :unsigned-long)
        (e32_lastpagesize :unsigned-long)
        (e32_fixupsize :unsigned-long)
        (e32_fixupsum :unsigned-long)
        (e32_ldrsize :unsigned-long)
        (e32_ldrsum :unsigned-long)
        (e32_objtab :unsigned-long)
        (e32_objcnt :unsigned-long)
        (e32_objmap :unsigned-long)
        (e32_itermap :unsigned-long)
        (e32_rsrctab :unsigned-long)
        (e32_rsrccnt :unsigned-long)
        (e32_restab :unsigned-long)
        (e32_enttab :unsigned-long)
        (e32_dirtab :unsigned-long)
        (e32_dircnt :unsigned-long)
        (e32_fpagetab :unsigned-long)
        (e32_frectab :unsigned-long)
        (e32_impmod :unsigned-long)
        (e32_impmodcnt :unsigned-long)
        (e32_impproc :unsigned-long)
        (e32_pagesum :unsigned-long)
        (e32_datapage :unsigned-long)
        (e32_preload :unsigned-long)
        (e32_nrestab :unsigned-long)
        (e32_cbnrestab :unsigned-long)
        (e32_nressum :unsigned-long)
        (e32_autodata :unsigned-long)
        (e32_debuginfo :unsigned-long)
        (e32_debuglen :unsigned-long)
        (e32_instpreload :unsigned-long)
        (e32_instdemand :unsigned-long)
        (e32_heapsize :unsigned-long)
        (e32_res3 :pointer)
        (e32_winresoff :unsigned-long)
        (e32_winreslen :unsigned-long)
        (e32_devid :unsigned-short)
        (e32_ddkver :unsigned-short))





(defcstructex-exported IMAGE_FILE_HEADER
        (Machine :unsigned-short)
        (NumberOfSections :unsigned-short)
        (TimeDateStamp :unsigned-long)
        (PointerToSymbolTable :unsigned-long)
        (NumberOfSymbols :unsigned-long)
        (SizeOfOptionalHeader :unsigned-short)
        (Characteristics :unsigned-short))





(defcstructex-exported IMAGE_DATA_DIRECTORY
        (VirtualAddress :unsigned-long)
        (Size :unsigned-long))





(defcstructex-exported IMAGE_OPTIONAL_HEADER
        (Magic :unsigned-short)
        (MajorLinkerVersion :unsigned-char)
        (MinorLinkerVersion :unsigned-char)
        (SizeOfCode :unsigned-long)
        (SizeOfInitializedData :unsigned-long)
        (SizeOfUninitializedData :unsigned-long)
        (AddressOfEntryPoint :unsigned-long)
        (BaseOfCode :unsigned-long)
        (BaseOfData :unsigned-long)
        (ImageBase :unsigned-long)
        (SectionAlignment :unsigned-long)
        (FileAlignment :unsigned-long)
        (MajorOperatingSystemVersion :unsigned-short)
        (MinorOperatingSystemVersion :unsigned-short)
        (MajorImageVersion :unsigned-short)
        (MinorImageVersion :unsigned-short)
        (MajorSubsystemVersion :unsigned-short)
        (MinorSubsystemVersion :unsigned-short)
        (Reserved1 :unsigned-long)
        (SizeOfImage :unsigned-long)
        (SizeOfHeaders :unsigned-long)
        (CheckSum :unsigned-long)
        (Subsystem :unsigned-short)
        (DllCharacteristics :unsigned-short)
        (SizeOfStackReserve :unsigned-long)
        (SizeOfStackCommit :unsigned-long)
        (SizeOfHeapReserve :unsigned-long)
        (SizeOfHeapCommit :unsigned-long)
        (LoaderFlags :unsigned-long)
        (NumberOfRvaAndSizes :unsigned-long)
        (DataDirectory :pointer))





(defcstructex-exported IMAGE_ROM_OPTIONAL_HEADER
        (Magic :unsigned-short)
        (MajorLinkerVersion :unsigned-char)
        (MinorLinkerVersion :unsigned-char)
        (SizeOfCode :unsigned-long)
        (SizeOfInitializedData :unsigned-long)
        (SizeOfUninitializedData :unsigned-long)
        (AddressOfEntryPoint :unsigned-long)
        (BaseOfCode :unsigned-long)
        (BaseOfData :unsigned-long)
        (BaseOfBss :unsigned-long)
        (GprMask :unsigned-long)
        (CprMask :pointer)
        (GpValue :unsigned-long))





(defcstructex-exported IMAGE_DOS_HEADER
        (e_magic :unsigned-short)
        (e_cblp :unsigned-short)
        (e_cp :unsigned-short)
        (e_crlc :unsigned-short)
        (e_cparhdr :unsigned-short)
        (e_minalloc :unsigned-short)
        (e_maxalloc :unsigned-short)
        (e_ss :unsigned-short)
        (e_sp :unsigned-short)
        (e_csum :unsigned-short)
        (e_ip :unsigned-short)
        (e_cs :unsigned-short)
        (e_lfarlc :unsigned-short)
        (e_ovno :unsigned-short)
        (e_res :pointer)
        (e_oemid :unsigned-short)
        (e_oeminfo :unsigned-short)
        (e_res2 :pointer)
        (e_lfanew :int32))





(defcstructex-exported IMAGE_OS2_HEADER
        (ne_magic :unsigned-short)
        (ne_ver :char)
        (ne_rev :char)
        (ne_enttab :unsigned-short)
        (ne_cbenttab :unsigned-short)
        (ne_crc :int32)
        (ne_flags :unsigned-short)
        (ne_autodata :unsigned-short)
        (ne_heap :unsigned-short)
        (ne_stack :unsigned-short)
        (ne_csip :int32)
        (ne_sssp :int32)
        (ne_cseg :unsigned-short)
        (ne_cmod :unsigned-short)
        (ne_cbnrestab :unsigned-short)
        (ne_segtab :unsigned-short)
        (ne_rsrctab :unsigned-short)
        (ne_restab :unsigned-short)
        (ne_modtab :unsigned-short)
        (ne_imptab :unsigned-short)
        (ne_nrestab :int32)
        (ne_cmovent :unsigned-short)
        (ne_align :unsigned-short)
        (ne_cres :unsigned-short)
        (ne_exetyp :unsigned-char)
        (ne_flagsothers :unsigned-char)
        (ne_pretthunks :unsigned-short)
        (ne_psegrefbytes :unsigned-short)
        (ne_swaparea :unsigned-short)
        (ne_expver :unsigned-short))





(defcstructex-exported IMAGE_NT_HEADERS
        (Signature :unsigned-long)
        (FileHeader IMAGE_FILE_HEADER)
        (OptionalHeader IMAGE_OPTIONAL_HEADER))





(defcstructex-exported IMAGE_ROM_HEADERS
        (FileHeader IMAGE_FILE_HEADER)
        (OptionalHeader IMAGE_ROM_OPTIONAL_HEADER))





(defcstructex-exported IMAGE_SECTION_HEADER
        (Name :pointer)
        (VirtualAddress :unsigned-long)
        (SizeOfRawData :unsigned-long)
        (PointerToRawData :unsigned-long)
        (PointerToRelocations :unsigned-long)
        (PointerToLinenumbers :unsigned-long)
        (NumberOfRelocations :unsigned-short)
        (NumberOfLinenumbers :unsigned-short)
        (Characteristics :unsigned-long)
        (Misc :pointer))





(cffi:defcunion IMAGE_SECTION_HEADER_Misc
        (PhysicalAddress :unsigned-long)
        (VirtualSize :unsigned-long))

(defcstructex-exported IMAGE_SYMBOL
        (Value :unsigned-long)
        (SectionNumber :short)
        (Type :unsigned-short)
        (StorageClass :unsigned-char)
        (NumberOfAuxSymbols :unsigned-char)
        (N :pointer))





(cffi:defcunion IMAGE_SYMBOL_N
        (ShortName :pointer)
        (LongName :pointer)
        (Name :pointer))

(defcstructex-exported IMAGE_SYMBOL_N_Name
        (Short :unsigned-long)
        (Long :unsigned-long))

(cffi:defcunion IMAGE_AUX_SYMBOL
        (Sym :pointer)
        (File :pointer)
        (Section :pointer))





(defcstructex-exported IMAGE_AUX_SYMBOL_Section
        (Length :unsigned-long)
        (NumberOfRelocations :unsigned-short)
        (NumberOfLinenumbers :unsigned-short)
        (CheckSum :unsigned-long)
        (Number :short)
        (Selection :unsigned-char))

(defcstructex-exported IMAGE_AUX_SYMBOL_File
        (Name :pointer))

(defcstructex-exported IMAGE_AUX_SYMBOL_Sym
        (TagIndex :unsigned-long)
        (TvIndex :unsigned-short)
        (Misc :pointer)
        (FcnAry :pointer))

(cffi:defcunion IMAGE_AUX_SYMBOL_Sym_FcnAry
        (Function :pointer)
        (Array :pointer))

(defcstructex-exported IMAGE_AUX_SYMBOL_Sym_FcnAry_Array
        (Dimension :pointer))

(defcstructex-exported IMAGE_AUX_SYMBOL_Sym_FcnAry_Function
        (PointerToLinenumber :unsigned-long)
        (PointerToNextFunction :unsigned-long))

(cffi:defcunion IMAGE_AUX_SYMBOL_Sym_Misc
        (TotalSize :unsigned-long)
        (LnSz :pointer))

(defcstructex-exported IMAGE_AUX_SYMBOL_Sym_Misc_LnSz
        (Linenumber :unsigned-short)
        (Size :unsigned-short))

(defcstructex-exported IMAGE_COFF_SYMBOLS_HEADER
        (NumberOfSymbols :unsigned-long)
        (LvaToFirstSymbol :unsigned-long)
        (NumberOfLinenumbers :unsigned-long)
        (LvaToFirstLinenumber :unsigned-long)
        (RvaToFirstByteOfCode :unsigned-long)
        (RvaToLastByteOfCode :unsigned-long)
        (RvaToFirstByteOfData :unsigned-long)
        (RvaToLastByteOfData :unsigned-long))





(defcstructex-exported IMAGE_RELOCATION
        (SymbolTableIndex :unsigned-long)
        (Type :unsigned-short)
        (u :pointer))





(cffi:defcunion IMAGE_RELOCATION_u
        (VirtualAddress :unsigned-long)
        (RelocCount :unsigned-long))

(defcstructex-exported IMAGE_BASE_RELOCATION
        (VirtualAddress :unsigned-long)
        (SizeOfBlock :unsigned-long))





(defcstructex-exported IMAGE_LINENUMBER
        (Linenumber :unsigned-short)
        (Type :pointer))





(cffi:defcunion IMAGE_LINENUMBER_Type
        (SymbolTableIndex :unsigned-long)
        (VirtualAddress :unsigned-long))

(defcstructex-exported IMAGE_ARCHIVE_MEMBER_HEADER
        (Name :pointer)
        (Date :pointer)
        (UserID :pointer)
        (GroupID :pointer)
        (Mode :pointer)
        (Size :pointer)
        (EndHeader :pointer))





(defcstructex-exported IMAGE_EXPORT_DIRECTORY
        (Characteristics :unsigned-long)
        (TimeDateStamp :unsigned-long)
        (MajorVersion :unsigned-short)
        (MinorVersion :unsigned-short)
        (Name :unsigned-long)
        (Base :unsigned-long)
        (NumberOfFunctions :unsigned-long)
        (NumberOfNames :unsigned-long)
        (AddressOfFunctions :unsigned-long)
        (AddressOfNames :unsigned-long)
        (AddressOfNameOrdinals :unsigned-long))





(defcstructex-exported IMAGE_IMPORT_BY_NAME
        (Hint :unsigned-short)
        (Name :pointer))





(defcstructex-exported IMAGE_THUNK_DATA32
        (u1 :pointer))





(cffi:defcunion IMAGE_THUNK_DATA32_u1
        (ForwarderString :unsigned-long)
        (Function :unsigned-long)
        (Ordinal :unsigned-long)
        (AddressOfData :unsigned-long))

(defcstructex-exported IMAGE_THUNK_DATA64
        (u1 :pointer))





(cffi:defcunion IMAGE_THUNK_DATA64_u1
        (ForwarderString :unsigned-long-long)
        (Function :unsigned-long-long)
        (Ordinal :unsigned-long-long)
        (AddressOfData :unsigned-long-long))





(defcstructex-exported IMAGE_IMPORT_DESCRIPTOR
        (TimeDateStamp :unsigned-long)
        (ForwarderChain :unsigned-long)
        (Name :unsigned-long)
        (FirstThunk :unsigned-long)
        (u :pointer))





(cffi:defcunion IMAGE_IMPORT_DESCRIPTOR_u
        (Characteristics :unsigned-long)
        (OriginalFirstThunk :unsigned-long))

(defcstructex-exported IMAGE_BOUND_IMPORT_DESCRIPTOR
        (TimeDateStamp :unsigned-long)
        (OffsetModuleName :unsigned-short)
        (NumberOfModuleForwarderRefs :unsigned-short))





(defcstructex-exported IMAGE_BOUND_FORWARDER_REF
        (TimeDateStamp :unsigned-long)
        (OffsetModuleName :unsigned-short)
        (Reserved :unsigned-short))







(defcstructex-exported IMAGE_TLS_DIRECTORY32
        (StartAddressOfRawData :unsigned-long)
        (EndAddressOfRawData :unsigned-long)
        (AddressOfIndex :unsigned-long)
        (AddressOfCallBacks :unsigned-long)
        (SizeOfZeroFill :unsigned-long)
        (Characteristics :unsigned-long))





(defcstructex-exported IMAGE_TLS_DIRECTORY64
        (StartAddressOfRawData :unsigned-long-long)
        (EndAddressOfRawData :unsigned-long-long)
        (AddressOfIndex :unsigned-long-long)
        (AddressOfCallBacks :unsigned-long-long)
        (SizeOfZeroFill :unsigned-long)
        (Characteristics :unsigned-long))









(defcstructex-exported IMAGE_RESOURCE_DIRECTORY
        (Characteristics :unsigned-long)
        (TimeDateStamp :unsigned-long)
        (MajorVersion :unsigned-short)
        (MinorVersion :unsigned-short)
        (NumberOfNamedEntries :unsigned-short)
        (NumberOfIdEntries :unsigned-short))





(defcstructex-exported IMAGE_RESOURCE_DIRECTORY_ENTRY
        (u :pointer)
        (u2 :pointer))





(cffi:defcunion IMAGE_RESOURCE_DIRECTORY_ENTRY_u2
        (OffsetToData :unsigned-long)
        (s2 :pointer))

(defcstructex-exported IMAGE_RESOURCE_DIRECTORY_ENTRY_u2_s2
        (OffsetToDirectory :unsigned-long)
        (DataIsDirectory :unsigned-long))

(cffi:defcunion IMAGE_RESOURCE_DIRECTORY_ENTRY_u
        (Name :unsigned-long)
        (Id :unsigned-short)
        (s :pointer))

(defcstructex-exported IMAGE_RESOURCE_DIRECTORY_ENTRY_u_s
        (NameOffset :unsigned-long)
        (NameIsString :unsigned-long))

(defcstructex-exported IMAGE_RESOURCE_DIRECTORY_STRING
        (Length :unsigned-short)
        (NameString :pointer))





(defcstructex-exported IMAGE_RESOURCE_DIR_STRING_U
        (Length :unsigned-short)
        (NameString :pointer))





(defcstructex-exported IMAGE_RESOURCE_DATA_ENTRY
        (OffsetToData :unsigned-long)
        (Size :unsigned-long)
        (CodePage :unsigned-long)
        (Reserved :unsigned-long))





(defcstructex-exported IMAGE_LOAD_CONFIG_DIRECTORY
        (Characteristics :unsigned-long)
        (TimeDateStamp :unsigned-long)
        (MajorVersion :unsigned-short)
        (MinorVersion :unsigned-short)
        (GlobalFlagsClear :unsigned-long)
        (GlobalFlagsSet :unsigned-long)
        (CriticalSectionDefaultTimeout :unsigned-long)
        (DeCommitFreeBlockThreshold :unsigned-long)
        (DeCommitTotalFreeThreshold :unsigned-long)
        (LockPrefixTable :pointer)
        (MaximumAllocationSize :unsigned-long)
        (VirtualMemoryThreshold :unsigned-long)
        (ProcessHeapFlags :unsigned-long)
        (Reserved :pointer))





(defcstructex-exported IMAGE_RUNTIME_FUNCTION_ENTRY
        (BeginAddress :unsigned-long)
        (EndAddress :unsigned-long)
        (ExceptionHandler :pointer)
        (HandlerData :pointer)
        (PrologEndAddress :unsigned-long))





(defcstructex-exported IMAGE_DEBUG_DIRECTORY
        (Characteristics :unsigned-long)
        (TimeDateStamp :unsigned-long)
        (MajorVersion :unsigned-short)
        (MinorVersion :unsigned-short)
        (Type :unsigned-long)
        (SizeOfData :unsigned-long)
        (AddressOfRawData :unsigned-long)
        (PointerToRawData :unsigned-long))





(defcstructex-exported FPO_DATA
        (ulOffStart :unsigned-long)
        (cbProcSize :unsigned-long)
        (cdwLocals :unsigned-long)
        (cdwParams :unsigned-short)
        (cbProlog :unsigned-short)
        (cbRegs :unsigned-short)
        (fHasSEH :unsigned-short)
        (fUseBP :unsigned-short)
        (reserved :unsigned-short)
        (cbFrame :unsigned-short))





(defcstructex-exported IMAGE_DEBUG_MISC
        (DataType :unsigned-long)
        (Length :unsigned-long)
        (Unicode :unsigned-char)
        (Reserved :pointer)
        (Data :pointer))





(defcstructex-exported IMAGE_FUNCTION_ENTRY
        (StartingAddress :unsigned-long)
        (EndingAddress :unsigned-long)
        (EndOfPrologue :unsigned-long))





(defcstructex-exported IMAGE_SEPARATE_DEBUG_HEADER
        (Signature :unsigned-short)
        (Flags :unsigned-short)
        (Machine :unsigned-short)
        (Characteristics :unsigned-short)
        (TimeDateStamp :unsigned-long)
        (CheckSum :unsigned-long)
        (ImageBase :unsigned-long)
        (SizeOfImage :unsigned-long)
        (NumberOfSections :unsigned-long)
        (ExportedNamesSize :unsigned-long)
        (DebugDirectorySize :unsigned-long)
        (SectionAlignment :unsigned-long)
        (Reserved :pointer))





(cffi:defcenum SERVICE_NODE_TYPE
        (:DriverType #.1)
        (:FileSystemType #.2)
        (:Win32ServiceOwnProcess #.16)
        (:Win32ServiceShareProcess #.32)
        (:AdapterType #.4)
        (:RecognizerType #.8))

(cffi:defcenum SERVICE_LOAD_TYPE
        (:BootLoad #.0)
        (:SystemLoad #.1)
        (:AutoLoad #.2)
        (:DemandLoad #.3)
        (:DisableLoad #.4))

(cffi:defcenum SERVICE_ERROR_TYPE
        (:IgnoreError #.0)
        (:NormalError #.1)
        (:SevereError #.2)
        (:CriticalError #.3))

(defcstructex-exported NT_TIB
        (ExceptionList :pointer)
        (StackBase :pointer)
        (StackLimit :pointer)
        (SubSystemTib :pointer)
        (ArbitraryUserPointer :pointer)
        (Self :pointer)
        (u :pointer))





(cffi:defcunion NT_TIB_u
        (FiberData :pointer)
        (Version :unsigned-long))

(defcstructex-exported REPARSE_DATA_BUFFER
        (ReparseTag :unsigned-long)
        (ReparseDataLength :unsigned-short)
        (Reserved :unsigned-short)
        (u :pointer))





(cffi:defcunion REPARSE_DATA_BUFFER_u
        (SymbolicLinkReparseBuffer :pointer)
        (MountPointReparseBuffer :pointer)
        (GenericReparseBuffer :pointer))

(defcstructex-exported REPARSE_DATA_BUFFER_u_GenericReparseBuffer
        (DataBuffer :pointer))

(defcstructex-exported REPARSE_DATA_BUFFER_u_MountPointReparseBuffer
        (SubstituteNameOffset :unsigned-short)
        (SubstituteNameLength :unsigned-short)
        (PrintNameOffset :unsigned-short)
        (PrintNameLength :unsigned-short)
        (PathBuffer :pointer))

(defcstructex-exported REPARSE_DATA_BUFFER_u_SymbolicLinkReparseBuffer
        (SubstituteNameOffset :unsigned-short)
        (SubstituteNameLength :unsigned-short)
        (PrintNameOffset :unsigned-short)
        (PrintNameLength :unsigned-short)
        (PathBuffer :pointer))

(defcstructex-exported REPARSE_GUID_DATA_BUFFER
        (ReparseTag :unsigned-long)
        (ReparseDataLength :unsigned-short)
        (Reserved :unsigned-short)
        (ReparseGuid GUID)
        (GenericReparseBuffer :pointer))





(defcstructex-exported REPARSE_GUID_DATA_BUFFER_GenericReparseBuffer
        (DataBuffer :pointer))

(defcstructex-exported REPARSE_POINT_INFORMATION
        (ReparseDataLength :unsigned-short)
        (UnparsedNameLength :unsigned-short))





(cffi:defcunion FILE_SEGMENT_ELEMENT
        (Buffer :pointer)
        (Alignment :unsigned-long-long))





(defconstant-exported JOB_OBJECT_LIMIT_WORKINGSET #x0001)

(defconstant-exported JOB_OBJECT_LIMIT_PROCESS_TIME #x0002)

(defconstant-exported JOB_OBJECT_LIMIT_JOB_TIME #x0004)

(defconstant-exported JOB_OBJECT_LIMIT_ACTIVE_PROCESS #x0008)

(defconstant-exported JOB_OBJECT_LIMIT_AFFINITY #x0010)

(defconstant-exported JOB_OBJECT_LIMIT_PRIORITY_CLASS #x0020)

(defconstant-exported JOB_OBJECT_LIMIT_PRESERVE_JOB_TIME #x0040)

(defconstant-exported JOB_OBJECT_LIMIT_SCHEDULING_CLASS #x0080)

(defconstant-exported JOB_OBJECT_LIMIT_PROCESS_MEMORY #x0100)

(defconstant-exported JOB_OBJECT_LIMIT_JOB_MEMORY #x0200)

(defconstant-exported JOB_OBJECT_LIMIT_DIE_ON_UNHANDLED_EXCEPTION #x0400)

(defconstant-exported JOB_OBJECT_BREAKAWAY_OK #x0800)

(defconstant-exported JOB_OBJECT_SILENT_BREAKAWAY #x1000)

(defconstant-exported JOB_OBJECT_UILIMIT_HANDLES #x0001)

(defconstant-exported JOB_OBJECT_UILIMIT_READCLIPBOARD #x0002)

(defconstant-exported JOB_OBJECT_UILIMIT_WRITECLIPBOARD #x0004)

(defconstant-exported JOB_OBJECT_UILIMIT_SYSTEMPARAMETERS #x0008)

(defconstant-exported JOB_OBJECT_UILIMIT_DISPLAYSETTINGS #x0010)

(defconstant-exported JOB_OBJECT_UILIMIT_GLOBALATOMS #x0020)

(defconstant-exported JOB_OBJECT_UILIMIT_DESKTOP #x0040)

(defconstant-exported JOB_OBJECT_UILIMIT_EXITWINDOWS #x0080)

(defconstant-exported JOB_OBJECT_SECURITY_NO_ADMIN #x0001)

(defconstant-exported JOB_OBJECT_SECURITY_RESTRICTED_TOKEN #x0002)

(defconstant-exported JOB_OBJECT_SECURITY_ONLY_TOKEN #x0004)

(defconstant-exported JOB_OBJECT_SECURITY_FILTER_TOKENS #x0008)

(defconstant-exported JOB_OBJECT_TERMINATE_AT_END_OF_JOB 0)

(defconstant-exported JOB_OBJECT_POST_AT_END_OF_JOB 1)

(defconstant-exported JOB_OBJECT_MSG_END_OF_JOB_TIME 1)

(defconstant-exported JOB_OBJECT_MSG_END_OF_PROCESS_TIME 2)

(defconstant-exported JOB_OBJECT_MSG_ACTIVE_PROCESS_LIMIT 3)

(defconstant-exported JOB_OBJECT_MSG_ACTIVE_PROCESS_ZERO 4)

(defconstant-exported JOB_OBJECT_MSG_NEW_PROCESS 6)

(defconstant-exported JOB_OBJECT_MSG_EXIT_PROCESS 7)

(defconstant-exported JOB_OBJECT_MSG_ABNORMAL_EXIT_PROCESS 8)

(defconstant-exported JOB_OBJECT_MSG_PROCESS_MEMORY_LIMIT 9)

(defconstant-exported JOB_OBJECT_MSG_JOB_MEMORY_LIMIT 10)

(cffi:defcenum JOBOBJECTINFOCLASS
        (:JobObjectBasicAccountingInformation #.1)
        :JobObjectBasicLimitInformation
        :JobObjectBasicProcessIdList
        :JobObjectBasicUIRestrictions
        :JobObjectSecurityLimitInformation
        :JobObjectEndOfJobTimeInformation
        :JobObjectAssociateCompletionPortInformation
        :JobObjectBasicAndIoAccountingInformation
        :JobObjectExtendedLimitInformation
        :JobObjectJobSetInformation
        :MaxJobObjectInfoClass)

(defcstructex-exported JOBOBJECT_BASIC_ACCOUNTING_INFORMATION
        (TotalUserTime LARGE_INTEGER)
        (TotalKernelTime LARGE_INTEGER)
        (ThisPeriodTotalUserTime LARGE_INTEGER)
        (ThisPeriodTotalKernelTime LARGE_INTEGER)
        (TotalPageFaultCount :unsigned-long)
        (TotalProcesses :unsigned-long)
        (ActiveProcesses :unsigned-long)
        (TotalTerminatedProcesses :unsigned-long))





(defcstructex-exported JOBOBJECT_BASIC_LIMIT_INFORMATION
        (PerProcessUserTimeLimit LARGE_INTEGER)
        (PerJobUserTimeLimit LARGE_INTEGER)
        (LimitFlags :unsigned-long)
        (MinimumWorkingSetSize :unsigned-long)
        (MaximumWorkingSetSize :unsigned-long)
        (ActiveProcessLimit :unsigned-long)
        (Affinity :unsigned-long)
        (PriorityClass :unsigned-long)
        (SchedulingClass :unsigned-long))





(defcstructex-exported JOBOBJECT_BASIC_PROCESS_ID_LIST
        (NumberOfAssignedProcesses :unsigned-long)
        (NumberOfProcessIdsInList :unsigned-long)
        (ProcessIdList :pointer))





(defcstructex-exported JOBOBJECT_BASIC_UI_RESTRICTIONS
        (UIRestrictionsClass :unsigned-long))





(defcstructex-exported JOBOBJECT_SECURITY_LIMIT_INFORMATION
        (SecurityLimitFlags :unsigned-long)
        (JobToken :pointer)
        (SidsToDisable :pointer)
        (PrivilegesToDelete :pointer)
        (RestrictedSids :pointer))





(defcstructex-exported JOBOBJECT_END_OF_JOB_TIME_INFORMATION
        (EndOfJobTimeAction :unsigned-long))





(defcstructex-exported JOBOBJECT_ASSOCIATE_COMPLETION_PORT
        (CompletionKey :pointer)
        (CompletionPort :pointer))





(defcstructex-exported JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION
        (BasicInfo JOBOBJECT_BASIC_ACCOUNTING_INFORMATION)
        (IoInfo IO_COUNTERS))





(defcstructex-exported JOBOBJECT_EXTENDED_LIMIT_INFORMATION
        (BasicLimitInformation JOBOBJECT_BASIC_LIMIT_INFORMATION)
        (IoInfo IO_COUNTERS)
        (ProcessMemoryLimit :unsigned-long)
        (JobMemoryLimit :unsigned-long)
        (PeakProcessMemoryUsed :unsigned-long)
        (PeakJobMemoryUsed :unsigned-long))





(defcstructex-exported JOBOBJECT_JOBSET_INFORMATION
        (MemberLevel :unsigned-long))





(defconstant-exported ES_SYSTEM_REQUIRED #x00000001)

(defconstant-exported ES_DISPLAY_REQUIRED #x00000002)

(defconstant-exported ES_USER_PRESENT #x00000004)

(defconstant-exported ES_CONTINUOUS #x80000000)

(cffi:defcenum LATENCY_TIME
        :LT_DONT_CARE
        :LT_LOWEST_LATENCY)



(cffi:defcenum SYSTEM_POWER_STATE
        :PowerSystemUnspecified
        :PowerSystemWorking
        :PowerSystemSleeping1
        :PowerSystemSleeping2
        :PowerSystemSleeping3
        :PowerSystemHibernate
        :PowerSystemShutdown
        :PowerSystemMaximum)



(cffi:defcenum POWER_ACTION
        :PowerActionNone
        :PowerActionReserved
        :PowerActionSleep
        :PowerActionHibernate
        :PowerActionShutdown
        :PowerActionShutdownReset
        :PowerActionShutdownOff
        :PowerActionWarmEject)



(cffi:defcenum DEVICE_POWER_STATE
        :PowerDeviceUnspecified
        :PowerDeviceD0
        :PowerDeviceD1
        :PowerDeviceD2
        :PowerDeviceD3
        :PowerDeviceMaximum)



(defcstructex-exported BATTERY_REPORTING_SCALE
        (Granularity :unsigned-long)
        (Capacity :unsigned-long))



(defcstructex-exported POWER_ACTION_POLICY
        (Action POWER_ACTION)
        (Flags :unsigned-long)
        (EventCode :unsigned-long))





(defconstant-exported POWER_ACTION_QUERY_ALLOWED #x00000001)

(defconstant-exported POWER_ACTION_UI_ALLOWED #x00000002)

(defconstant-exported POWER_ACTION_OVERRIDE_APPS #x00000004)

(defconstant-exported POWER_ACTION_LIGHTEST_FIRST #x10000000)

(defconstant-exported POWER_ACTION_LOCK_CONSOLE #x20000000)

(defconstant-exported POWER_ACTION_DISABLE_WAKES #x40000000)

(defconstant-exported POWER_ACTION_CRITICAL #x80000000)

(defconstant-exported POWER_LEVEL_USER_NOTIFY_TEXT #x00000001)

(defconstant-exported POWER_LEVEL_USER_NOTIFY_SOUND #x00000002)

(defconstant-exported POWER_LEVEL_USER_NOTIFY_EXEC #x00000004)

(defconstant-exported POWER_USER_NOTIFY_BUTTON #x00000008)

(defconstant-exported POWER_USER_NOTIFY_SHUTDOWN #x00000010)

(defconstant-exported POWER_FORCE_TRIGGER_RESET #x80000000)

(defconstant-exported DISCHARGE_POLICY_CRITICAL 0)

(defconstant-exported DISCHARGE_POLICY_LOW 1)

(defconstant-exported NUM_DISCHARGE_POLICIES 4)

(defconstant-exported PO_THROTTLE_NONE 0)

(defconstant-exported PO_THROTTLE_CONSTANT 1)

(defconstant-exported PO_THROTTLE_DEGRADE 2)

(defconstant-exported PO_THROTTLE_ADAPTIVE 3)

(defconstant-exported PO_THROTTLE_MAXIMUM 4)

(defcstructex-exported SYSTEM_POWER_LEVEL
        (Enable :unsigned-char)
        (Spare :pointer)
        (BatteryLevel :unsigned-long)
        (PowerPolicy POWER_ACTION_POLICY)
        (MinSystemState SYSTEM_POWER_STATE))





(defcstructex-exported SYSTEM_POWER_POLICY
        (Revision :unsigned-long)
        (PowerButton POWER_ACTION_POLICY)
        (SleepButton POWER_ACTION_POLICY)
        (LidClose POWER_ACTION_POLICY)
        (LidOpenWake SYSTEM_POWER_STATE)
        (Reserved :unsigned-long)
        (Idle POWER_ACTION_POLICY)
        (IdleTimeout :unsigned-long)
        (IdleSensitivity :unsigned-char)
        (DynamicThrottle :unsigned-char)
        (Spare2 :pointer)
        (MinSleep SYSTEM_POWER_STATE)
        (MaxSleep SYSTEM_POWER_STATE)
        (ReducedLatencySleep SYSTEM_POWER_STATE)
        (WinLogonFlags :unsigned-long)
        (Spare3 :unsigned-long)
        (DozeS4Timeout :unsigned-long)
        (BroadcastCapacityResolution :unsigned-long)
        (DischargePolicy :pointer)
        (VideoTimeout :unsigned-long)
        (VideoDimDisplay :unsigned-char)
        (VideoReserved :pointer)
        (SpindownTimeout :unsigned-long)
        (OptimizeForPower :unsigned-char)
        (FanThrottleTolerance :unsigned-char)
        (ForcedThrottle :unsigned-char)
        (MinThrottle :unsigned-char)
        (OverThrottled POWER_ACTION_POLICY))





(defcstructex-exported SYSTEM_POWER_CAPABILITIES
        (PowerButtonPresent :unsigned-char)
        (SleepButtonPresent :unsigned-char)
        (LidPresent :unsigned-char)
        (SystemS1 :unsigned-char)
        (SystemS2 :unsigned-char)
        (SystemS3 :unsigned-char)
        (SystemS4 :unsigned-char)
        (SystemS5 :unsigned-char)
        (HiberFilePresent :unsigned-char)
        (FullWake :unsigned-char)
        (VideoDimPresent :unsigned-char)
        (ApmPresent :unsigned-char)
        (UpsPresent :unsigned-char)
        (ThermalControl :unsigned-char)
        (ProcessorThrottle :unsigned-char)
        (ProcessorMinThrottle :unsigned-char)
        (ProcessorMaxThrottle :unsigned-char)
        (spare2 :pointer)
        (DiskSpinDown :unsigned-char)
        (spare3 :pointer)
        (SystemBatteriesPresent :unsigned-char)
        (BatteriesAreShortTerm :unsigned-char)
        (BatteryScale :pointer)
        (AcOnLineWake SYSTEM_POWER_STATE)
        (SoftLidWake SYSTEM_POWER_STATE)
        (RtcWake SYSTEM_POWER_STATE)
        (MinDeviceWakeState SYSTEM_POWER_STATE)
        (DefaultLowLatencyWake SYSTEM_POWER_STATE))





(defcstructex-exported SYSTEM_BATTERY_STATE
        (AcOnLine :unsigned-char)
        (BatteryPresent :unsigned-char)
        (Charging :unsigned-char)
        (Discharging :unsigned-char)
        (Spare1 :pointer)
        (MaxCapacity :unsigned-long)
        (RemainingCapacity :unsigned-long)
        (Rate :unsigned-long)
        (EstimatedTime :unsigned-long)
        (DefaultAlert1 :unsigned-long)
        (DefaultAlert2 :unsigned-long))





(cffi:defcenum POWER_INFORMATION_LEVEL
        :SystemPowerPolicyAc
        :SystemPowerPolicyDc
        :VerifySystemPolicyAc
        :VerifySystemPolicyDc
        :SystemPowerCapabilities
        :SystemBatteryState
        :SystemPowerStateHandler
        :ProcessorStateHandler
        :SystemPowerPolicyCurrent
        :AdministratorPowerPolicy
        :SystemReserveHiberFile
        :ProcessorInformation
        :SystemPowerInformation
        :ProcessorStateHandler2
        :LastWakeTime
        :LastSleepTime
        :SystemExecutionState
        :SystemPowerStateNotifyHandler
        :ProcessorPowerPolicyAc
        :ProcessorPowerPolicyDc
        :VerifyProcessorPowerPolicyAc
        :VerifyProcessorPowerPolicyDc
        :ProcessorPowerPolicyCurrent)

(defcstructex-exported SYSTEM_POWER_INFORMATION
        (MaxIdlenessAllowed :unsigned-long)
        (Idleness :unsigned-long)
        (TimeRemaining :unsigned-long)
        (CoolingMode :unsigned-char))





(defcstructex-exported PROCESSOR_POWER_POLICY_INFO
        (TimeCheck :unsigned-long)
        (DemoteLimit :unsigned-long)
        (PromoteLimit :unsigned-long)
        (DemotePercent :unsigned-char)
        (PromotePercent :unsigned-char)
        (Spare :pointer)
        (AllowDemotion :unsigned-long)
        (AllowPromotion :unsigned-long)
        (Reserved :unsigned-long))





(defcstructex-exported PROCESSOR_POWER_POLICY
        (Revision :unsigned-long)
        (DynamicThrottle :unsigned-char)
        (Spare :pointer)
        (Reserved :unsigned-long)
        (PolicyCount :unsigned-long)
        (Policy :pointer))





(defcstructex-exported ADMINISTRATOR_POWER_POLICY
        (MinSleep SYSTEM_POWER_STATE)
        (MaxSleep SYSTEM_POWER_STATE)
        (MinVideoTimeout :unsigned-long)
        (MaxVideoTimeout :unsigned-long)
        (MinSpindownTimeout :unsigned-long)
        (MaxSpindownTimeout :unsigned-long))




























(defconstant-exported HMONITOR_DECLARED 1)















(defcstructex-exported RECT
                 (left :int32)
                 (top :int32)
                 (right :int32)
                 (bottom :int32))







(defcstructex-exported RECTL
        (left :int32)
        (top :int32)
        (right :int32)
        (bottom :int32))










(defcstructex-exported SIZE
        (cx :int32)
        (cy :int32))













(defcstructex-exported POINTS
        (x :short)
        (y :short))







(defconstant-exported FOREGROUND_BLUE 1)

(defconstant-exported FOREGROUND_GREEN 2)

(defconstant-exported FOREGROUND_RED 4)

(defconstant-exported FOREGROUND_INTENSITY 8)

(defconstant-exported BACKGROUND_BLUE 16)

(defconstant-exported BACKGROUND_GREEN 32)

(defconstant-exported BACKGROUND_RED 64)

(defconstant-exported BACKGROUND_INTENSITY 128)

(defconstant-exported CTRL_C_EVENT 0)

(defconstant-exported CTRL_BREAK_EVENT 1)

(defconstant-exported CTRL_CLOSE_EVENT 2)

(defconstant-exported CTRL_LOGOFF_EVENT 5)

(defconstant-exported CTRL_SHUTDOWN_EVENT 6)

(defconstant-exported ENABLE_LINE_INPUT 2)

(defconstant-exported ENABLE_ECHO_INPUT 4)

(defconstant-exported ENABLE_PROCESSED_INPUT 1)

(defconstant-exported ENABLE_WINDOW_INPUT 8)

(defconstant-exported ENABLE_MOUSE_INPUT 16)

(defconstant-exported ENABLE_PROCESSED_OUTPUT 1)

(defconstant-exported ENABLE_WRAP_AT_EOL_OUTPUT 2)

(defconstant-exported KEY_EVENT 1)

(defconstant-exported MOUSE_EVENT 2)

(defconstant-exported WINDOW_BUFFER_SIZE_EVENT 4)

(defconstant-exported MENU_EVENT 8)

(defconstant-exported FOCUS_EVENT 16)

(defconstant-exported CAPSLOCK_ON 128)

(defconstant-exported ENHANCED_KEY 256)

(defconstant-exported RIGHT_ALT_PRESSED 1)

(defconstant-exported LEFT_ALT_PRESSED 2)

(defconstant-exported RIGHT_CTRL_PRESSED 4)

(defconstant-exported LEFT_CTRL_PRESSED 8)

(defconstant-exported SHIFT_PRESSED 16)

(defconstant-exported NUMLOCK_ON 32)

(defconstant-exported SCROLLLOCK_ON 64)

(defconstant-exported FROM_LEFT_1ST_BUTTON_PRESSED 1)

(defconstant-exported RIGHTMOST_BUTTON_PRESSED 2)

(defconstant-exported FROM_LEFT_2ND_BUTTON_PRESSED 4)

(defconstant-exported FROM_LEFT_3RD_BUTTON_PRESSED 8)

(defconstant-exported FROM_LEFT_4TH_BUTTON_PRESSED 16)

(defconstant-exported MOUSE_MOVED 1)

(defconstant-exported DOUBLE_CLICK 2)

(defconstant-exported MOUSE_WHEELED 4)

(defcstructex-exported CHAR_INFO
        (Attributes :unsigned-short)
        (Char :pointer))





(cffi:defcunion CHAR_INFO_Char
        (UnicodeChar :pointer)
        (AsciiChar :char))

(defcstructex-exported SMALL_RECT
        (Left :short)
        (Top :short)
        (Right :short)
        (Bottom :short))





(defcstructex-exported CONSOLE_CURSOR_INFO
        (dwSize :unsigned-long)
        (bVisible :int))





(defcstructex-exported COORD
        (X :short)
        (Y :short))





(defcstructex-exported CONSOLE_FONT_INFO
        (nFont :unsigned-long)
        (dwFontSize COORD))





(defcstructex-exported CONSOLE_SCREEN_BUFFER_INFO
        (dwSize COORD)
        (dwCursorPosition COORD)
        (wAttributes :unsigned-short)
        (srWindow SMALL_RECT)
        (dwMaximumWindowSize COORD))







(defcstructex-exported KEY_EVENT_RECORD
        (bKeyDown :int)
        (wRepeatCount :unsigned-short)
        (wVirtualKeyCode :unsigned-short)
        (wVirtualScanCode :unsigned-short)
        (dwControlKeyState :unsigned-long)
        (uChar :pointer))



(cffi:defcunion KEY_EVENT_RECORD_uChar
        (UnicodeChar :pointer)
        (AsciiChar :char))

(defcstructex-exported MOUSE_EVENT_RECORD
        (dwMousePosition COORD)
        (dwButtonState :unsigned-long)
        (dwControlKeyState :unsigned-long)
        (dwEventFlags :unsigned-long))



(defcstructex-exported WINDOW_BUFFER_SIZE_RECORD
        (dwSize COORD))



(defcstructex-exported MENU_EVENT_RECORD
        (dwCommandId :unsigned-int))





(defcstructex-exported FOCUS_EVENT_RECORD
        (bSetFocus :int))



(defcstructex-exported INPUT_RECORD
        (EventType :unsigned-short)
        (Event :pointer))





(cffi:defcunion INPUT_RECORD_Event
        (KeyEvent KEY_EVENT_RECORD)
        (MouseEvent MOUSE_EVENT_RECORD)
        (WindowBufferSizeEvent WINDOW_BUFFER_SIZE_RECORD)
        (MenuEvent MENU_EVENT_RECORD)
        (FocusEvent FOCUS_EVENT_RECORD))

(defconstant-exported SP_SERIALCOMM 1)

(defconstant-exported PST_UNSPECIFIED 0)

(defconstant-exported PST_RS232 1)

(defconstant-exported PST_PARALLELPORT 2)

(defconstant-exported PST_RS422 3)

(defconstant-exported PST_RS423 4)

(defconstant-exported PST_RS449 5)

(defconstant-exported PST_MODEM 6)

(defconstant-exported PST_FAX #x21)

(defconstant-exported PST_SCANNER #x22)

(defconstant-exported PST_NETWORK_BRIDGE #x100)

(defconstant-exported PST_LAT #x101)

(defconstant-exported PST_TCPIP_TELNET #x102)

(defconstant-exported PST_X25 #x103)

(defconstant-exported BAUD_075 1)

(defconstant-exported BAUD_110 2)

(defconstant-exported BAUD_134_5 4)

(defconstant-exported BAUD_150 8)

(defconstant-exported BAUD_300 16)

(defconstant-exported BAUD_600 32)

(defconstant-exported BAUD_1200 64)

(defconstant-exported BAUD_1800 128)

(defconstant-exported BAUD_2400 256)

(defconstant-exported BAUD_4800 512)

(defconstant-exported BAUD_7200 1024)

(defconstant-exported BAUD_9600 2048)

(defconstant-exported BAUD_14400 4096)

(defconstant-exported BAUD_19200 8192)

(defconstant-exported BAUD_38400 16384)

(defconstant-exported BAUD_56K 32768)

(defconstant-exported BAUD_128K 65536)

(defconstant-exported BAUD_115200 131072)

(defconstant-exported BAUD_57600 262144)

(defconstant-exported BAUD_USER #x10000000)

(defconstant-exported PCF_DTRDSR 1)

(defconstant-exported PCF_RTSCTS 2)

(defconstant-exported PCF_RLSD 4)

(defconstant-exported PCF_PARITY_CHECK 8)

(defconstant-exported PCF_XONXOFF 16)

(defconstant-exported PCF_SETXCHAR 32)

(defconstant-exported PCF_TOTALTIMEOUTS 64)

(defconstant-exported PCF_INTTIMEOUTS 128)

(defconstant-exported PCF_SPECIALCHARS 256)

(defconstant-exported PCF_16BITMODE 512)

(defconstant-exported SP_PARITY 1)

(defconstant-exported SP_BAUD 2)

(defconstant-exported SP_DATABITS 4)

(defconstant-exported SP_STOPBITS 8)

(defconstant-exported SP_HANDSHAKING 16)

(defconstant-exported SP_PARITY_CHECK 32)

(defconstant-exported SP_RLSD 64)

(defconstant-exported DATABITS_5 1)

(defconstant-exported DATABITS_6 2)

(defconstant-exported DATABITS_7 4)

(defconstant-exported DATABITS_8 8)

(defconstant-exported DATABITS_16 16)

(defconstant-exported DATABITS_16X 32)

(defconstant-exported STOPBITS_10 1)

(defconstant-exported STOPBITS_15 2)

(defconstant-exported STOPBITS_20 4)

(defconstant-exported PARITY_NONE 256)

(defconstant-exported PARITY_ODD 512)

(defconstant-exported PARITY_EVEN 1024)

(defconstant-exported PARITY_MARK 2048)

(defconstant-exported PARITY_SPACE 4096)

(defconstant-exported EXCEPTION_DEBUG_EVENT 1)

(defconstant-exported CREATE_THREAD_DEBUG_EVENT 2)

(defconstant-exported CREATE_PROCESS_DEBUG_EVENT 3)

(defconstant-exported EXIT_THREAD_DEBUG_EVENT 4)

(defconstant-exported EXIT_PROCESS_DEBUG_EVENT 5)

(defconstant-exported LOAD_DLL_DEBUG_EVENT 6)

(defconstant-exported UNLOAD_DLL_DEBUG_EVENT 7)

(defconstant-exported OUTPUT_DEBUG_STRING_EVENT 8)

(defconstant-exported RIP_EVENT 9)

(defconstant-exported FILE_BEGIN 0)

(defconstant-exported FILE_CURRENT 1)

(defconstant-exported FILE_END 2)

(defconstant-exported OF_READ 0)

(defconstant-exported OF_READWRITE 2)

(defconstant-exported OF_WRITE 1)

(defconstant-exported OF_SHARE_COMPAT 0)

(defconstant-exported OF_SHARE_DENY_NONE 64)

(defconstant-exported OF_SHARE_DENY_READ 48)

(defconstant-exported OF_SHARE_DENY_WRITE 32)

(defconstant-exported OF_SHARE_EXCLUSIVE 16)

(defconstant-exported OF_CANCEL 2048)

(defconstant-exported OF_CREATE 4096)

(defconstant-exported OF_DELETE 512)

(defconstant-exported OF_EXIST 16384)

(defconstant-exported OF_PARSE 256)

(defconstant-exported OF_PROMPT 8192)

(defconstant-exported OF_REOPEN 32768)

(defconstant-exported OF_VERIFY 1024)

(defconstant-exported NMPWAIT_NOWAIT 1)

(defconstant-exported NMPWAIT_USE_DEFAULT_WAIT 0)

(defconstant-exported CE_BREAK 16)

(defconstant-exported CE_DNS 2048)

(defconstant-exported CE_FRAME 8)

(defconstant-exported CE_IOE 1024)

(defconstant-exported CE_MODE 32768)

(defconstant-exported CE_OOP 4096)

(defconstant-exported CE_OVERRUN 2)

(defconstant-exported CE_PTO 512)

(defconstant-exported CE_RXOVER 1)

(defconstant-exported CE_RXPARITY 4)

(defconstant-exported CE_TXFULL 256)

(defconstant-exported PROGRESS_CONTINUE 0)

(defconstant-exported PROGRESS_CANCEL 1)

(defconstant-exported PROGRESS_STOP 2)

(defconstant-exported PROGRESS_QUIET 3)

(defconstant-exported CALLBACK_CHUNK_FINISHED 0)

(defconstant-exported CALLBACK_STREAM_SWITCH 1)

(defconstant-exported COPY_FILE_FAIL_IF_EXISTS 1)

(defconstant-exported COPY_FILE_RESTARTABLE 2)

(defconstant-exported OFS_MAXPATHNAME 128)

(defconstant-exported FILE_MAP_ALL_ACCESS #xf001f)

(defconstant-exported FILE_MAP_READ 4)

(defconstant-exported FILE_MAP_WRITE 2)

(defconstant-exported FILE_MAP_COPY 1)

(defconstant-exported MUTEX_ALL_ACCESS #x1f0001)

(defconstant-exported MUTEX_MODIFY_STATE 1)

(defconstant-exported SEMAPHORE_ALL_ACCESS #x1f0003)

(defconstant-exported SEMAPHORE_MODIFY_STATE 2)

(defconstant-exported EVENT_ALL_ACCESS #x1f0003)

(defconstant-exported EVENT_MODIFY_STATE 2)

(defconstant-exported PIPE_ACCESS_DUPLEX 3)

(defconstant-exported PIPE_ACCESS_INBOUND 1)

(defconstant-exported PIPE_ACCESS_OUTBOUND 2)

(defconstant-exported PIPE_TYPE_BYTE 0)

(defconstant-exported PIPE_TYPE_MESSAGE 4)

(defconstant-exported PIPE_READMODE_BYTE 0)

(defconstant-exported PIPE_READMODE_MESSAGE 2)

(defconstant-exported PIPE_WAIT 0)

(defconstant-exported PIPE_NOWAIT 1)

(defconstant-exported PIPE_CLIENT_END 0)

(defconstant-exported PIPE_SERVER_END 1)

(defconstant-exported PIPE_UNLIMITED_INSTANCES 255)

(defconstant-exported DEBUG_PROCESS #x00000001)

(defconstant-exported DEBUG_ONLY_THIS_PROCESS #x00000002)

(defconstant-exported CREATE_SUSPENDED #x00000004)

(defconstant-exported DETACHED_PROCESS #x00000008)

(defconstant-exported CREATE_NEW_CONSOLE #x00000010)

(defconstant-exported NORMAL_PRIORITY_CLASS #x00000020)

(defconstant-exported IDLE_PRIORITY_CLASS #x00000040)

(defconstant-exported HIGH_PRIORITY_CLASS #x00000080)

(defconstant-exported REALTIME_PRIORITY_CLASS #x00000100)

(defconstant-exported CREATE_NEW_PROCESS_GROUP #x00000200)

(defconstant-exported CREATE_UNICODE_ENVIRONMENT #x00000400)

(defconstant-exported CREATE_SEPARATE_WOW_VDM #x00000800)

(defconstant-exported CREATE_SHARED_WOW_VDM #x00001000)

(defconstant-exported CREATE_FORCEDOS #x00002000)

(defconstant-exported BELOW_NORMAL_PRIORITY_CLASS #x00004000)

(defconstant-exported ABOVE_NORMAL_PRIORITY_CLASS #x00008000)

(defconstant-exported CREATE_BREAKAWAY_FROM_JOB #x01000000)

(defconstant-exported CREATE_WITH_USERPROFILE #x02000000)

(defconstant-exported CREATE_DEFAULT_ERROR_MODE #x04000000)

(defconstant-exported CREATE_NO_WINDOW #x08000000)

(defconstant-exported PROFILE_USER #x10000000)

(defconstant-exported PROFILE_KERNEL #x20000000)

(defconstant-exported PROFILE_SERVER #x40000000)

(defconstant-exported CONSOLE_TEXTMODE_BUFFER 1)

(defconstant-exported CREATE_NEW 1)

(defconstant-exported CREATE_ALWAYS 2)

(defconstant-exported OPEN_EXISTING 3)

(defconstant-exported OPEN_ALWAYS 4)

(defconstant-exported TRUNCATE_EXISTING 5)

(defconstant-exported FILE_FLAG_WRITE_THROUGH #x80000000)

(defconstant-exported FILE_FLAG_OVERLAPPED 1073741824)

(defconstant-exported FILE_FLAG_NO_BUFFERING 536870912)

(defconstant-exported FILE_FLAG_RANDOM_ACCESS 268435456)

(defconstant-exported FILE_FLAG_SEQUENTIAL_SCAN 134217728)

(defconstant-exported FILE_FLAG_DELETE_ON_CLOSE 67108864)

(defconstant-exported FILE_FLAG_BACKUP_SEMANTICS 33554432)

(defconstant-exported FILE_FLAG_POSIX_SEMANTICS 16777216)

(defconstant-exported FILE_FLAG_OPEN_REPARSE_POINT 2097152)

(defconstant-exported FILE_FLAG_OPEN_NO_RECALL 1048576)

(defconstant-exported CLRDTR 6)

(defconstant-exported CLRRTS 4)

(defconstant-exported SETDTR 5)

(defconstant-exported SETRTS 3)

(defconstant-exported SETXOFF 1)

(defconstant-exported SETXON 2)

(defconstant-exported SETBREAK 8)

(defconstant-exported CLRBREAK 9)

(defconstant-exported STILL_ACTIVE #x103)

(defconstant-exported FIND_FIRST_EX_CASE_SENSITIVE 1)

(defconstant-exported SCS_32BIT_BINARY 0)

(defconstant-exported SCS_DOS_BINARY 1)

(defconstant-exported SCS_OS216_BINARY 5)

(defconstant-exported SCS_PIF_BINARY 3)

(defconstant-exported SCS_POSIX_BINARY 4)

(defconstant-exported SCS_WOW_BINARY 2)

(defconstant-exported MAX_COMPUTERNAME_LENGTH 15)

(defconstant-exported HW_PROFILE_GUIDLEN 39)

(defconstant-exported MAX_PROFILE_LEN 80)

(defconstant-exported DOCKINFO_UNDOCKED 1)

(defconstant-exported DOCKINFO_DOCKED 2)

(defconstant-exported DOCKINFO_USER_SUPPLIED 4)

(defconstant-exported DOCKINFO_USER_UNDOCKED (cl:logior 4 1))

(defconstant-exported DOCKINFO_USER_DOCKED (cl:logior 4 2))

(defconstant-exported DRIVE_REMOVABLE 2)

(defconstant-exported DRIVE_FIXED 3)

(defconstant-exported DRIVE_REMOTE 4)

(defconstant-exported DRIVE_CDROM 5)

(defconstant-exported DRIVE_RAMDISK 6)

(defconstant-exported DRIVE_UNKNOWN 0)

(defconstant-exported DRIVE_NO_ROOT_DIR 1)

(defconstant-exported FILE_TYPE_UNKNOWN 0)

(defconstant-exported FILE_TYPE_DISK 1)

(defconstant-exported FILE_TYPE_CHAR 2)

(defconstant-exported FILE_TYPE_PIPE 3)

(defconstant-exported FILE_TYPE_REMOTE #x8000)

(defconstant-exported HANDLE_FLAG_INHERIT #x01)

(defconstant-exported HANDLE_FLAG_PROTECT_FROM_CLOSE #x02)

(defconstant-exported GET_TAPE_MEDIA_INFORMATION 0)

(defconstant-exported GET_TAPE_DRIVE_INFORMATION 1)

(defconstant-exported SET_TAPE_MEDIA_INFORMATION 0)

(defconstant-exported SET_TAPE_DRIVE_INFORMATION 1)

(defconstant-exported THREAD_PRIORITY_ABOVE_NORMAL 1)

(defconstant-exported THREAD_PRIORITY_BELOW_NORMAL -1)

(defconstant-exported THREAD_PRIORITY_HIGHEST 2)

(defconstant-exported THREAD_PRIORITY_IDLE -15)

(defconstant-exported THREAD_PRIORITY_LOWEST -2)

(defconstant-exported THREAD_PRIORITY_NORMAL 0)

(defconstant-exported THREAD_PRIORITY_TIME_CRITICAL 15)

(defconstant-exported THREAD_PRIORITY_ERROR_RETURN 2147483647)

(defconstant-exported TIME_ZONE_ID_UNKNOWN 0)

(defconstant-exported TIME_ZONE_ID_STANDARD 1)

(defconstant-exported TIME_ZONE_ID_DAYLIGHT 2)

(defconstant-exported TIME_ZONE_ID_INVALID #xFFFFFFFF)

(defconstant-exported FS_CASE_IS_PRESERVED 2)

(defconstant-exported FS_CASE_SENSITIVE 1)

(defconstant-exported FS_UNICODE_STORED_ON_DISK 4)

(defconstant-exported FS_PERSISTENT_ACLS 8)

(defconstant-exported FS_FILE_COMPRESSION 16)

(defconstant-exported FS_VOL_IS_COMPRESSED 32768)

(defconstant-exported GMEM_FIXED 0)

(defconstant-exported GMEM_MOVEABLE 2)

(defconstant-exported GMEM_MODIFY 128)

(defconstant-exported GPTR 64)

(defconstant-exported GHND 66)

(defconstant-exported GMEM_DDESHARE 8192)

(defconstant-exported GMEM_DISCARDABLE 256)

(defconstant-exported GMEM_LOWER 4096)

(defconstant-exported GMEM_NOCOMPACT 16)

(defconstant-exported GMEM_NODISCARD 32)

(defconstant-exported GMEM_NOT_BANKED 4096)

(defconstant-exported GMEM_NOTIFY 16384)

(defconstant-exported GMEM_SHARE 8192)

(defconstant-exported GMEM_ZEROINIT 64)

(defconstant-exported GMEM_DISCARDED 16384)

(defconstant-exported GMEM_INVALID_HANDLE 32768)

(defconstant-exported GMEM_LOCKCOUNT 255)

(defconstant-exported GMEM_VALID_FLAGS 32626)

(defconstant-exported STATUS_WAIT_0 0)

(defconstant-exported STATUS_ABANDONED_WAIT_0 #x80)

(defconstant-exported STATUS_USER_APC #xC0)

(defconstant-exported STATUS_TIMEOUT #x102)

(defconstant-exported STATUS_PENDING #x103)

(defconstant-exported STATUS_SEGMENT_NOTIFICATION #x40000005)

(defconstant-exported STATUS_GUARD_PAGE_VIOLATION #x80000001)

(defconstant-exported STATUS_DATATYPE_MISALIGNMENT #x80000002)

(defconstant-exported STATUS_BREAKPOINT #x80000003)

(defconstant-exported STATUS_SINGLE_STEP #x80000004)

(defconstant-exported STATUS_ACCESS_VIOLATION #xC0000005)

(defconstant-exported STATUS_IN_PAGE_ERROR #xC0000006)

(defconstant-exported STATUS_INVALID_HANDLE #xC0000008)

(defconstant-exported STATUS_NO_MEMORY #xC0000017)

(defconstant-exported STATUS_ILLEGAL_INSTRUCTION #xC000001D)

(defconstant-exported STATUS_NONCONTINUABLE_EXCEPTION #xC0000025)

(defconstant-exported STATUS_INVALID_DISPOSITION #xC0000026)

(defconstant-exported STATUS_ARRAY_BOUNDS_EXCEEDED #xC000008C)

(defconstant-exported STATUS_FLOAT_DENORMAL_OPERAND #xC000008D)

(defconstant-exported STATUS_FLOAT_DIVIDE_BY_ZERO #xC000008E)

(defconstant-exported STATUS_FLOAT_INEXACT_RESULT #xC000008F)

(defconstant-exported STATUS_FLOAT_INVALID_OPERATION #xC0000090)

(defconstant-exported STATUS_FLOAT_OVERFLOW #xC0000091)

(defconstant-exported STATUS_FLOAT_STACK_CHECK #xC0000092)

(defconstant-exported STATUS_FLOAT_UNDERFLOW #xC0000093)

(defconstant-exported STATUS_INTEGER_DIVIDE_BY_ZERO #xC0000094)

(defconstant-exported STATUS_INTEGER_OVERFLOW #xC0000095)

(defconstant-exported STATUS_PRIVILEGED_INSTRUCTION #xC0000096)

(defconstant-exported STATUS_STACK_OVERFLOW #xC00000FD)

(defconstant-exported STATUS_CONTROL_C_EXIT #xC000013A)

(defconstant-exported EXCEPTION_ACCESS_VIOLATION #xC0000005)

(defconstant-exported EXCEPTION_DATATYPE_MISALIGNMENT #x80000002)

(defconstant-exported EXCEPTION_BREAKPOINT #x80000003)

(defconstant-exported EXCEPTION_SINGLE_STEP #x80000004)

(defconstant-exported EXCEPTION_ARRAY_BOUNDS_EXCEEDED #xC000008C)

(defconstant-exported EXCEPTION_FLT_DENORMAL_OPERAND #xC000008D)

(defconstant-exported EXCEPTION_FLT_DIVIDE_BY_ZERO #xC000008E)

(defconstant-exported EXCEPTION_FLT_INEXACT_RESULT #xC000008F)

(defconstant-exported EXCEPTION_FLT_INVALID_OPERATION #xC0000090)

(defconstant-exported EXCEPTION_FLT_OVERFLOW #xC0000091)

(defconstant-exported EXCEPTION_FLT_STACK_CHECK #xC0000092)

(defconstant-exported EXCEPTION_FLT_UNDERFLOW #xC0000093)

(defconstant-exported EXCEPTION_INT_DIVIDE_BY_ZERO #xC0000094)

(defconstant-exported EXCEPTION_INT_OVERFLOW #xC0000095)

(defconstant-exported EXCEPTION_PRIV_INSTRUCTION #xC0000096)

(defconstant-exported EXCEPTION_IN_PAGE_ERROR #xC0000006)

(defconstant-exported EXCEPTION_ILLEGAL_INSTRUCTION #xC000001D)

(defconstant-exported EXCEPTION_NONCONTINUABLE_EXCEPTION #xC0000025)

(defconstant-exported EXCEPTION_STACK_OVERFLOW #xC00000FD)

(defconstant-exported EXCEPTION_INVALID_DISPOSITION #xC0000026)

(defconstant-exported EXCEPTION_GUARD_PAGE #x80000001)

(defconstant-exported EXCEPTION_INVALID_HANDLE #xC0000008)

(defconstant-exported CONTROL_C_EXIT #xC000013A)

(defconstant-exported PROCESS_HEAP_REGION 1)

(defconstant-exported PROCESS_HEAP_UNCOMMITTED_RANGE 2)

(defconstant-exported PROCESS_HEAP_ENTRY_BUSY 4)

(defconstant-exported PROCESS_HEAP_ENTRY_MOVEABLE 16)

(defconstant-exported PROCESS_HEAP_ENTRY_DDESHARE 32)

(defconstant-exported DONT_RESOLVE_DLL_REFERENCES 1)

(defconstant-exported LOAD_LIBRARY_AS_DATAFILE 2)

(defconstant-exported LOAD_WITH_ALTERED_SEARCH_PATH 8)

(defconstant-exported LMEM_FIXED 0)

(defconstant-exported LMEM_MOVEABLE 2)

(defconstant-exported LMEM_NONZEROLHND 2)

(defconstant-exported LMEM_NONZEROLPTR 0)

(defconstant-exported LMEM_DISCARDABLE 3840)

(defconstant-exported LMEM_NOCOMPACT 16)

(defconstant-exported LMEM_NODISCARD 32)

(defconstant-exported LMEM_ZEROINIT 64)

(defconstant-exported LMEM_DISCARDED 16384)

(defconstant-exported LMEM_MODIFY 128)

(defconstant-exported LMEM_INVALID_HANDLE 32768)

(defconstant-exported LMEM_LOCKCOUNT 255)

(defconstant-exported LPTR 64)

(defconstant-exported LHND 66)

(defconstant-exported NONZEROLHND 2)

(defconstant-exported NONZEROLPTR 0)

(defconstant-exported LOCKFILE_FAIL_IMMEDIATELY 1)

(defconstant-exported LOCKFILE_EXCLUSIVE_LOCK 2)

(defconstant-exported LOGON32_PROVIDER_DEFAULT 0)

(defconstant-exported LOGON32_PROVIDER_WINNT35 1)

(defconstant-exported LOGON32_LOGON_INTERACTIVE 2)

(defconstant-exported LOGON32_LOGON_BATCH 4)

(defconstant-exported LOGON32_LOGON_SERVICE 5)

(defconstant-exported MOVEFILE_REPLACE_EXISTING 1)

(defconstant-exported MOVEFILE_COPY_ALLOWED 2)

(defconstant-exported MOVEFILE_DELAY_UNTIL_REBOOT 4)

(defconstant-exported MOVEFILE_WRITE_THROUGH 8)

(defconstant-exported MAXIMUM_WAIT_OBJECTS 64)

(defconstant-exported MAXIMUM_SUSPEND_COUNT #x7F)

(defconstant-exported WAIT_OBJECT_0 0)

(defconstant-exported WAIT_ABANDONED_0 128)

(defconstant-exported WAIT_IO_COMPLETION #xC0)

(defconstant-exported WAIT_ABANDONED 128)

(defconstant-exported PURGE_TXABORT 1)

(defconstant-exported PURGE_RXABORT 2)

(defconstant-exported PURGE_TXCLEAR 4)

(defconstant-exported PURGE_RXCLEAR 8)

(defconstant-exported EVENTLOG_SUCCESS 0)

(defconstant-exported EVENTLOG_FORWARDS_READ 4)

(defconstant-exported EVENTLOG_BACKWARDS_READ 8)

(defconstant-exported EVENTLOG_SEEK_READ 2)

(defconstant-exported EVENTLOG_SEQUENTIAL_READ 1)

(defconstant-exported EVENTLOG_ERROR_TYPE 1)

(defconstant-exported EVENTLOG_WARNING_TYPE 2)

(defconstant-exported EVENTLOG_INFORMATION_TYPE 4)

(defconstant-exported EVENTLOG_AUDIT_SUCCESS 8)

(defconstant-exported EVENTLOG_AUDIT_FAILURE 16)

(defconstant-exported FORMAT_MESSAGE_ALLOCATE_BUFFER 256)

(defconstant-exported FORMAT_MESSAGE_IGNORE_INSERTS 512)

(defconstant-exported FORMAT_MESSAGE_FROM_STRING 1024)

(defconstant-exported FORMAT_MESSAGE_FROM_HMODULE 2048)

(defconstant-exported FORMAT_MESSAGE_FROM_SYSTEM 4096)

(defconstant-exported FORMAT_MESSAGE_ARGUMENT_ARRAY 8192)

(defconstant-exported FORMAT_MESSAGE_MAX_WIDTH_MASK 255)

(defconstant-exported EV_BREAK 64)

(defconstant-exported EV_CTS 8)

(defconstant-exported EV_DSR 16)

(defconstant-exported EV_ERR 128)

(defconstant-exported EV_EVENT1 2048)

(defconstant-exported EV_EVENT2 4096)

(defconstant-exported EV_PERR 512)

(defconstant-exported EV_RING 256)

(defconstant-exported EV_RLSD 32)

(defconstant-exported EV_RX80FULL 1024)

(defconstant-exported EV_RXCHAR 1)

(defconstant-exported EV_RXFLAG 2)

(defconstant-exported EV_TXEMPTY 4)

(defconstant-exported SEM_FAILCRITICALERRORS #x0001)

(defconstant-exported SEM_NOGPFAULTERRORBOX #x0002)

(defconstant-exported SEM_NOALIGNMENTFAULTEXCEPT #x0004)

(defconstant-exported SEM_NOOPENFILEERRORBOX #x8000)

(defconstant-exported SLE_ERROR 1)

(defconstant-exported SLE_MINORERROR 2)

(defconstant-exported SLE_WARNING 3)

(defconstant-exported SHUTDOWN_NORETRY 1)

(defconstant-exported EXCEPTION_EXECUTE_HANDLER 1)

(defconstant-exported EXCEPTION_CONTINUE_EXECUTION -1)

(defconstant-exported EXCEPTION_CONTINUE_SEARCH 0)

(defconstant-exported MAXINTATOM #xC000)

(defconstant-exported IGNORE 0)

(defconstant-exported INFINITE #xFFFFFFFF)

(defconstant-exported NOPARITY 0)

(defconstant-exported ODDPARITY 1)

(defconstant-exported EVENPARITY 2)

(defconstant-exported MARKPARITY 3)

(defconstant-exported SPACEPARITY 4)

(defconstant-exported ONESTOPBIT 0)

(defconstant-exported ONE5STOPBITS 1)

(defconstant-exported TWOSTOPBITS 2)

(defconstant-exported CBR_110 110)

(defconstant-exported CBR_300 300)

(defconstant-exported CBR_600 600)

(defconstant-exported CBR_1200 1200)

(defconstant-exported CBR_2400 2400)

(defconstant-exported CBR_4800 4800)

(defconstant-exported CBR_9600 9600)

(defconstant-exported CBR_14400 14400)

(defconstant-exported CBR_19200 19200)

(defconstant-exported CBR_38400 38400)

(defconstant-exported CBR_56000 56000)

(defconstant-exported CBR_57600 57600)

(defconstant-exported CBR_115200 115200)

(defconstant-exported CBR_128000 128000)

(defconstant-exported CBR_256000 256000)

(defconstant-exported BACKUP_INVALID 0)

(defconstant-exported BACKUP_DATA 1)

(defconstant-exported BACKUP_EA_DATA 2)

(defconstant-exported BACKUP_SECURITY_DATA 3)

(defconstant-exported BACKUP_ALTERNATE_DATA 4)

(defconstant-exported BACKUP_LINK 5)

(defconstant-exported BACKUP_PROPERTY_DATA 6)

(defconstant-exported BACKUP_OBJECT_ID 7)

(defconstant-exported BACKUP_REPARSE_DATA 8)

(defconstant-exported BACKUP_SPARSE_BLOCK 9)

(defconstant-exported STREAM_NORMAL_ATTRIBUTE 0)

(defconstant-exported STREAM_MODIFIED_WHEN_READ 1)

(defconstant-exported STREAM_CONTAINS_SECURITY 2)

(defconstant-exported STREAM_CONTAINS_PROPERTIES 4)

(defconstant-exported STARTF_USESHOWWINDOW 1)

(defconstant-exported STARTF_USESIZE 2)

(defconstant-exported STARTF_USEPOSITION 4)

(defconstant-exported STARTF_USECOUNTCHARS 8)

(defconstant-exported STARTF_USEFILLATTRIBUTE 16)

(defconstant-exported STARTF_RUNFULLSCREEN 32)

(defconstant-exported STARTF_FORCEONFEEDBACK 64)

(defconstant-exported STARTF_FORCEOFFFEEDBACK 128)

(defconstant-exported STARTF_USESTDHANDLES 256)

(defconstant-exported STARTF_USEHOTKEY 512)

(defconstant-exported TC_NORMAL 0)

(defconstant-exported TC_HARDERR 1)

(defconstant-exported TC_GP_TRAP 2)

(defconstant-exported TC_SIGNAL 3)

(defconstant-exported AC_LINE_OFFLINE 0)

(defconstant-exported AC_LINE_ONLINE 1)

(defconstant-exported AC_LINE_BACKUP_POWER 2)

(defconstant-exported AC_LINE_UNKNOWN 255)

(defconstant-exported BATTERY_FLAG_HIGH 1)

(defconstant-exported BATTERY_FLAG_LOW 2)

(defconstant-exported BATTERY_FLAG_CRITICAL 4)

(defconstant-exported BATTERY_FLAG_CHARGING 8)

(defconstant-exported BATTERY_FLAG_NO_BATTERY 128)

(defconstant-exported BATTERY_FLAG_UNKNOWN 255)

(defconstant-exported BATTERY_PERCENTAGE_UNKNOWN 255)

(defconstant-exported BATTERY_LIFE_UNKNOWN #xFFFFFFFF)

(defconstant-exported DDD_RAW_TARGET_PATH 1)

(defconstant-exported DDD_REMOVE_DEFINITION 2)

(defconstant-exported DDD_EXACT_MATCH_ON_REMOVE 4)

(defconstant-exported HINSTANCE_ERROR 32)

(defconstant-exported MS_CTS_ON 16)

(defconstant-exported MS_DSR_ON 32)

(defconstant-exported MS_RING_ON 64)

(defconstant-exported MS_RLSD_ON 128)

(defconstant-exported DTR_CONTROL_DISABLE 0)

(defconstant-exported DTR_CONTROL_ENABLE 1)

(defconstant-exported DTR_CONTROL_HANDSHAKE 2)

(defconstant-exported RTS_CONTROL_DISABLE 0)

(defconstant-exported RTS_CONTROL_ENABLE 1)

(defconstant-exported RTS_CONTROL_HANDSHAKE 2)

(defconstant-exported RTS_CONTROL_TOGGLE 3)

(defconstant-exported SECURITY_CONTEXT_TRACKING #x40000)

(defconstant-exported SECURITY_EFFECTIVE_ONLY #x80000)

(defconstant-exported SECURITY_SQOS_PRESENT #x100000)

(defconstant-exported SECURITY_VALID_SQOS_FLAGS #x1F0000)

(defconstant-exported INVALID_FILE_SIZE #xFFFFFFFF)

(defconstant-exported WRITE_WATCH_FLAG_RESET 1)

(defcstructex-exported FILETIME
        (dwLowDateTime :unsigned-long)
        (dwHighDateTime :unsigned-long))







(defcstructex-exported BY_HANDLE_FILE_INFORMATION
        (dwFileAttributes :unsigned-long)
        (ftCreationTime FILETIME)
        (ftLastAccessTime FILETIME)
        (ftLastWriteTime FILETIME)
        (dwVolumeSerialNumber :unsigned-long)
        (nFileSizeHigh :unsigned-long)
        (nFileSizeLow :unsigned-long)
        (nNumberOfLinks :unsigned-long)
        (nFileIndexHigh :unsigned-long)
        (nFileIndexLow :unsigned-long))





(defcstructex-exported DCB
        (DCBlength :unsigned-long)
        (BaudRate :unsigned-long)
        (fBinary :unsigned-long)
        (fParity :unsigned-long)
        (fOutxCtsFlow :unsigned-long)
        (fOutxDsrFlow :unsigned-long)
        (fDtrControl :unsigned-long)
        (fDsrSensitivity :unsigned-long)
        (fTXContinueOnXoff :unsigned-long)
        (fOutX :unsigned-long)
        (fInX :unsigned-long)
        (fErrorChar :unsigned-long)
        (fNull :unsigned-long)
        (fRtsControl :unsigned-long)
        (fAbortOnError :unsigned-long)
        (fDummy2 :unsigned-long)
        (wReserved :unsigned-short)
        (XonLim :unsigned-short)
        (XoffLim :unsigned-short)
        (ByteSize :unsigned-char)
        (Parity :unsigned-char)
        (StopBits :unsigned-char)
        (XonChar :char)
        (XoffChar :char)
        (ErrorChar :char)
        (EofChar :char)
        (EvtChar :char)
        (wReserved1 :unsigned-short))





(defcstructex-exported COMMCONFIG
        (dwSize :unsigned-long)
        (wVersion :unsigned-short)
        (wReserved :unsigned-short)
        (dcb DCB)
        (dwProviderSubType :unsigned-long)
        (dwProviderOffset :unsigned-long)
        (dwProviderSize :unsigned-long)
        (wcProviderData :pointer))





(defcstructex-exported COMMPROP
        (wPacketLength :unsigned-short)
        (wPacketVersion :unsigned-short)
        (dwServiceMask :unsigned-long)
        (dwReserved1 :unsigned-long)
        (dwMaxTxQueue :unsigned-long)
        (dwMaxRxQueue :unsigned-long)
        (dwMaxBaud :unsigned-long)
        (dwProvSubType :unsigned-long)
        (dwProvCapabilities :unsigned-long)
        (dwSettableParams :unsigned-long)
        (dwSettableBaud :unsigned-long)
        (wSettableData :unsigned-short)
        (wSettableStopParity :unsigned-short)
        (dwCurrentTxQueue :unsigned-long)
        (dwCurrentRxQueue :unsigned-long)
        (dwProvSpec1 :unsigned-long)
        (dwProvSpec2 :unsigned-long)
        (wcProvChar :pointer))





(defcstructex-exported COMMTIMEOUTS
        (ReadIntervalTimeout :unsigned-long)
        (ReadTotalTimeoutMultiplier :unsigned-long)
        (ReadTotalTimeoutConstant :unsigned-long)
        (WriteTotalTimeoutMultiplier :unsigned-long)
        (WriteTotalTimeoutConstant :unsigned-long))





(defcstructex-exported COMSTAT
        (fCtsHold :unsigned-long)
        (fDsrHold :unsigned-long)
        (fRlsdHold :unsigned-long)
        (fXoffHold :unsigned-long)
        (fXoffSent :unsigned-long)
        (fEof :unsigned-long)
        (fTxim :unsigned-long)
        (fReserved :unsigned-long)
        (cbInQue :unsigned-long)
        (cbOutQue :unsigned-long))







(defcstructex-exported CREATE_PROCESS_DEBUG_INFO
        (hFile :pointer)
        (hProcess :pointer)
        (hThread :pointer)
        (lpBaseOfImage :pointer)
        (dwDebugInfoFileOffset :unsigned-long)
        (nDebugInfoSize :unsigned-long)
        (lpThreadLocalBase :pointer)
        (lpStartAddress :pointer)
        (lpImageName :pointer)
        (fUnicode :unsigned-short))





(defcstructex-exported CREATE_THREAD_DEBUG_INFO
        (hThread :pointer)
        (lpThreadLocalBase :pointer)
        (lpStartAddress :pointer))





(defcstructex-exported EXCEPTION_DEBUG_INFO
        (ExceptionRecord EXCEPTION_RECORD)
        (dwFirstChance :unsigned-long))





(defcstructex-exported EXIT_THREAD_DEBUG_INFO
        (dwExitCode :unsigned-long))





(defcstructex-exported EXIT_PROCESS_DEBUG_INFO
        (dwExitCode :unsigned-long))





(defcstructex-exported LOAD_DLL_DEBUG_INFO
        (hFile :pointer)
        (lpBaseOfDll :pointer)
        (dwDebugInfoFileOffset :unsigned-long)
        (nDebugInfoSize :unsigned-long)
        (lpImageName :pointer)
        (fUnicode :unsigned-short))





(defcstructex-exported UNLOAD_DLL_DEBUG_INFO
        (lpBaseOfDll :pointer))





(defcstructex-exported OUTPUT_DEBUG_STRING_INFO
        (lpDebugStringData :string)
        (fUnicode :unsigned-short)
        (nDebugStringLength :unsigned-short))





(defcstructex-exported RIP_INFO
        (dwError :unsigned-long)
        (dwType :unsigned-long))





(defcstructex-exported DEBUG_EVENT
        (dwDebugEventCode :unsigned-long)
        (dwProcessId :unsigned-long)
        (dwThreadId :unsigned-long)
        (u :pointer))





(cffi:defcunion DEBUG_EVENT_u
        (Exception EXCEPTION_DEBUG_INFO)
        (CreateThread CREATE_THREAD_DEBUG_INFO)
        (CreateProcessInfo CREATE_PROCESS_DEBUG_INFO)
        (ExitThread EXIT_THREAD_DEBUG_INFO)
        (ExitProcess EXIT_PROCESS_DEBUG_INFO)
        (LoadDll LOAD_DLL_DEBUG_INFO)
        (UnloadDll UNLOAD_DLL_DEBUG_INFO)
        (DebugString OUTPUT_DEBUG_STRING_INFO)
        (RipInfo RIP_INFO))

(defcstructex-exported OVERLAPPED
        (Internal :unsigned-long)
        (InternalHigh :unsigned-long)
        (Offset :unsigned-long)
        (OffsetHigh :unsigned-long)
        (hEvent :pointer))







(defcstructex-exported STARTUPINFOA
        (cb :unsigned-long)
        (lpReserved :string)
        (lpDesktop :string)
        (lpTitle :string)
        (dwX :unsigned-long)
        (dwY :unsigned-long)
        (dwXSize :unsigned-long)
        (dwYSize :unsigned-long)
        (dwXCountChars :unsigned-long)
        (dwYCountChars :unsigned-long)
        (dwFillAttribute :unsigned-long)
        (dwFlags :unsigned-long)
        (wShowWindow :unsigned-short)
        (cbReserved2 :unsigned-short)
        (lpReserved2 :pointer)
        (hStdInput :pointer)
        (hStdOutput :pointer)
        (hStdError :pointer))





(defcstructex-exported STARTUPINFOW
        (cb :unsigned-long)
        (lpReserved :pointer)
        (lpDesktop :pointer)
        (lpTitle :pointer)
        (dwX :unsigned-long)
        (dwY :unsigned-long)
        (dwXSize :unsigned-long)
        (dwYSize :unsigned-long)
        (dwXCountChars :unsigned-long)
        (dwYCountChars :unsigned-long)
        (dwFillAttribute :unsigned-long)
        (dwFlags :unsigned-long)
        (wShowWindow :unsigned-short)
        (cbReserved2 :unsigned-short)
        (lpReserved2 :pointer)
        (hStdInput :pointer)
        (hStdOutput :pointer)
        (hStdError :pointer))





(defcstructex-exported PROCESS_INFORMATION
        (hProcess :pointer)
        (hThread :pointer)
        (dwProcessId :unsigned-long)
        (dwThreadId :unsigned-long))







(defcstructex-exported CRITICAL_SECTION_DEBUG
        (Type :unsigned-short)
        (CreatorBackTraceIndex :unsigned-short)
        (CriticalSection :pointer)
        (ProcessLocksList LIST_ENTRY)
        (EntryCount :unsigned-long)
        (ContentionCount :unsigned-long)
        (Spare :pointer))





(defcstructex-exported CRITICAL_SECTION
        (DebugInfo :pointer)
        (LockCount :int32)
        (RecursionCount :int32)
        (OwningThread :pointer)
        (LockSemaphore :pointer)
        (SpinCount :unsigned-long))







(defcstructex-exported SYSTEMTIME
        (wYear :unsigned-short)
        (wMonth :unsigned-short)
        (wDayOfWeek :unsigned-short)
        (wDay :unsigned-short)
        (wHour :unsigned-short)
        (wMinute :unsigned-short)
        (wSecond :unsigned-short)
        (wMilliseconds :unsigned-short))





(defcstructex-exported WIN32_FILE_ATTRIBUTE_DATA
        (dwFileAttributes :unsigned-long)
        (ftCreationTime FILETIME)
        (ftLastAccessTime FILETIME)
        (ftLastWriteTime FILETIME)
        (nFileSizeHigh :unsigned-long)
        (nFileSizeLow :unsigned-long))





(defcstructex-exported WIN32_FIND_DATAA
        (dwFileAttributes :unsigned-long)
        (ftCreationTime FILETIME)
        (ftLastAccessTime FILETIME)
        (ftLastWriteTime FILETIME)
        (nFileSizeHigh :unsigned-long)
        (nFileSizeLow :unsigned-long)
        (dwReserved0 :unsigned-long)
        (dwReserved1 :unsigned-long)
        (cFileName :pointer)
        (cAlternateFileName :pointer))







(defcstructex-exported WIN32_FIND_DATAW
        (dwFileAttributes :unsigned-long)
        (ftCreationTime FILETIME)
        (ftLastAccessTime FILETIME)
        (ftLastWriteTime FILETIME)
        (nFileSizeHigh :unsigned-long)
        (nFileSizeLow :unsigned-long)
        (dwReserved0 :unsigned-long)
        (dwReserved1 :unsigned-long)
        (cFileName :pointer)
        (cAlternateFileName :pointer))







(defcstructex-exported WIN32_STREAM_ID
        (dwStreamId :unsigned-long)
        (dwStreamAttributes :unsigned-long)
        (Size LARGE_INTEGER)
        (dwStreamNameSize :unsigned-long)
        (cStreamName :pointer))





(cffi:defcenum FINDEX_INFO_LEVELS
        :FindExInfoStandard
        :FindExInfoMaxInfoLevel)

(cffi:defcenum FINDEX_SEARCH_OPS
        :FindExSearchNameMatch
        :FindExSearchLimitToDirectories
        :FindExSearchLimitToDevices
        :FindExSearchMaxSearchOp)

(cffi:defcenum ACL_INFORMATION_CLASS
        (:AclRevisionInformation #.1)
        :AclSizeInformation)

(defcstructex-exported HW_PROFILE_INFOA
        (dwDockInfo :unsigned-long)
        (szHwProfileGuid :pointer)
        (szHwProfileName :pointer))





(defcstructex-exported HW_PROFILE_INFOW
        (dwDockInfo :unsigned-long)
        (szHwProfileGuid :pointer)
        (szHwProfileName :pointer))





(cffi:defcenum GET_FILEEX_INFO_LEVELS
        :GetFileExInfoStandard
        :GetFileExMaxInfoLevel)

(defcstructex-exported SYSTEM_INFO
        (dwPageSize :unsigned-long)
        (lpMinimumApplicationAddress :pointer)
        (lpMaximumApplicationAddress :pointer)
        (dwActiveProcessorMask :unsigned-long)
        (dwNumberOfProcessors :unsigned-long)
        (dwProcessorType :unsigned-long)
        (dwAllocationGranularity :unsigned-long)
        (wProcessorLevel :unsigned-short)
        (wProcessorRevision :unsigned-short)
        (u :pointer))





(cffi:defcunion SYSTEM_INFO_u
        (dwOemId :unsigned-long)
        (s :pointer))

(defcstructex-exported SYSTEM_INFO_u_s
        (wProcessorArchitecture :unsigned-short)
        (wReserved :unsigned-short))

(defcstructex-exported SYSTEM_POWER_STATUS
        (ACLineStatus :unsigned-char)
        (BatteryFlag :unsigned-char)
        (BatteryLifePercent :unsigned-char)
        (Reserved1 :unsigned-char)
        (BatteryLifeTime :unsigned-long)
        (BatteryFullLifeTime :unsigned-long))





(defcstructex-exported TIME_ZONE_INFORMATION
        (Bias :int32)
        (StandardName :pointer)
        (StandardDate SYSTEMTIME)
        (StandardBias :int32)
        (DaylightName :pointer)
        (DaylightDate SYSTEMTIME)
        (DaylightBias :int32))





(defcstructex-exported MEMORYSTATUS
        (dwLength :unsigned-long)
        (dwMemoryLoad :unsigned-long)
        (dwTotalPhys :unsigned-long)
        (dwAvailPhys :unsigned-long)
        (dwTotalPageFile :unsigned-long)
        (dwAvailPageFile :unsigned-long)
        (dwTotalVirtual :unsigned-long)
        (dwAvailVirtual :unsigned-long))





(defcstructex-exported LDT_ENTRY
        (LimitLow :unsigned-short)
        (BaseLow :unsigned-short)
        (HighWord :pointer))







(cffi:defcunion LDT_ENTRY_HighWord
        (Bytes :pointer)
        (Bits :pointer))

(defcstructex-exported LDT_ENTRY_HighWord_Bits
        (BaseMid :unsigned-long)
        (Type :unsigned-long)
        (Dpl :unsigned-long)
        (Pres :unsigned-long)
        (LimitHi :unsigned-long)
        (Sys :unsigned-long)
        (Reserved_0 :unsigned-long)
        (Default_Big :unsigned-long)
        (Granularity :unsigned-long)
        (BaseHi :unsigned-long))

(defcstructex-exported LDT_ENTRY_HighWord_Bytes
        (BaseMid :unsigned-char)
        (Flags1 :unsigned-char)
        (Flags2 :unsigned-char)
        (BaseHi :unsigned-char))

(defcstructex-exported PROCESS_HEAP_ENTRY
        (lpData :pointer)
        (cbData :unsigned-long)
        (cbOverhead :unsigned-char)
        (iRegionIndex :unsigned-char)
        (wFlags :unsigned-short)
        (u :pointer))





(cffi:defcunion PROCESS_HEAP_ENTRY_u
        (Block :pointer)
        (Region :pointer))

(defcstructex-exported PROCESS_HEAP_ENTRY_u_Region
        (dwCommittedSize :unsigned-long)
        (dwUnCommittedSize :unsigned-long)
        (lpFirstBlock :pointer)
        (lpLastBlock :pointer))

(defcstructex-exported PROCESS_HEAP_ENTRY_u_Block
        (hMem :pointer)
        (dwReserved :pointer))

(defcstructex-exported OFSTRUCT
        (cBytes :unsigned-char)
        (fFixedDisk :unsigned-char)
        (nErrCode :unsigned-short)
        (Reserved1 :unsigned-short)
        (Reserved2 :unsigned-short)
        (szPathName :pointer))







(defcstructex-exported WIN_CERTIFICATE
        (dwLength :unsigned-long)
        (wRevision :unsigned-short)
        (wCertificateType :unsigned-short)
        (bCertificate :pointer))























(defcfunex-exported ("CancelDeviceWakeupRequest" CancelDeviceWakeupRequest :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ConvertFiberToThread" ConvertFiberToThread :convention :stdcall) :int)

(defcfunex-exported ("EncryptFileA" EncryptFileA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("EncryptFileW" EncryptFileW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FileEncryptionStatusA" FileEncryptionStatusA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("FileEncryptionStatusW" FileEncryptionStatusW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetProcessPriorityBoost" GetProcessPriorityBoost :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))


(defcfunex-exported ("GetWriteWatch" GetWriteWatch :convention :stdcall) :unsigned-int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("InitializeCriticalSectionAndSpinCount" InitializeCriticalSectionAndSpinCount :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetCriticalSectionSpinCount" SetCriticalSectionSpinCount :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("IsSystemResumeAutomatic" IsSystemResumeAutomatic :convention :stdcall) :int)

(defcfunex-exported ("IsTextUnicode" IsTextUnicode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))






(defcfunex-exported ("MulDiv" MulDiv :convention :stdcall) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("ReadFileScatter" ReadFileScatter :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("ResetWriteWatch" ResetWriteWatch :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetSecurityDescriptorControl" SetSecurityDescriptorControl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-short)
  (arg2 :unsigned-short))

(defcfunex-exported ("VerifyVersionInfoA" VerifyVersionInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long-long))

(defcfunex-exported ("VerifyVersionInfoW" VerifyVersionInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long-long))

(defcfunex-exported ("WinLoadTrustProvider" WinLoadTrustProvider :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WriteFileGather" WriteFileGather :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))















(defconstant-exported BI_RGB 0)

(defconstant-exported BI_RLE8 1)

(defconstant-exported BI_RLE4 2)

(defconstant-exported BI_BITFIELDS 3)

(defconstant-exported BI_JPEG 4)

(defconstant-exported BI_PNG 5)

(defconstant-exported LF_FACESIZE 32)

(defconstant-exported LF_FULLFACESIZE 64)

(defconstant-exported CA_NEGATIVE 1)

(defconstant-exported CA_LOG_FILTER 2)

(defconstant-exported ILLUMINANT_DEVICE_DEFAULT 0)

(defconstant-exported ILLUMINANT_A 1)

(defconstant-exported ILLUMINANT_B 2)

(defconstant-exported ILLUMINANT_C 3)

(defconstant-exported ILLUMINANT_D50 4)

(defconstant-exported ILLUMINANT_D55 5)

(defconstant-exported ILLUMINANT_D65 6)

(defconstant-exported ILLUMINANT_D75 7)

(defconstant-exported ILLUMINANT_F2 8)

(defconstant-exported ILLUMINANT_MAX_INDEX 8)

(defconstant-exported ILLUMINANT_TUNGSTEN 1)

(defconstant-exported ILLUMINANT_DAYLIGHT 3)

(defconstant-exported ILLUMINANT_FLUORESCENT 8)

(defconstant-exported ILLUMINANT_NTSC 3)

(defconstant-exported RGB_GAMMA_MIN 2500)

(defconstant-exported RGB_GAMMA_MAX 65000)

(defconstant-exported REFERENCE_WHITE_MIN 6000)

(defconstant-exported REFERENCE_WHITE_MAX 10000)

(defconstant-exported REFERENCE_BLACK_MIN 0)

(defconstant-exported REFERENCE_BLACK_MAX 4000)

(defconstant-exported COLOR_ADJ_MIN -100)

(defconstant-exported COLOR_ADJ_MAX 100)

(defconstant-exported CCHDEVICENAME 32)

(defconstant-exported CCHFORMNAME 32)

(defconstant-exported DI_COMPAT 4)

(defconstant-exported DI_DEFAULTSIZE 8)

(defconstant-exported DI_IMAGE 2)

(defconstant-exported DI_MASK 1)

(defconstant-exported DI_NORMAL 3)

(defconstant-exported DI_APPBANDING 1)

(defconstant-exported EMR_HEADER 1)

(defconstant-exported EMR_POLYBEZIER 2)

(defconstant-exported EMR_POLYGON 3)

(defconstant-exported EMR_POLYLINE 4)

(defconstant-exported EMR_POLYBEZIERTO 5)

(defconstant-exported EMR_POLYLINETO 6)

(defconstant-exported EMR_POLYPOLYLINE 7)

(defconstant-exported EMR_POLYPOLYGON 8)

(defconstant-exported EMR_SETWINDOWEXTEX 9)

(defconstant-exported EMR_SETWINDOWORGEX 10)

(defconstant-exported EMR_SETVIEWPORTEXTEX 11)

(defconstant-exported EMR_SETVIEWPORTORGEX 12)

(defconstant-exported EMR_SETBRUSHORGEX 13)

(defconstant-exported EMR_EOF 14)

(defconstant-exported EMR_SETPIXELV 15)

(defconstant-exported EMR_SETMAPPERFLAGS 16)

(defconstant-exported EMR_SETMAPMODE 17)

(defconstant-exported EMR_SETBKMODE 18)

(defconstant-exported EMR_SETPOLYFILLMODE 19)

(defconstant-exported EMR_SETROP2 20)

(defconstant-exported EMR_SETSTRETCHBLTMODE 21)

(defconstant-exported EMR_SETTEXTALIGN 22)

(defconstant-exported EMR_SETCOLORADJUSTMENT 23)

(defconstant-exported EMR_SETTEXTCOLOR 24)

(defconstant-exported EMR_SETBKCOLOR 25)

(defconstant-exported EMR_OFFSETCLIPRGN 26)

(defconstant-exported EMR_MOVETOEX 27)

(defconstant-exported EMR_SETMETARGN 28)

(defconstant-exported EMR_EXCLUDECLIPRECT 29)

(defconstant-exported EMR_INTERSECTCLIPRECT 30)

(defconstant-exported EMR_SCALEVIEWPORTEXTEX 31)

(defconstant-exported EMR_SCALEWINDOWEXTEX 32)

(defconstant-exported EMR_SAVEDC 33)

(defconstant-exported EMR_RESTOREDC 34)

(defconstant-exported EMR_SETWORLDTRANSFORM 35)

(defconstant-exported EMR_MODIFYWORLDTRANSFORM 36)

(defconstant-exported EMR_SELECTOBJECT 37)

(defconstant-exported EMR_CREATEPEN 38)

(defconstant-exported EMR_CREATEBRUSHINDIRECT 39)

(defconstant-exported EMR_DELETEOBJECT 40)

(defconstant-exported EMR_ANGLEARC 41)

(defconstant-exported EMR_ELLIPSE 42)

(defconstant-exported EMR_RECTANGLE 43)

(defconstant-exported EMR_ROUNDRECT 44)

(defconstant-exported EMR_ARC 45)

(defconstant-exported EMR_CHORD 46)

(defconstant-exported EMR_PIE 47)

(defconstant-exported EMR_SELECTPALETTE 48)

(defconstant-exported EMR_CREATEPALETTE 49)

(defconstant-exported EMR_SETPALETTEENTRIES 50)

(defconstant-exported EMR_RESIZEPALETTE 51)

(defconstant-exported EMR_REALIZEPALETTE 52)

(defconstant-exported EMR_EXTFLOODFILL 53)

(defconstant-exported EMR_LINETO 54)

(defconstant-exported EMR_ARCTO 55)

(defconstant-exported EMR_POLYDRAW 56)

(defconstant-exported EMR_SETARCDIRECTION 57)

(defconstant-exported EMR_SETMITERLIMIT 58)

(defconstant-exported EMR_BEGINPATH 59)

(defconstant-exported EMR_ENDPATH 60)

(defconstant-exported EMR_CLOSEFIGURE 61)

(defconstant-exported EMR_FILLPATH 62)

(defconstant-exported EMR_STROKEANDFILLPATH 63)

(defconstant-exported EMR_STROKEPATH 64)

(defconstant-exported EMR_FLATTENPATH 65)

(defconstant-exported EMR_WIDENPATH 66)

(defconstant-exported EMR_SELECTCLIPPATH 67)

(defconstant-exported EMR_ABORTPATH 68)

(defconstant-exported EMR_GDICOMMENT 70)

(defconstant-exported EMR_FILLRGN 71)

(defconstant-exported EMR_FRAMERGN 72)

(defconstant-exported EMR_INVERTRGN 73)

(defconstant-exported EMR_PAINTRGN 74)

(defconstant-exported EMR_EXTSELECTCLIPRGN 75)

(defconstant-exported EMR_BITBLT 76)

(defconstant-exported EMR_STRETCHBLT 77)

(defconstant-exported EMR_MASKBLT 78)

(defconstant-exported EMR_PLGBLT 79)

(defconstant-exported EMR_SETDIBITSTODEVICE 80)

(defconstant-exported EMR_STRETCHDIBITS 81)

(defconstant-exported EMR_EXTCREATEFONTINDIRECTW 82)

(defconstant-exported EMR_EXTTEXTOUTA 83)

(defconstant-exported EMR_EXTTEXTOUTW 84)

(defconstant-exported EMR_POLYBEZIER16 85)

(defconstant-exported EMR_POLYGON16 86)

(defconstant-exported EMR_POLYLINE16 87)

(defconstant-exported EMR_POLYBEZIERTO16 88)

(defconstant-exported EMR_POLYLINETO16 89)

(defconstant-exported EMR_POLYPOLYLINE16 90)

(defconstant-exported EMR_POLYPOLYGON16 91)

(defconstant-exported EMR_POLYDRAW16 92)

(defconstant-exported EMR_CREATEMONOBRUSH 93)

(defconstant-exported EMR_CREATEDIBPATTERNBRUSHPT 94)

(defconstant-exported EMR_EXTCREATEPEN 95)

(defconstant-exported EMR_POLYTEXTOUTA 96)

(defconstant-exported EMR_POLYTEXTOUTW 97)

(defconstant-exported EMR_SETICMMODE 98)

(defconstant-exported EMR_CREATECOLORSPACE 99)

(defconstant-exported EMR_SETCOLORSPACE 100)

(defconstant-exported EMR_DELETECOLORSPACE 101)

(defconstant-exported EMR_GLSRECORD 102)

(defconstant-exported EMR_GLSBOUNDEDRECORD 103)

(defconstant-exported EMR_PIXELFORMAT 104)

(defconstant-exported ENHMETA_SIGNATURE 1179469088)

(defconstant-exported EPS_SIGNATURE #x46535045)

(defconstant-exported META_SETBKCOLOR #x201)

(defconstant-exported META_SETBKMODE #x102)

(defconstant-exported META_SETMAPMODE #x103)

(defconstant-exported META_SETROP2 #x104)

(defconstant-exported META_SETRELABS #x105)

(defconstant-exported META_SETPOLYFILLMODE #x106)

(defconstant-exported META_SETSTRETCHBLTMODE #x107)

(defconstant-exported META_SETTEXTCHAREXTRA #x108)

(defconstant-exported META_SETTEXTCOLOR #x209)

(defconstant-exported META_SETTEXTJUSTIFICATION #x20A)

(defconstant-exported META_SETWINDOWORG #x20B)

(defconstant-exported META_SETWINDOWEXT #x20C)

(defconstant-exported META_SETVIEWPORTORG #x20D)

(defconstant-exported META_SETVIEWPORTEXT #x20E)

(defconstant-exported META_OFFSETWINDOWORG #x20F)

(defconstant-exported META_SCALEWINDOWEXT #x410)

(defconstant-exported META_OFFSETVIEWPORTORG #x211)

(defconstant-exported META_SCALEVIEWPORTEXT #x412)

(defconstant-exported META_LINETO #x213)

(defconstant-exported META_MOVETO #x214)

(defconstant-exported META_EXCLUDECLIPRECT #x415)

(defconstant-exported META_INTERSECTCLIPRECT #x416)

(defconstant-exported META_ARC #x817)

(defconstant-exported META_ELLIPSE #x418)

(defconstant-exported META_FLOODFILL #x419)

(defconstant-exported META_PIE #x81A)

(defconstant-exported META_RECTANGLE #x41B)

(defconstant-exported META_ROUNDRECT #x61C)

(defconstant-exported META_PATBLT #x61D)

(defconstant-exported META_SAVEDC #x1E)

(defconstant-exported META_SETPIXEL #x41F)

(defconstant-exported META_OFFSETCLIPRGN #x220)

(defconstant-exported META_TEXTOUT #x521)

(defconstant-exported META_BITBLT #x922)

(defconstant-exported META_STRETCHBLT #xB23)

(defconstant-exported META_POLYGON #x324)

(defconstant-exported META_POLYLINE #x325)

(defconstant-exported META_ESCAPE #x626)

(defconstant-exported META_RESTOREDC #x127)

(defconstant-exported META_FILLREGION #x228)

(defconstant-exported META_FRAMEREGION #x429)

(defconstant-exported META_INVERTREGION #x12A)

(defconstant-exported META_PAINTREGION #x12B)

(defconstant-exported META_SELECTCLIPREGION #x12C)

(defconstant-exported META_SELECTOBJECT #x12D)

(defconstant-exported META_SETTEXTALIGN #x12E)

(defconstant-exported META_CHORD #x830)

(defconstant-exported META_SETMAPPERFLAGS #x231)

(defconstant-exported META_EXTTEXTOUT #xa32)

(defconstant-exported META_SETDIBTODEV #xd33)

(defconstant-exported META_SELECTPALETTE #x234)

(defconstant-exported META_REALIZEPALETTE #x35)

(defconstant-exported META_ANIMATEPALETTE #x436)

(defconstant-exported META_SETPALENTRIES #x37)

(defconstant-exported META_POLYPOLYGON #x538)

(defconstant-exported META_RESIZEPALETTE #x139)

(defconstant-exported META_DIBBITBLT #x940)

(defconstant-exported META_DIBSTRETCHBLT #xb41)

(defconstant-exported META_DIBCREATEPATTERNBRUSH #x142)

(defconstant-exported META_STRETCHDIB #xf43)

(defconstant-exported META_EXTFLOODFILL #x548)

(defconstant-exported META_DELETEOBJECT #x1f0)

(defconstant-exported META_CREATEPALETTE #xf7)

(defconstant-exported META_CREATEPATTERNBRUSH #x1F9)

(defconstant-exported META_CREATEPENINDIRECT #x2FA)

(defconstant-exported META_CREATEFONTINDIRECT #x2FB)

(defconstant-exported META_CREATEBRUSHINDIRECT #x2FC)

(defconstant-exported META_CREATEREGION #x6FF)

(defconstant-exported PT_MOVETO 6)

(defconstant-exported PT_LINETO 2)

(defconstant-exported PT_BEZIERTO 4)

(defconstant-exported PT_CLOSEFIGURE 1)

(defconstant-exported ELF_VENDOR_SIZE 4)

(defconstant-exported ELF_VERSION 0)

(defconstant-exported ELF_CULTURE_LATIN 0)

(defconstant-exported PFD_TYPE_RGBA 0)

(defconstant-exported PFD_TYPE_COLORINDEX 1)

(defconstant-exported PFD_MAIN_PLANE 0)

(defconstant-exported PFD_OVERLAY_PLANE 1)

(defconstant-exported PFD_UNDERLAY_PLANE -1)

(defconstant-exported PFD_DOUBLEBUFFER 1)

(defconstant-exported PFD_STEREO 2)

(defconstant-exported PFD_DRAW_TO_WINDOW 4)

(defconstant-exported PFD_DRAW_TO_BITMAP 8)

(defconstant-exported PFD_SUPPORT_GDI 16)

(defconstant-exported PFD_SUPPORT_OPENGL 32)

(defconstant-exported PFD_GENERIC_FORMAT 64)

(defconstant-exported PFD_NEED_PALETTE 128)

(defconstant-exported PFD_NEED_SYSTEM_PALETTE #x00000100)

(defconstant-exported PFD_SWAP_EXCHANGE #x00000200)

(defconstant-exported PFD_SWAP_COPY #x00000400)

(defconstant-exported PFD_SWAP_LAYER_BUFFERS #x00000800)

(defconstant-exported PFD_GENERIC_ACCELERATED #x00001000)

(defconstant-exported PFD_DEPTH_DONTCARE #x20000000)

(defconstant-exported PFD_DOUBLEBUFFER_DONTCARE #x40000000)

(defconstant-exported PFD_STEREO_DONTCARE #x80000000)

(defconstant-exported SP_ERROR -1)

(defconstant-exported SP_OUTOFDISK -4)

(defconstant-exported SP_OUTOFMEMORY -5)

(defconstant-exported SP_USERABORT -3)

(defconstant-exported SP_APPABORT -2)

(defconstant-exported BLACKNESS #x42)

(defconstant-exported NOTSRCERASE #x1100A6)

(defconstant-exported NOTSRCCOPY #x330008)

(defconstant-exported SRCERASE #x440328)

(defconstant-exported DSTINVERT #x550009)

(defconstant-exported PATINVERT #x5A0049)

(defconstant-exported SRCINVERT #x660046)

(defconstant-exported SRCAND #x8800C6)

(defconstant-exported MERGEPAINT #xBB0226)

(defconstant-exported MERGECOPY #xC000CA)

(defconstant-exported SRCCOPY #xCC0020)

(defconstant-exported SRCPAINT #xEE0086)

(defconstant-exported PATCOPY #xF00021)

(defconstant-exported PATPAINT #xFB0A09)

(defconstant-exported WHITENESS #xFF0062)

(defconstant-exported R2_BLACK 1)

(defconstant-exported R2_COPYPEN 13)

(defconstant-exported R2_MASKNOTPEN 3)

(defconstant-exported R2_MASKPEN 9)

(defconstant-exported R2_MASKPENNOT 5)

(defconstant-exported R2_MERGENOTPEN 12)

(defconstant-exported R2_MERGEPEN 15)

(defconstant-exported R2_MERGEPENNOT 14)

(defconstant-exported R2_NOP 11)

(defconstant-exported R2_NOT 6)

(defconstant-exported R2_NOTCOPYPEN 4)

(defconstant-exported R2_NOTMASKPEN 8)

(defconstant-exported R2_NOTMERGEPEN 2)

(defconstant-exported R2_NOTXORPEN 10)

(defconstant-exported R2_WHITE 16)

(defconstant-exported R2_XORPEN 7)

(defconstant-exported CM_OUT_OF_GAMUT 255)

(defconstant-exported CM_IN_GAMUT 0)

(defconstant-exported RGN_AND 1)

(defconstant-exported RGN_COPY 5)

(defconstant-exported RGN_DIFF 4)

(defconstant-exported RGN_OR 2)

(defconstant-exported RGN_XOR 3)

(defconstant-exported NULLREGION 1)

(defconstant-exported SIMPLEREGION 2)

(defconstant-exported COMPLEXREGION 3)

(defconstant-exported ERROR 0)

(defconstant-exported CBM_INIT 4)

(defconstant-exported DIB_PAL_COLORS 1)

(defconstant-exported DIB_RGB_COLORS 0)

(defconstant-exported FW_DONTCARE 0)

(defconstant-exported FW_THIN 100)

(defconstant-exported FW_EXTRALIGHT 200)

(defconstant-exported FW_ULTRALIGHT 200)

(defconstant-exported FW_LIGHT 300)

(defconstant-exported FW_NORMAL 400)

(defconstant-exported FW_REGULAR 400)

(defconstant-exported FW_MEDIUM 500)

(defconstant-exported FW_SEMIBOLD 600)

(defconstant-exported FW_DEMIBOLD 600)

(defconstant-exported FW_BOLD 700)

(defconstant-exported FW_EXTRABOLD 800)

(defconstant-exported FW_ULTRABOLD 800)

(defconstant-exported FW_HEAVY 900)

(defconstant-exported FW_BLACK 900)

(defconstant-exported ANSI_CHARSET 0)

(defconstant-exported DEFAULT_CHARSET 1)

(defconstant-exported SYMBOL_CHARSET 2)

(defconstant-exported SHIFTJIS_CHARSET 128)

(defconstant-exported HANGEUL_CHARSET 129)

(defconstant-exported HANGUL_CHARSET 129)

(defconstant-exported GB2312_CHARSET 134)

(defconstant-exported CHINESEBIG5_CHARSET 136)

(defconstant-exported GREEK_CHARSET 161)

(defconstant-exported TURKISH_CHARSET 162)

(defconstant-exported HEBREW_CHARSET 177)

(defconstant-exported ARABIC_CHARSET 178)

(defconstant-exported BALTIC_CHARSET 186)

(defconstant-exported RUSSIAN_CHARSET 204)

(defconstant-exported THAI_CHARSET 222)

(defconstant-exported EASTEUROPE_CHARSET 238)

(defconstant-exported OEM_CHARSET 255)

(defconstant-exported JOHAB_CHARSET 130)

(defconstant-exported VIETNAMESE_CHARSET 163)

(defconstant-exported MAC_CHARSET 77)

(defconstant-exported OUT_DEFAULT_PRECIS 0)

(defconstant-exported OUT_STRING_PRECIS 1)

(defconstant-exported OUT_CHARACTER_PRECIS 2)

(defconstant-exported OUT_STROKE_PRECIS 3)

(defconstant-exported OUT_TT_PRECIS 4)

(defconstant-exported OUT_DEVICE_PRECIS 5)

(defconstant-exported OUT_RASTER_PRECIS 6)

(defconstant-exported OUT_TT_ONLY_PRECIS 7)

(defconstant-exported OUT_OUTLINE_PRECIS 8)

(defconstant-exported CLIP_DEFAULT_PRECIS 0)

(defconstant-exported CLIP_CHARACTER_PRECIS 1)

(defconstant-exported CLIP_STROKE_PRECIS 2)

(defconstant-exported CLIP_MASK 15)

(defconstant-exported CLIP_LH_ANGLES 16)

(defconstant-exported CLIP_TT_ALWAYS 32)

(defconstant-exported CLIP_EMBEDDED 128)

(defconstant-exported DEFAULT_QUALITY 0)

(defconstant-exported DRAFT_QUALITY 1)

(defconstant-exported PROOF_QUALITY 2)

(defconstant-exported NONANTIALIASED_QUALITY 3)

(defconstant-exported ANTIALIASED_QUALITY 4)

(defconstant-exported DEFAULT_PITCH 0)

(defconstant-exported FIXED_PITCH 1)

(defconstant-exported VARIABLE_PITCH 2)

(defconstant-exported MONO_FONT 8)

(defconstant-exported FF_DECORATIVE 80)

(defconstant-exported FF_DONTCARE 0)

(defconstant-exported FF_MODERN 48)

(defconstant-exported FF_ROMAN 16)

(defconstant-exported FF_SCRIPT 64)

(defconstant-exported FF_SWISS 32)

(defconstant-exported PANOSE_COUNT 10)

(defconstant-exported PAN_FAMILYTYPE_INDEX 0)

(defconstant-exported PAN_SERIFSTYLE_INDEX 1)

(defconstant-exported PAN_WEIGHT_INDEX 2)

(defconstant-exported PAN_PROPORTION_INDEX 3)

(defconstant-exported PAN_CONTRAST_INDEX 4)

(defconstant-exported PAN_STROKEVARIATION_INDEX 5)

(defconstant-exported PAN_ARMSTYLE_INDEX 6)

(defconstant-exported PAN_LETTERFORM_INDEX 7)

(defconstant-exported PAN_MIDLINE_INDEX 8)

(defconstant-exported PAN_XHEIGHT_INDEX 9)

(defconstant-exported PAN_CULTURE_LATIN 0)

(defconstant-exported PAN_ANY 0)

(defconstant-exported PAN_NO_FIT 1)

(defconstant-exported PAN_FAMILY_TEXT_DISPLAY 2)

(defconstant-exported PAN_FAMILY_SCRIPT 3)

(defconstant-exported PAN_FAMILY_DECORATIVE 4)

(defconstant-exported PAN_FAMILY_PICTORIAL 5)

(defconstant-exported PAN_SERIF_COVE 2)

(defconstant-exported PAN_SERIF_OBTUSE_COVE 3)

(defconstant-exported PAN_SERIF_SQUARE_COVE 4)

(defconstant-exported PAN_SERIF_OBTUSE_SQUARE_COVE 5)

(defconstant-exported PAN_SERIF_SQUARE 6)

(defconstant-exported PAN_SERIF_THIN 7)

(defconstant-exported PAN_SERIF_BONE 8)

(defconstant-exported PAN_SERIF_EXAGGERATED 9)

(defconstant-exported PAN_SERIF_TRIANGLE 10)

(defconstant-exported PAN_SERIF_NORMAL_SANS 11)

(defconstant-exported PAN_SERIF_OBTUSE_SANS 12)

(defconstant-exported PAN_SERIF_PERP_SANS 13)

(defconstant-exported PAN_SERIF_FLARED 14)

(defconstant-exported PAN_SERIF_ROUNDED 15)

(defconstant-exported PAN_WEIGHT_VERY_LIGHT 2)

(defconstant-exported PAN_WEIGHT_LIGHT 3)

(defconstant-exported PAN_WEIGHT_THIN 4)

(defconstant-exported PAN_WEIGHT_BOOK 5)

(defconstant-exported PAN_WEIGHT_MEDIUM 6)

(defconstant-exported PAN_WEIGHT_DEMI 7)

(defconstant-exported PAN_WEIGHT_BOLD 8)

(defconstant-exported PAN_WEIGHT_HEAVY 9)

(defconstant-exported PAN_WEIGHT_BLACK 10)

(defconstant-exported PAN_WEIGHT_NORD 11)

(defconstant-exported PAN_PROP_OLD_STYLE 2)

(defconstant-exported PAN_PROP_MODERN 3)

(defconstant-exported PAN_PROP_EVEN_WIDTH 4)

(defconstant-exported PAN_PROP_EXPANDED 5)

(defconstant-exported PAN_PROP_CONDENSED 6)

(defconstant-exported PAN_PROP_VERY_EXPANDED 7)

(defconstant-exported PAN_PROP_VERY_CONDENSED 8)

(defconstant-exported PAN_PROP_MONOSPACED 9)

(defconstant-exported PAN_CONTRAST_NONE 2)

(defconstant-exported PAN_CONTRAST_VERY_LOW 3)

(defconstant-exported PAN_CONTRAST_LOW 4)

(defconstant-exported PAN_CONTRAST_MEDIUM_LOW 5)

(defconstant-exported PAN_CONTRAST_MEDIUM 6)

(defconstant-exported PAN_CONTRAST_MEDIUM_HIGH 7)

(defconstant-exported PAN_CONTRAST_HIGH 8)

(defconstant-exported PAN_CONTRAST_VERY_HIGH 9)

(defconstant-exported PAN_STROKE_GRADUAL_DIAG 2)

(defconstant-exported PAN_STROKE_GRADUAL_TRAN 3)

(defconstant-exported PAN_STROKE_GRADUAL_VERT 4)

(defconstant-exported PAN_STROKE_GRADUAL_HORZ 5)

(defconstant-exported PAN_STROKE_RAPID_VERT 6)

(defconstant-exported PAN_STROKE_RAPID_HORZ 7)

(defconstant-exported PAN_STROKE_INSTANT_VERT 8)

(defconstant-exported PAN_STRAIGHT_ARMS_HORZ 2)

(defconstant-exported PAN_STRAIGHT_ARMS_WEDGE 3)

(defconstant-exported PAN_STRAIGHT_ARMS_VERT 4)

(defconstant-exported PAN_STRAIGHT_ARMS_SINGLE_SERIF 5)

(defconstant-exported PAN_STRAIGHT_ARMS_DOUBLE_SERIF 6)

(defconstant-exported PAN_BENT_ARMS_HORZ 7)

(defconstant-exported PAN_BENT_ARMS_WEDGE 8)

(defconstant-exported PAN_BENT_ARMS_VERT 9)

(defconstant-exported PAN_BENT_ARMS_SINGLE_SERIF 10)

(defconstant-exported PAN_BENT_ARMS_DOUBLE_SERIF 11)

(defconstant-exported PAN_LETT_NORMAL_CONTACT 2)

(defconstant-exported PAN_LETT_NORMAL_WEIGHTED 3)

(defconstant-exported PAN_LETT_NORMAL_BOXED 4)

(defconstant-exported PAN_LETT_NORMAL_FLATTENED 5)

(defconstant-exported PAN_LETT_NORMAL_ROUNDED 6)

(defconstant-exported PAN_LETT_NORMAL_OFF_CENTER 7)

(defconstant-exported PAN_LETT_NORMAL_SQUARE 8)

(defconstant-exported PAN_LETT_OBLIQUE_CONTACT 9)

(defconstant-exported PAN_LETT_OBLIQUE_WEIGHTED 10)

(defconstant-exported PAN_LETT_OBLIQUE_BOXED 11)

(defconstant-exported PAN_LETT_OBLIQUE_FLATTENED 12)

(defconstant-exported PAN_LETT_OBLIQUE_ROUNDED 13)

(defconstant-exported PAN_LETT_OBLIQUE_OFF_CENTER 14)

(defconstant-exported PAN_LETT_OBLIQUE_SQUARE 15)

(defconstant-exported PAN_MIDLINE_STANDARD_TRIMMED 2)

(defconstant-exported PAN_MIDLINE_STANDARD_POINTED 3)

(defconstant-exported PAN_MIDLINE_STANDARD_SERIFED 4)

(defconstant-exported PAN_MIDLINE_HIGH_TRIMMED 5)

(defconstant-exported PAN_MIDLINE_HIGH_POINTED 6)

(defconstant-exported PAN_MIDLINE_HIGH_SERIFED 7)

(defconstant-exported PAN_MIDLINE_CONSTANT_TRIMMED 8)

(defconstant-exported PAN_MIDLINE_CONSTANT_POINTED 9)

(defconstant-exported PAN_MIDLINE_CONSTANT_SERIFED 10)

(defconstant-exported PAN_MIDLINE_LOW_TRIMMED 11)

(defconstant-exported PAN_MIDLINE_LOW_POINTED 12)

(defconstant-exported PAN_MIDLINE_LOW_SERIFED 13)

(defconstant-exported PAN_XHEIGHT_CONSTANT_SMALL 2)

(defconstant-exported PAN_XHEIGHT_CONSTANT_STD 3)

(defconstant-exported PAN_XHEIGHT_CONSTANT_LARGE 4)

(defconstant-exported PAN_XHEIGHT_DUCKING_SMALL 5)

(defconstant-exported PAN_XHEIGHT_DUCKING_STD 6)

(defconstant-exported PAN_XHEIGHT_DUCKING_LARGE 7)

(defconstant-exported FS_LATIN1 1)

(defconstant-exported FS_LATIN2 2)

(defconstant-exported FS_CYRILLIC 4)

(defconstant-exported FS_GREEK 8)

(defconstant-exported FS_TURKISH 16)

(defconstant-exported FS_HEBREW 32)

(defconstant-exported FS_ARABIC 64)

(defconstant-exported FS_BALTIC 128)

(defconstant-exported FS_THAI #x10000)

(defconstant-exported FS_JISJAPAN #x20000)

(defconstant-exported FS_CHINESESIMP #x40000)

(defconstant-exported FS_WANSUNG #x80000)

(defconstant-exported FS_CHINESETRAD #x100000)

(defconstant-exported FS_JOHAB #x200000)

(defconstant-exported FS_SYMBOL #x80000000)

(defconstant-exported HS_BDIAGONAL 3)

(defconstant-exported HS_CROSS 4)

(defconstant-exported HS_DIAGCROSS 5)

(defconstant-exported HS_FDIAGONAL 2)

(defconstant-exported HS_HORIZONTAL 0)

(defconstant-exported HS_VERTICAL 1)

(defconstant-exported PS_GEOMETRIC 65536)

(defconstant-exported PS_COSMETIC 0)

(defconstant-exported PS_ALTERNATE 8)

(defconstant-exported PS_SOLID 0)

(defconstant-exported PS_DASH 1)

(defconstant-exported PS_DOT 2)

(defconstant-exported PS_DASHDOT 3)

(defconstant-exported PS_DASHDOTDOT 4)

(defconstant-exported PS_NULL 5)

(defconstant-exported PS_USERSTYLE 7)

(defconstant-exported PS_INSIDEFRAME 6)

(defconstant-exported PS_ENDCAP_ROUND 0)

(defconstant-exported PS_ENDCAP_SQUARE 256)

(defconstant-exported PS_ENDCAP_FLAT 512)

(defconstant-exported PS_JOIN_BEVEL 4096)

(defconstant-exported PS_JOIN_MITER 8192)

(defconstant-exported PS_JOIN_ROUND 0)

(defconstant-exported PS_STYLE_MASK 15)

(defconstant-exported PS_ENDCAP_MASK 3840)

(defconstant-exported PS_TYPE_MASK 983040)

(defconstant-exported ALTERNATE 1)

(defconstant-exported WINDING 2)

(defconstant-exported DC_BINNAMES 12)

(defconstant-exported DC_BINS 6)

(defconstant-exported DC_COPIES 18)

(defconstant-exported DC_DRIVER 11)

(defconstant-exported DC_DATATYPE_PRODUCED 21)

(defconstant-exported DC_DUPLEX 7)

(defconstant-exported DC_EMF_COMPLIANT 20)

(defconstant-exported DC_ENUMRESOLUTIONS 13)

(defconstant-exported DC_EXTRA 9)

(defconstant-exported DC_FIELDS 1)

(defconstant-exported DC_FILEDEPENDENCIES 14)

(defconstant-exported DC_MAXEXTENT 5)

(defconstant-exported DC_MINEXTENT 4)

(defconstant-exported DC_ORIENTATION 17)

(defconstant-exported DC_PAPERNAMES 16)

(defconstant-exported DC_PAPERS 2)

(defconstant-exported DC_PAPERSIZE 3)

(defconstant-exported DC_SIZE 8)

(defconstant-exported DC_TRUETYPE 15)

(defconstant-exported DCTT_BITMAP 1)

(defconstant-exported DCTT_DOWNLOAD 2)

(defconstant-exported DCTT_SUBDEV 4)

(defconstant-exported DCTT_DOWNLOAD_OUTLINE 8)

(defconstant-exported DC_VERSION 10)

(defconstant-exported DC_BINADJUST 19)

(defconstant-exported DC_MANUFACTURER 23)

(defconstant-exported DC_MODEL 24)

(defconstant-exported DCBA_FACEUPNONE 0)

(defconstant-exported DCBA_FACEUPCENTER 1)

(defconstant-exported DCBA_FACEUPLEFT 2)

(defconstant-exported DCBA_FACEUPRIGHT 3)

(defconstant-exported DCBA_FACEDOWNNONE 256)

(defconstant-exported DCBA_FACEDOWNCENTER 257)

(defconstant-exported DCBA_FACEDOWNLEFT 258)

(defconstant-exported DCBA_FACEDOWNRIGHT 259)

(defconstant-exported FLOODFILLBORDER 0)

(defconstant-exported FLOODFILLSURFACE 1)

(defconstant-exported ETO_CLIPPED 4)

(defconstant-exported ETO_GLYPH_INDEX 16)

(defconstant-exported ETO_OPAQUE 2)

(defconstant-exported ETO_RTLREADING 128)

(defconstant-exported GDICOMMENT_WINDOWS_METAFILE -2147483647)

(defconstant-exported GDICOMMENT_BEGINGROUP 2)

(defconstant-exported GDICOMMENT_ENDGROUP 3)

(defconstant-exported GDICOMMENT_MULTIFORMATS 1073741828)

(defconstant-exported GDICOMMENT_IDENTIFIER 1128875079)

(defconstant-exported AD_COUNTERCLOCKWISE 1)

(defconstant-exported AD_CLOCKWISE 2)

(defconstant-exported RDH_RECTANGLES 1)

(defconstant-exported GCPCLASS_LATIN 1)

(defconstant-exported GCPCLASS_HEBREW 2)

(defconstant-exported GCPCLASS_ARABIC 2)

(defconstant-exported GCPCLASS_NEUTRAL 3)

(defconstant-exported GCPCLASS_LOCALNUMBER 4)

(defconstant-exported GCPCLASS_LATINNUMBER 5)

(defconstant-exported GCPCLASS_LATINNUMERICTERMINATOR 6)

(defconstant-exported GCPCLASS_LATINNUMERICSEPARATOR 7)

(defconstant-exported GCPCLASS_NUMERICSEPARATOR 8)

(defconstant-exported GCPCLASS_PREBOUNDLTR 128)

(defconstant-exported GCPCLASS_PREBOUNDRTL 64)

(defconstant-exported GCPCLASS_POSTBOUNDLTR 32)

(defconstant-exported GCPCLASS_POSTBOUNDRTL 16)

(defconstant-exported GCPGLYPH_LINKBEFORE #x8000)

(defconstant-exported GCPGLYPH_LINKAFTER #x4000)

(defconstant-exported DCB_DISABLE 8)

(defconstant-exported DCB_ENABLE 4)

(defconstant-exported DCB_RESET 1)

(defconstant-exported DCB_SET 3)

(defconstant-exported DCB_ACCUMULATE 2)

(defconstant-exported DCB_DIRTY 2)

(defconstant-exported OBJ_BRUSH 2)

(defconstant-exported OBJ_PEN 1)

(defconstant-exported OBJ_PAL 5)

(defconstant-exported OBJ_FONT 6)

(defconstant-exported OBJ_BITMAP 7)

(defconstant-exported OBJ_EXTPEN 11)

(defconstant-exported OBJ_REGION 8)

(defconstant-exported OBJ_DC 3)

(defconstant-exported OBJ_MEMDC 10)

(defconstant-exported OBJ_METAFILE 9)

(defconstant-exported OBJ_METADC 4)

(defconstant-exported OBJ_ENHMETAFILE 13)

(defconstant-exported OBJ_ENHMETADC 12)

(defconstant-exported DRIVERVERSION 0)

(defconstant-exported TECHNOLOGY 2)

(defconstant-exported DT_PLOTTER 0)

(defconstant-exported DT_RASDISPLAY 1)

(defconstant-exported DT_RASPRINTER 2)

(defconstant-exported DT_RASCAMERA 3)

(defconstant-exported DT_CHARSTREAM 4)

(defconstant-exported DT_METAFILE 5)

(defconstant-exported DT_DISPFILE 6)

(defconstant-exported HORZSIZE 4)

(defconstant-exported VERTSIZE 6)

(defconstant-exported HORZRES 8)

(defconstant-exported VERTRES 10)

(defconstant-exported LOGPIXELSX 88)

(defconstant-exported LOGPIXELSY 90)

(defconstant-exported BITSPIXEL 12)

(defconstant-exported PLANES 14)

(defconstant-exported NUMBRUSHES 16)

(defconstant-exported NUMPENS 18)

(defconstant-exported NUMFONTS 22)

(defconstant-exported NUMCOLORS 24)

(defconstant-exported NUMMARKERS 20)

(defconstant-exported ASPECTX 40)

(defconstant-exported ASPECTY 42)

(defconstant-exported ASPECTXY 44)

(defconstant-exported PDEVICESIZE 26)

(defconstant-exported CLIPCAPS 36)

(defconstant-exported SIZEPALETTE 104)

(defconstant-exported NUMRESERVED 106)

(defconstant-exported COLORRES 108)

(defconstant-exported PHYSICALWIDTH 110)

(defconstant-exported PHYSICALHEIGHT 111)

(defconstant-exported PHYSICALOFFSETX 112)

(defconstant-exported PHYSICALOFFSETY 113)

(defconstant-exported SCALINGFACTORX 114)

(defconstant-exported SCALINGFACTORY 115)

(defconstant-exported VREFRESH 116)

(defconstant-exported DESKTOPHORZRES 118)

(defconstant-exported DESKTOPVERTRES 117)

(defconstant-exported BLTALIGNMENT 119)

(defconstant-exported RASTERCAPS 38)

(defconstant-exported RC_BANDING 2)

(defconstant-exported RC_BITBLT 1)

(defconstant-exported RC_BITMAP64 8)

(defconstant-exported RC_DI_BITMAP 128)

(defconstant-exported RC_DIBTODEV 512)

(defconstant-exported RC_FLOODFILL 4096)

(defconstant-exported RC_GDI20_OUTPUT 16)

(defconstant-exported RC_PALETTE 256)

(defconstant-exported RC_SCALING 4)

(defconstant-exported RC_STRETCHBLT 2048)

(defconstant-exported RC_STRETCHDIB 8192)

(defconstant-exported RC_DEVBITS #x8000)

(defconstant-exported RC_OP_DX_OUTPUT #x4000)

(defconstant-exported CURVECAPS 28)

(defconstant-exported CC_NONE 0)

(defconstant-exported CC_CIRCLES 1)

(defconstant-exported CC_PIE 2)

(defconstant-exported CC_CHORD 4)

(defconstant-exported CC_ELLIPSES 8)

(defconstant-exported CC_WIDE 16)

(defconstant-exported CC_STYLED 32)

(defconstant-exported CC_WIDESTYLED 64)

(defconstant-exported CC_INTERIORS 128)

(defconstant-exported CC_ROUNDRECT 256)

(defconstant-exported LINECAPS 30)

(defconstant-exported LC_NONE 0)

(defconstant-exported LC_POLYLINE 2)

(defconstant-exported LC_MARKER 4)

(defconstant-exported LC_POLYMARKER 8)

(defconstant-exported LC_WIDE 16)

(defconstant-exported LC_STYLED 32)

(defconstant-exported LC_WIDESTYLED 64)

(defconstant-exported LC_INTERIORS 128)

(defconstant-exported POLYGONALCAPS 32)

(defconstant-exported RC_BIGFONT 1024)

(defconstant-exported RC_GDI20_STATE 32)

(defconstant-exported RC_NONE 0)

(defconstant-exported RC_SAVEBITMAP 64)

(defconstant-exported PC_NONE 0)

(defconstant-exported PC_POLYGON 1)

(defconstant-exported PC_POLYPOLYGON 256)

(defconstant-exported PC_PATHS 512)

(defconstant-exported PC_RECTANGLE 2)

(defconstant-exported PC_WINDPOLYGON 4)

(defconstant-exported PC_SCANLINE 8)

(defconstant-exported PC_TRAPEZOID 4)

(defconstant-exported PC_WIDE 16)

(defconstant-exported PC_STYLED 32)

(defconstant-exported PC_WIDESTYLED 64)

(defconstant-exported PC_INTERIORS 128)

(defconstant-exported TEXTCAPS 34)

(defconstant-exported TC_OP_CHARACTER 1)

(defconstant-exported TC_OP_STROKE 2)

(defconstant-exported TC_CP_STROKE 4)

(defconstant-exported TC_CR_90 8)

(defconstant-exported TC_CR_ANY 16)

(defconstant-exported TC_SF_X_YINDEP 32)

(defconstant-exported TC_SA_DOUBLE 64)

(defconstant-exported TC_SA_INTEGER 128)

(defconstant-exported TC_SA_CONTIN 256)

(defconstant-exported TC_EA_DOUBLE 512)

(defconstant-exported TC_IA_ABLE 1024)

(defconstant-exported TC_UA_ABLE 2048)

(defconstant-exported TC_SO_ABLE 4096)

(defconstant-exported TC_RA_ABLE 8192)

(defconstant-exported TC_VA_ABLE 16384)

(defconstant-exported TC_RESERVED 32768)

(defconstant-exported TC_SCROLLBLT 65536)

(defconstant-exported GCP_DBCS 1)

(defconstant-exported GCP_ERROR #x8000)

(defconstant-exported GCP_CLASSIN #x80000)

(defconstant-exported GCP_DIACRITIC 256)

(defconstant-exported GCP_DISPLAYZWG #x400000)

(defconstant-exported GCP_GLYPHSHAPE 16)

(defconstant-exported GCP_JUSTIFY #x10000)

(defconstant-exported GCP_JUSTIFYIN #x200000)

(defconstant-exported GCP_KASHIDA 1024)

(defconstant-exported GCP_LIGATE 32)

(defconstant-exported GCP_MAXEXTENT #x100000)

(defconstant-exported GCP_NEUTRALOVERRIDE #x2000000)

(defconstant-exported GCP_NUMERICOVERRIDE #x1000000)

(defconstant-exported GCP_NUMERICSLATIN #x4000000)

(defconstant-exported GCP_NUMERICSLOCAL #x8000000)

(defconstant-exported GCP_REORDER 2)

(defconstant-exported GCP_SYMSWAPOFF #x800000)

(defconstant-exported GCP_USEKERNING 8)

(defconstant-exported FLI_GLYPHS #x40000)

(defconstant-exported FLI_MASK #x103b)

(defconstant-exported GGO_METRICS 0)

(defconstant-exported GGO_BITMAP 1)

(defconstant-exported GGO_NATIVE 2)

(defconstant-exported GGO_BEZIER 3)

(defconstant-exported GGO_GRAY2_BITMAP 4)

(defconstant-exported GGO_GRAY4_BITMAP 5)

(defconstant-exported GGO_GRAY8_BITMAP 6)

(defconstant-exported GGO_GLYPH_INDEX 128)

(defconstant-exported GGO_UNHINTED 256)

(defconstant-exported GM_COMPATIBLE 1)

(defconstant-exported GM_ADVANCED 2)

(defconstant-exported MM_ANISOTROPIC 8)

(defconstant-exported MM_HIENGLISH 5)

(defconstant-exported MM_HIMETRIC 3)

(defconstant-exported MM_ISOTROPIC 7)

(defconstant-exported MM_LOENGLISH 4)

(defconstant-exported MM_LOMETRIC 2)

(defconstant-exported MM_TEXT 1)

(defconstant-exported MM_TWIPS 6)

(defconstant-exported MM_MAX_FIXEDSCALE 6)

(defconstant-exported ABSOLUTE 1)

(defconstant-exported RELATIVE 2)

(defconstant-exported PC_EXPLICIT 2)

(defconstant-exported PC_NOCOLLAPSE 4)

(defconstant-exported PC_RESERVED 1)

(defconstant-exported CLR_NONE #xffffffff)

(defconstant-exported CLR_INVALID #xffffffff)

(defconstant-exported CLR_DEFAULT #xff000000)

(defconstant-exported TT_AVAILABLE 1)

(defconstant-exported TT_ENABLED 2)

(defconstant-exported BLACK_BRUSH 4)

(defconstant-exported DKGRAY_BRUSH 3)

(defconstant-exported GRAY_BRUSH 2)

(defconstant-exported HOLLOW_BRUSH 5)

(defconstant-exported LTGRAY_BRUSH 1)

(defconstant-exported NULL_BRUSH 5)

(defconstant-exported WHITE_BRUSH 0)

(defconstant-exported BLACK_PEN 7)

(defconstant-exported NULL_PEN 8)

(defconstant-exported WHITE_PEN 6)

(defconstant-exported ANSI_FIXED_FONT 11)

(defconstant-exported ANSI_VAR_FONT 12)

(defconstant-exported DEVICE_DEFAULT_FONT 14)

(defconstant-exported DEFAULT_GUI_FONT 17)

(defconstant-exported OEM_FIXED_FONT 10)

(defconstant-exported SYSTEM_FONT 13)

(defconstant-exported SYSTEM_FIXED_FONT 16)

(defconstant-exported DEFAULT_PALETTE 15)

(defconstant-exported SYSPAL_ERROR 0)

(defconstant-exported SYSPAL_STATIC 1)

(defconstant-exported SYSPAL_NOSTATIC 2)

(defconstant-exported SYSPAL_NOSTATIC256 3)

(defconstant-exported TA_BASELINE 24)

(defconstant-exported TA_BOTTOM 8)

(defconstant-exported TA_TOP 0)

(defconstant-exported TA_CENTER 6)

(defconstant-exported TA_LEFT 0)

(defconstant-exported TA_RIGHT 2)

(defconstant-exported TA_RTLREADING 256)

(defconstant-exported TA_NOUPDATECP 0)

(defconstant-exported TA_UPDATECP 1)

(defconstant-exported TA_MASK (cl:+ 24 6 1 256))

(defconstant-exported VTA_BASELINE 24)

(defconstant-exported VTA_CENTER 6)

(defconstant-exported VTA_LEFT 8)

(defconstant-exported VTA_RIGHT 0)

(defconstant-exported VTA_BOTTOM 2)

(defconstant-exported VTA_TOP 0)

(defconstant-exported MWT_IDENTITY 1)

(defconstant-exported MWT_LEFTMULTIPLY 2)

(defconstant-exported MWT_RIGHTMULTIPLY 3)

(defconstant-exported OPAQUE 2)

(defconstant-exported TRANSPARENT 1)

(defconstant-exported BLACKONWHITE 1)

(defconstant-exported WHITEONBLACK 2)

(defconstant-exported COLORONCOLOR 3)

(defconstant-exported HALFTONE 4)

(defconstant-exported MAXSTRETCHBLTMODE 4)

(defconstant-exported STRETCH_ANDSCANS 1)

(defconstant-exported STRETCH_DELETESCANS 3)

(defconstant-exported STRETCH_HALFTONE 4)

(defconstant-exported STRETCH_ORSCANS 2)

(defconstant-exported TCI_SRCCHARSET 1)

(defconstant-exported TCI_SRCCODEPAGE 2)

(defconstant-exported TCI_SRCFONTSIG 3)

(defconstant-exported ICM_ON 2)

(defconstant-exported ICM_OFF 1)

(defconstant-exported ICM_QUERY 3)

(defconstant-exported NEWFRAME 1)

(defconstant-exported ABORTDOC 2)

(defconstant-exported NEXTBAND 3)

(defconstant-exported SETCOLORTABLE 4)

(defconstant-exported GETCOLORTABLE 5)

(defconstant-exported FLUSHOUTPUT 6)

(defconstant-exported DRAFTMODE 7)

(defconstant-exported QUERYESCSUPPORT 8)

(defconstant-exported SETABORTPROC 9)

(defconstant-exported STARTDOC 10)

(defconstant-exported ENDDOC 11)

(defconstant-exported GETPHYSPAGESIZE 12)

(defconstant-exported GETPRINTINGOFFSET 13)

(defconstant-exported GETSCALINGFACTOR 14)

(defconstant-exported MFCOMMENT 15)

(defconstant-exported GETPENWIDTH 16)

(defconstant-exported SETCOPYCOUNT 17)

(defconstant-exported SELECTPAPERSOURCE 18)

(defconstant-exported DEVICEDATA 19)

(defconstant-exported PASSTHROUGH 19)

(defconstant-exported GETTECHNOLGY 20)

(defconstant-exported GETTECHNOLOGY 20)

(defconstant-exported SETLINECAP 21)

(defconstant-exported SETLINEJOIN 22)

(defconstant-exported SETMITERLIMIT 23)

(defconstant-exported BANDINFO 24)

(defconstant-exported DRAWPATTERNRECT 25)

(defconstant-exported GETVECTORPENSIZE 26)

(defconstant-exported GETVECTORBRUSHSIZE 27)

(defconstant-exported ENABLEDUPLEX 28)

(defconstant-exported GETSETPAPERBINS 29)

(defconstant-exported GETSETPRINTORIENT 30)

(defconstant-exported ENUMPAPERBINS 31)

(defconstant-exported SETDIBSCALING 32)

(defconstant-exported EPSPRINTING 33)

(defconstant-exported ENUMPAPERMETRICS 34)

(defconstant-exported GETSETPAPERMETRICS 35)

(defconstant-exported POSTSCRIPT_DATA 37)

(defconstant-exported POSTSCRIPT_IGNORE 38)

(defconstant-exported MOUSETRAILS 39)

(defconstant-exported GETDEVICEUNITS 42)

(defconstant-exported GETEXTENDEDTEXTMETRICS 256)

(defconstant-exported GETEXTENTTABLE 257)

(defconstant-exported GETPAIRKERNTABLE 258)

(defconstant-exported GETTRACKKERNTABLE 259)

(defconstant-exported EXTTEXTOUT 512)

(defconstant-exported GETFACENAME 513)

(defconstant-exported DOWNLOADFACE 514)

(defconstant-exported ENABLERELATIVEWIDTHS 768)

(defconstant-exported ENABLEPAIRKERNING 769)

(defconstant-exported SETKERNTRACK 770)

(defconstant-exported SETALLJUSTVALUES 771)

(defconstant-exported SETCHARSET 772)

(defconstant-exported STRETCHBLT 2048)

(defconstant-exported GETSETSCREENPARAMS 3072)

(defconstant-exported QUERYDIBSUPPORT 3073)

(defconstant-exported BEGIN_PATH 4096)

(defconstant-exported CLIP_TO_PATH 4097)

(defconstant-exported END_PATH 4098)

(defconstant-exported EXT_DEVICE_CAPS 4099)

(defconstant-exported RESTORE_CTM 4100)

(defconstant-exported SAVE_CTM 4101)

(defconstant-exported SET_ARC_DIRECTION 4102)

(defconstant-exported SET_BACKGROUND_COLOR 4103)

(defconstant-exported SET_POLY_MODE 4104)

(defconstant-exported SET_SCREEN_ANGLE 4105)

(defconstant-exported SET_SPREAD 4106)

(defconstant-exported TRANSFORM_CTM 4107)

(defconstant-exported SET_CLIP_BOX 4108)

(defconstant-exported SET_BOUNDS 4109)

(defconstant-exported SET_MIRROR_MODE 4110)

(defconstant-exported OPENCHANNEL 4110)

(defconstant-exported DOWNLOADHEADER 4111)

(defconstant-exported CLOSECHANNEL 4112)

(defconstant-exported POSTSCRIPT_PASSTHROUGH 4115)

(defconstant-exported ENCAPSULATED_POSTSCRIPT 4116)

(defconstant-exported QDI_SETDIBITS 1)

(defconstant-exported QDI_GETDIBITS 2)

(defconstant-exported QDI_DIBTOSCREEN 4)

(defconstant-exported QDI_STRETCHDIB 8)

(defconstant-exported SP_NOTREPORTED #x4000)

(defconstant-exported PR_JOBSTATUS 0)

(defconstant-exported ASPECT_FILTERING 1)

(defconstant-exported BS_SOLID 0)

(defconstant-exported BS_NULL 1)

(defconstant-exported BS_HOLLOW 1)

(defconstant-exported BS_HATCHED 2)

(defconstant-exported BS_PATTERN 3)

(defconstant-exported BS_INDEXED 4)

(defconstant-exported BS_DIBPATTERN 5)

(defconstant-exported BS_DIBPATTERNPT 6)

(defconstant-exported BS_PATTERN8X8 7)

(defconstant-exported BS_DIBPATTERN8X8 8)

(defconstant-exported LCS_CALIBRATED_RGB 0)

(defconstant-exported LCS_DEVICE_RGB 1)

(defconstant-exported LCS_DEVICE_CMYK 2)

(defconstant-exported LCS_GM_BUSINESS 1)

(defconstant-exported LCS_GM_GRAPHICS 2)

(defconstant-exported LCS_GM_IMAGES 4)

(defconstant-exported RASTER_FONTTYPE 1)

(defconstant-exported DEVICE_FONTTYPE 2)

(defconstant-exported TRUETYPE_FONTTYPE 4)

(defconstant-exported DMORIENT_PORTRAIT 1)

(defconstant-exported DMORIENT_LANDSCAPE 2)

(defconstant-exported DMPAPER_FIRST 1)

(defconstant-exported DMPAPER_LETTER 1)

(defconstant-exported DMPAPER_LETTERSMALL 2)

(defconstant-exported DMPAPER_TABLOID 3)

(defconstant-exported DMPAPER_LEDGER 4)

(defconstant-exported DMPAPER_LEGAL 5)

(defconstant-exported DMPAPER_STATEMENT 6)

(defconstant-exported DMPAPER_EXECUTIVE 7)

(defconstant-exported DMPAPER_A3 8)

(defconstant-exported DMPAPER_A4 9)

(defconstant-exported DMPAPER_A4SMALL 10)

(defconstant-exported DMPAPER_A5 11)

(defconstant-exported DMPAPER_B4 12)

(defconstant-exported DMPAPER_B5 13)

(defconstant-exported DMPAPER_FOLIO 14)

(defconstant-exported DMPAPER_QUARTO 15)

(defconstant-exported DMPAPER_10X14 16)

(defconstant-exported DMPAPER_11X17 17)

(defconstant-exported DMPAPER_NOTE 18)

(defconstant-exported DMPAPER_ENV_9 19)

(defconstant-exported DMPAPER_ENV_10 20)

(defconstant-exported DMPAPER_ENV_11 21)

(defconstant-exported DMPAPER_ENV_12 22)

(defconstant-exported DMPAPER_ENV_14 23)

(defconstant-exported DMPAPER_CSHEET 24)

(defconstant-exported DMPAPER_DSHEET 25)

(defconstant-exported DMPAPER_ESHEET 26)

(defconstant-exported DMPAPER_ENV_DL 27)

(defconstant-exported DMPAPER_ENV_C5 28)

(defconstant-exported DMPAPER_ENV_C3 29)

(defconstant-exported DMPAPER_ENV_C4 30)

(defconstant-exported DMPAPER_ENV_C6 31)

(defconstant-exported DMPAPER_ENV_C65 32)

(defconstant-exported DMPAPER_ENV_B4 33)

(defconstant-exported DMPAPER_ENV_B5 34)

(defconstant-exported DMPAPER_ENV_B6 35)

(defconstant-exported DMPAPER_ENV_ITALY 36)

(defconstant-exported DMPAPER_ENV_MONARCH 37)

(defconstant-exported DMPAPER_ENV_PERSONAL 38)

(defconstant-exported DMPAPER_FANFOLD_US 39)

(defconstant-exported DMPAPER_FANFOLD_STD_GERMAN 40)

(defconstant-exported DMPAPER_FANFOLD_LGL_GERMAN 41)

(defconstant-exported DMPAPER_ISO_B4 42)

(defconstant-exported DMPAPER_JAPANESE_POSTCARD 43)

(defconstant-exported DMPAPER_9X11 44)

(defconstant-exported DMPAPER_10X11 45)

(defconstant-exported DMPAPER_15X11 46)

(defconstant-exported DMPAPER_ENV_INVITE 47)

(defconstant-exported DMPAPER_RESERVED_48 48)

(defconstant-exported DMPAPER_RESERVED_49 49)

(defconstant-exported DMPAPER_LETTER_EXTRA 50)

(defconstant-exported DMPAPER_LEGAL_EXTRA 51)

(defconstant-exported DMPAPER_TABLOID_EXTRA 52)

(defconstant-exported DMPAPER_A4_EXTRA 53)

(defconstant-exported DMPAPER_LETTER_TRANSVERSE 54)

(defconstant-exported DMPAPER_A4_TRANSVERSE 55)

(defconstant-exported DMPAPER_LETTER_EXTRA_TRANSVERSE 56)

(defconstant-exported DMPAPER_A_PLUS 57)

(defconstant-exported DMPAPER_B_PLUS 58)

(defconstant-exported DMPAPER_LETTER_PLUS 59)

(defconstant-exported DMPAPER_A4_PLUS 60)

(defconstant-exported DMPAPER_A5_TRANSVERSE 61)

(defconstant-exported DMPAPER_B5_TRANSVERSE 62)

(defconstant-exported DMPAPER_A3_EXTRA 63)

(defconstant-exported DMPAPER_A5_EXTRA 64)

(defconstant-exported DMPAPER_B5_EXTRA 65)

(defconstant-exported DMPAPER_A2 66)

(defconstant-exported DMPAPER_A3_TRANSVERSE 67)

(defconstant-exported DMPAPER_A3_EXTRA_TRANSVERSE 68)

(defconstant-exported DMPAPER_LAST 68)

(defconstant-exported DMPAPER_USER 256)

(defconstant-exported DMBIN_FIRST 1)

(defconstant-exported DMBIN_UPPER 1)

(defconstant-exported DMBIN_ONLYONE 1)

(defconstant-exported DMBIN_LOWER 2)

(defconstant-exported DMBIN_MIDDLE 3)

(defconstant-exported DMBIN_MANUAL 4)

(defconstant-exported DMBIN_ENVELOPE 5)

(defconstant-exported DMBIN_ENVMANUAL 6)

(defconstant-exported DMBIN_AUTO 7)

(defconstant-exported DMBIN_TRACTOR 8)

(defconstant-exported DMBIN_SMALLFMT 9)

(defconstant-exported DMBIN_LARGEFMT 10)

(defconstant-exported DMBIN_LARGECAPACITY 11)

(defconstant-exported DMBIN_CASSETTE 14)

(defconstant-exported DMBIN_FORMSOURCE 15)

(defconstant-exported DMBIN_LAST 15)

(defconstant-exported DMBIN_USER 256)

(defconstant-exported DMRES_DRAFT -1)

(defconstant-exported DMRES_LOW -2)

(defconstant-exported DMRES_MEDIUM -3)

(defconstant-exported DMRES_HIGH -4)

(defconstant-exported DMCOLOR_MONOCHROME 1)

(defconstant-exported DMCOLOR_COLOR 2)

(defconstant-exported DMDUP_SIMPLEX 1)

(defconstant-exported DMDUP_VERTICAL 2)

(defconstant-exported DMDUP_HORIZONTAL 3)

(defconstant-exported DMTT_BITMAP 1)

(defconstant-exported DMTT_DOWNLOAD 2)

(defconstant-exported DMTT_SUBDEV 3)

(defconstant-exported DMTT_DOWNLOAD_OUTLINE 4)

(defconstant-exported DMCOLLATE_FALSE 0)

(defconstant-exported DMCOLLATE_TRUE 1)

(defconstant-exported DM_SPECVERSION 800)

(defconstant-exported DM_GRAYSCALE 1)

(defconstant-exported DM_INTERLACED 2)

(defconstant-exported DM_UPDATE 1)

(defconstant-exported DM_COPY 2)

(defconstant-exported DM_PROMPT 4)

(defconstant-exported DM_MODIFY 8)

(defconstant-exported DM_IN_BUFFER 8)

(defconstant-exported DM_IN_PROMPT 4)

(defconstant-exported DM_OUT_BUFFER 2)

(defconstant-exported DM_OUT_DEFAULT 1)

(defconstant-exported DM_ORIENTATION 1)

(defconstant-exported DM_PAPERSIZE 2)

(defconstant-exported DM_PAPERLENGTH 4)

(defconstant-exported DM_PAPERWIDTH 8)

(defconstant-exported DM_SCALE 16)

(defconstant-exported DM_COPIES 256)

(defconstant-exported DM_DEFAULTSOURCE 512)

(defconstant-exported DM_PRINTQUALITY 1024)

(defconstant-exported DM_COLOR 2048)

(defconstant-exported DM_DUPLEX 4096)

(defconstant-exported DM_YRESOLUTION 8192)

(defconstant-exported DM_TTOPTION 16384)

(defconstant-exported DM_COLLATE 32768)

(defconstant-exported DM_FORMNAME 65536)

(defconstant-exported DM_LOGPIXELS #x20000)

(defconstant-exported DM_BITSPERPEL #x40000)

(defconstant-exported DM_PELSWIDTH #x80000)

(defconstant-exported DM_PELSHEIGHT #x100000)

(defconstant-exported DM_DISPLAYFLAGS #x200000)

(defconstant-exported DM_DISPLAYFREQUENCY #x400000)

(defconstant-exported DM_ICMMETHOD #x800000)

(defconstant-exported DM_ICMINTENT #x1000000)

(defconstant-exported DM_MEDIATYPE #x2000000)

(defconstant-exported DM_DITHERTYPE #x4000000)

(defconstant-exported DMICMMETHOD_NONE 1)

(defconstant-exported DMICMMETHOD_SYSTEM 2)

(defconstant-exported DMICMMETHOD_DRIVER 3)

(defconstant-exported DMICMMETHOD_DEVICE 4)

(defconstant-exported DMICMMETHOD_USER 256)

(defconstant-exported DMICM_SATURATE 1)

(defconstant-exported DMICM_CONTRAST 2)

(defconstant-exported DMICM_COLORMETRIC 3)

(defconstant-exported DMICM_USER 256)

(defconstant-exported DMMEDIA_STANDARD 1)

(defconstant-exported DMMEDIA_TRANSPARENCY 2)

(defconstant-exported DMMEDIA_GLOSSY 3)

(defconstant-exported DMMEDIA_USER 256)

(defconstant-exported DMDITHER_NONE 1)

(defconstant-exported DMDITHER_COARSE 2)

(defconstant-exported DMDITHER_FINE 3)

(defconstant-exported DMDITHER_LINEART 4)

(defconstant-exported DMDITHER_ERRORDIFFUSION 5)

(defconstant-exported DMDITHER_RESERVED6 6)

(defconstant-exported DMDITHER_RESERVED7 7)

(defconstant-exported DMDITHER_RESERVED8 8)

(defconstant-exported DMDITHER_RESERVED9 9)

(defconstant-exported DMDITHER_GRAYSCALE 10)

(defconstant-exported DMDITHER_USER 256)

(defconstant-exported GDI_ERROR #xFFFFFFFF)

(defconstant-exported TMPF_FIXED_PITCH 1)

(defconstant-exported TMPF_VECTOR 2)

(defconstant-exported TMPF_TRUETYPE 4)

(defconstant-exported TMPF_DEVICE 8)

(defconstant-exported NTM_ITALIC 1)

(defconstant-exported NTM_BOLD 32)

(defconstant-exported NTM_REGULAR 64)

(defconstant-exported TT_POLYGON_TYPE 24)

(defconstant-exported TT_PRIM_LINE 1)

(defconstant-exported TT_PRIM_QSPLINE 2)

(defconstant-exported TT_PRIM_CSPLINE 3)

(defconstant-exported FONTMAPPER_MAX 10)

(defconstant-exported ENHMETA_STOCK_OBJECT #x80000000)

(defconstant-exported WGL_FONT_LINES 0)

(defconstant-exported WGL_FONT_POLYGONS 1)

(defconstant-exported LPD_DOUBLEBUFFER 1)

(defconstant-exported LPD_STEREO 2)

(defconstant-exported LPD_SUPPORT_GDI 16)

(defconstant-exported LPD_SUPPORT_OPENGL 32)

(defconstant-exported LPD_SHARE_DEPTH 64)

(defconstant-exported LPD_SHARE_STENCIL 128)

(defconstant-exported LPD_SHARE_ACCUM 256)

(defconstant-exported LPD_SWAP_EXCHANGE 512)

(defconstant-exported LPD_SWAP_COPY 1024)

(defconstant-exported LPD_TRANSPARENT 4096)

(defconstant-exported LPD_TYPE_RGBA 0)

(defconstant-exported LPD_TYPE_COLORINDEX 1)

(defconstant-exported WGL_SWAP_MAIN_PLANE 1)

(defconstant-exported WGL_SWAP_OVERLAY1 2)

(defconstant-exported WGL_SWAP_OVERLAY2 4)

(defconstant-exported WGL_SWAP_OVERLAY3 8)

(defconstant-exported WGL_SWAP_OVERLAY4 16)

(defconstant-exported WGL_SWAP_OVERLAY5 32)

(defconstant-exported WGL_SWAP_OVERLAY6 64)

(defconstant-exported WGL_SWAP_OVERLAY7 128)

(defconstant-exported WGL_SWAP_OVERLAY8 256)

(defconstant-exported WGL_SWAP_OVERLAY9 512)

(defconstant-exported WGL_SWAP_OVERLAY10 1024)

(defconstant-exported WGL_SWAP_OVERLAY11 2048)

(defconstant-exported WGL_SWAP_OVERLAY12 4096)

(defconstant-exported WGL_SWAP_OVERLAY13 8192)

(defconstant-exported WGL_SWAP_OVERLAY14 16384)

(defconstant-exported WGL_SWAP_OVERLAY15 32768)

(defconstant-exported WGL_SWAP_UNDERLAY1 65536)

(defconstant-exported WGL_SWAP_UNDERLAY2 #x20000)

(defconstant-exported WGL_SWAP_UNDERLAY3 #x40000)

(defconstant-exported WGL_SWAP_UNDERLAY4 #x80000)

(defconstant-exported WGL_SWAP_UNDERLAY5 #x100000)

(defconstant-exported WGL_SWAP_UNDERLAY6 #x200000)

(defconstant-exported WGL_SWAP_UNDERLAY7 #x400000)

(defconstant-exported WGL_SWAP_UNDERLAY8 #x800000)

(defconstant-exported WGL_SWAP_UNDERLAY9 #x1000000)

(defconstant-exported WGL_SWAP_UNDERLAY10 #x2000000)

(defconstant-exported WGL_SWAP_UNDERLAY11 #x4000000)

(defconstant-exported WGL_SWAP_UNDERLAY12 #x8000000)

(defconstant-exported WGL_SWAP_UNDERLAY13 #x10000000)

(defconstant-exported WGL_SWAP_UNDERLAY14 #x20000000)

(defconstant-exported WGL_SWAP_UNDERLAY15 #x40000000)

(defconstant-exported AC_SRC_OVER #x00)

(defconstant-exported AC_SRC_ALPHA #x01)

(defconstant-exported AC_SRC_NO_PREMULT_ALPHA #x01)

(defconstant-exported AC_SRC_NO_ALPHA #x02)

(defconstant-exported AC_DST_NO_PREMULT_ALPHA #x10)

(defconstant-exported AC_DST_NO_ALPHA #x20)

(defconstant-exported LAYOUT_RTL 1)

(defconstant-exported LAYOUT_BITMAPORIENTATIONPRESERVED 8)

(defconstant-exported DISPLAY_DEVICE_ATTACHED_TO_DESKTOP #x00000001)

(defconstant-exported DISPLAY_DEVICE_MULTI_DRIVER #x00000002)

(defconstant-exported DISPLAY_DEVICE_PRIMARY_DEVICE #x00000004)

(defconstant-exported DISPLAY_DEVICE_MIRRORING_DRIVER #x00000008)

(defconstant-exported DISPLAY_DEVICE_VGA_COMPATIBLE #x00000010)

(defconstant-exported DISPLAY_DEVICE_REMOVABLE #x00000020)

(defconstant-exported DISPLAY_DEVICE_MODESPRUNED #x08000000)

(defcstructex-exported ABC
        (abcA :int)
        (abcB :unsigned-int)
        (abcC :int))





(defcstructex-exported ABCFLOAT
        (abcfA :float)
        (abcfB :float)
        (abcfC :float))





(defcstructex-exported BITMAP
        (bmType :int32)
        (bmWidth :int32)
        (bmHeight :int32)
        (bmWidthBytes :int32)
        (bmPlanes :unsigned-short)
        (bmBitsPixel :unsigned-short)
        (bmBits :pointer))







(defcstructex-exported BITMAPCOREHEADER
        (bcSize :unsigned-long)
        (bcWidth :unsigned-short)
        (bcHeight :unsigned-short)
        (bcPlanes :unsigned-short)
        (bcBitCount :unsigned-short))







(defcstructex-exported RGBTRIPLE
        (rgbtBlue :unsigned-char)
        (rgbtGreen :unsigned-char)
        (rgbtRed :unsigned-char))





(defcstructex-exported BITMAPFILEHEADER
        (bfType :unsigned-short)
        (bfSize :unsigned-long)
        (bfReserved1 :unsigned-short)
        (bfReserved2 :unsigned-short)
        (bfOffBits :unsigned-long))







(defcstructex-exported BITMAPCOREINFO
        (bmciHeader BITMAPCOREHEADER)
        (bmciColors :pointer))







(defcstructex-exported BITMAPINFOHEADER
        (biSize :unsigned-long)
        (biWidth :int32)
        (biHeight :int32)
        (biPlanes :unsigned-short)
        (biBitCount :unsigned-short)
        (biCompression :unsigned-long)
        (biSizeImage :unsigned-long)
        (biXPelsPerMeter :int32)
        (biYPelsPerMeter :int32)
        (biClrUsed :unsigned-long)
        (biClrImportant :unsigned-long))







(defcstructex-exported RGBQUAD
        (rgbBlue :unsigned-char)
        (rgbGreen :unsigned-char)
        (rgbRed :unsigned-char)
        (rgbReserved :unsigned-char))





(defcstructex-exported BITMAPINFO
        (bmiHeader BITMAPINFOHEADER)
        (bmiColors :pointer))















(defcstructex-exported CIEXYZ
        (ciexyzX :int32)
        (ciexyzY :int32)
        (ciexyzZ :int32))





(defcstructex-exported CIEXYZTRIPLE
        (ciexyzRed CIEXYZ)
        (ciexyzGreen CIEXYZ)
        (ciexyzBlue CIEXYZ))





(defcstructex-exported BITMAPV4HEADER
        (bV4Size :unsigned-long)
        (bV4Width :int32)
        (bV4Height :int32)
        (bV4Planes :unsigned-short)
        (bV4BitCount :unsigned-short)
        (bV4V4Compression :unsigned-long)
        (bV4SizeImage :unsigned-long)
        (bV4XPelsPerMeter :int32)
        (bV4YPelsPerMeter :int32)
        (bV4ClrUsed :unsigned-long)
        (bV4ClrImportant :unsigned-long)
        (bV4RedMask :unsigned-long)
        (bV4GreenMask :unsigned-long)
        (bV4BlueMask :unsigned-long)
        (bV4AlphaMask :unsigned-long)
        (bV4CSType :unsigned-long)
        (bV4Endpoints CIEXYZTRIPLE)
        (bV4GammaRed :unsigned-long)
        (bV4GammaGreen :unsigned-long)
        (bV4GammaBlue :unsigned-long))





(defcstructex-exported FONTSIGNATURE
        (fsUsb :pointer)
        (fsCsb :pointer))





(defcstructex-exported CHARSETINFO
        (ciCharset :unsigned-int)
        (ciACP :unsigned-int)
        (fs FONTSIGNATURE))



(defcstructex-exported COLORADJUSTMENT
        (caSize :unsigned-short)
        (caFlags :unsigned-short)
        (caIlluminantIndex :unsigned-short)
        (caRedGamma :unsigned-short)
        (caGreenGamma :unsigned-short)
        (caBlueGamma :unsigned-short)
        (caReferenceBlack :unsigned-short)
        (caReferenceWhite :unsigned-short)
        (caContrast :short)
        (caBrightness :short)
        (caColorfulness :short)
        (caRedGreenTint :short))





(defcstructex-exported DEVMODEA
        (dmDeviceName :pointer)
        (dmSpecVersion :unsigned-short)
        (dmDriverVersion :unsigned-short)
        (dmSize :unsigned-short)
        (dmDriverExtra :unsigned-short)
        (dmFields :unsigned-long)
        (dmColor :short)
        (dmDuplex :short)
        (dmYResolution :short)
        (dmTTOption :short)
        (dmCollate :short)
        (dmFormName :pointer)
        (dmLogPixels :unsigned-short)
        (dmBitsPerPel :unsigned-long)
        (dmPelsWidth :unsigned-long)
        (dmPelsHeight :unsigned-long)
        (dmDisplayFrequency :unsigned-long)
        (dmICMMethod :unsigned-long)
        (dmICMIntent :unsigned-long)
        (dmMediaType :unsigned-long)
        (dmDitherType :unsigned-long)
        (dmReserved1 :unsigned-long)
        (dmReserved2 :unsigned-long)
        (dmPanningWidth :unsigned-long)
        (dmPanningHeight :unsigned-long)
        (u :pointer)
        (u2 :pointer))







(cffi:defcunion DEVMODEA_u2
        (dmDisplayFlags :unsigned-long)
        (dmNup :unsigned-long))

(cffi:defcunion DEVMODEA_u
        (dmPosition :pointer)
        (dmDisplayOrientation :unsigned-long)
        (dmDisplayFixedOutput :unsigned-long)
        (s :pointer))

(defcstructex-exported DEVMODEA_u_s
        (dmOrientation :short)
        (dmPaperSize :short)
        (dmPaperLength :short)
        (dmPaperWidth :short)
        (dmScale :short)
        (dmCopies :short)
        (dmDefaultSource :short)
        (dmPrintQuality :short))

(defcstructex-exported DEVMODEW
        (dmDeviceName :pointer)
        (dmSpecVersion :unsigned-short)
        (dmDriverVersion :unsigned-short)
        (dmSize :unsigned-short)
        (dmDriverExtra :unsigned-short)
        (dmFields :unsigned-long)
        (dmColor :short)
        (dmDuplex :short)
        (dmYResolution :short)
        (dmTTOption :short)
        (dmCollate :short)
        (dmFormName :pointer)
        (dmLogPixels :unsigned-short)
        (dmBitsPerPel :unsigned-long)
        (dmPelsWidth :unsigned-long)
        (dmPelsHeight :unsigned-long)
        (dmDisplayFrequency :unsigned-long)
        (dmICMMethod :unsigned-long)
        (dmICMIntent :unsigned-long)
        (dmMediaType :unsigned-long)
        (dmDitherType :unsigned-long)
        (dmReserved1 :unsigned-long)
        (dmReserved2 :unsigned-long)
        (dmPanningWidth :unsigned-long)
        (dmPanningHeight :unsigned-long)
        (u :pointer)
        (u2 :pointer))







(cffi:defcunion DEVMODEW_u2
        (dmDisplayFlags :unsigned-long)
        (dmNup :unsigned-long))

(cffi:defcunion DEVMODEW_u
        (dmPosition :pointer)
        (dmDisplayOrientation :unsigned-long)
        (dmDisplayFixedOutput :unsigned-long)
        (s :pointer))

(defcstructex-exported DEVMODEW_u_s
        (dmOrientation :short)
        (dmPaperSize :short)
        (dmPaperLength :short)
        (dmPaperWidth :short)
        (dmScale :short)
        (dmCopies :short)
        (dmDefaultSource :short)
        (dmPrintQuality :short))

(defcstructex-exported DIBSECTION
        (dsBm BITMAP)
        (dsBmih BITMAPINFOHEADER)
        (dsBitfields :pointer)
        (dshSection :pointer)
        (dsOffset :unsigned-long))



(defcstructex-exported DOCINFOA
        (cbSize :int)
        (lpszDocName :string)
        (lpszOutput :string)
        (lpszDatatype :string)
        (fwType :unsigned-long))





(defcstructex-exported DOCINFOW
        (cbSize :int)
        (lpszDocName :pointer)
        (lpszOutput :pointer)
        (lpszDatatype :pointer)
        (fwType :unsigned-long))





(defcstructex-exported EMR
        (iType :unsigned-long)
        (nSize :unsigned-long))





(defcstructex-exported EMRANGLEARC
        (emr EMR)
        (ptlCenter :pointer)
        (nRadius :unsigned-long)
        (eStartAngle :float)
        (eSweepAngle :float))





(defcstructex-exported EMRARC
        (emr EMR)
        (rclBox RECTL)
        (ptlStart :pointer)
        (ptlEnd :pointer))

















(defcstructex-exported XFORM
        (eM11 :float)
        (eM12 :float)
        (eM21 :float)
        (eM22 :float)
        (eDx :float)
        (eDy :float))







(defcstructex-exported EMRBITBLT
        (emr EMR)
        (rclBounds RECTL)
        (xDest :int32)
        (yDest :int32)
        (cxDest :int32)
        (cyDest :int32)
        (dwRop :unsigned-long)
        (xSrc :int32)
        (ySrc :int32)
        (xformSrc XFORM)
        (crBkColorSrc :unsigned-long)
        (iUsageSrc :unsigned-long)
        (offBmiSrc :unsigned-long)
        (offBitsSrc :unsigned-long)
        (cbBitsSrc :unsigned-long))





(defcstructex-exported LOGBRUSH
        (lbStyle :unsigned-int)
        (lbColor :unsigned-long)
        (lbHatch :int32))













(defcstructex-exported EMRCREATEBRUSHINDIRECT
        (emr EMR)
        (ihBrush :unsigned-long)
        (lb LOGBRUSH))









(defcstructex-exported LOGCOLORSPACEA
        (lcsSignature :unsigned-long)
        (lcsVersion :unsigned-long)
        (lcsSize :unsigned-long)
        (lcsCSType :int32)
        (lcsIntent :int32)
        (lcsEndpoints CIEXYZTRIPLE)
        (lcsGammaRed :unsigned-long)
        (lcsGammaGreen :unsigned-long)
        (lcsGammaBlue :unsigned-long)
        (lcsFilename :pointer))





(defcstructex-exported LOGCOLORSPACEW
        (lcsSignature :unsigned-long)
        (lcsVersion :unsigned-long)
        (lcsSize :unsigned-long)
        (lcsCSType :int32)
        (lcsIntent :int32)
        (lcsEndpoints CIEXYZTRIPLE)
        (lcsGammaRed :unsigned-long)
        (lcsGammaGreen :unsigned-long)
        (lcsGammaBlue :unsigned-long)
        (lcsFilename :pointer))





(defcstructex-exported EMRCREATECOLORSPACE
        (emr EMR)
        (ihCS :unsigned-long)
        (lcs LOGCOLORSPACEW))





(defcstructex-exported EMRCREATEDIBPATTERNBRUSHPT
        (emr EMR)
        (ihBrush :unsigned-long)
        (iUsage :unsigned-long)
        (offBmi :unsigned-long)
        (cbBmi :unsigned-long)
        (offBits :unsigned-long)
        (cbBits :unsigned-long))





(defcstructex-exported EMRCREATEMONOBRUSH
        (emr EMR)
        (ihBrush :unsigned-long)
        (iUsage :unsigned-long)
        (offBmi :unsigned-long)
        (cbBmi :unsigned-long)
        (offBits :unsigned-long)
        (cbBits :unsigned-long))





(defcstructex-exported PALETTEENTRY
        (peRed :unsigned-char)
        (peGreen :unsigned-char)
        (peBlue :unsigned-char)
        (peFlags :unsigned-char))







(defcstructex-exported LOGPALETTE
        (palVersion :unsigned-short)
        (palNumEntries :unsigned-short)
        (palPalEntry :pointer))









(defcstructex-exported EMRCREATEPALETTE
        (emr EMR)
        (ihPal :unsigned-long)
        (lgpl LOGPALETTE))





(defcstructex-exported LOGPEN
        (lopnStyle :unsigned-int)
        (lopnWidth POINT)
        (lopnColor :unsigned-long))







(defcstructex-exported EMRCREATEPEN
        (emr EMR)
        (ihPen :unsigned-long)
        (lopn LOGPEN))





(defcstructex-exported EMRELLIPSE
        (emr EMR)
        (rclBox RECTL))









(defcstructex-exported EMREOF
        (emr EMR)
        (nPalEntries :unsigned-long)
        (offPalEntries :unsigned-long)
        (nSizeLast :unsigned-long))





(defcstructex-exported EMREXCLUDECLIPRECT
        (emr EMR)
        (rclClip RECTL))









(defcstructex-exported PANOSE
        (bFamilyType :unsigned-char)
        (bSerifStyle :unsigned-char)
        (bWeight :unsigned-char)
        (bProportion :unsigned-char)
        (bContrast :unsigned-char)
        (bStrokeVariation :unsigned-char)
        (bArmStyle :unsigned-char)
        (bLetterform :unsigned-char)
        (bMidline :unsigned-char)
        (bXHeight :unsigned-char))



(defcstructex-exported LOGFONTA
        (lfHeight :int32)
        (lfWidth :int32)
        (lfEscapement :int32)
        (lfOrientation :int32)
        (lfWeight :int32)
        (lfItalic :unsigned-char)
        (lfUnderline :unsigned-char)
        (lfStrikeOut :unsigned-char)
        (lfCharSet :unsigned-char)
        (lfOutPrecision :unsigned-char)
        (lfClipPrecision :unsigned-char)
        (lfQuality :unsigned-char)
        (lfPitchAndFamily :unsigned-char)
        (lfFaceName :pointer))







(defcstructex-exported LOGFONTW
        (lfHeight :int32)
        (lfWidth :int32)
        (lfEscapement :int32)
        (lfOrientation :int32)
        (lfWeight :int32)
        (lfItalic :unsigned-char)
        (lfUnderline :unsigned-char)
        (lfStrikeOut :unsigned-char)
        (lfCharSet :unsigned-char)
        (lfOutPrecision :unsigned-char)
        (lfClipPrecision :unsigned-char)
        (lfQuality :unsigned-char)
        (lfPitchAndFamily :unsigned-char)
        (lfFaceName :pointer))







(defcstructex-exported EXTLOGFONTA
        (elfLogFont LOGFONTA)
        (elfFullName :pointer)
        (elfStyle :pointer)
        (elfVersion :unsigned-long)
        (elfStyleSize :unsigned-long)
        (elfMatch :unsigned-long)
        (elfReserved :unsigned-long)
        (elfVendorId :pointer)
        (elfCulture :unsigned-long)
        (elfPanose PANOSE))







(defcstructex-exported EXTLOGFONTW
        (elfLogFont LOGFONTW)
        (elfFullName :pointer)
        (elfStyle :pointer)
        (elfVersion :unsigned-long)
        (elfStyleSize :unsigned-long)
        (elfMatch :unsigned-long)
        (elfReserved :unsigned-long)
        (elfVendorId :pointer)
        (elfCulture :unsigned-long)
        (elfPanose PANOSE))







(defcstructex-exported EMREXTCREATEFONTINDIRECTW
        (emr EMR)
        (ihFont :unsigned-long)
        (elfw EXTLOGFONTW))





(defcstructex-exported EXTLOGPEN
        (elpPenStyle :unsigned-int)
        (elpWidth :unsigned-int)
        (elpBrushStyle :unsigned-int)
        (elpColor :unsigned-long)
        (elpHatch :int32)
        (elpNumEntries :unsigned-long)
        (elpStyleEntry :pointer))







(defcstructex-exported EMREXTCREATEPEN
        (emr EMR)
        (ihPen :unsigned-long)
        (offBmi :unsigned-long)
        (cbBmi :unsigned-long)
        (offBits :unsigned-long)
        (cbBits :unsigned-long)
        (elp EXTLOGPEN))





(defcstructex-exported EMREXTFLOODFILL
        (emr EMR)
        (ptlStart :pointer)
        (crColor :unsigned-long)
        (iMode :unsigned-long))





(defcstructex-exported EMREXTSELECTCLIPRGN
        (emr EMR)
        (cbRgnData :unsigned-long)
        (iMode :unsigned-long)
        (RgnData :pointer))





(defcstructex-exported EMRTEXT
        (ptlReference :pointer)
        (nChars :unsigned-long)
        (offString :unsigned-long)
        (fOptions :unsigned-long)
        (rcl RECTL)
        (offDx :unsigned-long))





(defcstructex-exported EMREXTTEXTOUTA
        (emr EMR)
        (rclBounds RECTL)
        (iGraphicsMode :unsigned-long)
        (exScale :float)
        (eyScale :float)
        (emrtext EMRTEXT))









(defcstructex-exported EMRFILLPATH
        (emr EMR)
        (rclBounds RECTL))













(defcstructex-exported EMRFILLRGN
        (emr EMR)
        (rclBounds RECTL)
        (cbRgnData :unsigned-long)
        (ihBrush :unsigned-long)
        (RgnData :pointer))





(defcstructex-exported EMRFORMAT
        (dSignature :unsigned-long)
        (nVersion :unsigned-long)
        (cbData :unsigned-long)
        (offData :unsigned-long))



(defcstructex-exported EMRFRAMERGN
        (emr EMR)
        (rclBounds RECTL)
        (cbRgnData :unsigned-long)
        (ihBrush :unsigned-long)
        (szlStroke :pointer)
        (RgnData :pointer))





(defcstructex-exported EMRGDICOMMENT
        (emr EMR)
        (cbData :unsigned-long)
        (Data :pointer))





(defcstructex-exported EMRINVERTRGN
        (emr EMR)
        (rclBounds RECTL)
        (cbRgnData :unsigned-long)
        (RgnData :pointer))









(defcstructex-exported EMRLINETO
        (emr EMR)
        (ptl :pointer))









(defcstructex-exported EMRMASKBLT
        (emr EMR)
        (rclBounds RECTL)
        (xDest :int32)
        (yDest :int32)
        (cxDest :int32)
        (cyDest :int32)
        (dwRop :unsigned-long)
        (xSrc :int32)
        (ySrc :int32)
        (xformSrc XFORM)
        (crBkColorSrc :unsigned-long)
        (iUsageSrc :unsigned-long)
        (offBmiSrc :unsigned-long)
        (cbBmiSrc :unsigned-long)
        (offBitsSrc :unsigned-long)
        (cbBitsSrc :unsigned-long)
        (xMask :int32)
        (yMask :int32)
        (iUsageMask :unsigned-long)
        (offBmiMask :unsigned-long)
        (cbBmiMask :unsigned-long)
        (offBitsMask :unsigned-long)
        (cbBitsMask :unsigned-long))





(defcstructex-exported EMRMODIFYWORLDTRANSFORM
        (emr EMR)
        (xform XFORM)
        (iMode :unsigned-long))





(defcstructex-exported EMROFFSETCLIPRGN
        (emr EMR)
        (ptlOffset :pointer))





(defcstructex-exported EMRPLGBLT
        (emr EMR)
        (rclBounds RECTL)
        (aptlDest :pointer)
        (xSrc :int32)
        (ySrc :int32)
        (cxSrc :int32)
        (cySrc :int32)
        (xformSrc XFORM)
        (crBkColorSrc :unsigned-long)
        (iUsageSrc :unsigned-long)
        (offBmiSrc :unsigned-long)
        (cbBmiSrc :unsigned-long)
        (offBitsSrc :unsigned-long)
        (cbBitsSrc :unsigned-long)
        (xMask :int32)
        (yMask :int32)
        (iUsageMask :unsigned-long)
        (offBmiMask :unsigned-long)
        (cbBmiMask :unsigned-long)
        (offBitsMask :unsigned-long)
        (cbBitsMask :unsigned-long))





(defcstructex-exported EMRPOLYDRAW
        (emr EMR)
        (rclBounds RECTL)
        (cptl :unsigned-long)
        (aptl :pointer)
        (abTypes :pointer))





(defcstructex-exported EMRPOLYDRAW16
        (emr EMR)
        (rclBounds RECTL)
        (cpts :unsigned-long)
        (apts :pointer)
        (abTypes :pointer))





(defcstructex-exported EMRPOLYLINE
        (emr EMR)
        (rclBounds RECTL)
        (cptl :unsigned-long)
        (aptl :pointer))





















(defcstructex-exported EMRPOLYLINE16
        (emr EMR)
        (rclBounds RECTL)
        (cpts :unsigned-long)
        (apts :pointer))





















(defcstructex-exported EMRPOLYPOLYLINE
        (emr EMR)
        (rclBounds RECTL)
        (nPolys :unsigned-long)
        (cptl :unsigned-long)
        (aPolyCounts :pointer)
        (aptl :pointer))









(defcstructex-exported EMRPOLYPOLYLINE16
        (emr EMR)
        (rclBounds RECTL)
        (nPolys :unsigned-long)
        (cpts :unsigned-long)
        (aPolyCounts :pointer)
        (apts :pointer))









(defcstructex-exported EMRPOLYTEXTOUTA
        (emr EMR)
        (rclBounds RECTL)
        (iGraphicsMode :unsigned-long)
        (exScale :float)
        (eyScale :float)
        (cStrings :int32)
        (aemrtext :pointer))









(defcstructex-exported EMRRESIZEPALETTE
        (emr EMR)
        (ihPal :unsigned-long)
        (cEntries :unsigned-long))





(defcstructex-exported EMRRESTOREDC
        (emr EMR)
        (iRelative :int32))





(defcstructex-exported EMRROUNDRECT
        (emr EMR)
        (rclBox RECTL)
        (szlCorner :pointer))





(defcstructex-exported EMRSCALEVIEWPORTEXTEX
        (emr EMR)
        (xNum :int32)
        (xDenom :int32)
        (yNum :int32)
        (yDenom :int32))









(defcstructex-exported EMRSELECTCOLORSPACE
        (emr EMR)
        (ihCS :unsigned-long))









(defcstructex-exported EMRSELECTOBJECT
        (emr EMR)
        (ihObject :unsigned-long))









(defcstructex-exported EMRSELECTPALETTE
        (emr EMR)
        (ihPal :unsigned-long))





(defcstructex-exported EMRSETARCDIRECTION
        (emr EMR)
        (iArcDirection :unsigned-long))





(defcstructex-exported EMRSETBKCOLOR
        (emr EMR)
        (crColor :unsigned-long))









(defcstructex-exported EMRSETCOLORADJUSTMENT
        (emr EMR)
        (ColorAdjustment COLORADJUSTMENT))





(defcstructex-exported EMRSETDIBITSTODEVICE
        (emr EMR)
        (rclBounds RECTL)
        (xDest :int32)
        (yDest :int32)
        (xSrc :int32)
        (ySrc :int32)
        (cxSrc :int32)
        (cySrc :int32)
        (offBmiSrc :unsigned-long)
        (cbBmiSrc :unsigned-long)
        (offBitsSrc :unsigned-long)
        (cbBitsSrc :unsigned-long)
        (iUsageSrc :unsigned-long)
        (iStartScan :unsigned-long)
        (cScans :unsigned-long))





(defcstructex-exported EMRSETMAPPERFLAGS
        (emr EMR)
        (dwFlags :unsigned-long))





(defcstructex-exported EMRSETMITERLIMIT
        (emr EMR)
        (eMiterLimit :float))





(defcstructex-exported EMRSETPALETTEENTRIES
        (emr EMR)
        (ihPal :unsigned-long)
        (iStart :unsigned-long)
        (cEntries :unsigned-long)
        (aPalEntries :pointer))





(defcstructex-exported EMRSETPIXELV
        (emr EMR)
        (ptlPixel :pointer)
        (crColor :unsigned-long))





(defcstructex-exported EMRSETVIEWPORTEXTEX
        (emr EMR)
        (szlExtent :pointer))









(defcstructex-exported EMRSETVIEWPORTORGEX
        (emr EMR)
        (ptlOrigin :pointer))













(defcstructex-exported EMRSETWORLDTRANSFORM
        (emr EMR)
        (xform XFORM))





(defcstructex-exported EMRSTRETCHBLT
        (emr EMR)
        (rclBounds RECTL)
        (xDest :int32)
        (yDest :int32)
        (cxDest :int32)
        (cyDest :int32)
        (dwRop :unsigned-long)
        (xSrc :int32)
        (ySrc :int32)
        (xformSrc XFORM)
        (crBkColorSrc :unsigned-long)
        (iUsageSrc :unsigned-long)
        (offBmiSrc :unsigned-long)
        (cbBmiSrc :unsigned-long)
        (offBitsSrc :unsigned-long)
        (cbBitsSrc :unsigned-long)
        (cxSrc :int32)
        (cySrc :int32))





(defcstructex-exported EMRSTRETCHDIBITS
        (emr EMR)
        (rclBounds RECTL)
        (xDest :int32)
        (yDest :int32)
        (xSrc :int32)
        (ySrc :int32)
        (cxSrc :int32)
        (cySrc :int32)
        (offBmiSrc :unsigned-long)
        (cbBmiSrc :unsigned-long)
        (offBitsSrc :unsigned-long)
        (cbBitsSrc :unsigned-long)
        (iUsageSrc :unsigned-long)
        (dwRop :unsigned-long)
        (cxDest :int32)
        (cyDest :int32))





(defcstructex-exported EMRABORTPATH
        (emr EMR))





































(defcstructex-exported EMRSELECTCLIPPATH
        (emr EMR)
        (iMode :unsigned-long))

































(defcstructex-exported METAHEADER
        (mtType :unsigned-short)
        (mtHeaderSize :unsigned-short)
        (mtVersion :unsigned-short)
        (mtSize :unsigned-long)
        (mtNoObjects :unsigned-short)
        (mtMaxRecord :unsigned-long)
        (mtNoParameters :unsigned-short))







(defcstructex-exported ENHMETAHEADER
        (iType :unsigned-long)
        (nSize :unsigned-long)
        (rclBounds RECTL)
        (rclFrame RECTL)
        (dSignature :unsigned-long)
        (nVersion :unsigned-long)
        (nBytes :unsigned-long)
        (nRecords :unsigned-long)
        (nHandles :unsigned-short)
        (sReserved :unsigned-short)
        (nDescription :unsigned-long)
        (offDescription :unsigned-long)
        (nPalEntries :unsigned-long)
        (szlDevice :pointer)
        (szlMillimeters :pointer)
        (cbPixelFormat :unsigned-long)
        (offPixelFormat :unsigned-long)
        (bOpenGL :unsigned-long))





(defcstructex-exported METARECORD
        (rdSize :unsigned-long)
        (rdFunction :unsigned-short)
        (rdParm :pointer))







(defcstructex-exported ENHMETARECORD
        (iType :unsigned-long)
        (nSize :unsigned-long)
        (dParm :pointer))





(defcstructex-exported HANDLETABLE
        (objectHandle :pointer))





(defcstructex-exported TEXTMETRICA
        (tmHeight :int32)
        (tmAscent :int32)
        (tmDescent :int32)
        (tmInternalLeading :int32)
        (tmExternalLeading :int32)
        (tmAveCharWidth :int32)
        (tmMaxCharWidth :int32)
        (tmWeight :int32)
        (tmOverhang :int32)
        (tmDigitizedAspectX :int32)
        (tmDigitizedAspectY :int32)
        (tmFirstChar :unsigned-char)
        (tmLastChar :unsigned-char)
        (tmDefaultChar :unsigned-char)
        (tmBreakChar :unsigned-char)
        (tmItalic :unsigned-char)
        (tmUnderlined :unsigned-char)
        (tmStruckOut :unsigned-char)
        (tmPitchAndFamily :unsigned-char)
        (tmCharSet :unsigned-char))







(defcstructex-exported TEXTMETRICW
        (tmHeight :int32)
        (tmAscent :int32)
        (tmDescent :int32)
        (tmInternalLeading :int32)
        (tmExternalLeading :int32)
        (tmAveCharWidth :int32)
        (tmMaxCharWidth :int32)
        (tmWeight :int32)
        (tmOverhang :int32)
        (tmDigitizedAspectX :int32)
        (tmDigitizedAspectY :int32)
        (tmFirstChar :pointer)
        (tmLastChar :pointer)
        (tmDefaultChar :pointer)
        (tmBreakChar :pointer)
        (tmItalic :unsigned-char)
        (tmUnderlined :unsigned-char)
        (tmStruckOut :unsigned-char)
        (tmPitchAndFamily :unsigned-char)
        (tmCharSet :unsigned-char))







(defcstructex-exported RGNDATAHEADER
        (dwSize :unsigned-long)
        (iType :unsigned-long)
        (nCount :unsigned-long)
        (nRgnSize :unsigned-long)
        (rcBound RECT))



(defcstructex-exported RGNDATA
        (rdh RGNDATAHEADER)
        (Buffer :pointer))







(defconstant-exported SYSRGN 4)

(defcstructex-exported GCP_RESULTSA
        (lStructSize :unsigned-long)
        (lpOutString :string)
        (lpOrder :pointer)
        (lpDx :pointer)
        (lpCaretPos :pointer)
        (lpClass :string)
        (lpGlyphs :pointer)
        (nGlyphs :unsigned-int)
        (nMaxFit :unsigned-int))





(defcstructex-exported GCP_RESULTSW
        (lStructSize :unsigned-long)
        (lpOutString :pointer)
        (lpOrder :pointer)
        (lpDx :pointer)
        (lpCaretPos :pointer)
        (lpClass :pointer)
        (lpGlyphs :pointer)
        (nGlyphs :unsigned-int)
        (nMaxFit :unsigned-int))





(defcstructex-exported GLYPHMETRICS
        (gmBlackBoxX :unsigned-int)
        (gmBlackBoxY :unsigned-int)
        (gmptGlyphOrigin POINT)
        (gmCellIncX :short)
        (gmCellIncY :short))





(defcstructex-exported KERNINGPAIR
        (wFirst :unsigned-short)
        (wSecond :unsigned-short)
        (iKernAmount :int))





(defcstructex-exported FIXED
        (fract :unsigned-short)
        (value :short))



(defcstructex-exported MAT2
        (eM11 FIXED)
        (eM12 FIXED)
        (eM21 FIXED)
        (eM22 FIXED))





(defcstructex-exported OUTLINETEXTMETRICA
        (otmSize :unsigned-int)
        (otmTextMetrics TEXTMETRICA)
        (otmFiller :unsigned-char)
        (otmPanoseNumber PANOSE)
        (otmfsSelection :unsigned-int)
        (otmfsType :unsigned-int)
        (otmsCharSlopeRise :int)
        (otmsCharSlopeRun :int)
        (otmItalicAngle :int)
        (otmEMSquare :unsigned-int)
        (otmAscent :int)
        (otmDescent :int)
        (otmLineGap :unsigned-int)
        (otmsCapEmHeight :unsigned-int)
        (otmsXHeight :unsigned-int)
        (otmrcFontBox RECT)
        (otmMacAscent :int)
        (otmMacDescent :int)
        (otmMacLineGap :unsigned-int)
        (otmusMinimumPPEM :unsigned-int)
        (otmptSubscriptSize POINT)
        (otmptSubscriptOffset POINT)
        (otmptSuperscriptSize POINT)
        (otmptSuperscriptOffset POINT)
        (otmsStrikeoutSize :unsigned-int)
        (otmsStrikeoutPosition :int)
        (otmsUnderscoreSize :int)
        (otmsUnderscorePosition :int)
        (otmpFamilyName :string)
        (otmpFaceName :string)
        (otmpStyleName :string)
        (otmpFullName :string))







(defcstructex-exported OUTLINETEXTMETRICW
        (otmSize :unsigned-int)
        (otmTextMetrics TEXTMETRICW)
        (otmFiller :unsigned-char)
        (otmPanoseNumber PANOSE)
        (otmfsSelection :unsigned-int)
        (otmfsType :unsigned-int)
        (otmsCharSlopeRise :int)
        (otmsCharSlopeRun :int)
        (otmItalicAngle :int)
        (otmEMSquare :unsigned-int)
        (otmAscent :int)
        (otmDescent :int)
        (otmLineGap :unsigned-int)
        (otmsCapEmHeight :unsigned-int)
        (otmsXHeight :unsigned-int)
        (otmrcFontBox RECT)
        (otmMacAscent :int)
        (otmMacDescent :int)
        (otmMacLineGap :unsigned-int)
        (otmusMinimumPPEM :unsigned-int)
        (otmptSubscriptSize POINT)
        (otmptSubscriptOffset POINT)
        (otmptSuperscriptSize POINT)
        (otmptSuperscriptOffset POINT)
        (otmsStrikeoutSize :unsigned-int)
        (otmsStrikeoutPosition :int)
        (otmsUnderscoreSize :int)
        (otmsUnderscorePosition :int)
        (otmpFamilyName :string)
        (otmpFaceName :string)
        (otmpStyleName :string)
        (otmpFullName :string))







(defcstructex-exported RASTERIZER_STATUS
        (nSize :short)
        (wFlags :short)
        (nLanguageID :short))





(defcstructex-exported POLYTEXTA
        (x :int)
        (y :int)
        (n :unsigned-int)
        (lpstr :string)
        (uiFlags :unsigned-int)
        (rcl RECT)
        (pdx :pointer))







(defcstructex-exported POLYTEXTW
        (x :int)
        (y :int)
        (n :unsigned-int)
        (lpstr :pointer)
        (uiFlags :unsigned-int)
        (rcl RECT)
        (pdx :pointer))







(defcstructex-exported PIXELFORMATDESCRIPTOR
        (nSize :unsigned-short)
        (nVersion :unsigned-short)
        (dwFlags :unsigned-long)
        (iPixelType :unsigned-char)
        (cColorBits :unsigned-char)
        (cRedBits :unsigned-char)
        (cRedShift :unsigned-char)
        (cGreenBits :unsigned-char)
        (cGreenShift :unsigned-char)
        (cBlueBits :unsigned-char)
        (cBlueShift :unsigned-char)
        (cAlphaBits :unsigned-char)
        (cAlphaShift :unsigned-char)
        (cAccumBits :unsigned-char)
        (cAccumRedBits :unsigned-char)
        (cAccumGreenBits :unsigned-char)
        (cAccumBlueBits :unsigned-char)
        (cAccumAlphaBits :unsigned-char)
        (cDepthBits :unsigned-char)
        (cStencilBits :unsigned-char)
        (cAuxBuffers :unsigned-char)
        (iLayerType :unsigned-char)
        (bReserved :unsigned-char)
        (dwLayerMask :unsigned-long)
        (dwVisibleMask :unsigned-long)
        (dwDamageMask :unsigned-long))







(defcstructex-exported METAFILEPICT
        (mm :int32)
        (xExt :int32)
        (yExt :int32)
        (hMF :pointer))





(defcstructex-exported LOCALESIGNATURE
        (lsUsb :pointer)
        (lsCsbDefault :pointer)
        (lsCsbSupported :pointer))









(defcstructex-exported NEWTEXTMETRICA
        (tmHeight :int32)
        (tmAscent :int32)
        (tmDescent :int32)
        (tmInternalLeading :int32)
        (tmExternalLeading :int32)
        (tmAveCharWidth :int32)
        (tmMaxCharWidth :int32)
        (tmWeight :int32)
        (tmOverhang :int32)
        (tmDigitizedAspectX :int32)
        (tmDigitizedAspectY :int32)
        (tmFirstChar :unsigned-char)
        (tmLastChar :unsigned-char)
        (tmDefaultChar :unsigned-char)
        (tmBreakChar :unsigned-char)
        (tmItalic :unsigned-char)
        (tmUnderlined :unsigned-char)
        (tmStruckOut :unsigned-char)
        (tmPitchAndFamily :unsigned-char)
        (tmCharSet :unsigned-char)
        (ntmFlags :unsigned-long)
        (ntmSizeEM :unsigned-int)
        (ntmCellHeight :unsigned-int)
        (ntmAvgWidth :unsigned-int))







(defcstructex-exported NEWTEXTMETRICW
        (tmHeight :int32)
        (tmAscent :int32)
        (tmDescent :int32)
        (tmInternalLeading :int32)
        (tmExternalLeading :int32)
        (tmAveCharWidth :int32)
        (tmMaxCharWidth :int32)
        (tmWeight :int32)
        (tmOverhang :int32)
        (tmDigitizedAspectX :int32)
        (tmDigitizedAspectY :int32)
        (tmFirstChar :pointer)
        (tmLastChar :pointer)
        (tmDefaultChar :pointer)
        (tmBreakChar :pointer)
        (tmItalic :unsigned-char)
        (tmUnderlined :unsigned-char)
        (tmStruckOut :unsigned-char)
        (tmPitchAndFamily :unsigned-char)
        (tmCharSet :unsigned-char)
        (ntmFlags :unsigned-long)
        (ntmSizeEM :unsigned-int)
        (ntmCellHeight :unsigned-int)
        (ntmAvgWidth :unsigned-int))







(defcstructex-exported NEWTEXTMETRICEXA
        (ntmTm NEWTEXTMETRICA)
        (ntmFontSig FONTSIGNATURE))



(defcstructex-exported NEWTEXTMETRICEXW
        (ntmTm NEWTEXTMETRICW)
        (ntmFontSig FONTSIGNATURE))



(defcstructex-exported PELARRAY
        (paXCount :int32)
        (paYCount :int32)
        (paXExt :int32)
        (paYExt :int32)
        (paRGBs :unsigned-char))







(defcstructex-exported ENUMLOGFONTA
        (elfLogFont LOGFONTA)
        (elfFullName :pointer)
        (elfStyle :pointer))





(defcstructex-exported ENUMLOGFONTW
        (elfLogFont LOGFONTW)
        (elfFullName :pointer)
        (elfStyle :pointer))





(defcstructex-exported ENUMLOGFONTEXA
        (elfLogFont LOGFONTA)
        (elfFullName :pointer)
        (elfStyle :pointer)
        (elfScript :pointer))





(defcstructex-exported ENUMLOGFONTEXW
        (elfLogFont LOGFONTW)
        (elfFullName :pointer)
        (elfStyle :pointer)
        (elfScript :pointer))





(defcstructex-exported POINTFX
        (x FIXED)
        (y FIXED))





(defcstructex-exported TTPOLYCURVE
        (wType :unsigned-short)
        (cpfx :unsigned-short)
        (apfx :pointer))





(defcstructex-exported TTPOLYGONHEADER
        (cb :unsigned-long)
        (dwType :unsigned-long)
        (pfxStart POINTFX))





(defcstructex-exported POINTFLOAT
        (x :float)
        (y :float))





(defcstructex-exported GLYPHMETRICSFLOAT
        (gmfBlackBoxX :float)
        (gmfBlackBoxY :float)
        (gmfptGlyphOrigin POINTFLOAT)
        (gmfCellIncX :float)
        (gmfCellIncY :float))







(defcstructex-exported LAYERPLANEDESCRIPTOR
        (nSize :unsigned-short)
        (nVersion :unsigned-short)
        (dwFlags :unsigned-long)
        (iPixelType :unsigned-char)
        (cColorBits :unsigned-char)
        (cRedBits :unsigned-char)
        (cRedShift :unsigned-char)
        (cGreenBits :unsigned-char)
        (cGreenShift :unsigned-char)
        (cBlueBits :unsigned-char)
        (cBlueShift :unsigned-char)
        (cAlphaBits :unsigned-char)
        (cAlphaShift :unsigned-char)
        (cAccumBits :unsigned-char)
        (cAccumRedBits :unsigned-char)
        (cAccumGreenBits :unsigned-char)
        (cAccumBlueBits :unsigned-char)
        (cAccumAlphaBits :unsigned-char)
        (cDepthBits :unsigned-char)
        (cStencilBits :unsigned-char)
        (cAuxBuffers :unsigned-char)
        (iLayerPlane :unsigned-char)
        (bReserved :unsigned-char)
        (crTransparent :unsigned-long))







(defcstructex-exported BLENDFUNCTION
        (BlendOp :unsigned-char)
        (BlendFlags :unsigned-char)
        (SourceConstantAlpha :unsigned-char)
        (AlphaFormat :unsigned-char))







(defconstant-exported MM_MAX_NUMAXES 16)

(defcstructex-exported DESIGNVECTOR
        (dvReserved :unsigned-long)
        (dvNumAxes :unsigned-long)
        (dvValues :pointer))









(defcstructex-exported TRIVERTEX
        (x :int32)
        (y :int32)
        (Red :unsigned-short)
        (Green :unsigned-short)
        (Blue :unsigned-short)
        (Alpha :unsigned-short))







(defcstructex-exported GRADIENT_TRIANGLE
        (Vertex1 :unsigned-long)
        (Vertex2 :unsigned-long)
        (Vertex3 :unsigned-long))







(defcstructex-exported GRADIENT_RECT
        (UpperLeft :unsigned-long)
        (LowerRight :unsigned-long))







(defcstructex-exported DISPLAY_DEVICEA
        (cb :unsigned-long)
        (DeviceName :pointer)
        (DeviceString :pointer)
        (StateFlags :unsigned-long)
        (DeviceID :pointer)
        (DeviceKey :pointer))







(defcstructex-exported DISPLAY_DEVICEW
        (cb :unsigned-long)
        (DeviceName :pointer)
        (DeviceString :pointer)
        (StateFlags :unsigned-long)
        (DeviceID :pointer)
        (DeviceKey :pointer))

































(defcfunex-exported ("ChoosePixelFormat" ChoosePixelFormat :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))


(defcfunex-exported ("DescribePixelFormat" DescribePixelFormat :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetAspectRatioFilterEx" GetAspectRatioFilterEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetDCOrgEx" GetDCOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetDeviceCaps" GetDeviceCaps :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))


(defcfunex-exported ("GetEnhMetaFilePixelFormat" GetEnhMetaFilePixelFormat :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))


(defcfunex-exported ("GetPixelFormat" GetPixelFormat :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetRandomRgn" GetRandomRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetStockObject" GetStockObject :convention :stdcall) :pointer
  (arg0 :int))

(defcfunex-exported ("SetPixelFormat" SetPixelFormat :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("SwapBuffers" SwapBuffers :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("wglCopyContext" wglCopyContext :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("wglCreateContext" wglCreateContext :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("wglCreateLayerContext" wglCreateLayerContext :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("wglDeleteContext" wglDeleteContext :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("wglDescribeLayerPlane" wglDescribeLayerPlane :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("wglGetCurrentContext" wglGetCurrentContext :convention :stdcall) :pointer)

(defcfunex-exported ("wglGetCurrentDC" wglGetCurrentDC :convention :stdcall) :pointer)

(defcfunex-exported ("wglGetLayerPaletteEntries" wglGetLayerPaletteEntries :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("wglGetProcAddress" wglGetProcAddress :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("wglMakeCurrent" wglMakeCurrent :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("wglRealizeLayerPalette" wglRealizeLayerPalette :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("wglSetLayerPaletteEntries" wglSetLayerPaletteEntries :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("wglShareLists" wglShareLists :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("wglSwapLayerBuffers" wglSwapLayerBuffers :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("wglUseFontBitmapsA" wglUseFontBitmapsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("wglUseFontBitmapsW" wglUseFontBitmapsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("wglUseFontOutlinesA" wglUseFontOutlinesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :float)
  (arg5 :float)
  (arg6 :int)
  (arg7 :pointer))

(defcfunex-exported ("wglUseFontOutlinesW" wglUseFontOutlinesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :float)
  (arg5 :float)
  (arg6 :int)
  (arg7 :pointer))









































































(defconstant-exported FALT 16)

(defconstant-exported FCONTROL 8)

(defconstant-exported FNOINVERT 2)

(defconstant-exported FSHIFT 4)

(defconstant-exported FVIRTKEY 1)

(defconstant-exported ATF_TIMEOUTON 1)

(defconstant-exported ATF_ONOFFFEEDBACK 2)

(defconstant-exported ATF_AVAILABLE 4)

(defconstant-exported WH_MIN -1)

(defconstant-exported WH_MSGFILTER -1)

(defconstant-exported WH_JOURNALRECORD 0)

(defconstant-exported WH_JOURNALPLAYBACK 1)

(defconstant-exported WH_KEYBOARD 2)

(defconstant-exported WH_GETMESSAGE 3)

(defconstant-exported WH_CALLWNDPROC 4)

(defconstant-exported WH_CBT 5)

(defconstant-exported WH_SYSMSGFILTER 6)

(defconstant-exported WH_MOUSE 7)

(defconstant-exported WH_HARDWARE 8)

(defconstant-exported WH_DEBUG 9)

(defconstant-exported WH_SHELL 10)

(defconstant-exported WH_FOREGROUNDIDLE 11)

(defconstant-exported WH_CALLWNDPROCRET 12)

(defconstant-exported WH_KEYBOARD_LL 13)

(defconstant-exported WH_MOUSE_LL 14)

(defconstant-exported WH_MAX 14)

(defconstant-exported WH_MINHOOK -1)

(defconstant-exported WH_MAXHOOK 14)

(defconstant-exported HC_ACTION 0)

(defconstant-exported HC_GETNEXT 1)

(defconstant-exported HC_SKIP 2)

(defconstant-exported HC_NOREMOVE 3)

(defconstant-exported HC_NOREM 3)

(defconstant-exported HC_SYSMODALON 4)

(defconstant-exported HC_SYSMODALOFF 5)

(defconstant-exported HCBT_MOVESIZE 0)

(defconstant-exported HCBT_MINMAX 1)

(defconstant-exported HCBT_QS 2)

(defconstant-exported HCBT_CREATEWND 3)

(defconstant-exported HCBT_DESTROYWND 4)

(defconstant-exported HCBT_ACTIVATE 5)

(defconstant-exported HCBT_CLICKSKIPPED 6)

(defconstant-exported HCBT_KEYSKIPPED 7)

(defconstant-exported HCBT_SYSCOMMAND 8)

(defconstant-exported HCBT_SETFOCUS 9)

(defconstant-exported CF_TEXT 1)

(defconstant-exported CF_BITMAP 2)

(defconstant-exported CF_METAFILEPICT 3)

(defconstant-exported CF_SYLK 4)

(defconstant-exported CF_DIF 5)

(defconstant-exported CF_TIFF 6)

(defconstant-exported CF_OEMTEXT 7)

(defconstant-exported CF_DIB 8)

(defconstant-exported CF_PALETTE 9)

(defconstant-exported CF_PENDATA 10)

(defconstant-exported CF_RIFF 11)

(defconstant-exported CF_WAVE 12)

(defconstant-exported CF_UNICODETEXT 13)

(defconstant-exported CF_ENHMETAFILE 14)

(defconstant-exported CF_HDROP 15)

(defconstant-exported CF_LOCALE 16)

(defconstant-exported CF_MAX 17)

(defconstant-exported CF_OWNERDISPLAY 128)

(defconstant-exported CF_DSPTEXT 129)

(defconstant-exported CF_DSPBITMAP 130)

(defconstant-exported CF_DSPMETAFILEPICT 131)

(defconstant-exported CF_DSPENHMETAFILE 142)

(defconstant-exported CF_PRIVATEFIRST 512)

(defconstant-exported CF_PRIVATELAST 767)

(defconstant-exported CF_GDIOBJFIRST 768)

(defconstant-exported CF_GDIOBJLAST 1023)

(defconstant-exported HKL_NEXT 1)

(defconstant-exported HKL_PREV 0)

(defconstant-exported KLF_ACTIVATE 1)

(defconstant-exported KLF_SUBSTITUTE_OK 2)

(defconstant-exported KLF_UNLOADPREVIOUS 4)

(defconstant-exported KLF_REORDER 8)

(defconstant-exported KLF_REPLACELANG 16)

(defconstant-exported KLF_NOTELLSHELL 128)

(defconstant-exported KLF_SETFORPROCESS 256)

(defconstant-exported KL_NAMELENGTH 9)

(defconstant-exported MF_ENABLED 0)

(defconstant-exported MF_GRAYED 1)

(defconstant-exported MF_DISABLED 2)

(defconstant-exported MF_BITMAP 4)

(defconstant-exported MF_CHECKED 8)

(defconstant-exported MF_MENUBARBREAK 32)

(defconstant-exported MF_MENUBREAK 64)

(defconstant-exported MF_OWNERDRAW 256)

(defconstant-exported MF_POPUP 16)

(defconstant-exported MF_SEPARATOR #x800)

(defconstant-exported MF_STRING 0)

(defconstant-exported MF_UNCHECKED 0)

(defconstant-exported MF_DEFAULT 4096)

(defconstant-exported MF_SYSMENU #x2000)

(defconstant-exported MF_HELP #x4000)

(defconstant-exported MF_END 128)

(defconstant-exported MF_RIGHTJUSTIFY #x4000)

(defconstant-exported MF_MOUSESELECT #x8000)

(defconstant-exported MF_INSERT 0)

(defconstant-exported MF_CHANGE 128)

(defconstant-exported MF_APPEND 256)

(defconstant-exported MF_DELETE 512)

(defconstant-exported MF_REMOVE 4096)

(defconstant-exported MF_USECHECKBITMAPS 512)

(defconstant-exported MF_UNHILITE 0)

(defconstant-exported MF_HILITE 128)

(defconstant-exported BSM_ALLCOMPONENTS 0)

(defconstant-exported BSM_APPLICATIONS 8)

(defconstant-exported BSM_ALLDESKTOPS 16)

(defconstant-exported BSM_INSTALLABLEDRIVERS 4)

(defconstant-exported BSM_NETDRIVER 2)

(defconstant-exported BSM_VXDS 1)

(defconstant-exported BSF_FLUSHDISK #x00000004)

(defconstant-exported BSF_FORCEIFHUNG #x00000020)

(defconstant-exported BSF_IGNORECURRENTTASK #x00000002)

(defconstant-exported BSF_NOHANG #x00000008)

(defconstant-exported BSF_NOTIMEOUTIFNOTHUNG #x00000040)

(defconstant-exported BSF_POSTMESSAGE #x00000010)

(defconstant-exported BSF_QUERY #x00000001)

(defconstant-exported BROADCAST_QUERY_DENY 1112363332)

(defconstant-exported CDS_UPDATEREGISTRY 1)

(defconstant-exported CDS_TEST 2)

(defconstant-exported CDS_FULLSCREEN 4)

(defconstant-exported CDS_GLOBAL 8)

(defconstant-exported CDS_SET_PRIMARY 16)

(defconstant-exported CDS_RESET #x40000000)

(defconstant-exported CDS_SETRECT #x20000000)

(defconstant-exported CDS_NORESET #x10000000)

(defconstant-exported DISP_CHANGE_SUCCESSFUL 0)

(defconstant-exported DISP_CHANGE_RESTART 1)

(defconstant-exported DISP_CHANGE_BADFLAGS -4)

(defconstant-exported DISP_CHANGE_BADPARAM -5)

(defconstant-exported DISP_CHANGE_FAILED -1)

(defconstant-exported DISP_CHANGE_BADMODE -2)

(defconstant-exported DISP_CHANGE_NOTUPDATED -3)

(defconstant-exported BST_CHECKED 1)

(defconstant-exported BST_INDETERMINATE 2)

(defconstant-exported BST_UNCHECKED 0)

(defconstant-exported BST_FOCUS 8)

(defconstant-exported BST_PUSHED 4)

(defconstant-exported MF_BYCOMMAND 0)

(defconstant-exported MF_BYPOSITION 1024)

(defconstant-exported CWP_ALL 0)

(defconstant-exported CWP_SKIPINVISIBLE 1)

(defconstant-exported CWP_SKIPDISABLED 2)

(defconstant-exported CWP_SKIPTRANSPARENT 4)

(defconstant-exported IMAGE_BITMAP 0)

(defconstant-exported IMAGE_ICON 1)

(defconstant-exported IMAGE_CURSOR 2)

(defconstant-exported IMAGE_ENHMETAFILE 3)

(defconstant-exported DF_ALLOWOTHERACCOUNTHOOK 1)

(defconstant-exported DESKTOP_CREATEMENU 4)

(defconstant-exported DESKTOP_CREATEWINDOW 2)

(defconstant-exported DESKTOP_ENUMERATE 64)

(defconstant-exported DESKTOP_HOOKCONTROL 8)

(defconstant-exported DESKTOP_JOURNALPLAYBACK 32)

(defconstant-exported DESKTOP_JOURNALRECORD 16)

(defconstant-exported DESKTOP_READOBJECTS 1)

(defconstant-exported DESKTOP_SWITCHDESKTOP 256)

(defconstant-exported DESKTOP_WRITEOBJECTS 128)

(defconstant-exported CW_USEDEFAULT #x80000000)

(defconstant-exported WS_BORDER #x800000)

(defconstant-exported WS_CAPTION #xc00000)

(defconstant-exported WS_CHILD #x40000000)

(defconstant-exported WS_CHILDWINDOW #x40000000)

(defconstant-exported WS_CLIPCHILDREN #x2000000)

(defconstant-exported WS_CLIPSIBLINGS #x4000000)

(defconstant-exported WS_DISABLED #x8000000)

(defconstant-exported WS_DLGFRAME #x400000)

(defconstant-exported WS_GROUP #x20000)

(defconstant-exported WS_HSCROLL #x100000)

(defconstant-exported WS_ICONIC #x20000000)

(defconstant-exported WS_MAXIMIZE #x1000000)

(defconstant-exported WS_MAXIMIZEBOX #x10000)

(defconstant-exported WS_MINIMIZE #x20000000)

(defconstant-exported WS_MINIMIZEBOX #x20000)

(defconstant-exported WS_OVERLAPPED 0)

(defconstant-exported WS_OVERLAPPEDWINDOW #xcf0000)

(defconstant-exported WS_POPUP #x80000000)

(defconstant-exported WS_POPUPWINDOW #x80880000)

(defconstant-exported WS_SIZEBOX #x40000)

(defconstant-exported WS_SYSMENU #x80000)

(defconstant-exported WS_TABSTOP #x10000)

(defconstant-exported WS_THICKFRAME #x40000)

(defconstant-exported WS_TILED 0)

(defconstant-exported WS_TILEDWINDOW #xcf0000)

(defconstant-exported WS_VISIBLE #x10000000)

(defconstant-exported WS_VSCROLL #x200000)

(defconstant-exported MDIS_ALLCHILDSTYLES 1)

(defconstant-exported BS_3STATE 5)

(defconstant-exported BS_AUTO3STATE 6)

(defconstant-exported BS_AUTOCHECKBOX 3)

(defconstant-exported BS_AUTORADIOBUTTON 9)

(defconstant-exported BS_BITMAP 128)

(defconstant-exported BS_BOTTOM #x800)

(defconstant-exported BS_CENTER #x300)

(defconstant-exported BS_CHECKBOX 2)

(defconstant-exported BS_DEFPUSHBUTTON 1)

(defconstant-exported BS_GROUPBOX 7)

(defconstant-exported BS_ICON 64)

(defconstant-exported BS_LEFT 256)

(defconstant-exported BS_LEFTTEXT 32)

(defconstant-exported BS_MULTILINE #x2000)

(defconstant-exported BS_NOTIFY #x4000)

(defconstant-exported BS_OWNERDRAW #xb)

(defconstant-exported BS_PUSHBUTTON 0)

(defconstant-exported BS_PUSHLIKE 4096)

(defconstant-exported BS_RADIOBUTTON 4)

(defconstant-exported BS_RIGHT 512)

(defconstant-exported BS_RIGHTBUTTON 32)

(defconstant-exported BS_TEXT 0)

(defconstant-exported BS_TOP #x400)

(defconstant-exported BS_USERBUTTON 8)

(defconstant-exported BS_VCENTER #xc00)

(defconstant-exported BS_FLAT #x8000)

(defconstant-exported CBS_AUTOHSCROLL 64)

(defconstant-exported CBS_DISABLENOSCROLL #x800)

(defconstant-exported CBS_DROPDOWN 2)

(defconstant-exported CBS_DROPDOWNLIST 3)

(defconstant-exported CBS_HASSTRINGS 512)

(defconstant-exported CBS_LOWERCASE #x4000)

(defconstant-exported CBS_NOINTEGRALHEIGHT #x400)

(defconstant-exported CBS_OEMCONVERT 128)

(defconstant-exported CBS_OWNERDRAWFIXED 16)

(defconstant-exported CBS_OWNERDRAWVARIABLE 32)

(defconstant-exported CBS_SIMPLE 1)

(defconstant-exported CBS_SORT 256)

(defconstant-exported CBS_UPPERCASE #x2000)

(defconstant-exported ES_AUTOHSCROLL 128)

(defconstant-exported ES_AUTOVSCROLL 64)

(defconstant-exported ES_CENTER 1)

(defconstant-exported ES_LEFT 0)

(defconstant-exported ES_LOWERCASE 16)

(defconstant-exported ES_MULTILINE 4)

(defconstant-exported ES_NOHIDESEL 256)

(defconstant-exported ES_NUMBER #x2000)

(defconstant-exported ES_OEMCONVERT #x400)

(defconstant-exported ES_PASSWORD 32)

(defconstant-exported ES_READONLY #x800)

(defconstant-exported ES_RIGHT 2)

(defconstant-exported ES_UPPERCASE 8)

(defconstant-exported ES_WANTRETURN 4096)

(defconstant-exported LBS_DISABLENOSCROLL 4096)

(defconstant-exported LBS_EXTENDEDSEL #x800)

(defconstant-exported LBS_HASSTRINGS 64)

(defconstant-exported LBS_MULTICOLUMN 512)

(defconstant-exported LBS_MULTIPLESEL 8)

(defconstant-exported LBS_NODATA #x2000)

(defconstant-exported LBS_NOINTEGRALHEIGHT 256)

(defconstant-exported LBS_NOREDRAW 4)

(defconstant-exported LBS_NOSEL #x4000)

(defconstant-exported LBS_NOTIFY 1)

(defconstant-exported LBS_OWNERDRAWFIXED 16)

(defconstant-exported LBS_OWNERDRAWVARIABLE 32)

(defconstant-exported LBS_SORT 2)

(defconstant-exported LBS_STANDARD #xa00003)

(defconstant-exported LBS_USETABSTOPS 128)

(defconstant-exported LBS_WANTKEYBOARDINPUT #x400)

(defconstant-exported SBS_BOTTOMALIGN 4)

(defconstant-exported SBS_HORZ 0)

(defconstant-exported SBS_LEFTALIGN 2)

(defconstant-exported SBS_RIGHTALIGN 4)

(defconstant-exported SBS_SIZEBOX 8)

(defconstant-exported SBS_SIZEBOXBOTTOMRIGHTALIGN 4)

(defconstant-exported SBS_SIZEBOXTOPLEFTALIGN 2)

(defconstant-exported SBS_SIZEGRIP 16)

(defconstant-exported SBS_TOPALIGN 2)

(defconstant-exported SBS_VERT 1)

(defconstant-exported SS_BITMAP 14)

(defconstant-exported SS_BLACKFRAME 7)

(defconstant-exported SS_BLACKRECT 4)

(defconstant-exported SS_CENTER 1)

(defconstant-exported SS_CENTERIMAGE 512)

(defconstant-exported SS_ENHMETAFILE 15)

(defconstant-exported SS_ETCHEDFRAME 18)

(defconstant-exported SS_ETCHEDHORZ 16)

(defconstant-exported SS_ETCHEDVERT 17)

(defconstant-exported SS_GRAYFRAME 8)

(defconstant-exported SS_GRAYRECT 5)

(defconstant-exported SS_ICON 3)

(defconstant-exported SS_LEFT 0)

(defconstant-exported SS_LEFTNOWORDWRAP #xc)

(defconstant-exported SS_NOPREFIX 128)

(defconstant-exported SS_NOTIFY 256)

(defconstant-exported SS_OWNERDRAW #xd)

(defconstant-exported SS_REALSIZEIMAGE #x800)

(defconstant-exported SS_RIGHT 2)

(defconstant-exported SS_RIGHTJUST #x400)

(defconstant-exported SS_SIMPLE 11)

(defconstant-exported SS_SUNKEN 4096)

(defconstant-exported SS_WHITEFRAME 9)

(defconstant-exported SS_WHITERECT 6)

(defconstant-exported SS_USERITEM 10)

#|
Windows 2000: A composite style bit that results from using the OR operator on SS_* style bits. Can be used to mask out valid SS_* bits from a given bitmask. Note that this is out of date and does not correctly include all valid styles. Thus, you should not use this style.
  
(defconstant-exported SS_TYPEMASK #x0000001FL)
|#
(defconstant-exported SS_ENDELLIPSIS #x00004000)

(defconstant-exported SS_PATHELLIPSIS #x00008000)

(defconstant-exported SS_WORDELLIPSIS #x0000C000)

(defconstant-exported SS_ELLIPSISMASK #x0000C000)

(defconstant-exported DS_3DLOOK 4)

(defconstant-exported DS_ABSALIGN 1)

(defconstant-exported DS_CENTER #x800)

(defconstant-exported DS_CENTERMOUSE 4096)

(defconstant-exported DS_CONTEXTHELP #x2000)

(defconstant-exported DS_CONTROL #x400)

(defconstant-exported DS_FIXEDSYS 8)

(defconstant-exported DS_LOCALEDIT 32)

(defconstant-exported DS_MODALFRAME 128)

(defconstant-exported DS_NOFAILCREATE 16)

(defconstant-exported DS_NOIDLEMSG 256)

(defconstant-exported DS_SETFONT 64)

(defconstant-exported DS_SETFOREGROUND 512)

(defconstant-exported DS_SYSMODAL 2)

(defconstant-exported DS_SHELLFONT (cl:logior 64 8))

(defconstant-exported WS_EX_ACCEPTFILES 16)

(defconstant-exported WS_EX_APPWINDOW #x40000)

(defconstant-exported WS_EX_CLIENTEDGE 512)

(defconstant-exported WS_EX_COMPOSITED #x2000000)

(defconstant-exported WS_EX_CONTEXTHELP #x400)

(defconstant-exported WS_EX_CONTROLPARENT #x10000)

(defconstant-exported WS_EX_DLGMODALFRAME 1)

(defconstant-exported WS_EX_LAYERED #x80000)

(defconstant-exported WS_EX_LAYOUTRTL #x400000)

(defconstant-exported WS_EX_LEFT 0)

(defconstant-exported WS_EX_LEFTSCROLLBAR #x4000)

(defconstant-exported WS_EX_LTRREADING 0)

(defconstant-exported WS_EX_MDICHILD 64)

(defconstant-exported WS_EX_NOACTIVATE #x8000000)

(defconstant-exported WS_EX_NOINHERITLAYOUT #x100000)

(defconstant-exported WS_EX_NOPARENTNOTIFY 4)

(defconstant-exported WS_EX_OVERLAPPEDWINDOW #x300)

(defconstant-exported WS_EX_PALETTEWINDOW #x188)

(defconstant-exported WS_EX_RIGHT #x1000)

(defconstant-exported WS_EX_RIGHTSCROLLBAR 0)

(defconstant-exported WS_EX_RTLREADING #x2000)

(defconstant-exported WS_EX_STATICEDGE #x20000)

(defconstant-exported WS_EX_TOOLWINDOW 128)

(defconstant-exported WS_EX_TOPMOST 8)

(defconstant-exported WS_EX_TRANSPARENT 32)

(defconstant-exported WS_EX_WINDOWEDGE 256)

(defconstant-exported WINSTA_ACCESSCLIPBOARD 4)

(defconstant-exported WINSTA_ACCESSGLOBALATOMS 32)

(defconstant-exported WINSTA_CREATEDESKTOP 8)

(defconstant-exported WINSTA_ENUMDESKTOPS 1)

(defconstant-exported WINSTA_ENUMERATE 256)

(defconstant-exported WINSTA_EXITWINDOWS 64)

(defconstant-exported WINSTA_READATTRIBUTES 2)

(defconstant-exported WINSTA_READSCREEN 512)

(defconstant-exported WINSTA_WRITEATTRIBUTES 16)

(defconstant-exported DDL_READWRITE 0)

(defconstant-exported DDL_READONLY 1)

(defconstant-exported DDL_HIDDEN 2)

(defconstant-exported DDL_SYSTEM 4)

(defconstant-exported DDL_DIRECTORY 16)

(defconstant-exported DDL_ARCHIVE 32)

(defconstant-exported DDL_POSTMSGS 8192)

(defconstant-exported DDL_DRIVES 16384)

(defconstant-exported DDL_EXCLUSIVE 32768)

(defconstant-exported DC_ACTIVE #x00000001)

(defconstant-exported DC_SMALLCAP #x00000002)

(defconstant-exported DC_ICON #x00000004)

(defconstant-exported DC_TEXT #x00000008)

(defconstant-exported DC_INBUTTON #x00000010)

(defconstant-exported BDR_RAISEDOUTER 1)

(defconstant-exported BDR_SUNKENOUTER 2)

(defconstant-exported BDR_RAISEDINNER 4)

(defconstant-exported BDR_SUNKENINNER 8)

(defconstant-exported BDR_OUTER 3)

(defconstant-exported BDR_INNER #xc)

(defconstant-exported BDR_RAISED 5)

(defconstant-exported BDR_SUNKEN 10)

(defconstant-exported EDGE_RAISED (cl:logior 1 4))

(defconstant-exported EDGE_SUNKEN (cl:logior 2 8))

(defconstant-exported EDGE_ETCHED (cl:logior 2 4))

(defconstant-exported EDGE_BUMP (cl:logior 1 8))

(defconstant-exported BF_LEFT 1)

(defconstant-exported BF_TOP 2)

(defconstant-exported BF_RIGHT 4)

(defconstant-exported BF_BOTTOM 8)

(defconstant-exported BF_TOPLEFT (cl:logior 2 1))

(defconstant-exported BF_TOPRIGHT (cl:logior 2 4))

(defconstant-exported BF_BOTTOMLEFT (cl:logior 8 1))

(defconstant-exported BF_BOTTOMRIGHT (cl:logior 8 4))

(defconstant-exported BF_RECT (cl:logior 1 2 4 8))

(defconstant-exported BF_DIAGONAL 16)

(defconstant-exported BF_DIAGONAL_ENDTOPRIGHT (cl:logior 16 2 4))

(defconstant-exported BF_DIAGONAL_ENDTOPLEFT (cl:logior 16 2 1))

(defconstant-exported BF_DIAGONAL_ENDBOTTOMLEFT (cl:logior 16 8 1))

(defconstant-exported BF_DIAGONAL_ENDBOTTOMRIGHT (cl:logior 16 8 4))

(defconstant-exported BF_MIDDLE #x800)

(defconstant-exported BF_SOFT #x1000)

(defconstant-exported BF_ADJUST #x2000)

(defconstant-exported BF_FLAT #x4000)

(defconstant-exported BF_MONO #x8000)

(defconstant-exported DFC_CAPTION 1)

(defconstant-exported DFC_MENU 2)

(defconstant-exported DFC_SCROLL 3)

(defconstant-exported DFC_BUTTON 4)

(defconstant-exported DFCS_CAPTIONCLOSE 0)

(defconstant-exported DFCS_CAPTIONMIN 1)

(defconstant-exported DFCS_CAPTIONMAX 2)

(defconstant-exported DFCS_CAPTIONRESTORE 3)

(defconstant-exported DFCS_CAPTIONHELP 4)

(defconstant-exported DFCS_MENUARROW 0)

(defconstant-exported DFCS_MENUCHECK 1)

(defconstant-exported DFCS_MENUBULLET 2)

(defconstant-exported DFCS_MENUARROWRIGHT 4)

(defconstant-exported DFCS_SCROLLUP 0)

(defconstant-exported DFCS_SCROLLDOWN 1)

(defconstant-exported DFCS_SCROLLLEFT 2)

(defconstant-exported DFCS_SCROLLRIGHT 3)

(defconstant-exported DFCS_SCROLLCOMBOBOX 5)

(defconstant-exported DFCS_SCROLLSIZEGRIP 8)

(defconstant-exported DFCS_SCROLLSIZEGRIPRIGHT 16)

(defconstant-exported DFCS_BUTTONCHECK 0)

(defconstant-exported DFCS_BUTTONRADIOIMAGE 1)

(defconstant-exported DFCS_BUTTONRADIOMASK 2)

(defconstant-exported DFCS_BUTTONRADIO 4)

(defconstant-exported DFCS_BUTTON3STATE 8)

(defconstant-exported DFCS_BUTTONPUSH 16)

(defconstant-exported DFCS_INACTIVE 256)

(defconstant-exported DFCS_PUSHED 512)

(defconstant-exported DFCS_CHECKED 1024)

(defconstant-exported DFCS_ADJUSTRECT #x2000)

(defconstant-exported DFCS_FLAT #x4000)

(defconstant-exported DFCS_MONO #x8000)

(defconstant-exported DST_COMPLEX 0)

(defconstant-exported DST_TEXT 1)

(defconstant-exported DST_PREFIXTEXT 2)

(defconstant-exported DST_ICON 3)

(defconstant-exported DST_BITMAP 4)

(defconstant-exported DSS_NORMAL 0)

(defconstant-exported DSS_UNION 16)

(defconstant-exported DSS_DISABLED 32)

(defconstant-exported DSS_MONO 128)

(defconstant-exported DSS_RIGHT #x8000)

(defconstant-exported WB_ISDELIMITER 2)

(defconstant-exported WB_LEFT 0)

(defconstant-exported WB_RIGHT 1)

(defconstant-exported SB_HORZ 0)

(defconstant-exported SB_VERT 1)

(defconstant-exported SB_CTL 2)

(defconstant-exported SB_BOTH 3)

(defconstant-exported ESB_DISABLE_BOTH 3)

(defconstant-exported ESB_DISABLE_DOWN 2)

(defconstant-exported ESB_DISABLE_LEFT 1)

(defconstant-exported ESB_DISABLE_LTUP 1)

(defconstant-exported ESB_DISABLE_RIGHT 2)

(defconstant-exported ESB_DISABLE_RTDN 2)

(defconstant-exported ESB_DISABLE_UP 1)

(defconstant-exported ESB_ENABLE_BOTH 0)

(defconstant-exported SB_LINEUP 0)

(defconstant-exported SB_LINEDOWN 1)

(defconstant-exported SB_LINELEFT 0)

(defconstant-exported SB_LINERIGHT 1)

(defconstant-exported SB_PAGEUP 2)

(defconstant-exported SB_PAGEDOWN 3)

(defconstant-exported SB_PAGELEFT 2)

(defconstant-exported SB_PAGERIGHT 3)

(defconstant-exported SB_THUMBPOSITION 4)

(defconstant-exported SB_THUMBTRACK 5)

(defconstant-exported SB_ENDSCROLL 8)

(defconstant-exported SB_LEFT 6)

(defconstant-exported SB_RIGHT 7)

(defconstant-exported SB_BOTTOM 7)

(defconstant-exported SB_TOP 6)

(defconstant-exported DIFFERENCE 11)

(defconstant-exported EWX_FORCE 4)

(defconstant-exported EWX_LOGOFF 0)

(defconstant-exported EWX_POWEROFF 8)

(defconstant-exported EWX_REBOOT 2)

(defconstant-exported EWX_SHUTDOWN 1)

(defconstant-exported GCW_ATOM -32)

(defconstant-exported GCL_CBCLSEXTRA -20)

(defconstant-exported GCL_CBWNDEXTRA -18)

(defconstant-exported GCL_HBRBACKGROUND -10)

(defconstant-exported GCL_HCURSOR -12)

(defconstant-exported GCL_HICON -14)

(defconstant-exported GCL_HICONSM -34)

(defconstant-exported GCL_HMODULE -16)

(defconstant-exported GCL_MENUNAME -8)

(defconstant-exported GCL_STYLE -26)

(defconstant-exported GCL_WNDPROC -24)

(defconstant-exported MIIM_STATE 1)

(defconstant-exported MIIM_ID 2)

(defconstant-exported MIIM_SUBMENU 4)

(defconstant-exported MIIM_CHECKMARKS 8)

(defconstant-exported MIIM_TYPE 16)

(defconstant-exported MIIM_DATA 32)

(defconstant-exported MIIM_STRING 64)

(defconstant-exported MIIM_BITMAP 128)

(defconstant-exported MIIM_FTYPE 256)

(defconstant-exported MFT_BITMAP 4)

(defconstant-exported MFT_MENUBARBREAK 32)

(defconstant-exported MFT_MENUBREAK 64)

(defconstant-exported MFT_OWNERDRAW 256)

(defconstant-exported MFT_RADIOCHECK 512)

(defconstant-exported MFT_RIGHTJUSTIFY #x4000)

(defconstant-exported MFT_SEPARATOR #x800)

(defconstant-exported MFT_RIGHTORDER #x2000)

(defconstant-exported MFT_STRING 0)

(defconstant-exported MFS_CHECKED 8)

(defconstant-exported MFS_DEFAULT 4096)

(defconstant-exported MFS_DISABLED 3)

(defconstant-exported MFS_ENABLED 0)

(defconstant-exported MFS_GRAYED 3)

(defconstant-exported MFS_HILITE 128)

(defconstant-exported MFS_UNCHECKED 0)

(defconstant-exported MFS_UNHILITE 0)

(defconstant-exported GW_HWNDNEXT 2)

(defconstant-exported GW_HWNDPREV 3)

(defconstant-exported GW_CHILD 5)

(defconstant-exported GW_HWNDFIRST 0)

(defconstant-exported GW_HWNDLAST 1)

(defconstant-exported GW_OWNER 4)

(defconstant-exported GWL_EXSTYLE -20)

(defconstant-exported GWL_STYLE -16)

(defconstant-exported GWL_WNDPROC -4)

(defconstant-exported GWLP_WNDPROC -4)

(defconstant-exported GWL_HINSTANCE -6)

(defconstant-exported GWLP_HINSTANCE -6)

(defconstant-exported GWL_HWNDPARENT -8)

(defconstant-exported GWLP_HWNDPARENT -8)

(defconstant-exported GWL_ID -12)

(defconstant-exported GWLP_ID -12)

(defconstant-exported GWL_USERDATA -21)

(defconstant-exported GWLP_USERDATA -21)

(defconstant-exported DWL_DLGPROC 4)

(defconstant-exported DWLP_DLGPROC 4)

(defconstant-exported DWL_MSGRESULT 0)

(defconstant-exported DWLP_MSGRESULT 0)

(defconstant-exported DWL_USER 8)

(defconstant-exported DWLP_USER 8)

(defconstant-exported QS_ALLEVENTS 191)

(defconstant-exported QS_ALLINPUT 255)

(defconstant-exported QS_ALLPOSTMESSAGE 256)

(defconstant-exported QS_HOTKEY 128)

(defconstant-exported QS_INPUT 7)

(defconstant-exported QS_KEY 1)

(defconstant-exported QS_MOUSE 6)

(defconstant-exported QS_MOUSEBUTTON 4)

(defconstant-exported QS_MOUSEMOVE 2)

(defconstant-exported QS_PAINT 32)

(defconstant-exported QS_POSTMESSAGE 8)

(defconstant-exported QS_SENDMESSAGE 64)

(defconstant-exported QS_TIMER 16)

(defconstant-exported MWMO_WAITALL 1)

(defconstant-exported MWMO_ALERTABLE 2)

(defconstant-exported MWMO_INPUTAVAILABLE 4)

(defconstant-exported CTLCOLOR_MSGBOX 0)

(defconstant-exported CTLCOLOR_EDIT 1)

(defconstant-exported CTLCOLOR_LISTBOX 2)

(defconstant-exported CTLCOLOR_BTN 3)

(defconstant-exported CTLCOLOR_DLG 4)

(defconstant-exported CTLCOLOR_SCROLLBAR 5)

(defconstant-exported CTLCOLOR_STATIC 6)

(defconstant-exported CTLCOLOR_MAX 7)

(defconstant-exported SM_CXSCREEN 0)

(defconstant-exported SM_CYSCREEN 1)

(defconstant-exported SM_CXVSCROLL 2)

(defconstant-exported SM_CYHSCROLL 3)

(defconstant-exported SM_CYCAPTION 4)

(defconstant-exported SM_CXBORDER 5)

(defconstant-exported SM_CYBORDER 6)

(defconstant-exported SM_CXDLGFRAME 7)

(defconstant-exported SM_CXFIXEDFRAME 7)

(defconstant-exported SM_CYDLGFRAME 8)

(defconstant-exported SM_CYFIXEDFRAME 8)

(defconstant-exported SM_CYVTHUMB 9)

(defconstant-exported SM_CXHTHUMB 10)

(defconstant-exported SM_CXICON 11)

(defconstant-exported SM_CYICON 12)

(defconstant-exported SM_CXCURSOR 13)

(defconstant-exported SM_CYCURSOR 14)

(defconstant-exported SM_CYMENU 15)

(defconstant-exported SM_CXFULLSCREEN 16)

(defconstant-exported SM_CYFULLSCREEN 17)

(defconstant-exported SM_CYKANJIWINDOW 18)

(defconstant-exported SM_MOUSEPRESENT 19)

(defconstant-exported SM_CYVSCROLL 20)

(defconstant-exported SM_CXHSCROLL 21)

(defconstant-exported SM_DEBUG 22)

(defconstant-exported SM_SWAPBUTTON 23)

(defconstant-exported SM_RESERVED1 24)

(defconstant-exported SM_RESERVED2 25)

(defconstant-exported SM_RESERVED3 26)

(defconstant-exported SM_RESERVED4 27)

(defconstant-exported SM_CXMIN 28)

(defconstant-exported SM_CYMIN 29)

(defconstant-exported SM_CXSIZE 30)

(defconstant-exported SM_CYSIZE 31)

(defconstant-exported SM_CXSIZEFRAME 32)

(defconstant-exported SM_CXFRAME 32)

(defconstant-exported SM_CYSIZEFRAME 33)

(defconstant-exported SM_CYFRAME 33)

(defconstant-exported SM_CXMINTRACK 34)

(defconstant-exported SM_CYMINTRACK 35)

(defconstant-exported SM_CXDOUBLECLK 36)

(defconstant-exported SM_CYDOUBLECLK 37)

(defconstant-exported SM_CXICONSPACING 38)

(defconstant-exported SM_CYICONSPACING 39)

(defconstant-exported SM_MENUDROPALIGNMENT 40)

(defconstant-exported SM_PENWINDOWS 41)

(defconstant-exported SM_DBCSENABLED 42)

(defconstant-exported SM_CMOUSEBUTTONS 43)

(defconstant-exported SM_SECURE 44)

(defconstant-exported SM_CXEDGE 45)

(defconstant-exported SM_CYEDGE 46)

(defconstant-exported SM_CXMINSPACING 47)

(defconstant-exported SM_CYMINSPACING 48)

(defconstant-exported SM_CXSMICON 49)

(defconstant-exported SM_CYSMICON 50)

(defconstant-exported SM_CYSMCAPTION 51)

(defconstant-exported SM_CXSMSIZE 52)

(defconstant-exported SM_CYSMSIZE 53)

(defconstant-exported SM_CXMENUSIZE 54)

(defconstant-exported SM_CYMENUSIZE 55)

(defconstant-exported SM_ARRANGE 56)

(defconstant-exported SM_CXMINIMIZED 57)

(defconstant-exported SM_CYMINIMIZED 58)

(defconstant-exported SM_CXMAXTRACK 59)

(defconstant-exported SM_CYMAXTRACK 60)

(defconstant-exported SM_CXMAXIMIZED 61)

(defconstant-exported SM_CYMAXIMIZED 62)

(defconstant-exported SM_NETWORK 63)

(defconstant-exported LR_DEFAULTSIZE 64)

(defconstant-exported SM_CLEANBOOT 67)

(defconstant-exported SM_CXDRAG 68)

(defconstant-exported SM_CYDRAG 69)

(defconstant-exported SM_SHOWSOUNDS 70)

(defconstant-exported SM_CXMENUCHECK 71)

(defconstant-exported SM_CYMENUCHECK 72)

(defconstant-exported SM_SLOWMACHINE 73)

(defconstant-exported SM_MIDEASTENABLED 74)

(defconstant-exported SM_MOUSEWHEELPRESENT 75)

(defconstant-exported SM_XVIRTUALSCREEN 76)

(defconstant-exported SM_YVIRTUALSCREEN 77)

(defconstant-exported SM_CXVIRTUALSCREEN 78)

(defconstant-exported SM_CYVIRTUALSCREEN 79)

(defconstant-exported SM_CMONITORS 80)

(defconstant-exported SM_SAMEDISPLAYFORMAT 81)

(defconstant-exported SM_IMMENABLED 82)

(defconstant-exported SM_CXFOCUSBORDER 83)

(defconstant-exported SM_CYFOCUSBORDER 84)

(defconstant-exported SM_TABLETPC 86)

(defconstant-exported SM_MEDIACENTER 87)

(defconstant-exported SM_CMETRICS 88)

(defconstant-exported SM_REMOTESESSION #x1000)

(defconstant-exported ARW_BOTTOMLEFT 0)

(defconstant-exported ARW_BOTTOMRIGHT 1)

(defconstant-exported ARW_HIDE 8)

(defconstant-exported ARW_TOPLEFT 2)

(defconstant-exported ARW_TOPRIGHT 3)

(defconstant-exported ARW_DOWN 4)

(defconstant-exported ARW_LEFT 0)

(defconstant-exported ARW_RIGHT 0)

(defconstant-exported ARW_UP 4)

(defconstant-exported UOI_FLAGS 1)

(defconstant-exported UOI_NAME 2)

(defconstant-exported UOI_TYPE 3)

(defconstant-exported UOI_USER_SID 4)

(defconstant-exported LR_DEFAULTCOLOR 0)

(defconstant-exported LR_MONOCHROME 1)

(defconstant-exported LR_COLOR 2)

(defconstant-exported LR_COPYRETURNORG 4)

(defconstant-exported LR_COPYDELETEORG 8)

(defconstant-exported LR_LOADFROMFILE 16)

(defconstant-exported LR_LOADTRANSPARENT 32)

(defconstant-exported LR_LOADREALSIZE 128)

(defconstant-exported LR_LOADMAP3DCOLORS 4096)

(defconstant-exported LR_CREATEDIBSECTION 8192)

(defconstant-exported LR_COPYFROMRESOURCE #x4000)

(defconstant-exported LR_SHARED 32768)

(defconstant-exported KEYEVENTF_EXTENDEDKEY #x00000001)

(defconstant-exported KEYEVENTF_KEYUP #o0000002)

(defconstant-exported OBM_BTNCORNERS 32758)

(defconstant-exported OBM_BTSIZE 32761)

(defconstant-exported OBM_CHECK 32760)

(defconstant-exported OBM_CHECKBOXES 32759)

(defconstant-exported OBM_CLOSE 32754)

(defconstant-exported OBM_COMBO 32738)

(defconstant-exported OBM_DNARROW 32752)

(defconstant-exported OBM_DNARROWD 32742)

(defconstant-exported OBM_DNARROWI 32736)

(defconstant-exported OBM_LFARROW 32750)

(defconstant-exported OBM_LFARROWI 32734)

(defconstant-exported OBM_LFARROWD 32740)

(defconstant-exported OBM_MNARROW 32739)

(defconstant-exported OBM_OLD_CLOSE 32767)

(defconstant-exported OBM_OLD_DNARROW 32764)

(defconstant-exported OBM_OLD_LFARROW 32762)

(defconstant-exported OBM_OLD_REDUCE 32757)

(defconstant-exported OBM_OLD_RESTORE 32755)

(defconstant-exported OBM_OLD_RGARROW 32763)

(defconstant-exported OBM_OLD_UPARROW 32765)

(defconstant-exported OBM_OLD_ZOOM 32756)

(defconstant-exported OBM_REDUCE 32749)

(defconstant-exported OBM_REDUCED 32746)

(defconstant-exported OBM_RESTORE 32747)

(defconstant-exported OBM_RESTORED 32744)

(defconstant-exported OBM_RGARROW 32751)

(defconstant-exported OBM_RGARROWD 32741)

(defconstant-exported OBM_RGARROWI 32735)

(defconstant-exported OBM_SIZE 32766)

(defconstant-exported OBM_UPARROW 32753)

(defconstant-exported OBM_UPARROWD 32743)

(defconstant-exported OBM_UPARROWI 32737)

(defconstant-exported OBM_ZOOM 32748)

(defconstant-exported OBM_ZOOMD 32745)

(defconstant-exported OCR_NORMAL 32512)

(defconstant-exported OCR_IBEAM 32513)

(defconstant-exported OCR_WAIT 32514)

(defconstant-exported OCR_CROSS 32515)

(defconstant-exported OCR_UP 32516)

(defconstant-exported OCR_SIZE 32640)

(defconstant-exported OCR_ICON 32641)

(defconstant-exported OCR_SIZENWSE 32642)

(defconstant-exported OCR_SIZENESW 32643)

(defconstant-exported OCR_SIZEWE 32644)

(defconstant-exported OCR_SIZENS 32645)

(defconstant-exported OCR_SIZEALL 32646)

(defconstant-exported OCR_NO 32648)

(defconstant-exported OCR_APPSTARTING 32650)

(defconstant-exported OIC_SAMPLE 32512)

(defconstant-exported OIC_HAND 32513)

(defconstant-exported OIC_QUES 32514)

(defconstant-exported OIC_BANG 32515)

(defconstant-exported OIC_NOTE 32516)

(defconstant-exported OIC_WINLOGO 32517)

(defconstant-exported OIC_WARNING 32515)

(defconstant-exported OIC_ERROR 32513)

(defconstant-exported OIC_INFORMATION 32516)

(defconstant-exported HELPINFO_MENUITEM 2)

(defconstant-exported HELPINFO_WINDOW 1)

(defconstant-exported MSGF_DIALOGBOX 0)

(defconstant-exported MSGF_MESSAGEBOX 1)

(defconstant-exported MSGF_MENU 2)

(defconstant-exported MSGF_MOVE 3)

(defconstant-exported MSGF_SIZE 4)

(defconstant-exported MSGF_SCROLLBAR 5)

(defconstant-exported MSGF_NEXTWINDOW 6)

(defconstant-exported MSGF_MAINLOOP 8)

(defconstant-exported MSGF_USER 4096)

(defconstant-exported MOUSEEVENTF_MOVE 1)

(defconstant-exported MOUSEEVENTF_LEFTDOWN 2)

(defconstant-exported MOUSEEVENTF_LEFTUP 4)

(defconstant-exported MOUSEEVENTF_RIGHTDOWN 8)

(defconstant-exported MOUSEEVENTF_RIGHTUP 16)

(defconstant-exported MOUSEEVENTF_MIDDLEDOWN 32)

(defconstant-exported MOUSEEVENTF_MIDDLEUP 64)

(defconstant-exported MOUSEEVENTF_WHEEL #x0800)

(defconstant-exported MOUSEEVENTF_ABSOLUTE 32768)

(defconstant-exported PM_NOREMOVE 0)

(defconstant-exported PM_REMOVE 1)

(defconstant-exported PM_NOYIELD 2)

(defconstant-exported RDW_ERASE 4)

(defconstant-exported RDW_FRAME 1024)

(defconstant-exported RDW_INTERNALPAINT 2)

(defconstant-exported RDW_INVALIDATE 1)

(defconstant-exported RDW_NOERASE 32)

(defconstant-exported RDW_NOFRAME 2048)

(defconstant-exported RDW_NOINTERNALPAINT 16)

(defconstant-exported RDW_VALIDATE 8)

(defconstant-exported RDW_ERASENOW 512)

(defconstant-exported RDW_UPDATENOW 256)

(defconstant-exported RDW_ALLCHILDREN 128)

(defconstant-exported RDW_NOCHILDREN 64)

(defconstant-exported SMTO_ABORTIFHUNG 2)

(defconstant-exported SMTO_BLOCK 1)

(defconstant-exported SMTO_NORMAL 0)

(defconstant-exported SIF_ALL 23)

(defconstant-exported SIF_PAGE 2)

(defconstant-exported SIF_POS 4)

(defconstant-exported SIF_RANGE 1)

(defconstant-exported SIF_DISABLENOSCROLL 8)

(defconstant-exported SIF_TRACKPOS 16)

(defconstant-exported SWP_DRAWFRAME 32)

(defconstant-exported SWP_FRAMECHANGED 32)

(defconstant-exported SWP_HIDEWINDOW 128)

(defconstant-exported SWP_NOACTIVATE 16)

(defconstant-exported SWP_NOCOPYBITS 256)

(defconstant-exported SWP_NOMOVE 2)

(defconstant-exported SWP_NOSIZE 1)

(defconstant-exported SWP_NOREDRAW 8)

(defconstant-exported SWP_NOZORDER 4)

(defconstant-exported SWP_SHOWWINDOW 64)

(defconstant-exported SWP_NOOWNERZORDER 512)

(defconstant-exported SWP_NOREPOSITION 512)

(defconstant-exported SWP_NOSENDCHANGING 1024)

(defconstant-exported SWP_DEFERERASE 8192)

(defconstant-exported SWP_ASYNCWINDOWPOS 16384)

(defconstant-exported HSHELL_ACTIVATESHELLWINDOW 3)

(defconstant-exported HSHELL_GETMINRECT 5)

(defconstant-exported HSHELL_LANGUAGE 8)

(defconstant-exported HSHELL_REDRAW 6)

(defconstant-exported HSHELL_TASKMAN 7)

(defconstant-exported HSHELL_WINDOWACTIVATED 4)

(defconstant-exported HSHELL_WINDOWCREATED 1)

(defconstant-exported HSHELL_WINDOWDESTROYED 2)

(defconstant-exported HSHELL_RUDEAPPACTIVATED 32772)

(defconstant-exported HSHELL_FLASH 32774)

(defconstant-exported SPI_GETACCESSTIMEOUT 60)

(defconstant-exported SPI_GETACTIVEWNDTRKTIMEOUT 8194)

(defconstant-exported SPI_GETANIMATION 72)

(defconstant-exported SPI_GETBEEP 1)

(defconstant-exported SPI_GETBORDER 5)

(defconstant-exported SPI_GETDEFAULTINPUTLANG 89)

(defconstant-exported SPI_GETDRAGFULLWINDOWS 38)

(defconstant-exported SPI_GETFASTTASKSWITCH 35)

(defconstant-exported SPI_GETFILTERKEYS 50)

(defconstant-exported SPI_GETFONTSMOOTHING 74)

(defconstant-exported SPI_GETGRIDGRANULARITY 18)

(defconstant-exported SPI_GETHIGHCONTRAST 66)

(defconstant-exported SPI_GETICONMETRICS 45)

(defconstant-exported SPI_GETICONTITLELOGFONT 31)

(defconstant-exported SPI_GETICONTITLEWRAP 25)

(defconstant-exported SPI_GETKEYBOARDDELAY 22)

(defconstant-exported SPI_GETKEYBOARDPREF 68)

(defconstant-exported SPI_GETKEYBOARDSPEED 10)

(defconstant-exported SPI_GETLOWPOWERACTIVE 83)

(defconstant-exported SPI_GETLOWPOWERTIMEOUT 79)

(defconstant-exported SPI_GETMENUDROPALIGNMENT 27)

(defconstant-exported SPI_GETMINIMIZEDMETRICS 43)

(defconstant-exported SPI_GETMOUSE 3)

(defconstant-exported SPI_GETMOUSEKEYS 54)

(defconstant-exported SPI_GETMOUSETRAILS 94)

(defconstant-exported SPI_GETNONCLIENTMETRICS 41)

(defconstant-exported SPI_GETPOWEROFFACTIVE 84)

(defconstant-exported SPI_GETPOWEROFFTIMEOUT 80)

(defconstant-exported SPI_GETSCREENREADER 70)

(defconstant-exported SPI_GETSCREENSAVEACTIVE 16)

(defconstant-exported SPI_GETSCREENSAVETIMEOUT 14)

(defconstant-exported SPI_GETSERIALKEYS 62)

(defconstant-exported SPI_GETSHOWSOUNDS 56)

(defconstant-exported SPI_GETSOUNDSENTRY 64)

(defconstant-exported SPI_GETSTICKYKEYS 58)

(defconstant-exported SPI_GETTOGGLEKEYS 52)

(defconstant-exported SPI_GETWHEELSCROLLLINES 104)

(defconstant-exported SPI_GETWINDOWSEXTENSION 92)

(defconstant-exported SPI_GETWORKAREA 48)

(defconstant-exported SPI_ICONHORIZONTALSPACING 13)

(defconstant-exported SPI_ICONVERTICALSPACING 24)

(defconstant-exported SPI_LANGDRIVER 12)

(defconstant-exported SPI_SCREENSAVERRUNNING 97)

(defconstant-exported SPI_SETACCESSTIMEOUT 61)

(defconstant-exported SPI_SETACTIVEWNDTRKTIMEOUT 8195)

(defconstant-exported SPI_SETANIMATION 73)

(defconstant-exported SPI_SETBEEP 2)

(defconstant-exported SPI_SETBORDER 6)

(defconstant-exported SPI_SETDEFAULTINPUTLANG 90)

(defconstant-exported SPI_SETDESKPATTERN 21)

(defconstant-exported SPI_SETDESKWALLPAPER 20)

(defconstant-exported SPI_SETDOUBLECLICKTIME 32)

(defconstant-exported SPI_SETDOUBLECLKHEIGHT 30)

(defconstant-exported SPI_SETDOUBLECLKWIDTH 29)

(defconstant-exported SPI_SETDRAGFULLWINDOWS 37)

(defconstant-exported SPI_SETDRAGHEIGHT 77)

(defconstant-exported SPI_SETDRAGWIDTH 76)

(defconstant-exported SPI_SETFASTTASKSWITCH 36)

(defconstant-exported SPI_SETFILTERKEYS 51)

(defconstant-exported SPI_SETFONTSMOOTHING 75)

(defconstant-exported SPI_SETGRIDGRANULARITY 19)

(defconstant-exported SPI_SETHANDHELD 78)

(defconstant-exported SPI_SETHIGHCONTRAST 67)

(defconstant-exported SPI_SETICONMETRICS 46)

(defconstant-exported SPI_SETICONTITLELOGFONT 34)

(defconstant-exported SPI_SETICONTITLEWRAP 26)

(defconstant-exported SPI_SETKEYBOARDDELAY 23)

(defconstant-exported SPI_SETKEYBOARDPREF 69)

(defconstant-exported SPI_SETKEYBOARDSPEED 11)

(defconstant-exported SPI_SETLANGTOGGLE 91)

(defconstant-exported SPI_SETLOWPOWERACTIVE 85)

(defconstant-exported SPI_SETLOWPOWERTIMEOUT 81)

(defconstant-exported SPI_SETMENUDROPALIGNMENT 28)

(defconstant-exported SPI_SETMINIMIZEDMETRICS 44)

(defconstant-exported SPI_SETMOUSE 4)

(defconstant-exported SPI_SETMOUSEBUTTONSWAP 33)

(defconstant-exported SPI_SETMOUSEKEYS 55)

(defconstant-exported SPI_SETMOUSETRAILS 93)

(defconstant-exported SPI_SETNONCLIENTMETRICS 42)

(defconstant-exported SPI_SETPENWINDOWS 49)

(defconstant-exported SPI_SETPOWEROFFACTIVE 86)

(defconstant-exported SPI_SETPOWEROFFTIMEOUT 82)

(defconstant-exported SPI_SETSCREENREADER 71)

(defconstant-exported SPI_SETSCREENSAVEACTIVE 17)

(defconstant-exported SPI_SETSCREENSAVERRUNNING 97)

(defconstant-exported SPI_SETSCREENSAVETIMEOUT 15)

(defconstant-exported SPI_SETSERIALKEYS 63)

(defconstant-exported SPI_SETSHOWSOUNDS 57)

(defconstant-exported SPI_SETSOUNDSENTRY 65)

(defconstant-exported SPI_SETSTICKYKEYS 59)

(defconstant-exported SPI_SETTOGGLEKEYS 53)

(defconstant-exported SPI_SETWHEELSCROLLLINES 105)

(defconstant-exported SPI_SETWORKAREA 47)

(defconstant-exported SPIF_UPDATEINIFILE 1)

(defconstant-exported SPIF_SENDWININICHANGE 2)

(defconstant-exported SPIF_SENDCHANGE 2)

(defconstant-exported WHEEL_DELTA 120)

(defconstant-exported BM_CLICK 245)

(defconstant-exported BM_GETCHECK 240)

(defconstant-exported BM_GETIMAGE 246)

(defconstant-exported BM_GETSTATE 242)

(defconstant-exported BM_SETCHECK 241)

(defconstant-exported BM_SETIMAGE 247)

(defconstant-exported BM_SETSTATE 243)

(defconstant-exported BM_SETSTYLE 244)

(defconstant-exported BN_CLICKED 0)

(defconstant-exported BN_DBLCLK 5)

(defconstant-exported BN_DISABLE 4)

(defconstant-exported BN_DOUBLECLICKED 5)

(defconstant-exported BN_HILITE 2)

(defconstant-exported BN_KILLFOCUS 7)

(defconstant-exported BN_PAINT 1)

(defconstant-exported BN_PUSHED 2)

(defconstant-exported BN_SETFOCUS 6)

(defconstant-exported BN_UNHILITE 3)

(defconstant-exported BN_UNPUSHED 3)

(defconstant-exported CB_ADDSTRING 323)

(defconstant-exported CB_DELETESTRING 324)

(defconstant-exported CB_DIR 325)

(defconstant-exported CB_FINDSTRING 332)

(defconstant-exported CB_FINDSTRINGEXACT 344)

(defconstant-exported CB_GETCOUNT 326)

(defconstant-exported CB_GETCURSEL 327)

(defconstant-exported CB_GETDROPPEDCONTROLRECT 338)

(defconstant-exported CB_GETDROPPEDSTATE 343)

(defconstant-exported CB_GETDROPPEDWIDTH 351)

(defconstant-exported CB_GETEDITSEL 320)

(defconstant-exported CB_GETEXTENDEDUI 342)

(defconstant-exported CB_GETHORIZONTALEXTENT 349)

(defconstant-exported CB_GETITEMDATA 336)

(defconstant-exported CB_GETITEMHEIGHT 340)

(defconstant-exported CB_GETLBTEXT 328)

(defconstant-exported CB_GETLBTEXTLEN 329)

(defconstant-exported CB_GETLOCALE 346)

(defconstant-exported CB_GETTOPINDEX 347)

(defconstant-exported CB_INITSTORAGE 353)

(defconstant-exported CB_INSERTSTRING 330)

(defconstant-exported CB_LIMITTEXT 321)

(defconstant-exported CB_RESETCONTENT 331)

(defconstant-exported CB_SELECTSTRING 333)

(defconstant-exported CB_SETCURSEL 334)

(defconstant-exported CB_SETDROPPEDWIDTH 352)

(defconstant-exported CB_SETEDITSEL 322)

(defconstant-exported CB_SETEXTENDEDUI 341)

(defconstant-exported CB_SETHORIZONTALEXTENT 350)

(defconstant-exported CB_SETITEMDATA 337)

(defconstant-exported CB_SETITEMHEIGHT 339)

(defconstant-exported CB_SETLOCALE 345)

(defconstant-exported CB_SETTOPINDEX 348)

(defconstant-exported CB_SHOWDROPDOWN 335)

(defconstant-exported CBN_CLOSEUP 8)

(defconstant-exported CBN_DBLCLK 2)

(defconstant-exported CBN_DROPDOWN 7)

(defconstant-exported CBN_EDITCHANGE 5)

(defconstant-exported CBN_EDITUPDATE 6)

(defconstant-exported CBN_ERRSPACE -1)

(defconstant-exported CBN_KILLFOCUS 4)

(defconstant-exported CBN_SELCHANGE 1)

(defconstant-exported CBN_SELENDCANCEL 10)

(defconstant-exported CBN_SELENDOK 9)

(defconstant-exported CBN_SETFOCUS 3)

(defconstant-exported EM_CANUNDO 198)

(defconstant-exported EM_CHARFROMPOS 215)

(defconstant-exported EM_EMPTYUNDOBUFFER 205)

(defconstant-exported EM_FMTLINES 200)

(defconstant-exported EM_GETFIRSTVISIBLELINE 206)

(defconstant-exported EM_GETHANDLE 189)

(defconstant-exported EM_GETLIMITTEXT 213)

(defconstant-exported EM_GETLINE 196)

(defconstant-exported EM_GETLINECOUNT 186)

(defconstant-exported EM_GETMARGINS 212)

(defconstant-exported EM_GETMODIFY 184)

(defconstant-exported EM_GETPASSWORDCHAR 210)

(defconstant-exported EM_GETRECT 178)

(defconstant-exported EM_GETSEL 176)

(defconstant-exported EM_GETTHUMB 190)

(defconstant-exported EM_GETWORDBREAKPROC 209)

(defconstant-exported EM_LIMITTEXT 197)

(defconstant-exported EM_LINEFROMCHAR 201)

(defconstant-exported EM_LINEINDEX 187)

(defconstant-exported EM_LINELENGTH 193)

(defconstant-exported EM_LINESCROLL 182)

(defconstant-exported EM_POSFROMCHAR 214)

(defconstant-exported EM_REPLACESEL 194)

(defconstant-exported EM_SCROLL 181)

(defconstant-exported EM_SCROLLCARET 183)

(defconstant-exported EM_SETHANDLE 188)

(defconstant-exported EM_SETLIMITTEXT 197)

(defconstant-exported EM_SETMARGINS 211)

(defconstant-exported EM_SETMODIFY 185)

(defconstant-exported EM_SETPASSWORDCHAR 204)

(defconstant-exported EM_SETREADONLY 207)

(defconstant-exported EM_SETRECT 179)

(defconstant-exported EM_SETRECTNP 180)

(defconstant-exported EM_SETSEL 177)

(defconstant-exported EM_SETTABSTOPS 203)

(defconstant-exported EM_SETWORDBREAKPROC 208)

(defconstant-exported EM_UNDO 199)

(defconstant-exported EN_CHANGE 768)

(defconstant-exported EN_ERRSPACE 1280)

(defconstant-exported EN_HSCROLL 1537)

(defconstant-exported EN_KILLFOCUS 512)

(defconstant-exported EN_MAXTEXT 1281)

(defconstant-exported EN_SETFOCUS 256)

(defconstant-exported EN_UPDATE 1024)

(defconstant-exported EN_VSCROLL 1538)

(defconstant-exported LB_ADDFILE 406)

(defconstant-exported LB_ADDSTRING 384)

(defconstant-exported LB_DELETESTRING 386)

(defconstant-exported LB_DIR 397)

(defconstant-exported LB_FINDSTRING 399)

(defconstant-exported LB_FINDSTRINGEXACT 418)

(defconstant-exported LB_GETANCHORINDEX 413)

(defconstant-exported LB_GETCARETINDEX 415)

(defconstant-exported LB_GETCOUNT 395)

(defconstant-exported LB_GETCURSEL 392)

(defconstant-exported LB_GETHORIZONTALEXTENT 403)

(defconstant-exported LB_GETITEMDATA 409)

(defconstant-exported LB_GETITEMHEIGHT 417)

(defconstant-exported LB_GETITEMRECT 408)

(defconstant-exported LB_GETLOCALE 422)

(defconstant-exported LB_GETSEL 391)

(defconstant-exported LB_GETSELCOUNT 400)

(defconstant-exported LB_GETSELITEMS 401)

(defconstant-exported LB_GETTEXT 393)

(defconstant-exported LB_GETTEXTLEN 394)

(defconstant-exported LB_GETTOPINDEX 398)

(defconstant-exported LB_INITSTORAGE 424)

(defconstant-exported LB_INSERTSTRING 385)

(defconstant-exported LB_ITEMFROMPOINT 425)

(defconstant-exported LB_RESETCONTENT 388)

(defconstant-exported LB_SELECTSTRING 396)

(defconstant-exported LB_SELITEMRANGE 411)

(defconstant-exported LB_SELITEMRANGEEX 387)

(defconstant-exported LB_SETANCHORINDEX 412)

(defconstant-exported LB_SETCARETINDEX 414)

(defconstant-exported LB_SETCOLUMNWIDTH 405)

(defconstant-exported LB_SETCOUNT 423)

(defconstant-exported LB_SETCURSEL 390)

(defconstant-exported LB_SETHORIZONTALEXTENT 404)

(defconstant-exported LB_SETITEMDATA 410)

(defconstant-exported LB_SETITEMHEIGHT 416)

(defconstant-exported LB_SETLOCALE 421)

(defconstant-exported LB_SETSEL 389)

(defconstant-exported LB_SETTABSTOPS 402)

(defconstant-exported LB_SETTOPINDEX 407)

(defconstant-exported LBN_DBLCLK 2)

(defconstant-exported LBN_ERRSPACE -2)

(defconstant-exported LBN_KILLFOCUS 5)

(defconstant-exported LBN_SELCANCEL 3)

(defconstant-exported LBN_SELCHANGE 1)

(defconstant-exported LBN_SETFOCUS 4)

(defconstant-exported SBM_ENABLE_ARROWS 228)

(defconstant-exported SBM_GETPOS 225)

(defconstant-exported SBM_GETRANGE 227)

(defconstant-exported SBM_GETSCROLLINFO 234)

(defconstant-exported SBM_SETPOS 224)

(defconstant-exported SBM_SETRANGE 226)

(defconstant-exported SBM_SETRANGEREDRAW 230)

(defconstant-exported SBM_SETSCROLLINFO 233)

(defconstant-exported STM_GETICON 369)

(defconstant-exported STM_GETIMAGE 371)

(defconstant-exported STM_SETICON 368)

(defconstant-exported STM_SETIMAGE 370)

(defconstant-exported STN_CLICKED 0)

(defconstant-exported STN_DBLCLK 1)

(defconstant-exported STN_DISABLE 3)

(defconstant-exported STN_ENABLE 2)

(defconstant-exported DM_GETDEFID 1024)

(defconstant-exported DM_SETDEFID (cl:+ 1024 1))

(defconstant-exported DM_REPOSITION (cl:+ 1024 2))

(defconstant-exported PSM_PAGEINFO (cl:+ 1024 100))

(defconstant-exported PSM_SHEETINFO (cl:+ 1024 101))

(defconstant-exported PSI_SETACTIVE 1)

(defconstant-exported PSI_KILLACTIVE 2)

(defconstant-exported PSI_APPLY 3)

(defconstant-exported PSI_RESET 4)

(defconstant-exported PSI_HASHELP 5)

(defconstant-exported PSI_HELP 6)

(defconstant-exported PSI_CHANGED 1)

(defconstant-exported PSI_GUISTART 2)

(defconstant-exported PSI_REBOOT 3)

(defconstant-exported PSI_GETSIBLINGS 4)

(defconstant-exported DCX_WINDOW 1)

(defconstant-exported DCX_CACHE 2)

(defconstant-exported DCX_PARENTCLIP 32)

(defconstant-exported DCX_CLIPSIBLINGS 16)

(defconstant-exported DCX_CLIPCHILDREN 8)

(defconstant-exported DCX_NORESETATTRS 4)

(defconstant-exported DCX_INTERSECTUPDATE #x200)

(defconstant-exported DCX_LOCKWINDOWUPDATE #x400)

(defconstant-exported DCX_EXCLUDERGN 64)

(defconstant-exported DCX_INTERSECTRGN 128)

(defconstant-exported DCX_VALIDATE #x200000)

(defconstant-exported GMDI_GOINTOPOPUPS 2)

(defconstant-exported GMDI_USEDISABLED 1)

(defconstant-exported FKF_AVAILABLE 2)

(defconstant-exported FKF_CLICKON 64)

(defconstant-exported FKF_FILTERKEYSON 1)

(defconstant-exported FKF_HOTKEYACTIVE 4)

(defconstant-exported FKF_HOTKEYSOUND 16)

(defconstant-exported FKF_CONFIRMHOTKEY 8)

(defconstant-exported FKF_INDICATOR 32)

(defconstant-exported HCF_HIGHCONTRASTON 1)

(defconstant-exported HCF_AVAILABLE 2)

(defconstant-exported HCF_HOTKEYACTIVE 4)

(defconstant-exported HCF_CONFIRMHOTKEY 8)

(defconstant-exported HCF_HOTKEYSOUND 16)

(defconstant-exported HCF_INDICATOR 32)

(defconstant-exported HCF_HOTKEYAVAILABLE 64)

(defconstant-exported MKF_AVAILABLE 2)

(defconstant-exported MKF_CONFIRMHOTKEY 8)

(defconstant-exported MKF_HOTKEYACTIVE 4)

(defconstant-exported MKF_HOTKEYSOUND 16)

(defconstant-exported MKF_INDICATOR 32)

(defconstant-exported MKF_MOUSEKEYSON 1)

(defconstant-exported MKF_MODIFIERS 64)

(defconstant-exported MKF_REPLACENUMBERS 128)

(defconstant-exported SERKF_ACTIVE 8)

(defconstant-exported SERKF_AVAILABLE 2)

(defconstant-exported SERKF_INDICATOR 4)

(defconstant-exported SERKF_SERIALKEYSON 1)

(defconstant-exported SSF_AVAILABLE 2)

(defconstant-exported SSF_SOUNDSENTRYON 1)

(defconstant-exported SSTF_BORDER 2)

(defconstant-exported SSTF_CHARS 1)

(defconstant-exported SSTF_DISPLAY 3)

(defconstant-exported SSTF_NONE 0)

(defconstant-exported SSGF_DISPLAY 3)

(defconstant-exported SSGF_NONE 0)

(defconstant-exported SSWF_CUSTOM 4)

(defconstant-exported SSWF_DISPLAY 3)

(defconstant-exported SSWF_NONE 0)

(defconstant-exported SSWF_TITLE 1)

(defconstant-exported SSWF_WINDOW 2)

(defconstant-exported SKF_AUDIBLEFEEDBACK 64)

(defconstant-exported SKF_AVAILABLE 2)

(defconstant-exported SKF_CONFIRMHOTKEY 8)

(defconstant-exported SKF_HOTKEYACTIVE 4)

(defconstant-exported SKF_HOTKEYSOUND 16)

(defconstant-exported SKF_INDICATOR 32)

(defconstant-exported SKF_STICKYKEYSON 1)

(defconstant-exported SKF_TRISTATE 128)

(defconstant-exported SKF_TWOKEYSOFF 256)

(defconstant-exported TKF_AVAILABLE 2)

(defconstant-exported TKF_CONFIRMHOTKEY 8)

(defconstant-exported TKF_HOTKEYACTIVE 4)

(defconstant-exported TKF_HOTKEYSOUND 16)

(defconstant-exported TKF_TOGGLEKEYSON 1)

(defconstant-exported MDITILE_SKIPDISABLED 2)

(defconstant-exported MDITILE_HORIZONTAL 1)

(defconstant-exported MDITILE_VERTICAL 0)

(defconstant-exported VK_LBUTTON 1)

(defconstant-exported VK_RBUTTON 2)

(defconstant-exported VK_CANCEL 3)

(defconstant-exported VK_MBUTTON 4)

(defconstant-exported VK_BACK 8)

(defconstant-exported VK_TAB 9)

(defconstant-exported VK_CLEAR 12)

(defconstant-exported VK_RETURN 13)

(defconstant-exported VK_SHIFT 16)

(defconstant-exported VK_CONTROL 17)

(defconstant-exported VK_MENU 18)

(defconstant-exported VK_PAUSE 19)

(defconstant-exported VK_CAPITAL 20)

(defconstant-exported VK_KANA #x15)

(defconstant-exported VK_HANGEUL #x15)

(defconstant-exported VK_HANGUL #x15)

(defconstant-exported VK_JUNJA #x17)

(defconstant-exported VK_FINAL #x18)

(defconstant-exported VK_HANJA #x19)

(defconstant-exported VK_KANJI #x19)

(defconstant-exported VK_ESCAPE #x1B)

(defconstant-exported VK_CONVERT #x1C)

(defconstant-exported VK_NONCONVERT #x1D)

(defconstant-exported VK_ACCEPT #x1E)

(defconstant-exported VK_MODECHANGE #x1F)

(defconstant-exported VK_SPACE 32)

(defconstant-exported VK_PRIOR 33)

(defconstant-exported VK_NEXT 34)

(defconstant-exported VK_END 35)

(defconstant-exported VK_HOME 36)

(defconstant-exported VK_LEFT 37)

(defconstant-exported VK_UP 38)

(defconstant-exported VK_RIGHT 39)

(defconstant-exported VK_DOWN 40)

(defconstant-exported VK_SELECT 41)

(defconstant-exported VK_PRINT 42)

(defconstant-exported VK_EXECUTE 43)

(defconstant-exported VK_SNAPSHOT 44)

(defconstant-exported VK_INSERT 45)

(defconstant-exported VK_DELETE 46)

(defconstant-exported VK_HELP 47)

(defconstant-exported VK_LWIN #x5B)

(defconstant-exported VK_RWIN #x5C)

(defconstant-exported VK_APPS #x5D)

(defconstant-exported VK_SLEEP #x5F)

(defconstant-exported VK_NUMPAD0 #x60)

(defconstant-exported VK_NUMPAD1 #x61)

(defconstant-exported VK_NUMPAD2 #x62)

(defconstant-exported VK_NUMPAD3 #x63)

(defconstant-exported VK_NUMPAD4 #x64)

(defconstant-exported VK_NUMPAD5 #x65)

(defconstant-exported VK_NUMPAD6 #x66)

(defconstant-exported VK_NUMPAD7 #x67)

(defconstant-exported VK_NUMPAD8 #x68)

(defconstant-exported VK_NUMPAD9 #x69)

(defconstant-exported VK_MULTIPLY #x6A)

(defconstant-exported VK_ADD #x6B)

(defconstant-exported VK_SEPARATOR #x6C)

(defconstant-exported VK_SUBTRACT #x6D)

(defconstant-exported VK_DECIMAL #x6E)

(defconstant-exported VK_DIVIDE #x6F)

(defconstant-exported VK_F1 #x70)

(defconstant-exported VK_F2 #x71)

(defconstant-exported VK_F3 #x72)

(defconstant-exported VK_F4 #x73)

(defconstant-exported VK_F5 #x74)

(defconstant-exported VK_F6 #x75)

(defconstant-exported VK_F7 #x76)

(defconstant-exported VK_F8 #x77)

(defconstant-exported VK_F9 #x78)

(defconstant-exported VK_F10 #x79)

(defconstant-exported VK_F11 #x7A)

(defconstant-exported VK_F12 #x7B)

(defconstant-exported VK_F13 #x7C)

(defconstant-exported VK_F14 #x7D)

(defconstant-exported VK_F15 #x7E)

(defconstant-exported VK_F16 #x7F)

(defconstant-exported VK_F17 #x80)

(defconstant-exported VK_F18 #x81)

(defconstant-exported VK_F19 #x82)

(defconstant-exported VK_F20 #x83)

(defconstant-exported VK_F21 #x84)

(defconstant-exported VK_F22 #x85)

(defconstant-exported VK_F23 #x86)

(defconstant-exported VK_F24 #x87)

(defconstant-exported VK_NUMLOCK #x90)

(defconstant-exported VK_SCROLL #x91)

(defconstant-exported VK_LSHIFT #xA0)

(defconstant-exported VK_RSHIFT #xA1)

(defconstant-exported VK_LCONTROL #xA2)

(defconstant-exported VK_RCONTROL #xA3)

(defconstant-exported VK_LMENU #xA4)

(defconstant-exported VK_RMENU #xA5)

(defconstant-exported VK_OEM_1 #xBA)

(defconstant-exported VK_OEM_2 #xBF)

(defconstant-exported VK_OEM_3 #xC0)

(defconstant-exported VK_OEM_4 #xDB)

(defconstant-exported VK_OEM_5 #xDC)

(defconstant-exported VK_OEM_6 #xDD)

(defconstant-exported VK_OEM_7 #xDE)

(defconstant-exported VK_OEM_8 #xDF)

(defconstant-exported VK_PROCESSKEY #xE5)

(defconstant-exported VK_ATTN #xF6)

(defconstant-exported VK_CRSEL #xF7)

(defconstant-exported VK_EXSEL #xF8)

(defconstant-exported VK_EREOF #xF9)

(defconstant-exported VK_PLAY #xFA)

(defconstant-exported VK_ZOOM #xFB)

(defconstant-exported VK_NONAME #xFC)

(defconstant-exported VK_PA1 #xFD)

(defconstant-exported VK_OEM_CLEAR #xFE)

(defconstant-exported TME_HOVER 1)

(defconstant-exported TME_LEAVE 2)

(defconstant-exported TME_QUERY #x40000000)

(defconstant-exported TME_CANCEL #x80000000)

(defconstant-exported HOVER_DEFAULT #xFFFFFFFF)

(defconstant-exported MK_LBUTTON 1)

(defconstant-exported MK_RBUTTON 2)

(defconstant-exported MK_SHIFT 4)

(defconstant-exported MK_CONTROL 8)

(defconstant-exported MK_MBUTTON 16)

(defconstant-exported TPM_CENTERALIGN 4)

(defconstant-exported TPM_LEFTALIGN 0)

(defconstant-exported TPM_RIGHTALIGN 8)

(defconstant-exported TPM_LEFTBUTTON 0)

(defconstant-exported TPM_RIGHTBUTTON 2)

(defconstant-exported TPM_HORIZONTAL 0)

(defconstant-exported TPM_VERTICAL 64)

(defconstant-exported TPM_TOPALIGN 0)

(defconstant-exported TPM_VCENTERALIGN 16)

(defconstant-exported TPM_BOTTOMALIGN 32)

(defconstant-exported TPM_NONOTIFY 128)

(defconstant-exported TPM_RETURNCMD 256)

(defconstant-exported HELP_COMMAND #x102)

(defconstant-exported HELP_CONTENTS 3)

(defconstant-exported HELP_CONTEXT 1)

(defconstant-exported HELP_CONTEXTPOPUP 8)

(defconstant-exported HELP_FORCEFILE 9)

(defconstant-exported HELP_HELPONHELP 4)

(defconstant-exported HELP_INDEX 3)

(defconstant-exported HELP_KEY #x101)

(defconstant-exported HELP_MULTIKEY #x201)

(defconstant-exported HELP_PARTIALKEY #x105)

(defconstant-exported HELP_QUIT 2)

(defconstant-exported HELP_SETCONTENTS 5)

(defconstant-exported HELP_SETINDEX 5)

(defconstant-exported HELP_SETWINPOS #x203)

(defconstant-exported HELP_CONTEXTMENU #xa)

(defconstant-exported HELP_FINDER #xb)

(defconstant-exported HELP_WM_HELP #xc)

(defconstant-exported HELP_TCARD #x8000)

(defconstant-exported HELP_TCARD_DATA 16)

(defconstant-exported HELP_TCARD_OTHER_CALLER #x11)

(defconstant-exported IDH_NO_HELP 28440)

(defconstant-exported IDH_MISSING_CONTEXT 28441)

(defconstant-exported IDH_GENERIC_HELP_BUTTON 28442)

(defconstant-exported IDH_OK 28443)

(defconstant-exported IDH_CANCEL 28444)

(defconstant-exported IDH_HELP 28445)

(defconstant-exported LB_CTLCODE 0)

(defconstant-exported LB_OKAY 0)

(defconstant-exported LB_ERR -1)

(defconstant-exported LB_ERRSPACE -2)

(defconstant-exported CB_OKAY 0)

(defconstant-exported CB_ERR -1)

(defconstant-exported CB_ERRSPACE -2)

(defconstant-exported HIDE_WINDOW 0)

(defconstant-exported SHOW_OPENWINDOW 1)

(defconstant-exported SHOW_ICONWINDOW 2)

(defconstant-exported SHOW_FULLSCREEN 3)

(defconstant-exported SHOW_OPENNOACTIVATE 4)

(defconstant-exported SW_PARENTCLOSING 1)

(defconstant-exported SW_OTHERZOOM 2)

(defconstant-exported SW_PARENTOPENING 3)

(defconstant-exported SW_OTHERUNZOOM 4)

(defconstant-exported KF_EXTENDED 256)

(defconstant-exported KF_DLGMODE 2048)

(defconstant-exported KF_MENUMODE 4096)

(defconstant-exported KF_ALTDOWN 8192)

(defconstant-exported KF_REPEAT 16384)

(defconstant-exported KF_UP 32768)

(defconstant-exported WSF_VISIBLE 1)

(defconstant-exported PWR_OK 1)

(defconstant-exported PWR_FAIL -1)

(defconstant-exported PWR_SUSPENDREQUEST 1)

(defconstant-exported PWR_SUSPENDRESUME 2)

(defconstant-exported PWR_CRITICALRESUME 3)

(defconstant-exported NFR_ANSI 1)

(defconstant-exported NFR_UNICODE 2)

(defconstant-exported NF_QUERY 3)

(defconstant-exported NF_REQUERY 4)

(defconstant-exported MENULOOP_WINDOW 0)

(defconstant-exported MENULOOP_POPUP 1)

(defconstant-exported WMSZ_LEFT 1)

(defconstant-exported WMSZ_RIGHT 2)

(defconstant-exported WMSZ_TOP 3)

(defconstant-exported WMSZ_TOPLEFT 4)

(defconstant-exported WMSZ_TOPRIGHT 5)

(defconstant-exported WMSZ_BOTTOM 6)

(defconstant-exported WMSZ_BOTTOMLEFT 7)

(defconstant-exported WMSZ_BOTTOMRIGHT 8)

(defconstant-exported HTERROR -2)

(defconstant-exported HTTRANSPARENT -1)

(defconstant-exported HTNOWHERE 0)

(defconstant-exported HTCLIENT 1)

(defconstant-exported HTCAPTION 2)

(defconstant-exported HTSYSMENU 3)

(defconstant-exported HTGROWBOX 4)

(defconstant-exported HTSIZE 4)

(defconstant-exported HTMENU 5)

(defconstant-exported HTHSCROLL 6)

(defconstant-exported HTVSCROLL 7)

(defconstant-exported HTMINBUTTON 8)

(defconstant-exported HTMAXBUTTON 9)

(defconstant-exported HTREDUCE 8)

(defconstant-exported HTZOOM 9)

(defconstant-exported HTLEFT 10)

(defconstant-exported HTSIZEFIRST 10)

(defconstant-exported HTRIGHT 11)

(defconstant-exported HTTOP 12)

(defconstant-exported HTTOPLEFT 13)

(defconstant-exported HTTOPRIGHT 14)

(defconstant-exported HTBOTTOM 15)

(defconstant-exported HTBOTTOMLEFT 16)

(defconstant-exported HTBOTTOMRIGHT 17)

(defconstant-exported HTSIZELAST 17)

(defconstant-exported HTBORDER 18)

(defconstant-exported HTOBJECT 19)

(defconstant-exported HTCLOSE 20)

(defconstant-exported HTHELP 21)

(defconstant-exported MA_ACTIVATE 1)

(defconstant-exported MA_ACTIVATEANDEAT 2)

(defconstant-exported MA_NOACTIVATE 3)

(defconstant-exported MA_NOACTIVATEANDEAT 4)

(defconstant-exported SIZE_RESTORED 0)

(defconstant-exported SIZE_MINIMIZED 1)

(defconstant-exported SIZE_MAXIMIZED 2)

(defconstant-exported SIZE_MAXSHOW 3)

(defconstant-exported SIZE_MAXHIDE 4)

(defconstant-exported SIZENORMAL 0)

(defconstant-exported SIZEICONIC 1)

(defconstant-exported SIZEFULLSCREEN 2)

(defconstant-exported SIZEZOOMSHOW 3)

(defconstant-exported SIZEZOOMHIDE 4)

(defconstant-exported WVR_ALIGNTOP 16)

(defconstant-exported WVR_ALIGNLEFT 32)

(defconstant-exported WVR_ALIGNBOTTOM 64)

(defconstant-exported WVR_ALIGNRIGHT 128)

(defconstant-exported WVR_HREDRAW 256)

(defconstant-exported WVR_VREDRAW 512)

(defconstant-exported WVR_REDRAW (cl:logior 256 512))

(defconstant-exported WVR_VALIDRECTS 1024)

(defconstant-exported PRF_CHECKVISIBLE 1)

(defconstant-exported PRF_NONCLIENT 2)

(defconstant-exported PRF_CLIENT 4)

(defconstant-exported PRF_ERASEBKGND 8)

(defconstant-exported PRF_CHILDREN 16)

(defconstant-exported PRF_OWNED 32)

(defconstant-exported IDANI_OPEN 1)

(defconstant-exported IDANI_CLOSE 2)

(defconstant-exported IDANI_CAPTION 3)

(defconstant-exported WPF_RESTORETOMAXIMIZED 2)

(defconstant-exported WPF_SETMINPOSITION 1)

(defconstant-exported ODT_MENU 1)

(defconstant-exported ODT_LISTBOX 2)

(defconstant-exported ODT_COMBOBOX 3)

(defconstant-exported ODT_BUTTON 4)

(defconstant-exported ODT_STATIC 5)

(defconstant-exported ODA_DRAWENTIRE 1)

(defconstant-exported ODA_SELECT 2)

(defconstant-exported ODA_FOCUS 4)

(defconstant-exported ODS_SELECTED 1)

(defconstant-exported ODS_GRAYED 2)

(defconstant-exported ODS_DISABLED 4)

(defconstant-exported ODS_CHECKED 8)

(defconstant-exported ODS_FOCUS 16)

(defconstant-exported ODS_DEFAULT 32)

(defconstant-exported ODS_COMBOBOXEDIT 4096)

(defconstant-exported IDHOT_SNAPWINDOW -1)

(defconstant-exported IDHOT_SNAPDESKTOP -2)

(defconstant-exported DBWF_LPARAMPOINTER #x8000)

(defconstant-exported DLGWINDOWEXTRA 30)

(defconstant-exported MNC_IGNORE 0)

(defconstant-exported MNC_CLOSE 1)

(defconstant-exported MNC_EXECUTE 2)

(defconstant-exported MNC_SELECT 3)

(defconstant-exported DOF_EXECUTABLE #x8001)

(defconstant-exported DOF_DOCUMENT #x8002)

(defconstant-exported DOF_DIRECTORY #x8003)

(defconstant-exported DOF_MULTIPLE #x8004)

(defconstant-exported DOF_PROGMAN 1)

(defconstant-exported DOF_SHELLDATA 2)

(defconstant-exported DO_DROPFILE #x454C4946)

(defconstant-exported DO_PRINTFILE #x544E5250)

(defconstant-exported SW_SCROLLCHILDREN 1)

(defconstant-exported SW_INVALIDATE 2)

(defconstant-exported SW_ERASE 4)

(defconstant-exported SC_SIZE #xF000)

(defconstant-exported SC_MOVE #xF010)

(defconstant-exported SC_MINIMIZE #xF020)

(defconstant-exported SC_ICON #xf020)

(defconstant-exported SC_MAXIMIZE #xF030)

(defconstant-exported SC_ZOOM #xF030)

(defconstant-exported SC_NEXTWINDOW #xF040)

(defconstant-exported SC_PREVWINDOW #xF050)

(defconstant-exported SC_CLOSE #xF060)

(defconstant-exported SC_VSCROLL #xF070)

(defconstant-exported SC_HSCROLL #xF080)

(defconstant-exported SC_MOUSEMENU #xF090)

(defconstant-exported SC_KEYMENU #xF100)

(defconstant-exported SC_ARRANGE #xF110)

(defconstant-exported SC_RESTORE #xF120)

(defconstant-exported SC_TASKLIST #xF130)

(defconstant-exported SC_SCREENSAVE #xF140)

(defconstant-exported SC_HOTKEY #xF150)

(defconstant-exported SC_DEFAULT #xF160)

(defconstant-exported SC_MONITORPOWER #xF170)

(defconstant-exported SC_CONTEXTHELP #xF180)

(defconstant-exported SC_SEPARATOR #xF00F)

(defconstant-exported EC_LEFTMARGIN 1)

(defconstant-exported EC_RIGHTMARGIN 2)

(defconstant-exported EC_USEFONTINFO #xffff)

(defconstant-exported DC_HASDEFID #x534B)

(defconstant-exported DLGC_WANTARROWS 1)

(defconstant-exported DLGC_WANTTAB 2)

(defconstant-exported DLGC_WANTALLKEYS 4)

(defconstant-exported DLGC_WANTMESSAGE 4)

(defconstant-exported DLGC_HASSETSEL 8)

(defconstant-exported DLGC_DEFPUSHBUTTON 16)

(defconstant-exported DLGC_UNDEFPUSHBUTTON 32)

(defconstant-exported DLGC_RADIOBUTTON 64)

(defconstant-exported DLGC_WANTCHARS 128)

(defconstant-exported DLGC_STATIC 256)

(defconstant-exported DLGC_BUTTON #x2000)

(defconstant-exported WA_INACTIVE 0)

(defconstant-exported WA_ACTIVE 1)

(defconstant-exported WA_CLICKACTIVE 2)

(defconstant-exported ICON_SMALL 0)

(defconstant-exported ICON_BIG 1)

(defconstant-exported MOD_ALT 1)

(defconstant-exported MOD_CONTROL 2)

(defconstant-exported MOD_SHIFT 4)

(defconstant-exported MOD_WIN 8)

(defconstant-exported MOD_IGNORE_ALL_MODIFIER 1024)

(defconstant-exported MOD_ON_KEYUP 2048)

(defconstant-exported MOD_RIGHT 16384)

(defconstant-exported MOD_LEFT 32768)

(defconstant-exported LLKHF_EXTENDED (cl:ash 256 -8))

(defconstant-exported LLKHF_INJECTED #x00000010)

(defconstant-exported LLKHF_ALTDOWN (cl:ash 8192 -8))

(defconstant-exported LLKHF_UP (cl:ash 32768 -8))

(defconstant-exported CURSOR_SHOWING #x00000001)

(defconstant-exported WS_ACTIVECAPTION #x00000001)

(defconstant-exported ENDSESSION_LOGOFF #x80000000)

(defconstant-exported GA_PARENT 1)

(defconstant-exported GA_ROOT 2)

(defconstant-exported GA_ROOTOWNER 3)













































(defcstructex-exported HHOOK__
        (i :int))



(defcstructex-exported HDWP__
        (i :int))



(defcstructex-exported HDEVNOTIFY__
        (i :int))



(defcstructex-exported ACCEL
        (fVirt :unsigned-char)
        (key :unsigned-short)
        (cmd :unsigned-short))





(defcstructex-exported ACCESSTIMEOUT
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (iTimeOutMSec :unsigned-long))





(defcstructex-exported ANIMATIONINFO
        (cbSize :unsigned-int)
        (iMinAnimate :int))





(defcstructex-exported CREATESTRUCTA
        (lpCreateParams :pointer)
        (hInstance :pointer)
        (hMenu :pointer)
        (hwndParent :pointer)
        (cy :int)
        (cx :int)
        (y :int)
        (x :int)
        (style :int32)
        (lpszName :string)
        (lpszClass :string)
        (dwExStyle :unsigned-long))





(defcstructex-exported CREATESTRUCTW
        (lpCreateParams :pointer)
        (hInstance :pointer)
        (hMenu :pointer)
        (hwndParent :pointer)
        (cy :int)
        (cx :int)
        (y :int)
        (x :int)
        (style :int32)
        (lpszName :pointer)
        (lpszClass :pointer)
        (dwExStyle :unsigned-long))





(defcstructex-exported CBT_CREATEWNDA
        (lpcs :pointer)
        (hwndInsertAfter :pointer))





(defcstructex-exported CBT_CREATEWNDW
        (lpcs :pointer)
        (hwndInsertAfter :pointer))





(defcstructex-exported CBTACTIVATESTRUCT
        (fMouse :int)
        (hWndActive :pointer))





(defcstructex-exported CLIENTCREATESTRUCT
        (hWindowMenu :pointer)
        (idFirstChild :unsigned-int))





(defcstructex-exported COMPAREITEMSTRUCT
        (CtlType :unsigned-int)
        (CtlID :unsigned-int)
        (hwndItem :pointer)
        (itemID1 :unsigned-int)
        (itemData1 :unsigned-long)
        (itemID2 :unsigned-int)
        (itemData2 :unsigned-long)
        (dwLocaleId :unsigned-long))





(defcstructex-exported COPYDATASTRUCT
        (dwData :unsigned-long)
        (cbData :unsigned-long)
        (lpData :pointer))





(defcstructex-exported CURSORSHAPE
        (xHotSpot :int)
        (yHotSpot :int)
        (cx :int)
        (cy :int)
        (cbWidth :int)
        (Planes :unsigned-char)
        (BitsPixel :unsigned-char))





(defcstructex-exported CWPRETSTRUCT
        (lResult :int32)
        (lParam :int32)
        (wParam :unsigned-int)
        (message :unsigned-long)
        (hwnd :pointer))



(defcstructex-exported CWPSTRUCT
        (lParam :int32)
        (wParam :unsigned-int)
        (message :unsigned-int)
        (hwnd :pointer))





(defcstructex-exported DEBUGHOOKINFO
        (idThread :unsigned-long)
        (idThreadInstaller :unsigned-long)
        (lParam :int32)
        (wParam :unsigned-int)
        (code :int))







(defcstructex-exported DELETEITEMSTRUCT
        (CtlType :unsigned-int)
        (CtlID :unsigned-int)
        (itemID :unsigned-int)
        (hwndItem :pointer)
        (itemData :unsigned-int))








(defcstructex-exported DRAWITEMSTRUCT
        (CtlType :unsigned-int)
        (CtlID :unsigned-int)
        (itemID :unsigned-int)
        (itemAction :unsigned-int)
        (itemState :unsigned-int)
        (hwndItem :pointer)
        (hDC :pointer)
        (rcItem RECT)
        (itemData :unsigned-long))







(defcstructex-exported DRAWTEXTPARAMS
        (cbSize :unsigned-int)
        (iTabLength :int)
        (iLeftMargin :int)
        (iRightMargin :int)
        (uiLengthDrawn :unsigned-int))











(defcstructex-exported ICONINFO
        (fIcon :int)
        (xHotspot :unsigned-long)
        (yHotspot :unsigned-long)
        (hbmMask :pointer)
        (hbmColor :pointer))





(defcstructex-exported NMHDR
        (hwndFrom :pointer)
        (idFrom :unsigned-int)
        (code :unsigned-int))










(defcstructex-exported MENUITEMINFOA
        (cbSize :unsigned-int)
        (fMask :unsigned-int)
        (fType :unsigned-int)
        (fState :unsigned-int)
        (wID :unsigned-int)
        (hSubMenu :pointer)
        (hbmpChecked :pointer)
        (hbmpUnchecked :pointer)
        (dwItemData :unsigned-long)
        (dwTypeData :string)
        (cch :unsigned-int))







(defcstructex-exported MENUITEMINFOW
        (cbSize :unsigned-int)
        (fMask :unsigned-int)
        (fType :unsigned-int)
        (fState :unsigned-int)
        (wID :unsigned-int)
        (hSubMenu :pointer)
        (hbmpChecked :pointer)
        (hbmpUnchecked :pointer)
        (dwItemData :unsigned-long)
        (dwTypeData :pointer)
        (cch :unsigned-int))







(defcstructex-exported SCROLLINFO
        (cbSize :unsigned-int)
        (fMask :unsigned-int)
        (nMin :int)
        (nMax :int)
        (nPage :unsigned-int)
        (nPos :int)
        (nTrackPos :int))







(defcstructex-exported WINDOWPLACEMENT
        (length :unsigned-int)
        (flags :unsigned-int)
        (showCmd :unsigned-int)
        (ptMinPosition POINT)
        (ptMaxPosition POINT)
        (rcNormalPosition RECT))







(defcstructex-exported MENUITEMTEMPLATEHEADER
        (versionNumber :unsigned-short)
        (offset :unsigned-short))

(defcstructex-exported MENUITEMTEMPLATE
        (mtOption :unsigned-short)
        (mtID :unsigned-short)
        (mtString :pointer))













(defcstructex-exported HELPINFO
        (cbSize :unsigned-int)
        (iContextType :int)
        (iCtrlId :int)
        (hItemHandle :pointer)
        (dwContextId :unsigned-long)
        (MousePos POINT))







(defcstructex-exported MSGBOXPARAMSA
        (cbSize :unsigned-int)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (lpszText :string)
        (lpszCaption :string)
        (dwStyle :unsigned-long)
        (lpszIcon :string)
        (dwContextHelpId :unsigned-long)
        (lpfnMsgBoxCallback :pointer)
        (dwLanguageId :unsigned-long))





(defcstructex-exported MSGBOXPARAMSW
        (cbSize :unsigned-int)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (lpszText :pointer)
        (lpszCaption :pointer)
        (dwStyle :unsigned-long)
        (lpszIcon :pointer)
        (dwContextHelpId :unsigned-long)
        (lpfnMsgBoxCallback :pointer)
        (dwLanguageId :unsigned-long))





(defcstructex-exported USEROBJECTFLAGS
        (fInherit :int)
        (fReserved :int)
        (dwFlags :unsigned-long))



(defcstructex-exported FILTERKEYS
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (iWaitMSec :unsigned-long)
        (iDelayMSec :unsigned-long)
        (iRepeatMSec :unsigned-long)
        (iBounceMSec :unsigned-long))



(defcstructex-exported HIGHCONTRASTA
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (lpszDefaultScheme :string))





(defcstructex-exported HIGHCONTRASTW
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (lpszDefaultScheme :pointer))





(defcstructex-exported ICONMETRICSA
        (cbSize :unsigned-int)
        (iHorzSpacing :int)
        (iVertSpacing :int)
        (iTitleWrap :int)
        (lfFont LOGFONTA))





(defcstructex-exported ICONMETRICSW
        (cbSize :unsigned-int)
        (iHorzSpacing :int)
        (iVertSpacing :int)
        (iTitleWrap :int)
        (lfFont LOGFONTW))





(defcstructex-exported MINIMIZEDMETRICS
        (cbSize :unsigned-int)
        (iWidth :int)
        (iHorzGap :int)
        (iVertGap :int)
        (iArrange :int))





(defcstructex-exported MOUSEKEYS
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (iMaxSpeed :unsigned-long)
        (iTimeToMaxSpeed :unsigned-long)
        (iCtrlSpeed :unsigned-long)
        (dwReserved1 :unsigned-long)
        (dwReserved2 :unsigned-long))





(defcstructex-exported NONCLIENTMETRICSA
        (cbSize :unsigned-int)
        (iBorderWidth :int)
        (iScrollWidth :int)
        (iScrollHeight :int)
        (iCaptionWidth :int)
        (iCaptionHeight :int)
        (lfCaptionFont LOGFONTA)
        (iSmCaptionWidth :int)
        (iSmCaptionHeight :int)
        (lfSmCaptionFont LOGFONTA)
        (iMenuWidth :int)
        (iMenuHeight :int)
        (lfMenuFont LOGFONTA)
        (lfStatusFont LOGFONTA)
        (lfMessageFont LOGFONTA))





(defcstructex-exported NONCLIENTMETRICSW
        (cbSize :unsigned-int)
        (iBorderWidth :int)
        (iScrollWidth :int)
        (iScrollHeight :int)
        (iCaptionWidth :int)
        (iCaptionHeight :int)
        (lfCaptionFont LOGFONTW)
        (iSmCaptionWidth :int)
        (iSmCaptionHeight :int)
        (lfSmCaptionFont LOGFONTW)
        (iMenuWidth :int)
        (iMenuHeight :int)
        (lfMenuFont LOGFONTW)
        (lfStatusFont LOGFONTW)
        (lfMessageFont LOGFONTW))





(defcstructex-exported SERIALKEYSA
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (lpszActivePort :string)
        (lpszPort :string)
        (iBaudRate :unsigned-int)
        (iPortState :unsigned-int)
        (iActive :unsigned-int))





(defcstructex-exported SERIALKEYSW
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (lpszActivePort :pointer)
        (lpszPort :pointer)
        (iBaudRate :unsigned-int)
        (iPortState :unsigned-int)
        (iActive :unsigned-int))





(defcstructex-exported SOUNDSENTRYA
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (iFSTextEffect :unsigned-long)
        (iFSTextEffectMSec :unsigned-long)
        (iFSTextEffectColorBits :unsigned-long)
        (iFSGrafEffect :unsigned-long)
        (iFSGrafEffectMSec :unsigned-long)
        (iFSGrafEffectColor :unsigned-long)
        (iWindowsEffect :unsigned-long)
        (iWindowsEffectMSec :unsigned-long)
        (lpszWindowsEffectDLL :string)
        (iWindowsEffectOrdinal :unsigned-long))





(defcstructex-exported SOUNDSENTRYW
        (cbSize :unsigned-int)
        (dwFlags :unsigned-long)
        (iFSTextEffect :unsigned-long)
        (iFSTextEffectMSec :unsigned-long)
        (iFSTextEffectColorBits :unsigned-long)
        (iFSGrafEffect :unsigned-long)
        (iFSGrafEffectMSec :unsigned-long)
        (iFSGrafEffectColor :unsigned-long)
        (iWindowsEffect :unsigned-long)
        (iWindowsEffectMSec :unsigned-long)
        (lpszWindowsEffectDLL :pointer)
        (iWindowsEffectOrdinal :unsigned-long))





(defcstructex-exported STICKYKEYS
        (cbSize :unsigned-long)
        (dwFlags :unsigned-long))





(defcstructex-exported TOGGLEKEYS
        (cbSize :unsigned-long)
        (dwFlags :unsigned-long))



(defcstructex-exported MOUSEHOOKSTRUCT
        (pt POINT)
        (hwnd :pointer)
        (wHitTestCode :unsigned-int)
        (dwExtraInfo :unsigned-long))







(defcstructex-exported TRACKMOUSEEVENT
        (cbSize :unsigned-long)
        (dwFlags :unsigned-long)
        (hwndTrack :pointer)
        (dwHoverTime :unsigned-long))





(defcstructex-exported TPMPARAMS
        (cbSize :unsigned-int)
        (rcExclude RECT))





(defcstructex-exported EVENTMSG
        (message :unsigned-int)
        (paramL :unsigned-int)
        (paramH :unsigned-int)
        (time :unsigned-long)
        (hwnd :pointer))











(defcstructex-exported WINDOWPOS
        (hwnd :pointer)
        (hwndInsertAfter :pointer)
        (x :int)
        (y :int)
        (cx :int)
        (cy :int)
        (flags :unsigned-int))







(defcstructex-exported NCCALCSIZE_PARAMS
        (rgrc :pointer)
        (lppos :pointer))





(defcstructex-exported MDICREATESTRUCTA
        (szClass :string)
        (szTitle :string)
        (hOwner :pointer)
        (x :int)
        (y :int)
        (cx :int)
        (cy :int)
        (style :unsigned-long)
        (lParam :int32))





(defcstructex-exported MDICREATESTRUCTW
        (szClass :pointer)
        (szTitle :pointer)
        (hOwner :pointer)
        (x :int)
        (y :int)
        (cx :int)
        (cy :int)
        (style :unsigned-long)
        (lParam :int32))





(defcstructex-exported MINMAXINFO
        (ptReserved POINT)
        (ptMaxSize POINT)
        (ptMaxPosition POINT)
        (ptMinTrackSize POINT)
        (ptMaxTrackSize POINT))







(defcstructex-exported MDINEXTMENU
        (hmenuIn :pointer)
        (hmenuNext :pointer)
        (hwndNext :pointer))







(defcstructex-exported MEASUREITEMSTRUCT
        (CtlType :unsigned-int)
        (CtlID :unsigned-int)
        (itemID :unsigned-int)
        (itemWidth :unsigned-int)
        (itemHeight :unsigned-int)
        (itemData :unsigned-long))







(defcstructex-exported DROPSTRUCT
        (hwndSource :pointer)
        (hwndSink :pointer)
        (wFmt :unsigned-long)
        (dwData :unsigned-long)
        (ptDrop POINT)
        (dwControlData :unsigned-long))









(defcstructex-exported MULTIKEYHELPA
        (mkSize :unsigned-long)
        (mkKeylist :char)
        (szKeyphrase :pointer))







(defcstructex-exported MULTIKEYHELPW
        (mkSize :unsigned-long)
        (mkKeylist :pointer)
        (szKeyphrase :pointer))







(defcstructex-exported HELPWININFOA
        (wStructSize :int)
        (x :int)
        (y :int)
        (dx :int)
        (dy :int)
        (wMax :int)
        (rgchMember :pointer))







(defcstructex-exported HELPWININFOW
        (wStructSize :int)
        (x :int)
        (y :int)
        (dx :int)
        (dy :int)
        (wMax :int)
        (rgchMember :pointer))







(defcstructex-exported STYLESTRUCT
        (styleOld :unsigned-long)
        (styleNew :unsigned-long))





(defcstructex-exported ALTTABINFO
        (cbSize :unsigned-long)
        (cItems :int)
        (cColumns :int)
        (cRows :int)
        (iColFocus :int)
        (iRowFocus :int)
        (cxItem :int)
        (cyItem :int)
        (ptStart POINT))







(defcstructex-exported COMBOBOXINFO
        (cbSize :unsigned-long)
        (rcItem RECT)
        (rcButton RECT)
        (stateButton :unsigned-long)
        (hwndCombo :pointer)
        (hwndItem :pointer)
        (hwndList :pointer))







(defcstructex-exported CURSORINFO
        (cbSize :unsigned-long)
        (flags :unsigned-long)
        (hCursor :pointer)
        (ptScreenPos POINT))







(defcstructex-exported MENUBARINFO
        (cbSize :unsigned-long)
        (rcBar RECT)
        (hMenu :pointer)
        (hwndMenu :pointer)
        (fBarFocused :int)
        (fFocused :int))





(defcstructex-exported MENUINFO
        (cbSize :unsigned-long)
        (fMask :unsigned-long)
        (dwStyle :unsigned-long)
        (cyMax :unsigned-int)
        (hbrBack :pointer)
        (dwContextHelpID :unsigned-long)
        (dwMenuData :unsigned-long))







(defconstant-exported CCHILDREN_SCROLLBAR 5)

(defcstructex-exported SCROLLBARINFO
        (cbSize :unsigned-long)
        (rcScrollBar RECT)
        (dxyLineButton :int)
        (xyThumbTop :int)
        (xyThumbBottom :int)
        (reserved :int)
        (rgstate :pointer))







(defconstant-exported CCHILDREN_TITLEBAR 5)

(defcstructex-exported TITLEBARINFO
        (cbSize :unsigned-long)
        (rcTitleBar RECT)
        (rgstate :pointer))







(defcstructex-exported WINDOWINFO
        (cbSize :unsigned-long)
        (rcWindow RECT)
        (rcClient RECT)
        (dwStyle :unsigned-long)
        (dwExStyle :unsigned-long)
        (dwWindowStatus :unsigned-long)
        (cxWindowBorders :unsigned-int)
        (cyWindowBorders :unsigned-int)
        (atomWindowType :unsigned-short)
        (wCreatorVersion :unsigned-short))







(defcstructex-exported LASTINPUTINFO
        (cbSize :unsigned-int)
        (dwTime :unsigned-long))





(defcstructex-exported MONITORINFO
        (cbSize :unsigned-long)
        (rcMonitor RECT)
        (rcWork RECT)
        (dwFlags :unsigned-long))





(defcstructex-exported MONITORINFOEXA
        (cbSize :unsigned-long)
        (rcMonitor RECT)
        (rcWork RECT)
        (dwFlags :unsigned-long)
        (szDevice :pointer))





(defcstructex-exported MONITORINFOEXW
        (cbSize :unsigned-long)
        (rcMonitor RECT)
        (rcWork RECT)
        (dwFlags :unsigned-long)
        (szDevice :pointer))





(defcstructex-exported KBDLLHOOKSTRUCT
        (vkCode :unsigned-long)
        (scanCode :unsigned-long)
        (flags :unsigned-long)
        (time :unsigned-long)
        (dwExtraInfo :unsigned-long))







(defcstructex-exported MSLLHOOKSTRUCT
        (pt POINT)
        (mouseData :unsigned-long)
        (flags :unsigned-long)
        (time :unsigned-long)
        (dwExtraInfo :unsigned-long))



(defcfunex-exported ("ChangeDisplaySettingsA" ChangeDisplaySettingsA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ChangeDisplaySettingsW" ChangeDisplaySettingsW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ChangeDisplaySettingsExA" ChangeDisplaySettingsExA :convention :stdcall) :int32
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("ChangeDisplaySettingsExW" ChangeDisplaySettingsExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("CheckDlgButton" CheckDlgButton :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int))

(defcfunex-exported ("CheckMenuItem" CheckMenuItem :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("CheckMenuRadioItem" CheckMenuRadioItem :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int))

(defcfunex-exported ("CheckRadioButton" CheckRadioButton :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("CopyRect" CopyRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CreateMDIWindowA" CreateMDIWindowA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :int32))

(defcfunex-exported ("CreateMDIWindowW" CreateMDIWindowW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :int32))

(defcfunex-exported ("CreateMenu" CreateMenu :convention :stdcall) :pointer)

(defcfunex-exported ("CreatePopupMenu" CreatePopupMenu :convention :stdcall) :pointer)

(defcfunex-exported ("DefDlgProcA" DefDlgProcA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("DefDlgProcW" DefDlgProcW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("DefFrameProcA" DefFrameProcA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("DefFrameProcW" DefFrameProcW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("DefMDIChildProcA" DefMDIChildProcA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("DefMDIChildProcW" DefMDIChildProcW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("DeleteMenu" DeleteMenu :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("DeregisterShellHookWindow" DeregisterShellHookWindow :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DestroyMenu" DestroyMenu :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DialogBoxIndirectParamA" DialogBoxIndirectParamA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("DialogBoxIndirectParamW" DialogBoxIndirectParamW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("DialogBoxParamA" DialogBoxParamA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("DialogBoxParamW" DialogBoxParamW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("DlgDirListA" DlgDirListA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("DlgDirListW" DlgDirListW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("DlgDirListComboBoxA" DlgDirListComboBoxA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("DlgDirListComboBoxW" DlgDirListComboBoxW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("DlgDirSelectComboBoxExA" DlgDirSelectComboBoxExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("DlgDirSelectComboBoxExW" DlgDirSelectComboBoxExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("DlgDirSelectExA" DlgDirSelectExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("DlgDirSelectExW" DlgDirSelectExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("DragObject" DragObject :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("DrawMenuBar" DrawMenuBar :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnableMenuItem" EnableMenuItem :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("EndDialog" EndDialog :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("EndMenu" EndMenu :convention :stdcall) :int)

(defcfunex-exported ("EnumChildWindows" EnumChildWindows :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumDisplayMonitors" EnumDisplayMonitors :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("EnumDisplaySettingsA" EnumDisplaySettingsA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("EnumDisplaySettingsW" EnumDisplaySettingsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("EnumDisplayDevicesA" EnumDisplayDevicesA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("EnumDisplayDevicesW" EnumDisplayDevicesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("EnumThreadWindows" EnumThreadWindows :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EqualRect" EqualRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ExitWindowsEx" ExitWindowsEx :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long))

(defcfunex-exported ("FrameRect" FrameRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("FrameRgn" FrameRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("GetAncestor" GetAncestor :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))



(defcfunex-exported ("GetFocus" GetFocus :convention :stdcall) :pointer)

(defcfunex-exported ("GetInputState" GetInputState :convention :stdcall) :int)

(defcfunex-exported ("GetKeyboardState" GetKeyboardState :convention :stdcall) :int
  (arg0 :pointer))



(defcfunex-exported ("GetMenuState" GetMenuState :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("GetMenuStringA" GetMenuStringA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("GetMenuStringW" GetMenuStringW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("GetNextDlgGroupItem" GetNextDlgGroupItem :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetNextDlgTabItem" GetNextDlgTabItem :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetParent" GetParent :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetQueueStatus" GetQueueStatus :convention :stdcall) :unsigned-long
  (arg0 :unsigned-int))

(defcfunex-exported ("GetSubMenu" GetSubMenu :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetSysColorBrush" GetSysColorBrush :convention :stdcall) :pointer
  (arg0 :int))

(defcfunex-exported ("GetSystemMenu" GetSystemMenu :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetWindowLongA" GetWindowLongA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetWindowLongW" GetWindowLongW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetWindow" GetWindow :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetWindowPlacement" GetWindowPlacement :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetWindowRect" GetWindowRect :convention :stdcall) :int
  (hWnd :pointer)
  (lpRECT :pointer))

(defcfunex-exported ("GetWindowWord" GetWindowWord :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetAltTabInfoA" GetAltTabInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :string)
  (arg4 :unsigned-int))

(defcfunex-exported ("GetAltTabInfoW" GetAltTabInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-int))

(defcfunex-exported ("GetComboBoxInfo" GetComboBoxInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCursorInfo" GetCursorInfo :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetLastInputInfo" GetLastInputInfo :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetListBoxInfo" GetListBoxInfo :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetMenuBarInfo" GetMenuBarInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :int32)
  (arg3 :pointer))

(defcfunex-exported ("GetMenuInfo" GetMenuInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetScrollBarInfo" GetScrollBarInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :pointer))

(defcfunex-exported ("GetTitleBarInfo" GetTitleBarInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetWindowInfo" GetWindowInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetMonitorInfoA" GetMonitorInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetMonitorInfoW" GetMonitorInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetWindowModuleFileNameA" GetWindowModuleFileNameA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("GetWindowModuleFileNameW" GetWindowModuleFileNameW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("HiliteMenuItem" HiliteMenuItem :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int))

(defcfunex-exported ("InflateRect" InflateRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("InsertMenuItemA" InsertMenuItemA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("InsertMenuItemW" InsertMenuItemW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("InternalGetWindowText" InternalGetWindowText :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("IntersectRect" IntersectRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("InvertRect" InvertRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IsChild" IsChild :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IsDialogMessageA" IsDialogMessageA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IsDialogMessageW" IsDialogMessageW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IsDlgButtonChecked" IsDlgButtonChecked :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("IsIconic" IsIconic :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsMenu" IsMenu :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsRectEmpty" IsRectEmpty :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsZoomed" IsZoomed :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("keybd_event" keybd_event :convention :stdcall) :void
  (arg0 :unsigned-char)
  (arg1 :unsigned-char)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("LoadKeyboardLayoutA" LoadKeyboardLayoutA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-int))

(defcfunex-exported ("LoadKeyboardLayoutW" LoadKeyboardLayoutW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("LoadMenuA" LoadMenuA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("LoadMenuIndirectA" LoadMenuIndirectA :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LoadMenuIndirectW" LoadMenuIndirectW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LoadMenuW" LoadMenuW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("LoadStringA" LoadStringA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :int))

(defcfunex-exported ("LoadStringW" LoadStringW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("MapDialogRect" MapDialogRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("MapVirtualKeyA" MapVirtualKeyA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int))

(defcfunex-exported ("MapVirtualKeyExA" MapVirtualKeyExA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("MapVirtualKeyExW" MapVirtualKeyExW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("MapVirtualKeyW" MapVirtualKeyW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int))

(defcfunex-exported ("MapWindowPoints" MapWindowPoints :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("MenuItemFromPoint" MenuItemFromPoint :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 POINT))

(defcfunex-exported ("OemKeyScan" OemKeyScan :convention :stdcall) :unsigned-long
  (arg0 :unsigned-short))

(defcfunex-exported ("OffsetRect" OffsetRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("PtInRect" PtInRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 POINT))

(defcfunex-exported ("RealChildWindowFromPoint" RealChildWindowFromPoint :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 POINT))

(defcfunex-exported ("RealGetWindowClassA" RealGetWindowClassA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("RealGetWindowClassW" RealGetWindowClassW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("RegisterWindowMessageA" RegisterWindowMessageA :convention :stdcall) :unsigned-int
  (arg0 :string))

(defcfunex-exported ("RegisterWindowMessageW" RegisterWindowMessageW :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("ReplyMessage" ReplyMessage :convention :stdcall) :int
  (arg0 :int32))

(defcfunex-exported ("SendNotifyMessageA" SendNotifyMessageA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("SendNotifyMessageW" SendNotifyMessageW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("SetForegroundWindow" SetForegroundWindow :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetMenuInfo" SetMenuInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetMessageQueue" SetMessageQueue :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("SetRect" SetRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("SetRectEmpty" SetRectEmpty :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SubtractRect" SubtractRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ToAscii" ToAscii :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-int))

(defcfunex-exported ("TranslateMDISysAccel" TranslateMDISysAccel :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UnionRect" UnionRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

