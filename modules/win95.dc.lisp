
(cl:in-package w32apimod)

(define-w32api-module win95.dc :win95.dc)

(cl:in-package cl-w32api.module.win95.dc)

(defcfunex-exported ("CancelDC" CancelDC :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CreateCompatibleDC" CreateCompatibleDC :convention :stdcall) :pointer
  (hdc :pointer))

(defcfunex-exported ("CreateDCA" CreateDCA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("CreateDCW" CreateDCW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("CreateICA" CreateICA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("CreateICW" CreateICW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("DeleteDC" DeleteDC :convention :stdcall) :int
  (hdc :pointer))

(defcfunex-exported ("DeleteObject" DeleteObject :convention :stdcall) :int
  (hObject :pointer))

(defcfunex-exported ("DrawEscape" DrawEscape :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string))

(defcfunex-exported ("EnumObjects" EnumObjects :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("GetCurrentObject" GetCurrentObject :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetDC" GetDC :convention :stdcall) :pointer
  (hWnd :pointer))

(defcfunex-exported ("GetDCEx" GetDCEx :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetObjectA" GetObjectA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("GetObjectW" GetObjectW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("GetObjectType" GetObjectType :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("ReleaseDC" ReleaseDC :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ResetDCA" ResetDCA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ResetDCW" ResetDCW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RestoreDC" RestoreDC :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SaveDC" SaveDC :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SelectObject" SelectObject :convention :stdcall) :pointer
  (hdc :pointer)
  (hgdiobj :pointer))

