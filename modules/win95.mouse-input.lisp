
(cl:in-package w32apimod)

(define-w32api-module win95.mouse-input :win95.mouse-input)

(cl:in-package cl-w32api.module.win95.mouse-input)

(defcfunex-exported ("DragDetect" DragDetect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 POINT))

(defcfunex-exported ("GetCapture" GetCapture :convention :stdcall) :pointer)

(defcfunex-exported ("GetDoubleClickTime" GetDoubleClickTime :convention :stdcall) :unsigned-int)

(defcfunex-exported ("mouse_event" mouse_event :convention :stdcall) :void
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("ReleaseCapture" ReleaseCapture :convention :stdcall) :int)

(defcfunex-exported ("SetCapture" SetCapture :convention :stdcall) :pointer
  (hWnd :pointer))

(defcfunex-exported ("SetDoubleClickTime" SetDoubleClickTime :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("SwapMouseButton" SwapMouseButton :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("TrackMouseEvent" TrackMouseEvent :convention :stdcall) :int
  (arg0 :pointer))

