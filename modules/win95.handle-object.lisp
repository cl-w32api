(defcfunex-exported ("CloseHandle" CloseHandle :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DuplicateHandle" DuplicateHandle :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :int)
  (arg6 :unsigned-long))

(defcfunex-exported ("GetHandleInformation" GetHandleInformation :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetHandleInformation" SetHandleInformation :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

