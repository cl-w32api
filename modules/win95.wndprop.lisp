
(cl:in-package w32apimod)

(define-w32api-module win95.wndprop :win95.wndprop)

(cl:in-package cl-w32api.module.win95.wndprop)

(defcfunex-exported ("EnumPropsA" EnumPropsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("EnumPropsW" EnumPropsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("EnumPropsExA" EnumPropsExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumPropsExW" EnumPropsExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("GetPropA" GetPropA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("GetPropW" GetPropW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RemovePropA" RemovePropA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("RemovePropW" RemovePropW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetPropA" SetPropA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("SetPropW" SetPropW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

