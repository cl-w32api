(cl:in-package w32apimod)

(define-w32api-module shell :shell)

(cl:in-package cl-w32api.module.shell)




(defconstant-exported ABE_LEFT 0)

(defconstant-exported ABE_TOP 1)

(defconstant-exported ABE_RIGHT 2)

(defconstant-exported ABE_BOTTOM 3)

(defconstant-exported ABS_AUTOHIDE 1)

(defconstant-exported ABS_ALWAYSONTOP 2)

(defconstant-exported SEE_MASK_CLASSNAME 1)

(defconstant-exported SEE_MASK_CLASSKEY 3)

(defconstant-exported SEE_MASK_IDLIST 4)

(defconstant-exported SEE_MASK_INVOKEIDLIST 12)

(defconstant-exported SEE_MASK_ICON #x10)

(defconstant-exported SEE_MASK_HOTKEY #x20)

(defconstant-exported SEE_MASK_NOCLOSEPROCESS #x40)

(defconstant-exported SEE_MASK_CONNECTNETDRV #x80)

(defconstant-exported SEE_MASK_FLAG_DDEWAIT #x100)

(defconstant-exported SEE_MASK_DOENVSUBST #x200)

(defconstant-exported SEE_MASK_FLAG_NO_UI #x400)

(defconstant-exported SEE_MASK_NO_CONSOLE #x8000)

(defconstant-exported SEE_MASK_UNICODE #x10000)

(defconstant-exported SEE_MASK_ASYNCOK #x100000)

(defconstant-exported SEE_MASK_HMONITOR #x200000)

(defconstant-exported ABM_NEW 0)

(defconstant-exported ABM_REMOVE 1)

(defconstant-exported ABM_QUERYPOS 2)

(defconstant-exported ABM_SETPOS 3)

(defconstant-exported ABM_GETSTATE 4)

(defconstant-exported ABM_GETTASKBARPOS 5)

(defconstant-exported ABM_ACTIVATE 6)

(defconstant-exported ABM_GETAUTOHIDEBAR 7)

(defconstant-exported ABM_SETAUTOHIDEBAR 8)

(defconstant-exported ABM_WINDOWPOSCHANGED 9)

(defconstant-exported ABN_STATECHANGE 0)

(defconstant-exported ABN_POSCHANGED 1)

(defconstant-exported ABN_FULLSCREENAPP 2)

(defconstant-exported ABN_WINDOWARRANGE 3)

(defconstant-exported NIM_ADD 0)

(defconstant-exported NIM_MODIFY 1)

(defconstant-exported NIM_DELETE 2)

(defconstant-exported NIF_MESSAGE #x00000001)

(defconstant-exported NIF_ICON #x00000002)

(defconstant-exported NIF_TIP #x00000004)

(defconstant-exported NIF_STATE #x00000008)

(defconstant-exported NIS_HIDDEN 1)

(defconstant-exported NIS_SHAREDICON 2)

(defconstant-exported SE_ERR_FNF 2)

(defconstant-exported SE_ERR_PNF 3)

(defconstant-exported SE_ERR_ACCESSDENIED 5)

(defconstant-exported SE_ERR_OOM 8)

(defconstant-exported SE_ERR_DLLNOTFOUND 32)

(defconstant-exported SE_ERR_SHARE 26)

(defconstant-exported SE_ERR_ASSOCINCOMPLETE 27)

(defconstant-exported SE_ERR_DDETIMEOUT 28)

(defconstant-exported SE_ERR_DDEFAIL 29)

(defconstant-exported SE_ERR_DDEBUSY 30)

(defconstant-exported SE_ERR_NOASSOC 31)

(defconstant-exported FO_MOVE 1)

(defconstant-exported FO_COPY 2)

(defconstant-exported FO_DELETE 3)

(defconstant-exported FO_RENAME 4)

(defconstant-exported FOF_MULTIDESTFILES 1)

(defconstant-exported FOF_CONFIRMMOUSE 2)

(defconstant-exported FOF_SILENT 4)

(defconstant-exported FOF_RENAMEONCOLLISION 8)

(defconstant-exported FOF_NOCONFIRMATION 16)

(defconstant-exported FOF_WANTMAPPINGHANDLE 32)

(defconstant-exported FOF_ALLOWUNDO 64)

(defconstant-exported FOF_FILESONLY 128)

(defconstant-exported FOF_SIMPLEPROGRESS 256)

(defconstant-exported FOF_NOCONFIRMMKDIR 512)

(defconstant-exported FOF_NOERRORUI 1024)

(defconstant-exported FOF_NOCOPYSECURITYATTRIBS 2048)

(defconstant-exported PO_DELETE 19)

(defconstant-exported PO_RENAME 20)

(defconstant-exported PO_PORTCHANGE 32)

(defconstant-exported PO_REN_PORT 52)

(defconstant-exported SHGFI_ICON 256)

(defconstant-exported SHGFI_DISPLAYNAME 512)

(defconstant-exported SHGFI_TYPENAME 1024)

(defconstant-exported SHGFI_ATTRIBUTES 2048)

(defconstant-exported SHGFI_ICONLOCATION 4096)

(defconstant-exported SHGFI_EXETYPE 8192)

(defconstant-exported SHGFI_SYSICONINDEX 16384)

(defconstant-exported SHGFI_LINKOVERLAY 32768)

(defconstant-exported SHGFI_SELECTED 65536)

(defconstant-exported SHGFI_ATTR_SPECIFIED 131072)

(defconstant-exported SHGFI_LARGEICON 0)

(defconstant-exported SHGFI_SMALLICON 1)

(defconstant-exported SHGFI_OPENICON 2)

(defconstant-exported SHGFI_SHELLICONSIZE 4)

(defconstant-exported SHGFI_PIDL 8)

(defconstant-exported SHGFI_USEFILEATTRIBUTES 16)

(defconstant-exported SHERB_NOCONFIRMATION 1)

(defconstant-exported SHERB_NOPROGRESSUI 2)

(defconstant-exported SHERB_NOSOUND 4)





(defcstructex-exported APPBARDATA
        (cbSize :unsigned-long)
        (hWnd :pointer)
        (uCallbackMessage :unsigned-int)
        (uEdge :unsigned-int)
        (rc RECT)
        (lParam :int32))





(defcstructex-exported HDROP__
        (i :int))



(defcstructex-exported NOTIFYICONDATAA
        (cbSize :unsigned-long)
        (hWnd :pointer)
        (uID :unsigned-int)
        (uFlags :unsigned-int)
        (uCallbackMessage :unsigned-int)
        (hIcon :pointer)
        (szTip :pointer))





(defcstructex-exported NOTIFYICONDATAW
        (cbSize :unsigned-long)
        (hWnd :pointer)
        (uID :unsigned-int)
        (uFlags :unsigned-int)
        (uCallbackMessage :unsigned-int)
        (hIcon :pointer)
        (szTip :pointer))





(defcstructex-exported SHELLEXECUTEINFOA
        (cbSize :unsigned-long)
        (fMask :unsigned-long)
        (hwnd :pointer)
        (lpVerb :string)
        (lpFile :string)
        (lpParameters :string)
        (lpDirectory :string)
        (nShow :int)
        (hInstApp :pointer)
        (lpIDList :pointer)
        (lpClass :string)
        (hkeyClass :pointer)
        (dwHotKey :unsigned-long)
        (hIcon :pointer)
        (hProcess :pointer))





(defcstructex-exported SHELLEXECUTEINFOW
        (cbSize :unsigned-long)
        (fMask :unsigned-long)
        (hwnd :pointer)
        (lpVerb :pointer)
        (lpFile :pointer)
        (lpParameters :pointer)
        (lpDirectory :pointer)
        (nShow :int)
        (hInstApp :pointer)
        (lpIDList :pointer)
        (lpClass :pointer)
        (hkeyClass :pointer)
        (dwHotKey :unsigned-long)
        (hIcon :pointer)
        (hProcess :pointer))





(defcstructex-exported SHFILEOPSTRUCTA
        (hwnd :pointer)
        (wFunc :unsigned-int)
        (pFrom :string)
        (pTo :string)
        (fFlags :unsigned-short)
        (fAnyOperationsAborted :int)
        (hNameMappings :pointer)
        (lpszProgressTitle :string))





(defcstructex-exported SHFILEOPSTRUCTW
        (hwnd :pointer)
        (wFunc :unsigned-int)
        (pFrom :pointer)
        (pTo :pointer)
        (fFlags :unsigned-short)
        (fAnyOperationsAborted :int)
        (hNameMappings :pointer)
        (lpszProgressTitle :pointer))





(defcstructex-exported SHFILEINFOA
        (hIcon :pointer)
        (iIcon :int)
        (dwAttributes :unsigned-long)
        (szDisplayName :pointer)
        (szTypeName :pointer))



(defcstructex-exported SHFILEINFOW
        (hIcon :pointer)
        (iIcon :int)
        (dwAttributes :unsigned-long)
        (szDisplayName :pointer)
        (szTypeName :pointer))



(defcstructex-exported SHQUERYRBINFO
        (cbSize :unsigned-long)
        (i64Size :long-long)
        (i64NumItems :long-long))





(defcfunex-exported ("DuplicateIcon" DuplicateIcon :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SHAppBarMessage" SHAppBarMessage :convention :stdcall) :unsigned-int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("Shell_NotifyIconA" Shell_NotifyIconA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("Shell_NotifyIconW" Shell_NotifyIconW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("SHFileOperationA" SHFileOperationA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SHFileOperationW" SHFileOperationW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SHFreeNameMappings" SHFreeNameMappings :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("SHGetFileInfoA" SHGetFileInfoA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int))

(defcfunex-exported ("SHGetFileInfoW" SHGetFileInfoW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int))

(defcfunex-exported ("SHQueryRecycleBinA" SHQueryRecycleBinA :convention :stdcall) :int32
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("SHQueryRecycleBinW" SHQueryRecycleBinW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SHEmptyRecycleBinA" SHEmptyRecycleBinA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("SHEmptyRecycleBinW" SHEmptyRecycleBinW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))
