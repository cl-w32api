(cl:in-package w32apimod)

(define-w32api-module commdlg :commdlg)

(cl:in-package cl-w32api.module.commdlg)


(defconstant-exported CDERR_DIALOGFAILURE #xFFFF)

(defconstant-exported CDERR_GENERALCODES #x0000)

(defconstant-exported CDERR_STRUCTSIZE #x0001)

(defconstant-exported CDERR_INITIALIZATION #x0002)

(defconstant-exported CDERR_NOTEMPLATE #x0003)

(defconstant-exported CDERR_NOHINSTANCE #x0004)

(defconstant-exported CDERR_LOADSTRFAILURE #x0005)

(defconstant-exported CDERR_FINDRESFAILURE #x0006)

(defconstant-exported CDERR_LOADRESFAILURE #x0007)

(defconstant-exported CDERR_LOCKRESFAILURE #x0008)

(defconstant-exported CDERR_MEMALLOCFAILURE #x0009)

(defconstant-exported CDERR_MEMLOCKFAILURE #x000A)

(defconstant-exported CDERR_NOHOOK #x000B)

(defconstant-exported CDERR_REGISTERMSGFAIL #x000C)

(defconstant-exported PDERR_PRINTERCODES #x1000)

(defconstant-exported PDERR_SETUPFAILURE #x1001)

(defconstant-exported PDERR_PARSEFAILURE #x1002)

(defconstant-exported PDERR_RETDEFFAILURE #x1003)

(defconstant-exported PDERR_LOADDRVFAILURE #x1004)

(defconstant-exported PDERR_GETDEVMODEFAIL #x1005)

(defconstant-exported PDERR_INITFAILURE #x1006)

(defconstant-exported PDERR_NODEVICES #x1007)

(defconstant-exported PDERR_NODEFAULTPRN #x1008)

(defconstant-exported PDERR_DNDMMISMATCH #x1009)

(defconstant-exported PDERR_CREATEICFAILURE #x100A)

(defconstant-exported PDERR_PRINTERNOTFOUND #x100B)

(defconstant-exported PDERR_DEFAULTDIFFERENT #x100C)

(defconstant-exported CFERR_CHOOSEFONTCODES #x2000)

(defconstant-exported CFERR_NOFONTS #x2001)

(defconstant-exported CFERR_MAXLESSTHANMIN #x2002)

(defconstant-exported FNERR_FILENAMECODES #x3000)

(defconstant-exported FNERR_SUBCLASSFAILURE #x3001)

(defconstant-exported FNERR_INVALIDFILENAME #x3002)

(defconstant-exported FNERR_BUFFERTOOSMALL #x3003)

(defconstant-exported FRERR_FINDREPLACECODES #x4000)

(defconstant-exported FRERR_BUFFERLENGTHZERO #x4001)

(defconstant-exported CCERR_CHOOSECOLORCODES #x5000)


(defconstant-exported LBSELCHSTRINGA "commdlg_LBSelChangedNotify")

(defconstant-exported SHAREVISTRINGA "commdlg_ShareViolation")

(defconstant-exported FILEOKSTRINGA "commdlg_FileNameOK")

(defconstant-exported COLOROKSTRINGA "commdlg_ColorOK")

(defconstant-exported SETRGBSTRINGA "commdlg_SetRGBColor")

(defconstant-exported HELPMSGSTRINGA "commdlg_help")

(defconstant-exported FINDMSGSTRINGA "commdlg_FindReplace")

(defconstant-exported CDM_FIRST (cl:+ 1024 100))

(defconstant-exported CDM_LAST (cl:+ 1024 200))

(defconstant-exported CDM_GETSPEC (cl:+ 1024 100))

(defconstant-exported CDM_GETFILEPATH (cl:+ 1024 100 1))

(defconstant-exported CDM_GETFOLDERPATH (cl:+ 1024 100 2))

(defconstant-exported CDM_GETFOLDERIDLIST (cl:+ 1024 100 3))

(defconstant-exported CDM_SETCONTROLTEXT (cl:+ 1024 100 4))

(defconstant-exported CDM_HIDECONTROL (cl:+ 1024 100 5))

(defconstant-exported CDM_SETDEFEXT (cl:+ 1024 100 6))

(defconstant-exported CC_RGBINIT 1)

(defconstant-exported CC_FULLOPEN 2)

(defconstant-exported CC_PREVENTFULLOPEN 4)

(defconstant-exported CC_SHOWHELP 8)

(defconstant-exported CC_ENABLEHOOK 16)

(defconstant-exported CC_ENABLETEMPLATE 32)

(defconstant-exported CC_ENABLETEMPLATEHANDLE 64)

(defconstant-exported CC_SOLIDCOLOR 128)

(defconstant-exported CC_ANYCOLOR 256)

(defconstant-exported CF_SCREENFONTS 1)

(defconstant-exported CF_PRINTERFONTS 2)

(defconstant-exported CF_BOTH 3)

(defconstant-exported CF_SHOWHELP 4)

(defconstant-exported CF_ENABLEHOOK 8)

(defconstant-exported CF_ENABLETEMPLATE 16)

(defconstant-exported CF_ENABLETEMPLATEHANDLE 32)

(defconstant-exported CF_INITTOLOGFONTSTRUCT 64)

(defconstant-exported CF_USESTYLE 128)

(defconstant-exported CF_EFFECTS 256)

(defconstant-exported CF_APPLY 512)

(defconstant-exported CF_ANSIONLY 1024)

(defconstant-exported CF_SCRIPTSONLY 1024)

(defconstant-exported CF_NOVECTORFONTS 2048)

(defconstant-exported CF_NOOEMFONTS 2048)

(defconstant-exported CF_NOSIMULATIONS 4096)

(defconstant-exported CF_LIMITSIZE 8192)

(defconstant-exported CF_FIXEDPITCHONLY 16384)

(defconstant-exported CF_WYSIWYG 32768)

(defconstant-exported CF_FORCEFONTEXIST 65536)

(defconstant-exported CF_SCALABLEONLY 131072)

(defconstant-exported CF_TTONLY 262144)

(defconstant-exported CF_NOFACESEL 524288)

(defconstant-exported CF_NOSTYLESEL 1048576)

(defconstant-exported CF_NOSIZESEL 2097152)

(defconstant-exported CF_SELECTSCRIPT 4194304)

(defconstant-exported CF_NOSCRIPTSEL 8388608)

(defconstant-exported CF_NOVERTFONTS #x1000000)

(defconstant-exported SIMULATED_FONTTYPE #x8000)

(defconstant-exported PRINTER_FONTTYPE #x4000)

(defconstant-exported SCREEN_FONTTYPE #x2000)

(defconstant-exported BOLD_FONTTYPE #x100)

(defconstant-exported ITALIC_FONTTYPE #x0200)

(defconstant-exported REGULAR_FONTTYPE #x0400)

(defconstant-exported WM_CHOOSEFONT_GETLOGFONT (cl:+ 1024 1))

(defconstant-exported WM_CHOOSEFONT_SETLOGFONT (cl:+ 1024 101))

(defconstant-exported WM_CHOOSEFONT_SETFLAGS (cl:+ 1024 102))

(defconstant-exported OFN_ALLOWMULTISELECT 512)

(defconstant-exported OFN_CREATEPROMPT #x2000)

(defconstant-exported OFN_ENABLEHOOK 32)

(defconstant-exported OFN_ENABLESIZING #x800000)

(defconstant-exported OFN_ENABLETEMPLATE 64)

(defconstant-exported OFN_ENABLETEMPLATEHANDLE 128)

(defconstant-exported OFN_EXPLORER #x80000)

(defconstant-exported OFN_EXTENSIONDIFFERENT #x400)

(defconstant-exported OFN_FILEMUSTEXIST #x1000)

(defconstant-exported OFN_HIDEREADONLY 4)

(defconstant-exported OFN_LONGNAMES #x200000)

(defconstant-exported OFN_NOCHANGEDIR 8)

(defconstant-exported OFN_NODEREFERENCELINKS #x100000)

(defconstant-exported OFN_NOLONGNAMES #x40000)

(defconstant-exported OFN_NONETWORKBUTTON #x20000)

(defconstant-exported OFN_NOREADONLYRETURN #x8000)

(defconstant-exported OFN_NOTESTFILECREATE #x10000)

(defconstant-exported OFN_NOVALIDATE 256)

(defconstant-exported OFN_OVERWRITEPROMPT 2)

(defconstant-exported OFN_PATHMUSTEXIST #x800)

(defconstant-exported OFN_READONLY 1)

(defconstant-exported OFN_SHAREAWARE #x4000)

(defconstant-exported OFN_SHOWHELP 16)

(defconstant-exported OFN_SHAREFALLTHROUGH 2)

(defconstant-exported OFN_SHARENOWARN 1)

(defconstant-exported OFN_SHAREWARN 0)

(defconstant-exported FR_DIALOGTERM 64)

(defconstant-exported FR_DOWN 1)

(defconstant-exported FR_ENABLEHOOK 256)

(defconstant-exported FR_ENABLETEMPLATE 512)

(defconstant-exported FR_ENABLETEMPLATEHANDLE #x2000)

(defconstant-exported FR_FINDNEXT 8)

(defconstant-exported FR_HIDEUPDOWN #x4000)

(defconstant-exported FR_HIDEMATCHCASE #x8000)

(defconstant-exported FR_HIDEWHOLEWORD #x10000)

(defconstant-exported FR_MATCHALEFHAMZA #x80000000)

(defconstant-exported FR_MATCHCASE 4)

(defconstant-exported FR_MATCHDIAC #x20000000)

(defconstant-exported FR_MATCHKASHIDA #x40000000)

(defconstant-exported FR_NOMATCHCASE #x800)

(defconstant-exported FR_NOUPDOWN #x400)

(defconstant-exported FR_NOWHOLEWORD 4096)

(defconstant-exported FR_REPLACE 16)

(defconstant-exported FR_REPLACEALL 32)

(defconstant-exported FR_SHOWHELP 128)

(defconstant-exported FR_WHOLEWORD 2)

(defconstant-exported PD_ALLPAGES 0)

(defconstant-exported PD_SELECTION 1)

(defconstant-exported PD_PAGENUMS 2)

(defconstant-exported PD_NOSELECTION 4)

(defconstant-exported PD_NOPAGENUMS 8)

(defconstant-exported PD_COLLATE 16)

(defconstant-exported PD_PRINTTOFILE 32)

(defconstant-exported PD_PRINTSETUP 64)

(defconstant-exported PD_NOWARNING 128)

(defconstant-exported PD_RETURNDC 256)

(defconstant-exported PD_RETURNIC 512)

(defconstant-exported PD_RETURNDEFAULT 1024)

(defconstant-exported PD_SHOWHELP 2048)

(defconstant-exported PD_ENABLEPRINTHOOK 4096)

(defconstant-exported PD_ENABLESETUPHOOK 8192)

(defconstant-exported PD_ENABLEPRINTTEMPLATE 16384)

(defconstant-exported PD_ENABLESETUPTEMPLATE 32768)

(defconstant-exported PD_ENABLEPRINTTEMPLATEHANDLE 65536)

(defconstant-exported PD_ENABLESETUPTEMPLATEHANDLE #x20000)

(defconstant-exported PD_USEDEVMODECOPIES #x40000)

(defconstant-exported PD_USEDEVMODECOPIESANDCOLLATE #x40000)

(defconstant-exported PD_DISABLEPRINTTOFILE #x80000)

(defconstant-exported PD_HIDEPRINTTOFILE #x100000)

(defconstant-exported PD_NONETWORKBUTTON #x200000)

(defconstant-exported PSD_DEFAULTMINMARGINS 0)

(defconstant-exported PSD_INWININIINTLMEASURE 0)

(defconstant-exported PSD_MINMARGINS 1)

(defconstant-exported PSD_MARGINS 2)

(defconstant-exported PSD_INTHOUSANDTHSOFINCHES 4)

(defconstant-exported PSD_INHUNDREDTHSOFMILLIMETERS 8)

(defconstant-exported PSD_DISABLEMARGINS 16)

(defconstant-exported PSD_DISABLEPRINTER 32)

(defconstant-exported PSD_NOWARNING 128)

(defconstant-exported PSD_DISABLEORIENTATION 256)

(defconstant-exported PSD_DISABLEPAPER 512)

(defconstant-exported PSD_RETURNDEFAULT 1024)

(defconstant-exported PSD_SHOWHELP 2048)

(defconstant-exported PSD_ENABLEPAGESETUPHOOK 8192)

(defconstant-exported PSD_ENABLEPAGESETUPTEMPLATE #x8000)

(defconstant-exported PSD_ENABLEPAGESETUPTEMPLATEHANDLE #x20000)

(defconstant-exported PSD_ENABLEPAGEPAINTHOOK #x40000)

(defconstant-exported PSD_DISABLEPAGEPAINTING #x80000)

(defconstant-exported WM_PSD_PAGESETUPDLG 1024)

(defconstant-exported WM_PSD_FULLPAGERECT (cl:+ 1024 1))

(defconstant-exported WM_PSD_MINMARGINRECT (cl:+ 1024 2))

(defconstant-exported WM_PSD_MARGINRECT (cl:+ 1024 3))

(defconstant-exported WM_PSD_GREEKTEXTRECT (cl:+ 1024 4))

(defconstant-exported WM_PSD_ENVSTAMPRECT (cl:+ 1024 5))

(defconstant-exported WM_PSD_YAFULLPAGERECT (cl:+ 1024 6))

(defconstant-exported CD_LBSELNOITEMS -1)

(defconstant-exported CD_LBSELCHANGE 0)

(defconstant-exported CD_LBSELSUB 1)

(defconstant-exported CD_LBSELADD 2)

(defconstant-exported DN_DEFAULTPRN 1)



















(defcstructex-exported CHOOSECOLORA
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (rgbResult :unsigned-long)
        (lpCustColors :pointer)
        (Flags :unsigned-long)
        (lCustData :int32)
        (lpfnHook :pointer)
        (lpTemplateName :string))





(defcstructex-exported CHOOSECOLORW
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (rgbResult :unsigned-long)
        (lpCustColors :pointer)
        (Flags :unsigned-long)
        (lCustData :int32)
        (lpfnHook :pointer)
        (lpTemplateName :pointer))





(defcstructex-exported CHOOSEFONTA
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hDC :pointer)
        (lpLogFont :pointer)
        (iPointSize :int)
        (Flags :unsigned-long)
        (rgbColors :unsigned-long)
        (lCustData :int32)
        (lpfnHook :pointer)
        (lpTemplateName :string)
        (hInstance :pointer)
        (lpszStyle :string)
        (nFontType :unsigned-short)
        (___MISSING_ALIGNMENT__ :unsigned-short)
        (nSizeMin :int)
        (nSizeMax :int))





(defcstructex-exported CHOOSEFONTW
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hDC :pointer)
        (lpLogFont :pointer)
        (iPointSize :int)
        (Flags :unsigned-long)
        (rgbColors :unsigned-long)
        (lCustData :int32)
        (lpfnHook :pointer)
        (lpTemplateName :pointer)
        (hInstance :pointer)
        (lpszStyle :pointer)
        (nFontType :unsigned-short)
        (___MISSING_ALIGNMENT__ :unsigned-short)
        (nSizeMin :int)
        (nSizeMax :int))





(defcstructex-exported DEVNAMES
        (wDriverOffset :unsigned-short)
        (wDeviceOffset :unsigned-short)
        (wOutputOffset :unsigned-short)
        (wDefault :unsigned-short))





(defcstructex-exported FINDREPLACEA
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (Flags :unsigned-long)
        (lpstrFindWhat :string)
        (lpstrReplaceWith :string)
        (wFindWhatLen :unsigned-short)
        (wReplaceWithLen :unsigned-short)
        (lCustData :int32)
        (lpfnHook :pointer)
        (lpTemplateName :string))



(defcstructex-exported FINDREPLACEW
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (Flags :unsigned-long)
        (lpstrFindWhat :pointer)
        (lpstrReplaceWith :pointer)
        (wFindWhatLen :unsigned-short)
        (wReplaceWithLen :unsigned-short)
        (lCustData :int32)
        (lpfnHook :pointer)
        (lpTemplateName :pointer))



(defcstructex-exported OPENFILENAMEA
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (lpstrFilter :string)
        (lpstrCustomFilter :string)
        (nMaxCustFilter :unsigned-long)
        (nFilterIndex :unsigned-long)
        (lpstrFile :string)
        (nMaxFile :unsigned-long)
        (lpstrFileTitle :string)
        (nMaxFileTitle :unsigned-long)
        (lpstrInitialDir :string)
        (lpstrTitle :string)
        (Flags :unsigned-long)
        (nFileOffset :unsigned-short)
        (nFileExtension :unsigned-short)
        (lpstrDefExt :string)
        (lCustData :unsigned-long)
        (lpfnHook :pointer)
        (lpTemplateName :string))





(defcstructex-exported OPENFILENAMEW
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hInstance :pointer)
        (lpstrFilter :pointer)
        (lpstrCustomFilter :pointer)
        (nMaxCustFilter :unsigned-long)
        (nFilterIndex :unsigned-long)
        (lpstrFile :pointer)
        (nMaxFile :unsigned-long)
        (lpstrFileTitle :pointer)
        (nMaxFileTitle :unsigned-long)
        (lpstrInitialDir :pointer)
        (lpstrTitle :pointer)
        (Flags :unsigned-long)
        (nFileOffset :unsigned-short)
        (nFileExtension :unsigned-short)
        (lpstrDefExt :pointer)
        (lCustData :unsigned-long)
        (lpfnHook :pointer)
        (lpTemplateName :pointer))





(defcstructex-exported OFNOTIFYA
        (hdr NMHDR)
        (lpOFN :pointer)
        (pszFile :string))





(defcstructex-exported OFNOTIFYW
        (hdr NMHDR)
        (lpOFN :pointer)
        (pszFile :pointer))





(defcstructex-exported PAGESETUPDLGA
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hDevMode :pointer)
        (hDevNames :pointer)
        (Flags :unsigned-long)
        (ptPaperSize POINT)
        (rtMinMargin RECT)
        (rtMargin RECT)
        (hInstance :pointer)
        (lCustData :int32)
        (lpfnPageSetupHook :pointer)
        (lpfnPagePaintHook :pointer)
        (lpPageSetupTemplateName :string)
        (hPageSetupTemplate :pointer))





(defcstructex-exported PAGESETUPDLGW
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hDevMode :pointer)
        (hDevNames :pointer)
        (Flags :unsigned-long)
        (ptPaperSize POINT)
        (rtMinMargin RECT)
        (rtMargin RECT)
        (hInstance :pointer)
        (lCustData :int32)
        (lpfnPageSetupHook :pointer)
        (lpfnPagePaintHook :pointer)
        (lpPageSetupTemplateName :pointer)
        (hPageSetupTemplate :pointer))





(defcstructex-exported PRINTDLGA
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hDevMode :pointer)
        (hDevNames :pointer)
        (hDC :pointer)
        (Flags :unsigned-long)
        (nFromPage :unsigned-short)
        (nToPage :unsigned-short)
        (nMinPage :unsigned-short)
        (nMaxPage :unsigned-short)
        (nCopies :unsigned-short)
        (hInstance :pointer)
        (lCustData :unsigned-long)
        (lpfnPrintHook :pointer)
        (lpfnSetupHook :pointer)
        (lpPrintTemplateName :string)
        (lpSetupTemplateName :string)
        (hPrintTemplate :pointer)
        (hSetupTemplate :pointer))





(defcstructex-exported PRINTDLGW
        (lStructSize :unsigned-long)
        (hwndOwner :pointer)
        (hDevMode :pointer)
        (hDevNames :pointer)
        (hDC :pointer)
        (Flags :unsigned-long)
        (nFromPage :unsigned-short)
        (nToPage :unsigned-short)
        (nMinPage :unsigned-short)
        (nMaxPage :unsigned-short)
        (nCopies :unsigned-short)
        (hInstance :pointer)
        (lCustData :unsigned-long)
        (lpfnPrintHook :pointer)
        (lpfnSetupHook :pointer)
        (lpPrintTemplateName :pointer)
        (lpSetupTemplateName :pointer)
        (hPrintTemplate :pointer)
        (hSetupTemplate :pointer))





(defconstant-exported LBSELCHSTRING "commdlg_LBSelChangedNotify")

(defconstant-exported SHAREVISTRING "commdlg_ShareViolation")

(defconstant-exported FILEOKSTRING "commdlg_FileNameOK")

(defconstant-exported COLOROKSTRING "commdlg_ColorOK")

(defconstant-exported SETRGBSTRING "commdlg_SetRGBColor")

(defconstant-exported HELPMSGSTRING "commdlg_help")

(defconstant-exported FINDMSGSTRING "commdlg_FindReplace")

