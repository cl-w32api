(cl:in-package w32apimod)

(define-w32api-module win95.file :win95.file)

(cl:in-package cl-w32api.module.win95.file)

(defcfunex-exported ("AreFileApisANSI" AreFileApisANSI :convention :stdcall) :int)

(defcfunex-exported ("CancelIo" CancelIo :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CopyFileA" CopyFileA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("CopyFileW" CopyFileW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("CopyFileExA" CopyFileExA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("CopyFileExW" CopyFileExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("CreateDirectoryA" CreateDirectoryA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("CreateDirectoryW" CreateDirectoryW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CreateDirectoryExA" CreateDirectoryExA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("CreateDirectoryExW" CreateDirectoryExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("CreateFileA" CreateFileA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("CreateFileW" CreateFileW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("CreateIoCompletionPort" CreateIoCompletionPort :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("DefineDosDeviceA" DefineDosDeviceA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("DefineDosDeviceW" DefineDosDeviceW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DeleteFileA" DeleteFileA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("DeleteFileW" DeleteFileW :convention :stdcall) :int
  (arg0 :pointer))


(defcfunex-exported ("FindClose" FindClose :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FindCloseChangeNotification" FindCloseChangeNotification :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FindFirstChangeNotificationA" FindFirstChangeNotificationA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("FindFirstChangeNotificationW" FindFirstChangeNotificationW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("FindFirstFileA" FindFirstFileA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("FindFirstFileW" FindFirstFileW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FindFirstFileExA" FindFirstFileExA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 FINDEX_INFO_LEVELS)
  (arg2 :pointer)
  (arg3 FINDEX_SEARCH_OPS)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("FindFirstFileExW" FindFirstFileExW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 FINDEX_INFO_LEVELS)
  (arg2 :pointer)
  (arg3 FINDEX_SEARCH_OPS)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("FindNextChangeNotification" FindNextChangeNotification :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FindNextFileA" FindNextFileA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FindNextFileW" FindNextFileW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FlushFileBuffers" FlushFileBuffers :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetBinaryTypeA" GetBinaryTypeA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("GetBinaryTypeW" GetBinaryTypeW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCompressedFileSizeA" GetCompressedFileSizeA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("GetCompressedFileSizeW" GetCompressedFileSizeW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCurrentDirectoryA" GetCurrentDirectoryA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string))

(defcfunex-exported ("GetCurrentDirectoryW" GetCurrentDirectoryW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("GetDiskFreeSpaceA" GetDiskFreeSpaceA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("GetDiskFreeSpaceW" GetDiskFreeSpaceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("GetDiskFreeSpaceExA" GetDiskFreeSpaceExA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetDiskFreeSpaceExW" GetDiskFreeSpaceExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetDriveTypeA" GetDriveTypeA :convention :stdcall) :unsigned-int
  (arg0 :string))

(defcfunex-exported ("GetDriveTypeW" GetDriveTypeW :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("GetFileAttributesA" GetFileAttributesA :convention :stdcall) :unsigned-long
  (arg0 :string))

(defcfunex-exported ("GetFileAttributesW" GetFileAttributesW :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetFileAttributesExA" GetFileAttributesExA :convention :stdcall) :int
  (arg0 :string)
  (arg1 GET_FILEEX_INFO_LEVELS)
  (arg2 :pointer))

(defcfunex-exported ("GetFileAttributesExW" GetFileAttributesExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 GET_FILEEX_INFO_LEVELS)
  (arg2 :pointer))

(defcfunex-exported ("GetFileInformationByHandle" GetFileInformationByHandle :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetFileSize" GetFileSize :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetFileType" GetFileType :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetFullPathNameA" GetFullPathNameA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("GetFullPathNameW" GetFullPathNameW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetLogicalDrives" GetLogicalDrives :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetLogicalDriveStringsA" GetLogicalDriveStringsA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string))

(defcfunex-exported ("GetLogicalDriveStringsW" GetLogicalDriveStringsW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("GetQueuedCompletionStatus" GetQueuedCompletionStatus :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("GetShortPathNameA" GetShortPathNameA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetShortPathNameW" GetShortPathNameW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetTempFileNameA" GetTempFileNameA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-int)
  (arg3 :string))

(defcfunex-exported ("GetTempFileNameW" GetTempFileNameW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetTempPathA" GetTempPathA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string))

(defcfunex-exported ("GetTempPathW" GetTempPathW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("GetVolumeInformationA" GetVolumeInformationA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :string)
  (arg7 :unsigned-long))

(defcfunex-exported ("GetVolumeInformationW" GetVolumeInformationW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :unsigned-long))

(defcfunex-exported ("LockFile" LockFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("LockFileEx" LockFileEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))


(defcfunex-exported ("MoveFileA" MoveFileA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("MoveFileW" MoveFileW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("MoveFileExA" MoveFileExA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("MoveFileExW" MoveFileExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("PostQueuedCompletionStatus" PostQueuedCompletionStatus :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("QueryDosDeviceA" QueryDosDeviceA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("QueryDosDeviceW" QueryDosDeviceW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("ReadDirectoryChangesW" ReadDirectoryChangesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :int)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("ReadFile" ReadFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("ReadFileEx" ReadFileEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RemoveDirectoryA" RemoveDirectoryA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("RemoveDirectoryW" RemoveDirectoryW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SearchPathA" SearchPathA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-long)
  (arg4 :string)
  (arg5 :pointer))

(defcfunex-exported ("SearchPathW" SearchPathW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("SetCurrentDirectoryA" SetCurrentDirectoryA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("SetCurrentDirectoryW" SetCurrentDirectoryW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetEndOfFile" SetEndOfFile :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetFileApisToANSI" SetFileApisToANSI :convention :stdcall) :void)

(defcfunex-exported ("SetFileApisToOEM" SetFileApisToOEM :convention :stdcall) :void)

(defcfunex-exported ("SetFileAttributesA" SetFileAttributesA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetFileAttributesW" SetFileAttributesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetFilePointer" SetFilePointer :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetFilePointerEx" SetFilePointerEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 LARGE_INTEGER)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetVolumeLabelA" SetVolumeLabelA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("SetVolumeLabelW" SetVolumeLabelW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UnlockFile" UnlockFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("UnlockFileEx" UnlockFileEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("WriteFile" WriteFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WriteFileEx" WriteFileEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

;obsolete

(defcfunex-exported ("_hread" _hread :convention :stdcall) :int32
  (arg0 :int)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("_hwrite" _hwrite :convention :stdcall) :int32
  (arg0 :int)
  (arg1 :string)
  (arg2 :int32))

(defcfunex-exported ("_lclose" _lclose :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("_lcreat" _lcreat :convention :stdcall) :int
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("_llseek" _llseek :convention :stdcall) :int32
  (arg0 :int)
  (arg1 :int32)
  (arg2 :int))

(defcfunex-exported ("_lopen" _lopen :convention :stdcall) :int
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("_lread" _lread :convention :stdcall) :unsigned-int
  (arg0 :int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("_lwrite" _lwrite :convention :stdcall) :unsigned-int
  (arg0 :int)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("OpenFile" OpenFile :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("SetHandleCount" SetHandleCount :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int))

