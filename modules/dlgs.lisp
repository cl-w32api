(cl:in-package w32apimod)

(define-w32api-module dlgs :dlgs)

(cl:in-package cl-w32api.module.dlgs)

(defconstant-exported FILEOPENORD 1536)

(defconstant-exported MULTIFILEOPENORD 1537)

(defconstant-exported PRINTDLGORD 1538)

(defconstant-exported PRNSETUPDLGORD 1539)

(defconstant-exported FINDDLGORD 1540)

(defconstant-exported REPLACEDLGORD 1541)

(defconstant-exported FONTDLGORD 1542)

(defconstant-exported FORMATDLGORD31 1543)

(defconstant-exported FORMATDLGORD30 1544)

(defconstant-exported PAGESETUPDLGORD 1546)

(defconstant-exported ctlFirst #x400)

(defconstant-exported ctlLast #x4ff)

(defconstant-exported chx1 #x410)

(defconstant-exported chx2 #x411)

(defconstant-exported chx3 #x412)

(defconstant-exported chx4 #x413)

(defconstant-exported chx5 #x414)

(defconstant-exported chx6 #x415)

(defconstant-exported chx7 #x416)

(defconstant-exported chx8 #x417)

(defconstant-exported chx9 #x418)

(defconstant-exported chx10 #x419)

(defconstant-exported chx11 #x41a)

(defconstant-exported chx12 #x41b)

(defconstant-exported chx13 #x41c)

(defconstant-exported chx14 #x41d)

(defconstant-exported chx15 #x41e)

(defconstant-exported chx16 #x41f)

(defconstant-exported cmb1 #x470)

(defconstant-exported cmb2 #x471)

(defconstant-exported cmb3 #x472)

(defconstant-exported cmb4 #x473)

(defconstant-exported cmb5 #x474)

(defconstant-exported cmb6 #x475)

(defconstant-exported cmb7 #x476)

(defconstant-exported cmb8 #x477)

(defconstant-exported cmb9 #x478)

(defconstant-exported cmb10 #x479)

(defconstant-exported cmb11 #x47a)

(defconstant-exported cmb12 #x47b)

(defconstant-exported cmb13 #x47c)

(defconstant-exported cmb14 #x47d)

(defconstant-exported cmb15 #x47e)

(defconstant-exported cmb16 #x47f)

(defconstant-exported edt1 #x480)

(defconstant-exported edt2 #x481)

(defconstant-exported edt3 #x482)

(defconstant-exported edt4 #x483)

(defconstant-exported edt5 #x484)

(defconstant-exported edt6 #x485)

(defconstant-exported edt7 #x486)

(defconstant-exported edt8 #x487)

(defconstant-exported edt9 #x488)

(defconstant-exported edt10 #x489)

(defconstant-exported edt11 #x48a)

(defconstant-exported edt12 #x48b)

(defconstant-exported edt13 #x48c)

(defconstant-exported edt14 #x48d)

(defconstant-exported edt15 #x48e)

(defconstant-exported edt16 #x48f)

(defconstant-exported frm1 #x434)

(defconstant-exported frm2 #x435)

(defconstant-exported frm3 #x436)

(defconstant-exported frm4 #x437)

(defconstant-exported grp1 #x430)

(defconstant-exported grp2 #x431)

(defconstant-exported grp3 #x432)

(defconstant-exported grp4 #x433)

(defconstant-exported ico1 #x43c)

(defconstant-exported ico2 #x43d)

(defconstant-exported ico3 #x43e)

(defconstant-exported ico4 #x43f)

(defconstant-exported lst1 #x460)

(defconstant-exported lst2 #x461)

(defconstant-exported lst3 #x462)

(defconstant-exported lst4 #x463)

(defconstant-exported lst5 #x464)

(defconstant-exported lst6 #x465)

(defconstant-exported lst7 #x466)

(defconstant-exported lst8 #x467)

(defconstant-exported lst9 #x468)

(defconstant-exported lst10 #x469)

(defconstant-exported lst11 #x46a)

(defconstant-exported lst12 #x46b)

(defconstant-exported lst13 #x46c)

(defconstant-exported lst14 #x46d)

(defconstant-exported lst15 #x46e)

(defconstant-exported lst16 #x46f)

(defconstant-exported psh1 #x400)

(defconstant-exported psh2 #x401)

(defconstant-exported psh3 #x402)

(defconstant-exported psh4 #x403)

(defconstant-exported psh5 #x404)

(defconstant-exported psh6 #x405)

(defconstant-exported psh7 #x406)

(defconstant-exported psh8 #x407)

(defconstant-exported psh9 #x408)

(defconstant-exported psh10 #x409)

(defconstant-exported psh11 #x40a)

(defconstant-exported psh12 #x40b)

(defconstant-exported psh13 #x40c)

(defconstant-exported psh14 #x40d)

(defconstant-exported psh15 #x40e)

(defconstant-exported pshHelp #x40e)

(defconstant-exported psh16 #x40f)

(defconstant-exported rad1 #x420)

(defconstant-exported rad2 #x421)

(defconstant-exported rad3 #x422)

(defconstant-exported rad4 #x423)

(defconstant-exported rad5 #x424)

(defconstant-exported rad6 #x425)

(defconstant-exported rad7 #x426)

(defconstant-exported rad8 #x427)

(defconstant-exported rad9 #x428)

(defconstant-exported rad10 #x429)

(defconstant-exported rad11 #x42a)

(defconstant-exported rad12 #x42b)

(defconstant-exported rad13 #x42c)

(defconstant-exported rad14 #x42d)

(defconstant-exported rad15 #x42e)

(defconstant-exported rad16 #x42f)

(defconstant-exported rct1 #x438)

(defconstant-exported rct2 #x439)

(defconstant-exported rct3 #x43a)

(defconstant-exported rct4 #x43b)

(defconstant-exported scr1 #x490)

(defconstant-exported scr2 #x491)

(defconstant-exported scr3 #x492)

(defconstant-exported scr4 #x493)

(defconstant-exported scr5 #x494)

(defconstant-exported scr6 #x495)

(defconstant-exported scr7 #x496)

(defconstant-exported scr8 #x497)

(defconstant-exported stc1 #x440)

(defconstant-exported stc2 #x441)

(defconstant-exported stc3 #x442)

(defconstant-exported stc4 #x443)

(defconstant-exported stc5 #x444)

(defconstant-exported stc6 #x445)

(defconstant-exported stc7 #x446)

(defconstant-exported stc8 #x447)

(defconstant-exported stc9 #x448)

(defconstant-exported stc10 #x449)

(defconstant-exported stc11 #x44a)

(defconstant-exported stc12 #x44b)

(defconstant-exported stc13 #x44c)

(defconstant-exported stc14 #x44d)

(defconstant-exported stc15 #x44e)

(defconstant-exported stc16 #x44f)

(defconstant-exported stc17 #x450)

(defconstant-exported stc18 #x451)

(defconstant-exported stc19 #x452)

(defconstant-exported stc20 #x453)

(defconstant-exported stc21 #x454)

(defconstant-exported stc22 #x455)

(defconstant-exported stc23 #x456)

(defconstant-exported stc24 #x457)

(defconstant-exported stc25 #x458)

(defconstant-exported stc26 #x459)

(defconstant-exported stc27 #x45a)

(defconstant-exported stc28 #x45b)

(defconstant-exported stc29 #x45c)

(defconstant-exported stc30 #x45d)

(defconstant-exported stc31 #x45e)

(defconstant-exported stc32 #x45f)

(defcstructex-exported CRGB
        (bRed :unsigned-char)
        (bGreen :unsigned-char)
        (bBlue :unsigned-char)
        (bExtra :unsigned-char))

