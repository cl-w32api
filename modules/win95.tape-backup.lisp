(defcfunex-exported ("BackupRead" BackupRead :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :int)
  (arg6 :pointer))

(defcfunex-exported ("BackupSeek" BackupSeek :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("BackupWrite" BackupWrite :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :int)
  (arg6 :pointer))

(defcfunex-exported ("CreateTapePartition" CreateTapePartition :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("EraseTape" EraseTape :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int))

(defcfunex-exported ("GetTapeParameters" GetTapeParameters :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetTapePosition" GetTapePosition :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("GetTapeStatus" GetTapeStatus :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("PrepareTape" PrepareTape :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int))

(defcfunex-exported ("SetTapeParameters" SetTapeParameters :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetTapePosition" SetTapePosition :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :int))

(defcfunex-exported ("WriteTapemark" WriteTapemark :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :int))

