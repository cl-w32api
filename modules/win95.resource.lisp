
(cl:in-package w32apimod)

(define-w32api-module win95.resource :win95.resource)

(cl:in-package cl-w32api.module.win95.resource)

(defcfunex-exported ("BeginUpdateResourceA" BeginUpdateResourceA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("BeginUpdateResourceW" BeginUpdateResourceW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("CopyImage" CopyImage :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :unsigned-int))

(defcfunex-exported ("EndUpdateResourceA" EndUpdateResourceA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("EndUpdateResourceW" EndUpdateResourceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("EnumResourceLanguagesA" EnumResourceLanguagesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("EnumResourceLanguagesW" EnumResourceLanguagesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("EnumResourceNamesA" EnumResourceNamesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("EnumResourceNamesW" EnumResourceNamesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("EnumResourceTypesA" EnumResourceTypesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumResourceTypesW" EnumResourceTypesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("FindResourceA" FindResourceA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("FindResourceW" FindResourceW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("FindResourceExA" FindResourceExA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-short))

(defcfunex-exported ("FindResourceExW" FindResourceExW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-short))

(defcfunex-exported ("LoadImageA" LoadImageA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :unsigned-int))

(defcfunex-exported ("LoadImageW" LoadImageW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :unsigned-int))

(defcfunex-exported ("LoadResource" LoadResource :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("LockResource" LockResource :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("SizeofResource" SizeofResource :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UpdateResourceA" UpdateResourceA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-short)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("UpdateResourceW" UpdateResourceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-short)
  (arg4 :pointer)
  (arg5 :unsigned-long))


;;obsolete
(defcfunex-exported ("FreeResource" FreeResource :convention :stdcall) :int
  (arg0 :pointer))

