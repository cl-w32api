
(cl:in-package w32apimod)

(define-w32api-module crt :crt)

(cl:in-package cl-w32api.module.crt)


(defcfunex-exported ("memchr" memchr :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("memcmp" memcmp :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("memcpy" memcpy :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("memmove" memmove :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("memset" memset :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("strcat" strcat :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strchr" strchr :convention :stdcall) :string
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("strcmp" strcmp :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strcoll" strcoll :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strcpy" strcpy :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strcspn" strcspn :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strerror" strerror :convention :stdcall) :string
  (arg0 :int))

(defcfunex-exported ("strlen" strlen :convention :stdcall) :unsigned-long
  (arg0 :string))

(defcfunex-exported ("strncat" strncat :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strncmp" strncmp :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strncpy" strncpy :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strpbrk" strpbrk :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strrchr" strrchr :convention :stdcall) :string
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("strspn" strspn :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strstr" strstr :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strtok" strtok :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strxfrm" strxfrm :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strtok_r" strtok_r :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("bcmp" bcmp :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("bcopy" bcopy :convention :stdcall) :void
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("bzero" bzero :convention :stdcall) :void
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ffs" ffs :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("index" index :convention :stdcall) :string
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("memccpy" memccpy :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("mempcpy" mempcpy :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("rindex" rindex :convention :stdcall) :string
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("strcasecmp" strcasecmp :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("strdup" strdup :convention :stdcall) :string
  (arg0 :string))

(defcfunex-exported ("_strdup_r" _strdup_r :convention :stdcall) :string
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("strndup" strndup :convention :stdcall) :string
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("_strndup_r" _strndup_r :convention :stdcall) :string
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strerror_r" strerror_r :convention :stdcall) :string
  (arg0 :int)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strlcat" strlcat :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strlcpy" strlcpy :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strncasecmp" strncasecmp :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("strnlen" strnlen :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("strsep" strsep :convention :stdcall) :string
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("strlwr" strlwr :convention :stdcall) :string
  (arg0 :string))

(defcfunex-exported ("strupr" strupr :convention :stdcall) :string
  (arg0 :string))

