
(cl:in-package w32apimod)

(define-w32api-module win95.wndclass :win95.wndclass)

(cl:in-package cl-w32api.module.win95.wndclass)

(require-and-inherit-module "win95.~")

(defbitfield (WNDCLASS-STYLES UINT)
  (:BYTEALIGNCLIENT 4096)
  (:BYTEALIGNWINDOW 8192)
  (:KEYCVTWINDOW 4)
  (:NOKEYCVT 256)
  (:CLASSDC 64)
  (:DBLCLKS 8)
  (:GLOBALCLASS 16384)
  (:HREDRAW 2)
  (:NOCLOSE 512)
  (:OWNDC 32)
  (:PARENTDC 128)
  (:SAVEBITS 2048)
  (:VREDRAW 1)
  (:IME #x10000))

(defcstructex-exported WNDCLASSA
        (style         WNDCLASS-STYLES)
        (lpfnWndProc   :pointer)
        (cbClsExtra    :int)
        (cbWndExtra    :int)
        (hInstance     HINSTANCE)
        (hIcon         HICON)
        (hCursor       HCURSOR)
        (hbrBackground HBRUSH)
        (lpszMenuName  ASTRING)
        (lpszClassName ASTRING))

(defcstructex-exported WNDCLASSW
        (style         WNDCLASS-STYLES)
        (lpfnWndProc   :pointer)
        (cbClsExtra    :int)
        (cbWndExtra    :int)
        (hInstance     HINSTANCE)
        (hIcon         HICON)
        (hCursor       HCURSOR)
        (hbrBackground HBRUSH)
        (lpszMenuName  WSTRING)
        (lpszClassName WSTRING))

(defcstructex-exported WNDCLASSEXA
        (cbSize        UINT)
        (style         WNDCLASS-STYLES)
        (lpfnWndProc   :pointer)
        (cbClsExtra    :int)
        (cbWndExtra    :int)
        (hInstance     HINSTANCE)
        (hIcon         HICON)
        (hCursor       HCURSOR)
        (hbrBackground HBRUSH)
        (lpszMenuName  ASTRING)
        (lpszClassName ASTRING)
        (hIconSm       HICON))

(defcstructex-exported WNDCLASSEXW
        (cbSize        UINT)
        (style         WNDCLASS-STYLES)
        (lpfnWndProc   :pointer)
        (cbClsExtra    :int)
        (cbWndExtra    :int)
        (hInstance     HINSTANCE)
        (hIcon         HICON)
        (hCursor       HCURSOR)
        (hbrBackground HBRUSH)
        (lpszMenuName  WSTRING)
        (lpszClassName WSTRING)
        (hIconSm       HICON))

(defcfunex-exported ("GetClassInfoA" GetClassInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("GetClassInfoExA" GetClassInfoExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("GetClassInfoW" GetClassInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetClassInfoExW" GetClassInfoExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetClassLongA" GetClassLongA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetClassLongW" GetClassLongW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :int))

;; TESTED
(defcfunex-exported ("GetClassNameA" GetClassNameA :convention :stdcall) :int
  (hWnd :pointer)
  (lpClassName :string)
  (nMaxCount :int))

(defcfunex-exported ("GetClassNameW" GetClassNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetClassWord" GetClassWord :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("RegisterClassA" RegisterClassA :convention :stdcall) ATOM_T
  (arg0 :pointer))

(defcfunex-exported ("RegisterClassW" RegisterClassW :convention :stdcall) ATOM_T
  (arg0 :pointer))

(defcfunex-exported ("RegisterClassExA" RegisterClassExA :convention :stdcall) ATOM_T
  (arg0 WNDCLASSEXA))

(defcfunex-exported ("RegisterClassExW" RegisterClassExW :convention :stdcall) ATOM_T
  (arg0 WNDCLASSEXW))

(define-abbrev-exported RegisterClassEx RegisterClassExW)

(defcfunex-exported ("SetClassLongA" SetClassLongA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int32))

(defcfunex-exported ("SetClassLongW" SetClassLongW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int32))

(defcfunex-exported ("SetClassWord" SetClassWord :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-short))

(defcfunex-exported ("SetWindowWord" SetWindowWord :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-short))

(defcfunex-exported ("UnregisterClassA" UnregisterClassA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("UnregisterClassW" UnregisterClassW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

