(cl:in-package w32apimod)

(define-w32api-module dde :dde)

(cl:in-package cl-w32api.module.dde)


(defconstant-exported WM_DDE_FIRST #x03E0)

(defconstant-exported WM_DDE_INITIATE #x03E0)

(defconstant-exported WM_DDE_TERMINATE (cl:+ #x03E0 1))

(defconstant-exported WM_DDE_ADVISE (cl:+ #x03E0 2))

(defconstant-exported WM_DDE_UNADVISE (cl:+ #x03E0 3))

(defconstant-exported WM_DDE_ACK (cl:+ #x03E0 4))

(defconstant-exported WM_DDE_DATA (cl:+ #x03E0 5))

(defconstant-exported WM_DDE_REQUEST (cl:+ #x03E0 6))

(defconstant-exported WM_DDE_POKE (cl:+ #x03E0 7))

(defconstant-exported WM_DDE_EXECUTE (cl:+ #x03E0 8))

(defconstant-exported WM_DDE_LAST (cl:+ #x03E0 8))

(defcstructex-exported DDEACK
        (bAppReturnCode :unsigned-short)
        (reserved :unsigned-short)
        (fBusy :unsigned-short)
        (fAck :unsigned-short))

(defcstructex-exported DDEADVISE
        (reserved :unsigned-short)
        (fDeferUpd :unsigned-short)
        (fAckReq :unsigned-short)
        (cfFormat :short))

(defcstructex-exported DDEDATA
        (unused :unsigned-short)
        (fResponse :unsigned-short)
        (fRelease :unsigned-short)
        (reserved :unsigned-short)
        (fAckReq :unsigned-short)
        (cfFormat :short)
        (Value :pointer))

(defcstructex-exported DDEPOKE
        (unused :unsigned-short)
        (fRelease :unsigned-short)
        (fReserved :unsigned-short)
        (cfFormat :short)
        (Value :pointer))

(defcstructex-exported DDELN
        (unused :unsigned-short)
        (fRelease :unsigned-short)
        (fDeferUpd :unsigned-short)
        (fAckReq :unsigned-short)
        (cfFormat :short))

(defcstructex-exported DDEUP
        (unused :unsigned-short)
        (fAck :unsigned-short)
        (fRelease :unsigned-short)
        (fReserved :unsigned-short)
        (fAckReq :unsigned-short)
        (cfFormat :short)
        (rgb :pointer))

(defcfunex-exported ("DdeSetQualityOfService" DdeSetQualityOfService :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ImpersonateDdeClientWindow" ImpersonateDdeClientWindow :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("PackDDElParam" PackDDElParam :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("UnpackDDElParam" UnpackDDElParam :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int32)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("FreeDDElParam" FreeDDElParam :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int32))

(defcfunex-exported ("ReuseDDElParam" ReuseDDElParam :convention :stdcall) :int32
  (arg0 :int32)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int))

(defconstant-exported CP_WINANSI 1004)

(defconstant-exported CP_WINUNICODE 1200)

(defconstant-exported XTYPF_NOBLOCK 2)

(defconstant-exported XTYPF_NODATA 4)

(defconstant-exported XTYPF_ACKREQ 8)

(defconstant-exported XCLASS_MASK #xFC00)

(defconstant-exported XCLASS_BOOL #x1000)

(defconstant-exported XCLASS_DATA #x2000)

(defconstant-exported XCLASS_FLAGS #x4000)

(defconstant-exported XCLASS_NOTIFICATION #x8000)

(defconstant-exported XST_NULL 0)

(defconstant-exported XST_INCOMPLETE 1)

(defconstant-exported XST_CONNECTED 2)

(defconstant-exported XST_INIT1 3)

(defconstant-exported XST_INIT2 4)

(defconstant-exported XST_REQSENT 5)

(defconstant-exported XST_DATARCVD 6)

(defconstant-exported XST_POKESENT 7)

(defconstant-exported XST_POKEACKRCVD 8)

(defconstant-exported XST_EXECSENT 9)

(defconstant-exported XST_EXECACKRCVD 10)

(defconstant-exported XST_ADVSENT 11)

(defconstant-exported XST_UNADVSENT 12)

(defconstant-exported XST_ADVACKRCVD 13)

(defconstant-exported XST_UNADVACKRCVD 14)

(defconstant-exported XST_ADVDATASENT 15)

(defconstant-exported XST_ADVDATAACKRCVD 16)

(defconstant-exported XTYP_ERROR (cl:logior #x8000 2))

(defconstant-exported XTYP_ADVDATA (cl:logior 16 #x4000))

(defconstant-exported XTYP_ADVREQ (cl:logior 32 #x2000 2))

(defconstant-exported XTYP_ADVSTART (cl:logior #x30 #x1000))

(defconstant-exported XTYP_ADVSTOP (cl:logior #x0040 #x8000))

(defconstant-exported XTYP_EXECUTE (cl:logior #x0050 #x4000))

(defconstant-exported XTYP_CONNECT (cl:logior #x0060 #x1000 2))

(defconstant-exported XTYP_CONNECT_CONFIRM (cl:logior #x0070 #x8000 2))

(defconstant-exported XTYP_XACT_COMPLETE (cl:logior #x0080 #x8000))

(defconstant-exported XTYP_POKE (cl:logior #x0090 #x4000))

(defconstant-exported XTYP_REGISTER (cl:logior #x00A0 #x8000 2))

(defconstant-exported XTYP_REQUEST (cl:logior #x00B0 #x2000))

(defconstant-exported XTYP_DISCONNECT (cl:logior #x00C0 #x8000 2))

(defconstant-exported XTYP_UNREGISTER (cl:logior #x00D0 #x8000 2))

(defconstant-exported XTYP_WILDCONNECT (cl:logior #x00E0 #x2000 2))

(defconstant-exported XTYP_MASK #xF0)

(defconstant-exported XTYP_SHIFT 4)

(defconstant-exported TIMEOUT_ASYNC #xFFFFFFFF)

(defconstant-exported QID_SYNC #xFFFFFFFF)

(defconstant-exported ST_CONNECTED 1)

(defconstant-exported ST_ADVISE 2)

(defconstant-exported ST_ISLOCAL 4)

(defconstant-exported ST_BLOCKED 8)

(defconstant-exported ST_CLIENT 16)

(defconstant-exported ST_TERMINATED 32)

(defconstant-exported ST_INLIST 64)

(defconstant-exported ST_BLOCKNEXT 128)

(defconstant-exported ST_ISSELF 256)

(defconstant-exported CADV_LATEACK #xFFFF)

(defconstant-exported DMLERR_NO_ERROR 0)

(defconstant-exported DMLERR_FIRST #x4000)

(defconstant-exported DMLERR_ADVACKTIMEOUT #x4000)

(defconstant-exported DMLERR_BUSY #x4001)

(defconstant-exported DMLERR_DATAACKTIMEOUT #x4002)

(defconstant-exported DMLERR_DLL_NOT_INITIALIZED #x4003)

(defconstant-exported DMLERR_DLL_USAGE #x4004)

(defconstant-exported DMLERR_EXECACKTIMEOUT #x4005)

(defconstant-exported DMLERR_INVALIDPARAMETER #x4006)

(defconstant-exported DMLERR_LOW_MEMORY #x4007)

(defconstant-exported DMLERR_MEMORY_ERROR #x4008)

(defconstant-exported DMLERR_NOTPROCESSED #x4009)

(defconstant-exported DMLERR_NO_CONV_ESTABLISHED #x400a)

(defconstant-exported DMLERR_POKEACKTIMEOUT #x400b)

(defconstant-exported DMLERR_POSTMSG_FAILED #x400c)

(defconstant-exported DMLERR_REENTRANCY #x400d)

(defconstant-exported DMLERR_SERVER_DIED #x400e)

(defconstant-exported DMLERR_SYS_ERROR #x400f)

(defconstant-exported DMLERR_UNADVACKTIMEOUT #x4010)

(defconstant-exported DMLERR_UNFOUND_QUEUE_ID #x4011)

(defconstant-exported DMLERR_LAST #x4011)

(defconstant-exported DDE_FACK #x8000)

(defconstant-exported DDE_FBUSY #x4000)

(defconstant-exported DDE_FDEFERUPD #x4000)

(defconstant-exported DDE_FACKREQ #x8000)

(defconstant-exported DDE_FRELEASE #x2000)

(defconstant-exported DDE_FREQUESTED #x1000)

(defconstant-exported DDE_FAPPSTATUS #x00ff)

(defconstant-exported DDE_FNOTPROCESSED 0)

; (defconstant-exported DDE_FACKRESERVED (cl:logior ~(0x8000 #x4000 #x00ff)))
(defconstant-exported DDE_FACKRESERVED (cl:lognot (cl:logior DDE_FACK DDE_FBUSY DDE_FAPPSTATUS)))

; (defconstant-exported DDE_FADVRESERVED (cl:logior ~(0x8000 #x4000)))
(defconstant-exported DDE_FADVRESERVED (cl:lognot (cl:logior DDE_FACKREQ DDE_FDEFERUPD)))

; (defconstant-exported DDE_FDATRESERVED (cl:logior ~(0x8000 #x2000 #x1000)))
(defconstant-exported DDE_FDATRESERVED (cl:lognot (cl:logior DDE_FACKREQ DDE_FRELEASE DDE_FREQUESTED)))

; (defconstant-exported DDE_FPOKRESERVED ~0x2000)
(defconstant-exported DDE_FPOKRESERVED (cl:lognot DDE_FRELEASE))

(defconstant-exported MSGF_DDEMGR #x8001)

(defconstant-exported CBF_FAIL_SELFCONNECTIONS #x1000)

(defconstant-exported CBF_FAIL_CONNECTIONS #x2000)

(defconstant-exported CBF_FAIL_ADVISES #x4000)

(defconstant-exported CBF_FAIL_EXECUTES #x8000)

(defconstant-exported CBF_FAIL_POKES #x10000)

(defconstant-exported CBF_FAIL_REQUESTS #x20000)

(defconstant-exported CBF_FAIL_ALLSVRXACTIONS #x3f000)

(defconstant-exported CBF_SKIP_CONNECT_CONFIRMS #x40000)

(defconstant-exported CBF_SKIP_REGISTRATIONS #x80000)

(defconstant-exported CBF_SKIP_UNREGISTRATIONS #x100000)

(defconstant-exported CBF_SKIP_DISCONNECTS #x200000)

(defconstant-exported CBF_SKIP_ALLNOTIFICATIONS #x3c0000)

(defconstant-exported APPCMD_CLIENTONLY #x10)

(defconstant-exported APPCMD_FILTERINITS #x20)

(defconstant-exported APPCMD_MASK #xFF0)

(defconstant-exported APPCLASS_STANDARD 0)

(defconstant-exported APPCLASS_MASK #xF)

(defconstant-exported EC_ENABLEALL 0)

(defconstant-exported EC_ENABLEONE 128)

(defconstant-exported EC_DISABLE 8)

(defconstant-exported EC_QUERYWAITING 2)

(defconstant-exported DNS_REGISTER 1)

(defconstant-exported DNS_UNREGISTER 2)

(defconstant-exported DNS_FILTERON 4)

(defconstant-exported DNS_FILTEROFF 8)

(defconstant-exported HDATA_APPOWNED 1)

(defconstant-exported MAX_MONITORS 4)

(defconstant-exported APPCLASS_MONITOR 1)

(defconstant-exported XTYP_MONITOR (cl:logior #x8000 2 #xF0))

(defconstant-exported MF_HSZ_INFO #x1000000)

(defconstant-exported MF_SENDMSGS #x2000000)

(defconstant-exported MF_POSTMSGS #x4000000)

(defconstant-exported MF_CALLBACKS #x8000000)

(defconstant-exported MF_ERRORS #x10000000)

(defconstant-exported MF_LINKS #x20000000)

(defconstant-exported MF_CONV #x40000000)

(defconstant-exported MF_MASK #xFF000000)

(defconstant-exported MH_CREATE 1)

(defconstant-exported MH_KEEP 2)

(defconstant-exported MH_DELETE 3)

(defconstant-exported MH_CLEANUP 4)

(defcstructex-exported HCONVLIST__
        (i :int))



(defcstructex-exported HCONV__
        (i :int))



(defcstructex-exported HSZ__
        (i :int))



(defcstructex-exported HDDEDATA__
        (i :int))







(defcstructex-exported HSZPAIR
        (hszSvc :pointer)
        (hszTopic :pointer))





(defcstructex-exported CONVCONTEXT
        (cb :unsigned-int)
        (wFlags :unsigned-int)
        (wCountryID :unsigned-int)
        (iCodePage :int)
        (dwLangID :unsigned-long)
        (dwSecurity :unsigned-long)
        (qos SECURITY_QUALITY_OF_SERVICE))





(defcstructex-exported CONVINFO
        (cb :unsigned-long)
        (hUser :unsigned-long)
        (hConvPartner :pointer)
        (hszSvcPartner :pointer)
        (hszServiceReq :pointer)
        (hszTopic :pointer)
        (hszItem :pointer)
        (wFmt :unsigned-int)
        (wType :unsigned-int)
        (wStatus :unsigned-int)
        (wConvst :unsigned-int)
        (wLastError :unsigned-int)
        (hConvList :pointer)
        (ConvCtxt CONVCONTEXT)
        (hwnd :pointer)
        (hwndPartner :pointer))





(defcstructex-exported DDEML_MSG_HOOK_DATA
        (uiLo :unsigned-int)
        (uiHi :unsigned-int)
        (cbData :unsigned-long)
        (Data :pointer))



(defcstructex-exported MONHSZSTRUCT
        (cb :unsigned-int)
        (fsAction :int)
        (dwTime :unsigned-long)
        (hsz :pointer)
        (hTask :pointer)
        (str :pointer))





(defcstructex-exported MONLINKSTRUCT
        (cb :unsigned-int)
        (dwTime :unsigned-long)
        (hTask :pointer)
        (fEstablished :int)
        (fNoData :int)
        (hszSvc :pointer)
        (hszTopic :pointer)
        (hszItem :pointer)
        (wFmt :unsigned-int)
        (fServer :int)
        (hConvServer :pointer)
        (hConvClient :pointer))





(defcstructex-exported MONCONVSTRUCT
        (cb :unsigned-int)
        (fConnect :int)
        (dwTime :unsigned-long)
        (hTask :pointer)
        (hszSvc :pointer)
        (hszTopic :pointer)
        (hConvClient :pointer)
        (hConvServer :pointer))





(defcstructex-exported MONCBSTRUCT
        (cb :unsigned-int)
        (dwTime :unsigned-long)
        (hTask :pointer)
        (dwRet :unsigned-long)
        (wType :unsigned-int)
        (wFmt :unsigned-int)
        (hConv :pointer)
        (hsz1 :pointer)
        (hsz2 :pointer)
        (hData :pointer)
        (dwData1 :unsigned-long)
        (dwData2 :unsigned-long)
        (cc CONVCONTEXT)
        (cbData :unsigned-long)
        (Data :pointer))





(defcstructex-exported MONERRSTRUCT
        (cb :unsigned-int)
        (wLastError :unsigned-int)
        (dwTime :unsigned-long)
        (hTask :pointer))





(defcstructex-exported MONMSGSTRUCT
        (cb :unsigned-int)
        (hwndTo :pointer)
        (dwTime :unsigned-long)
        (hTask :pointer)
        (wMsg :unsigned-int)
        (wParam :unsigned-int)
        (lParam :int32)
        (dmhd DDEML_MSG_HOOK_DATA))





(defconstant-exported SZDDESYS_TOPIC "System")

(defconstant-exported SZDDESYS_ITEM_TOPICS "Topics")

(defconstant-exported SZDDESYS_ITEM_SYSITEMS "SysItems")

(defconstant-exported SZDDESYS_ITEM_RTNMSG "ReturnMessage")

(defconstant-exported SZDDESYS_ITEM_STATUS "Status")

(defconstant-exported SZDDESYS_ITEM_FORMATS "Formats")

(defconstant-exported SZDDESYS_ITEM_HELP "Help")

(defconstant-exported SZDDE_ITEM_ITEMLIST "TopicItemList")
