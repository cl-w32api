
(defcfunex-exported ("CombineRgn" CombineRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("CreateEllipticRgn" CreateEllipticRgn :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("CreateEllipticRgnIndirect" CreateEllipticRgnIndirect :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreatePolygonRgn" CreatePolygonRgn :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("CreatePolyPolygonRgn" CreatePolyPolygonRgn :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("CreateRectRgn" CreateRectRgn :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("CreateRectRgnIndirect" CreateRectRgnIndirect :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateRoundRectRgn" CreateRoundRectRgn :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int))

(defcfunex-exported ("EqualRgn" EqualRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ExtCreateRegion" ExtCreateRegion :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("FillRgn" FillRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetPolyFillMode" GetPolyFillMode :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetRegionData" GetRegionData :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetRgnBox" GetRgnBox :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("OffsetRgn" OffsetRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("PaintRgn" PaintRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("PtInRegion" PtInRegion :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("RectInRegion" RectInRegion :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetPolyFillMode" SetPolyFillMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

