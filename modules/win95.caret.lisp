
(cl:in-package w32apimod)

(define-w32api-module win95.caret :win95.caret)

(cl:in-package cl-w32api.module.win95.caret)

(defcfunex-exported ("CreateCaret" CreateCaret :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("DestroyCaret" DestroyCaret :convention :stdcall) :int)

(defcfunex-exported ("GetCaretBlinkTime" GetCaretBlinkTime :convention :stdcall) :unsigned-int)

(defcfunex-exported ("GetCaretPos" GetCaretPos :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("HideCaret" HideCaret :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetCaretBlinkTime" SetCaretBlinkTime :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("SetCaretPos" SetCaretPos :convention :stdcall) :int
  (arg0 :int)
  (arg1 :int))

(defcfunex-exported ("ShowCaret" ShowCaret :convention :stdcall) :int
  (arg0 :pointer))



