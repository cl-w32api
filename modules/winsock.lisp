
(cl:in-package w32apimod)

(define-w32api-module winsock :winsock)

(cl:in-package cl-w32api.module.winsock)


(defconstant-exported FD_SETSIZE 64)

(defconstant-exported SD_RECEIVE #x00)

(defconstant-exported SD_SEND #x01)

(defconstant-exported SD_BOTH #x02)

(defcstructex-exported fd_set
        (fd_count :unsigned-int)
        (fd_array :pointer))



(defcfunex-exported ("__WSAFDIsSet" __WSAFDIsSet :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcstructex-exported timeval
        (tv_sec :int32)
        (tv_usec :int32))

(defcstructex-exported hostent
        (h_name :string)
        (h_aliases :pointer)
        (h_addrtype :short)
        (h_length :short)
        (h_addr_list :pointer))

(defcstructex-exported linger
        (l_onoff :unsigned-short)
        (l_linger :unsigned-short))

(defconstant-exported IOCPARM_MASK #x7f)

(defconstant-exported IOC_VOID #x20000000)

(defconstant-exported IOC_OUT #x40000000)

(defconstant-exported IOC_IN #x80000000)

(defconstant-exported IOC_INOUT (cl:logior #x80000000 #x40000000))

(defcstructex-exported netent
        (n_name :string)
        (n_aliases :pointer)
        (n_addrtype :short)
        (n_net :unsigned-long))

(defcstructex-exported servent
        (s_name :string)
        (s_aliases :pointer)
        (s_port :short)
        (s_proto :string))

(defcstructex-exported protoent
        (p_name :string)
        (p_aliases :pointer)
        (p_proto :short))

(defconstant-exported IPPROTO_IP 0)

(defconstant-exported IPPROTO_ICMP 1)

(defconstant-exported IPPROTO_IGMP 2)

(defconstant-exported IPPROTO_GGP 3)

(defconstant-exported IPPROTO_TCP 6)

(defconstant-exported IPPROTO_PUP 12)

(defconstant-exported IPPROTO_UDP 17)

(defconstant-exported IPPROTO_IDP 22)

(defconstant-exported IPPROTO_ND 77)

(defconstant-exported IPPROTO_RAW 255)

(defconstant-exported IPPROTO_MAX 256)

(defconstant-exported IPPROTO_HOPOPTS 0)

(defconstant-exported IPPROTO_IPV6 41)

(defconstant-exported IPPROTO_ROUTING 43)

(defconstant-exported IPPROTO_FRAGMENT 44)

(defconstant-exported IPPROTO_ESP 50)

(defconstant-exported IPPROTO_AH 51)

(defconstant-exported IPPROTO_ICMPV6 58)

(defconstant-exported IPPROTO_NONE 59)

(defconstant-exported IPPROTO_DSTOPTS 60)

(defconstant-exported IPPORT_ECHO 7)

(defconstant-exported IPPORT_DISCARD 9)

(defconstant-exported IPPORT_SYSTAT 11)

(defconstant-exported IPPORT_DAYTIME 13)

(defconstant-exported IPPORT_NETSTAT 15)

(defconstant-exported IPPORT_FTP 21)

(defconstant-exported IPPORT_TELNET 23)

(defconstant-exported IPPORT_SMTP 25)

(defconstant-exported IPPORT_TIMESERVER 37)

(defconstant-exported IPPORT_NAMESERVER 42)

(defconstant-exported IPPORT_WHOIS 43)

(defconstant-exported IPPORT_MTP 57)

(defconstant-exported IPPORT_TFTP 69)

(defconstant-exported IPPORT_RJE 77)

(defconstant-exported IPPORT_FINGER 79)

(defconstant-exported IPPORT_TTYLINK 87)

(defconstant-exported IPPORT_SUPDUP 95)

(defconstant-exported IPPORT_EXECSERVER 512)

(defconstant-exported IPPORT_LOGINSERVER 513)

(defconstant-exported IPPORT_CMDSERVER 514)

(defconstant-exported IPPORT_EFSSERVER 520)

(defconstant-exported IPPORT_BIFFUDP 512)

(defconstant-exported IPPORT_WHOSERVER 513)

(defconstant-exported IPPORT_ROUTESERVER 520)

(defconstant-exported IPPORT_RESERVED 1024)

(defconstant-exported IMPLINK_IP 155)

(defconstant-exported IMPLINK_LOWEXPER 156)

(defconstant-exported IMPLINK_HIGHEXPER 158)

(defcstructex-exported in_addr
        (S_un :pointer))

(cffi:defcunion in_addr_S_un
        (S_addr :unsigned-long)
        (S_un_b :pointer)
        (S_un_w :pointer))

(defcstructex-exported in_addr_S_un_S_un_w
        (s_w1 :unsigned-short)
        (s_w2 :unsigned-short))

(defcstructex-exported in_addr_S_un_S_un_b
        (s_b1 :unsigned-char)
        (s_b2 :unsigned-char)
        (s_b3 :unsigned-char)
        (s_b4 :unsigned-char))

(defconstant-exported IN_CLASSA_NET #xff000000)

(defconstant-exported IN_CLASSA_NSHIFT 24)

(defconstant-exported IN_CLASSA_HOST #x00ffffff)

(defconstant-exported IN_CLASSA_MAX 128)

(defconstant-exported IN_CLASSB_NET #xffff0000)

(defconstant-exported IN_CLASSB_NSHIFT 16)

(defconstant-exported IN_CLASSB_HOST #x0000ffff)

(defconstant-exported IN_CLASSB_MAX 65536)

(defconstant-exported IN_CLASSC_NET #xffffff00)

(defconstant-exported IN_CLASSC_NSHIFT 8)

(defconstant-exported IN_CLASSC_HOST #xff)

(defconstant-exported INADDR_LOOPBACK #x7f000001)

(defconstant-exported INADDR_NONE #xffffffff)

(defcstructex-exported sockaddr_in
        (sin_family :short)
        (sin_port :unsigned-short)
        (sin_addr in_addr)
        (sin_zero :pointer))

(defconstant-exported WSADESCRIPTION_LEN 256)

(defconstant-exported WSASYS_STATUS_LEN 128)

(defcstructex-exported WSADATA
        (wVersion :unsigned-short)
        (wHighVersion :unsigned-short)
        (szDescription :pointer)
        (szSystemStatus :pointer)
        (iMaxSockets :unsigned-short)
        (iMaxUdpDg :unsigned-short)
        (lpVendorInfo :string))





(defconstant-exported IP_OPTIONS 1)

(defconstant-exported SO_DEBUG 1)

(defconstant-exported SO_ACCEPTCONN 2)

(defconstant-exported SO_REUSEADDR 4)

(defconstant-exported SO_KEEPALIVE 8)

(defconstant-exported SO_DONTROUTE 16)

(defconstant-exported SO_BROADCAST 32)

(defconstant-exported SO_USELOOPBACK 64)

(defconstant-exported SO_LINGER 128)

(defconstant-exported SO_OOBINLINE 256)

(defconstant-exported SO_SNDBUF #x1001)

(defconstant-exported SO_RCVBUF #x1002)

(defconstant-exported SO_SNDLOWAT #x1003)

(defconstant-exported SO_RCVLOWAT #x1004)

(defconstant-exported SO_SNDTIMEO #x1005)

(defconstant-exported SO_RCVTIMEO #x1006)

(defconstant-exported SO_ERROR #x1007)

(defconstant-exported SO_TYPE #x1008)

(defconstant-exported SOCKET_ERROR -1)

(defconstant-exported SOCK_STREAM 1)

(defconstant-exported SOCK_DGRAM 2)

(defconstant-exported SOCK_RAW 3)

(defconstant-exported SOCK_RDM 4)

(defconstant-exported SOCK_SEQPACKET 5)

(defconstant-exported TCP_NODELAY #x0001)

(defconstant-exported AF_UNSPEC 0)

(defconstant-exported AF_UNIX 1)

(defconstant-exported AF_INET 2)

(defconstant-exported AF_IMPLINK 3)

(defconstant-exported AF_PUP 4)

(defconstant-exported AF_CHAOS 5)

(defconstant-exported AF_IPX 6)

(defconstant-exported AF_NS 6)

(defconstant-exported AF_ISO 7)

(defconstant-exported AF_OSI 7)

(defconstant-exported AF_ECMA 8)

(defconstant-exported AF_DATAKIT 9)

(defconstant-exported AF_CCITT 10)

(defconstant-exported AF_SNA 11)

(defconstant-exported AF_DECnet 12)

(defconstant-exported AF_DLI 13)

(defconstant-exported AF_LAT 14)

(defconstant-exported AF_HYLINK 15)

(defconstant-exported AF_APPLETALK 16)

(defconstant-exported AF_NETBIOS 17)

(defconstant-exported AF_VOICEVIEW 18)

(defconstant-exported AF_FIREFOX 19)

(defconstant-exported AF_UNKNOWN1 20)

(defconstant-exported AF_BAN 21)

(defconstant-exported AF_ATM 22)

(defconstant-exported AF_INET6 23)

(defconstant-exported AF_CLUSTER 24)

(defconstant-exported AF_12844 25)

(defconstant-exported AF_IRDA 26)

(defconstant-exported AF_NETDES 28)

(defconstant-exported AF_MAX 29)

(defcstructex-exported sockaddr
        (sa_family :unsigned-short)
        (sa_data :pointer))

(defcstructex-exported sockaddr_storage
        (ss_family :short)
        (__ss_pad1 :pointer)
        (__ss_align :long-long)
        (__ss_pad2 :pointer))

(defcstructex-exported sockproto
        (sp_family :unsigned-short)
        (sp_protocol :unsigned-short))

(defconstant-exported PF_UNSPEC 0)

(defconstant-exported PF_UNIX 1)

(defconstant-exported PF_INET 2)

(defconstant-exported PF_IMPLINK 3)

(defconstant-exported PF_PUP 4)

(defconstant-exported PF_CHAOS 5)

(defconstant-exported PF_NS 6)

(defconstant-exported PF_IPX 6)

(defconstant-exported PF_ISO 7)

(defconstant-exported PF_OSI 7)

(defconstant-exported PF_ECMA 8)

(defconstant-exported PF_DATAKIT 9)

(defconstant-exported PF_CCITT 10)

(defconstant-exported PF_SNA 11)

(defconstant-exported PF_DECnet 12)

(defconstant-exported PF_DLI 13)

(defconstant-exported PF_LAT 14)

(defconstant-exported PF_HYLINK 15)

(defconstant-exported PF_APPLETALK 16)

(defconstant-exported PF_VOICEVIEW 18)

(defconstant-exported PF_FIREFOX 19)

(defconstant-exported PF_UNKNOWN1 20)

(defconstant-exported PF_BAN 21)

(defconstant-exported PF_ATM 22)

(defconstant-exported PF_INET6 23)

(defconstant-exported PF_MAX 29)

(defconstant-exported SOL_SOCKET #xffff)

(defconstant-exported SOMAXCONN #x7fffffff)

(defconstant-exported MSG_OOB 1)

(defconstant-exported MSG_PEEK 2)

(defconstant-exported MSG_DONTROUTE 4)

(defconstant-exported MSG_MAXIOVLEN 16)

(defconstant-exported MSG_PARTIAL #x8000)

(defconstant-exported MAXGETHOSTSTRUCT 1024)

(defconstant-exported FD_READ_BIT 0)

(defconstant-exported FD_READ (cl:ash 1 0))

(defconstant-exported FD_WRITE_BIT 1)

(defconstant-exported FD_WRITE (cl:ash 1 1))

(defconstant-exported FD_OOB_BIT 2)

(defconstant-exported FD_OOB (cl:ash 1 2))

(defconstant-exported FD_ACCEPT_BIT 3)

(defconstant-exported FD_ACCEPT (cl:ash 1 3))

(defconstant-exported FD_CONNECT_BIT 4)

(defconstant-exported FD_CONNECT (cl:ash 1 4))

(defconstant-exported FD_CLOSE_BIT 5)

(defconstant-exported FD_CLOSE (cl:ash 1 5))

(defconstant-exported FD_QOS_BIT 6)

(defconstant-exported FD_QOS (cl:ash 1 6))

(defconstant-exported FD_GROUP_QOS_BIT 7)

(defconstant-exported FD_GROUP_QOS (cl:ash 1 7))

(defconstant-exported FD_ROUTING_INTERFACE_CHANGE_BIT 8)

(defconstant-exported FD_ROUTING_INTERFACE_CHANGE (cl:ash 1 8))

(defconstant-exported FD_ADDRESS_LIST_CHANGE_BIT 9)

(defconstant-exported FD_ADDRESS_LIST_CHANGE (cl:ash 1 9))

(defconstant-exported FD_MAX_EVENTS 10)

(defconstant-exported FD_ALL_EVENTS (cl:- (cl:ash 1 10) 1))

(defconstant-exported WSANO_ADDRESS 11004)

(defconstant-exported HOST_NOT_FOUND 11001)

(defconstant-exported TRY_AGAIN 11002)

(defconstant-exported NO_RECOVERY 11003)

(defconstant-exported NO_DATA 11004)

(defconstant-exported NO_ADDRESS 11004)

(defcfunex-exported ("accept" accept :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("bind" bind :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("closesocket" closesocket :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("connect" connect :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("ioctlsocket" ioctlsocket :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int32)
  (arg2 :pointer))

(defcfunex-exported ("getpeername" getpeername :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("getsockname" getsockname :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("getsockopt" getsockopt :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :pointer))

(defcfunex-exported ("inet_addr" inet_addr :convention :stdcall) :unsigned-long
  (arg0 :string))

(defcfunex-exported ("inet_ntoa" inet_ntoa :convention :stdcall) :string
  (arg0 in_addr))

(defcfunex-exported ("listen" listen :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int))

(defcfunex-exported ("recv" recv :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("recvfrom" recvfrom :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("send" send :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("sendto" sendto :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("setsockopt" setsockopt :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfunex-exported ("shutdown" shutdown :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :int))

(defcfunex-exported ("socket" socket :convention :stdcall) :unsigned-int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("gethostbyaddr" gethostbyaddr :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("gethostbyname" gethostbyname :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("getservbyport" getservbyport :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :string))

(defcfunex-exported ("getservbyname" getservbyname :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("getprotobynumber" getprotobynumber :convention :stdcall) :pointer
  (arg0 :int))

(defcfunex-exported ("getprotobyname" getprotobyname :convention :stdcall) :pointer
  (arg0 :string))





























































(defcfunex-exported ("WSAStartup" WSAStartup :convention :stdcall) :int
  (arg0 :unsigned-short)
  (arg1 :pointer))

(defcfunex-exported ("WSACleanup" WSACleanup :convention :stdcall) :int)

(defcfunex-exported ("WSASetLastError" WSASetLastError :convention :stdcall) :void
  (arg0 :int))

(defcfunex-exported ("WSAGetLastError" WSAGetLastError :convention :stdcall) :int)









(defcfunex-exported ("WSAIsBlocking" WSAIsBlocking :convention :stdcall) :int)

(defcfunex-exported ("WSAUnhookBlockingHook" WSAUnhookBlockingHook :convention :stdcall) :int)

(defcfunex-exported ("WSASetBlockingHook" WSASetBlockingHook :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("WSACancelBlockingCall" WSACancelBlockingCall :convention :stdcall) :int)









(defcfunex-exported ("WSAAsyncGetServByName" WSAAsyncGetServByName :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :string)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("WSAAsyncGetServByPort" WSAAsyncGetServByPort :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("WSAAsyncGetProtoByName" WSAAsyncGetProtoByName :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :string)
  (arg4 :int))

(defcfunex-exported ("WSAAsyncGetProtoByNumber" WSAAsyncGetProtoByNumber :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfunex-exported ("WSAAsyncGetHostByName" WSAAsyncGetHostByName :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :string)
  (arg4 :int))

(defcfunex-exported ("WSAAsyncGetHostByAddr" WSAAsyncGetHostByAddr :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :int)
  (arg4 :int)
  (arg5 :string)
  (arg6 :int))

(defcfunex-exported ("WSACancelAsyncRequest" WSACancelAsyncRequest :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSAAsyncSelect" WSAAsyncSelect :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :int32))















(defcfunex-exported ("htonl" htonl :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("ntohl" ntohl :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("htons" htons :convention :stdcall) :unsigned-short
  (arg0 :unsigned-short))

(defcfunex-exported ("ntohs" ntohs :convention :stdcall) :unsigned-short
  (arg0 :unsigned-short))

(defcfunex-exported ("select" select :convention :stdcall) :int
  (nfds :int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("gethostname" gethostname :convention :stdcall) :int
  (arg0 :string)
  (arg1 :int))



























































(defconstant-exported IN_CLASSD_NET #xf0000000)

(defconstant-exported IN_CLASSD_NSHIFT 28)

(defconstant-exported IN_CLASSD_HOST #x0fffffff)

(defconstant-exported FROM_PROTOCOL_INFO -1)

(defconstant-exported SO_GROUP_ID #x2001)

(defconstant-exported SO_GROUP_PRIORITY #x2002)

(defconstant-exported SO_MAX_MSG_SIZE #x2003)

(defconstant-exported SO_PROTOCOL_INFOA #x2004)

(defconstant-exported SO_PROTOCOL_INFOW #x2005)

(defconstant-exported SO_PROTOCOL_INFO #x2004)

(defconstant-exported PVD_CONFIG #x3001)

(defconstant-exported MSG_INTERRUPT #x10)



(defconstant-exported WSA_IO_PENDING 997)

(defconstant-exported WSA_IO_INCOMPLETE 996)

(defconstant-exported WSA_INVALID_HANDLE 6)

(defconstant-exported WSA_INVALID_PARAMETER 87)

(defconstant-exported WSA_NOT_ENOUGH_MEMORY 8)

(defconstant-exported WSA_OPERATION_ABORTED 995)

(defconstant-exported WSA_MAXIMUM_WAIT_EVENTS 64)

(defconstant-exported WSA_WAIT_EVENT_0 0)

(defconstant-exported WSA_WAIT_IO_COMPLETION #xC0)

(defconstant-exported WSA_WAIT_TIMEOUT 258)

(defconstant-exported WSA_INFINITE #xFFFFFFFF)

(defcstructex-exported WSABUF
        (len :unsigned-long)
        (buf :string))





(cffi:defcenum GUARANTEE
        :BestEffortService
        :ControlledLoadService
        :PredictiveService
        :GuaranteedDelayService
        :GuaranteedService)



(defcstructex-exported FLOWSPEC
        (TokenRate :unsigned-int)
        (TokenBucketSize :unsigned-int)
        (PeakBandwidth :unsigned-int)
        (Latency :unsigned-int)
        (DelayVariation :unsigned-int)
        (ServiceType :unsigned-int)
        (MaxSduSize :unsigned-int)
        (MinimumPolicedSize :unsigned-int))







(defcstructex-exported QOS
        (SendingFlowspec FLOWSPEC)
        (ReceivingFlowspec FLOWSPEC)
        (ProviderSpecific WSABUF))





(defconstant-exported CF_ACCEPT #x0000)

(defconstant-exported CF_REJECT #x0001)

(defconstant-exported CF_DEFER #x0002)



(defconstant-exported SG_UNCONSTRAINED_GROUP #x01)

(defconstant-exported SG_CONSTRAINED_GROUP #x02)

(defcstructex-exported WSANETWORKEVENTS
        (lNetworkEvents :int32)
        (iErrorCode :pointer))





(defconstant-exported MAX_PROTOCOL_CHAIN 7)

(defconstant-exported BASE_PROTOCOL 1)

(defconstant-exported LAYERED_PROTOCOL 0)

(cffi:defcenum WSAESETSERVICEOP
        (:RNRSERVICE_REGISTER #.0)
        :RNRSERVICE_DEREGISTER
        :RNRSERVICE_DELETE)





(defcstructex-exported AFPROTOCOLS
        (iAddressFamily :int)
        (iProtocol :int))







(cffi:defcenum WSAECOMPARATOR
        (:COMP_EQUAL #.0)
        :COMP_NOTLESS)





(defcstructex-exported WSAVERSION
        (dwVersion :unsigned-long)
        (ecHow WSAECOMPARATOR))







(defcstructex-exported SOCKET_ADDRESS
        (lpSockaddr :pointer)
        (iSockaddrLength :int))







(defcstructex-exported CSADDR_INFO
        (LocalAddr SOCKET_ADDRESS)
        (RemoteAddr SOCKET_ADDRESS)
        (iSocketType :int)
        (iProtocol :int))







(defcstructex-exported SOCKET_ADDRESS_LIST
        (iAddressCount :int)
        (Address :pointer))





(defcstructex-exported BLOB
        (cbSize :unsigned-long)
        (pBlobData :pointer))







(defcstructex-exported WSAQUERYSETA
        (dwSize :unsigned-long)
        (lpszServiceInstanceName :string)
        (lpServiceClassId :pointer)
        (lpVersion :pointer)
        (lpszComment :string)
        (dwNameSpace :unsigned-long)
        (lpNSProviderId :pointer)
        (lpszContext :string)
        (dwNumberOfProtocols :unsigned-long)
        (lpafpProtocols :pointer)
        (lpszQueryString :string)
        (dwNumberOfCsAddrs :unsigned-long)
        (lpcsaBuffer :pointer)
        (dwOutputFlags :unsigned-long)
        (lpBlob :pointer))







(defcstructex-exported WSAQUERYSETW
        (dwSize :unsigned-long)
        (lpszServiceInstanceName :pointer)
        (lpServiceClassId :pointer)
        (lpVersion :pointer)
        (lpszComment :pointer)
        (dwNameSpace :unsigned-long)
        (lpNSProviderId :pointer)
        (lpszContext :pointer)
        (dwNumberOfProtocols :unsigned-long)
        (lpafpProtocols :pointer)
        (lpszQueryString :pointer)
        (dwNumberOfCsAddrs :unsigned-long)
        (lpcsaBuffer :pointer)
        (dwOutputFlags :unsigned-long)
        (lpBlob :pointer))













(defconstant-exported LUP_DEEP #x0001)

(defconstant-exported LUP_CONTAINERS #x0002)

(defconstant-exported LUP_NOCONTAINERS #x0004)

(defconstant-exported LUP_NEAREST #x0008)

(defconstant-exported LUP_RETURN_NAME #x0010)

(defconstant-exported LUP_RETURN_TYPE #x0020)

(defconstant-exported LUP_RETURN_VERSION #x0040)

(defconstant-exported LUP_RETURN_COMMENT #x0080)

(defconstant-exported LUP_RETURN_ADDR #x0100)

(defconstant-exported LUP_RETURN_BLOB #x0200)

(defconstant-exported LUP_RETURN_ALIASES #x0400)

(defconstant-exported LUP_RETURN_QUERY_STRING #x0800)

(defconstant-exported LUP_RETURN_ALL #x0FF0)

(defconstant-exported LUP_RES_SERVICE #x8000)

(defconstant-exported LUP_FLUSHCACHE #x1000)

(defconstant-exported LUP_FLUSHPREVIOUS #x2000)

(defcstructex-exported WSANSCLASSINFOA
        (lpszName :string)
        (dwNameSpace :unsigned-long)
        (dwValueType :unsigned-long)
        (dwValueSize :unsigned-long)
        (lpValue :pointer))







(defcstructex-exported WSANSCLASSINFOW
        (lpszName :pointer)
        (dwNameSpace :unsigned-long)
        (dwValueType :unsigned-long)
        (dwValueSize :unsigned-long)
        (lpValue :pointer))













(defcstructex-exported WSASERVICECLASSINFOA
        (lpServiceClassId :pointer)
        (lpszServiceClassName :string)
        (dwCount :unsigned-long)
        (lpClassInfos :pointer))







(defcstructex-exported WSASERVICECLASSINFOW
        (lpServiceClassId :pointer)
        (lpszServiceClassName :pointer)
        (dwCount :unsigned-long)
        (lpClassInfos :pointer))













(defcstructex-exported WSANAMESPACE_INFOA
        (NSProviderId GUID)
        (dwNameSpace :unsigned-long)
        (fActive :int)
        (dwVersion :unsigned-long)
        (lpszIdentifier :string))







(defcstructex-exported WSANAMESPACE_INFOW
        (NSProviderId GUID)
        (dwNameSpace :unsigned-long)
        (fActive :int)
        (dwVersion :unsigned-long)
        (lpszIdentifier :pointer))













(defcstructex-exported WSAPROTOCOLCHAIN
        (ChainLen :int)
        (ChainEntries :pointer))





(defconstant-exported WSAPROTOCOL_LEN 255)

(defcstructex-exported WSAPROTOCOL_INFOA
        (dwServiceFlags1 :unsigned-long)
        (dwServiceFlags2 :unsigned-long)
        (dwServiceFlags3 :unsigned-long)
        (dwServiceFlags4 :unsigned-long)
        (dwProviderFlags :unsigned-long)
        (ProviderId GUID)
        (dwCatalogEntryId :unsigned-long)
        (ProtocolChain WSAPROTOCOLCHAIN)
        (iVersion :int)
        (iAddressFamily :int)
        (iMaxSockAddr :int)
        (iMinSockAddr :int)
        (iSocketType :int)
        (iProtocol :int)
        (iProtocolMaxOffset :int)
        (iNetworkByteOrder :int)
        (iSecurityScheme :int)
        (dwMessageSize :unsigned-long)
        (dwProviderReserved :unsigned-long)
        (szProtocol :pointer))





(defcstructex-exported WSAPROTOCOL_INFOW
        (dwServiceFlags1 :unsigned-long)
        (dwServiceFlags2 :unsigned-long)
        (dwServiceFlags3 :unsigned-long)
        (dwServiceFlags4 :unsigned-long)
        (dwProviderFlags :unsigned-long)
        (ProviderId GUID)
        (dwCatalogEntryId :unsigned-long)
        (ProtocolChain WSAPROTOCOLCHAIN)
        (iVersion :int)
        (iAddressFamily :int)
        (iMaxSockAddr :int)
        (iMinSockAddr :int)
        (iSocketType :int)
        (iProtocol :int)
        (iProtocolMaxOffset :int)
        (iNetworkByteOrder :int)
        (iSecurityScheme :int)
        (dwMessageSize :unsigned-long)
        (dwProviderReserved :unsigned-long)
        (szProtocol :pointer))













(cffi:defcenum WSACOMPLETIONTYPE
        (:NSP_NOTIFY_IMMEDIATELY #.0)
        :NSP_NOTIFY_HWND
        :NSP_NOTIFY_EVENT
        :NSP_NOTIFY_PORT
        :NSP_NOTIFY_APC)





(defcstructex-exported WSACOMPLETION
        (Type WSACOMPLETIONTYPE)
        (Parameters :pointer))







(cffi:defcunion WSACOMPLETION_Parameters
        (WindowMessage :pointer)
        (Event :pointer)
        (Apc :pointer)
        (Port :pointer))

(defcstructex-exported WSACOMPLETION_Parameters_Port
        (lpOverlapped :pointer)
        (hPort :pointer)
        (Key :unsigned-long))

(defcstructex-exported WSACOMPLETION_Parameters_Apc
        (lpOverlapped :pointer)
        (lpfnCompletionProc :pointer))

(defcstructex-exported WSACOMPLETION_Parameters_Event
        (lpOverlapped :pointer))

(defcstructex-exported WSACOMPLETION_Parameters_WindowMessage
        (hWnd :pointer)
        (uMsg :unsigned-int)
        (context :unsigned-int))

(defconstant-exported PFL_MULTIPLE_PROTO_ENTRIES #x00000001)

(defconstant-exported PFL_RECOMMENDED_PROTO_ENTRY #x00000002)

(defconstant-exported PFL_HIDDEN #x00000004)

(defconstant-exported PFL_MATCHES_PROTOCOL_ZERO #x00000008)

(defconstant-exported XP1_CONNECTIONLESS #x00000001)

(defconstant-exported XP1_GUARANTEED_DELIVERY #x00000002)

(defconstant-exported XP1_GUARANTEED_ORDER #x00000004)

(defconstant-exported XP1_MESSAGE_ORIENTED #x00000008)

(defconstant-exported XP1_PSEUDO_STREAM #x00000010)

(defconstant-exported XP1_GRACEFUL_CLOSE #x00000020)

(defconstant-exported XP1_EXPEDITED_DATA #x00000040)

(defconstant-exported XP1_CONNECT_DATA #x00000080)

(defconstant-exported XP1_DISCONNECT_DATA #x00000100)

(defconstant-exported XP1_SUPPORT_BROADCAST #x00000200)

(defconstant-exported XP1_SUPPORT_MULTIPOINT #x00000400)

(defconstant-exported XP1_MULTIPOINT_CONTROL_PLANE #x00000800)

(defconstant-exported XP1_MULTIPOINT_DATA_PLANE #x00001000)

(defconstant-exported XP1_QOS_SUPPORTED #x00002000)

(defconstant-exported XP1_INTERRUPT #x00004000)

(defconstant-exported XP1_UNI_SEND #x00008000)

(defconstant-exported XP1_UNI_RECV #x00010000)

(defconstant-exported XP1_IFS_HANDLES #x00020000)

(defconstant-exported XP1_PARTIAL_MESSAGE #x00040000)

(defconstant-exported BIGENDIAN #x0000)

(defconstant-exported LITTLEENDIAN #x0001)

(defconstant-exported SECURITY_PROTOCOL_NONE #x0000)

(defconstant-exported JL_SENDER_ONLY #x01)

(defconstant-exported JL_RECEIVER_ONLY #x02)

(defconstant-exported JL_BOTH #x04)

(defconstant-exported WSA_FLAG_OVERLAPPED #x01)

(defconstant-exported WSA_FLAG_MULTIPOINT_C_ROOT #x02)

(defconstant-exported WSA_FLAG_MULTIPOINT_C_LEAF #x04)

(defconstant-exported WSA_FLAG_MULTIPOINT_D_ROOT #x08)

(defconstant-exported WSA_FLAG_MULTIPOINT_D_LEAF #x10)

(defconstant-exported IOC_UNIX #x00000000)

(defconstant-exported IOC_WS2 #x08000000)

(defconstant-exported IOC_PROTOCOL #x10000000)

(defconstant-exported IOC_VENDOR #x18000000)

(defconstant-exported SIO_ASSOCIATE_HANDLE (cl:logior #x80000000 #x08000000 1))

(defconstant-exported SIO_ENABLE_CIRCULAR_QUEUEING (cl:logior #x20000000 #x08000000 2))

(defconstant-exported SIO_FIND_ROUTE (cl:logior #x40000000 #x08000000 3))

(defconstant-exported SIO_FLUSH (cl:logior #x20000000 #x08000000 4))

(defconstant-exported SIO_GET_BROADCAST_ADDRESS (cl:logior #x40000000 #x08000000 5))

(defconstant-exported SIO_GET_EXTENSION_FUNCTION_POINTER (cl:logior IOC_INOUT IOC_WS2 6))

(defconstant-exported SIO_GET_QOS (cl:logior IOC_INOUT IOC_WS2 7))

(defconstant-exported SIO_GET_GROUP_QOS (cl:logior IOC_INOUT IOC_WS2 8))

(defconstant-exported SIO_MULTIPOINT_LOOPBACK (cl:logior #x80000000 #x08000000 9))

(defconstant-exported SIO_MULTICAST_SCOPE (cl:logior #x80000000 #x08000000 10))

(defconstant-exported SIO_SET_QOS (cl:logior #x80000000 #x08000000 11))

(defconstant-exported SIO_SET_GROUP_QOS (cl:logior #x80000000 #x08000000 12))

(defconstant-exported SIO_TRANSLATE_HANDLE (cl:logior IOC_INOUT IOC_WS2 13))

(defconstant-exported SIO_ROUTING_INTERFACE_QUERY (cl:logior IOC_INOUT IOC_WS2 20))

(defconstant-exported SIO_ROUTING_INTERFACE_CHANGE (cl:logior #x80000000 #x08000000 21))

(defconstant-exported SIO_ADDRESS_LIST_QUERY (cl:logior #x40000000 #x08000000 22))

(defconstant-exported SIO_ADDRESS_LIST_CHANGE (cl:logior #x20000000 #x08000000 23))

(defconstant-exported SIO_QUERY_TARGET_PNP_HANDLE (cl:logior #x40000000 #x08000000 24))

(defconstant-exported SIO_NSP_NOTIFY_CHANGE (cl:logior #x80000000 #x08000000 25))

(defconstant-exported TH_NETDEV #x00000001)

(defconstant-exported TH_TAPI #x00000002)

(defcfunex-exported ("WSAAccept" WSAAccept :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("WSAAddressToStringA" WSAAddressToStringA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :string)
  (arg4 :pointer))

(defcfunex-exported ("WSAAddressToStringW" WSAAddressToStringW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WSACloseEvent" WSACloseEvent :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSAConnect" WSAConnect :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("WSACreateEvent" WSACreateEvent :convention :stdcall) :pointer)

(defcfunex-exported ("WSADuplicateSocketA" WSADuplicateSocketA :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WSADuplicateSocketW" WSADuplicateSocketW :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WSAEnumNameSpaceProvidersA" WSAEnumNameSpaceProvidersA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("WSAEnumNameSpaceProvidersW" WSAEnumNameSpaceProvidersW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("WSAEnumNetworkEvents" WSAEnumNetworkEvents :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WSAEnumProtocolsA" WSAEnumProtocolsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WSAEnumProtocolsW" WSAEnumProtocolsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WSAEventSelect" WSAEventSelect :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("WSAGetOverlappedResult" WSAGetOverlappedResult :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("WSAGetQOSByName" WSAGetQOSByName :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WSAGetServiceClassInfoA" WSAGetServiceClassInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WSAGetServiceClassInfoW" WSAGetServiceClassInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WSAGetServiceClassNameByClassIdA" WSAGetServiceClassNameByClassIdA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("WSAGetServiceClassNameByClassIdW" WSAGetServiceClassNameByClassIdW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WSAHtonl" WSAHtonl :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WSAHtons" WSAHtons :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-short)
  (arg2 :pointer))

(defcfunex-exported ("WSAInstallServiceClassA" WSAInstallServiceClassA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSAInstallServiceClassW" WSAInstallServiceClassW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSAIoctl" WSAIoctl :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer))

(defcfunex-exported ("WSAJoinLeaf" WSAJoinLeaf :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :unsigned-long))

(defcfunex-exported ("WSALookupServiceBeginA" WSALookupServiceBeginA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WSALookupServiceBeginW" WSALookupServiceBeginW :convention :stdcall) :int
  (lpqsRestrictions :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WSALookupServiceNextA" WSALookupServiceNextA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WSALookupServiceNextW" WSALookupServiceNextW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WSALookupServiceEnd" WSALookupServiceEnd :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSANSPIoctl" WSANSPIoctl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("WSANtohl" WSANtohl :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WSANtohs" WSANtohs :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-short)
  (arg2 :pointer))

(defcfunex-exported ("WSARecv" WSARecv :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("WSARecvDisconnect" WSARecvDisconnect :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("WSARecvFrom" WSARecvFrom :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer))

(defcfunex-exported ("WSARemoveServiceClass" WSARemoveServiceClass :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSAResetEvent" WSAResetEvent :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSASend" WSASend :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("WSASendDisconnect" WSASendDisconnect :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("WSASendTo" WSASendTo :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :int)
  (arg7 :pointer)
  (arg8 :pointer))

(defcfunex-exported ("WSASetEvent" WSASetEvent :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WSASetServiceA" WSASetServiceA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 WSAESETSERVICEOP)
  (arg2 :unsigned-long))

(defcfunex-exported ("WSASetServiceW" WSASetServiceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 WSAESETSERVICEOP)
  (arg2 :unsigned-long))

(defcfunex-exported ("WSASocketA" WSASocketA :convention :stdcall) :unsigned-int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :unsigned-int)
  (arg5 :unsigned-long))

(defcfunex-exported ("WSASocketW" WSASocketW :convention :stdcall) :unsigned-int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :unsigned-int)
  (arg5 :unsigned-long))

(defcfunex-exported ("WSAStringToAddressA" WSAStringToAddressA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WSAStringToAddressW" WSAStringToAddressW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WSAWaitForMultipleEvents" WSAWaitForMultipleEvents :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :int))





