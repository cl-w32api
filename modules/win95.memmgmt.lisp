
(cl:in-package w32apimod)

(define-w32api-module win95.memmgmt :win95.memmgmt)

(cl:in-package cl-w32api.module.win95.memmgmt)

(defcfunex-exported ("GetProcessHeap" GetProcessHeap :convention :stdcall) :pointer)

(defcfunex-exported ("GetProcessHeaps" GetProcessHeaps :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("GlobalAlloc" GlobalAlloc :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :unsigned-long))

(defcfunex-exported ("GlobalFlags" GlobalFlags :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("GlobalFree" GlobalFree :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GlobalHandle" GlobalHandle :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GlobalLock" GlobalLock :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GlobalMemoryStatus" GlobalMemoryStatus :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GlobalReAlloc" GlobalReAlloc :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-int))

(defcfunex-exported ("GlobalSize" GlobalSize :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GlobalUnlock" GlobalUnlock :convention :stdcall) :int
  (arg0 :pointer))


(defcfunex-exported ("HeapAlloc" HeapAlloc :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("HeapCompact" HeapCompact :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("HeapCreate" HeapCreate :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("HeapDestroy" HeapDestroy :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("HeapFree" HeapFree :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("HeapLock" HeapLock :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("HeapReAlloc" HeapReAlloc :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("HeapSize" HeapSize :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("HeapUnlock" HeapUnlock :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("HeapValidate" HeapValidate :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("HeapWalk" HeapWalk :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IsBadCodePtr" IsBadCodePtr :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsBadHugeReadPtr" IsBadHugeReadPtr :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("IsBadHugeWritePtr" IsBadHugeWritePtr :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("IsBadReadPtr" IsBadReadPtr :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("IsBadStringPtrA" IsBadStringPtrA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-int))

(defcfunex-exported ("IsBadStringPtrW" IsBadStringPtrW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("IsBadWritePtr" IsBadWritePtr :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("LocalAlloc" LocalAlloc :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :unsigned-long))

(defcfunex-exported ("LocalDiscard" LocalDiscard :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LocalFlags" LocalFlags :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("LocalFree" LocalFree :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LocalHandle" LocalHandle :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LocalLock" LocalLock :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LocalReAlloc" LocalReAlloc :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-int))

(defcfunex-exported ("LocalSize" LocalSize :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("LocalUnlock" LocalUnlock :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("VirtualAlloc" VirtualAlloc :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("VirtualAllocEx" VirtualAllocEx :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("VirtualFree" VirtualFree :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("VirtualFreeEx" VirtualFreeEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("VirtualLock" VirtualLock :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("VirtualProtect" VirtualProtect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("VirtualProtectEx" VirtualProtectEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("VirtualQuery" VirtualQuery :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("VirtualQueryEx" VirtualQueryEx :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("VirtualUnlock" VirtualUnlock :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))


; obsolete

(defcfunex-exported ("GlobalCompact" GlobalCompact :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("GlobalFix" GlobalFix :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GlobalUnfix" GlobalUnfix :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GlobalUnWire" GlobalUnWire :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GlobalWire" GlobalWire :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LocalCompact" LocalCompact :convention :stdcall) :unsigned-long
  (arg0 :unsigned-int))

(defcfunex-exported ("LocalShrink" LocalShrink :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-int))

