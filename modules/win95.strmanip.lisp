
(cl:in-package w32apimod)

(define-w32api-module win95.strmanip :win95.strmanip)

(cl:in-package cl-w32api.module.win95.strmanip)

(defcfunex-exported ("CharLowerA" CharLowerA :convention :stdcall) :string
  (arg0 :string))

(defcfunex-exported ("CharLowerW" CharLowerW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CharLowerBuffA" CharLowerBuffA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("CharLowerBuffW" CharLowerBuffW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("CharNextA" CharNextA :convention :stdcall) :string
  (arg0 :string))

(defcfunex-exported ("CharNextW" CharNextW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CharNextExA" CharNextExA :convention :stdcall) :string
  (arg0 :unsigned-short)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("CharNextExW" CharNextExW :convention :stdcall) :pointer
  (arg0 :unsigned-short)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("CharPrevA" CharPrevA :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("CharPrevW" CharPrevW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CharPrevExA" CharPrevExA :convention :stdcall) :string
  (arg0 :unsigned-short)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-long))

(defcfunex-exported ("CharPrevExW" CharPrevExW :convention :stdcall) :pointer
  (arg0 :unsigned-short)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("CharToOemA" CharToOemA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("CharToOemW" CharToOemW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("CharToOemBuffA" CharToOemBuffA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("CharToOemBuffW" CharToOemBuffW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("CharUpperA" CharUpperA :convention :stdcall) :string
  (arg0 :string))

(defcfunex-exported ("CharUpperW" CharUpperW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CharUpperBuffA" CharUpperBuffA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("CharUpperBuffW" CharUpperBuffW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("CompareStringA" CompareStringA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("CompareStringW" CompareStringW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("ConvertDefaultLocale" ConvertDefaultLocale :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("EnumCalendarInfoA" EnumCalendarInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("EnumCalendarInfoW" EnumCalendarInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("EnumDateFormatsA" EnumDateFormatsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("EnumDateFormatsW" EnumDateFormatsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("EnumSystemLocalesA" EnumSystemLocalesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("EnumSystemLocalesW" EnumSystemLocalesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("EnumTimeFormatsA" EnumTimeFormatsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("EnumTimeFormatsW" EnumTimeFormatsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("EnumSystemCodePagesA" EnumSystemCodePagesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("EnumSystemCodePagesW" EnumSystemCodePagesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("FoldStringA" FoldStringA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int))

(defcfunex-exported ("FoldStringW" FoldStringW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :int))

(defcfunex-exported ("FormatMessageA" FormatMessageA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :string)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("FormatMessageW" FormatMessageW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("GetACP" GetACP :convention :stdcall) :unsigned-int)

(defcfunex-exported ("GetCPInfo" GetCPInfo :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("GetCPInfoExA" GetCPInfoExA :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetCPInfoExW" GetCPInfoExW :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetCurrencyFormatA" GetCurrencyFormatA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("GetCurrencyFormatW" GetCurrencyFormatW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("GetDateFormatA" GetDateFormatA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :string)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("GetDateFormatW" GetDateFormatW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int))


(defcfunex-exported ("IsCharAlphaA" IsCharAlphaA :convention :stdcall) :int
  (ch :char))

(defcfunex-exported ("IsCharAlphaW" IsCharAlphaW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsCharAlphaNumericA" IsCharAlphaNumericA :convention :stdcall) :int
  (arg0 :char))

(defcfunex-exported ("IsCharAlphaNumericW" IsCharAlphaNumericW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsCharLowerA" IsCharLowerA :convention :stdcall) :int
  (arg0 :char))

(defcfunex-exported ("IsCharLowerW" IsCharLowerW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsCharUpperA" IsCharUpperA :convention :stdcall) :int
  (arg0 :char))

(defcfunex-exported ("IsCharUpperW" IsCharUpperW :convention :stdcall) :int
  (arg0 :pointer))


(defcfunex-exported ("GetNumberFormatA" GetNumberFormatA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("GetNumberFormatW" GetNumberFormatW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("GetOEMCP" GetOEMCP :convention :stdcall) :unsigned-int)

(defcfunex-exported ("GetStringTypeA" GetStringTypeA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("GetStringTypeW" GetStringTypeW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetStringTypeExA" GetStringTypeExA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("GetStringTypeExW" GetStringTypeExW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :pointer))


(defcfunex-exported ("IsValidCodePage" IsValidCodePage :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("IsValidLocale" IsValidLocale :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long))

(defcfunex-exported ("LCMapStringA" LCMapStringA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("LCMapStringW" LCMapStringW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("lstrcatA" lstrcatA :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("lstrcatW" lstrcatW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("lstrcmpA" lstrcmpA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("lstrcmpiA" lstrcmpiA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("lstrcmpiW" lstrcmpiW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("lstrcmpW" lstrcmpW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("lstrcpyA" lstrcpyA :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("lstrcpynA" lstrcpynA :convention :stdcall) :string
  (arg0 :string)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("lstrcpynW" lstrcpynW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("lstrcpyW" lstrcpyW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("lstrlenA" lstrlenA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("lstrlenW" lstrlenW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("MultiByteToWideChar" MultiByteToWideChar :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("OemToCharA" OemToCharA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("OemToCharW" OemToCharW :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("OemToCharBuffA" OemToCharBuffA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("OemToCharBuffW" OemToCharBuffW :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("WideCharToMultiByte" WideCharToMultiByte :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :string)
  (arg5 :int)
  (arg6 :string)
  (arg7 :pointer))

(defcfunex-exported ("wsprintfA" wsprintfA :convention :cdecl) :int
  (arg0 :pointer)
  (arg1 :string)
  &rest)

(defcfunex-exported ("wsprintfW" wsprintfW :convention :cdecl) :int
  (arg0 :pointer)
  (arg1 :pointer)
  &rest)

(defcfunex-exported ("wvsprintfA" wvsprintfA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arglist :pointer))

(defcfunex-exported ("wvsprintfW" wvsprintfW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arglist :pointer))
