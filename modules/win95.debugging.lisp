
(cl:in-package w32apimod)

(define-w32api-module win95.debugging :win95.debugging)

(cl:in-package cl-w32api.module.win95.debugging)

(defcfunex-exported ("ContinueDebugEvent" ContinueDebugEvent :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("DebugActiveProcess" DebugActiveProcess :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("DebugBreak" DebugBreak :convention :stdcall) :void)

(defcfunex-exported ("FatalExit" FatalExit :convention :stdcall) :void
  (arg0 :int))

(defcfunex-exported ("FlushInstructionCache" FlushInstructionCache :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetThreadContext" GetThreadContext :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetThreadSelectorEntry" GetThreadSelectorEntry :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("IsDebuggerPresent" IsDebuggerPresent :convention :stdcall) :int)

(defcfunex-exported ("OutputDebugStringA" OutputDebugStringA :convention :stdcall) :void
  (arg0 :string))

(defcfunex-exported ("OutputDebugStringW" OutputDebugStringW :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("ReadProcessMemory" ReadProcessMemory :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("SetDebugErrorLevel" SetDebugErrorLevel :convention :stdcall) :void
  (arg0 :unsigned-long))

(defcfunex-exported ("SetThreadContext" SetThreadContext :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("WaitForDebugEvent" WaitForDebugEvent :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("WriteProcessMemory" WriteProcessMemory :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

