(cl:in-package w32apimod)

(define-w32api-module winmm :winmm)

(cl:in-package cl-w32api.module.winmm)


(defconstant-exported MAXPNAMELEN 32)

(defconstant-exported MAXERRORLENGTH 256)

(defconstant-exported MAX_JOYSTICKOEMVXDNAME 260)

(defconstant-exported TIME_MS 1)

(defconstant-exported TIME_SAMPLES 2)

(defconstant-exported TIME_BYTES 4)

(defconstant-exported TIME_SMPTE 8)

(defconstant-exported TIME_MIDI 16)

(defconstant-exported TIME_TICKS 32)

(defconstant-exported MM_JOY1MOVE #x3A0)

(defconstant-exported MM_JOY2MOVE #x3A1)

(defconstant-exported MM_JOY1ZMOVE #x3A2)

(defconstant-exported MM_JOY2ZMOVE #x3A3)

(defconstant-exported MM_JOY1BUTTONDOWN #x3B5)

(defconstant-exported MM_JOY2BUTTONDOWN #x3B6)

(defconstant-exported MM_JOY1BUTTONUP #x3B7)

(defconstant-exported MM_JOY2BUTTONUP #x3B8)

(defconstant-exported MM_MCINOTIFY #x3B9)

(defconstant-exported MM_WOM_OPEN #x3BB)

(defconstant-exported MM_WOM_CLOSE #x3BC)

(defconstant-exported MM_WOM_DONE #x3BD)

(defconstant-exported MM_WIM_OPEN #x3BE)

(defconstant-exported MM_WIM_CLOSE #x3BF)

(defconstant-exported MM_WIM_DATA #x3C0)

(defconstant-exported MM_MIM_OPEN #x3C1)

(defconstant-exported MM_MIM_CLOSE #x3C2)

(defconstant-exported MM_MIM_DATA #x3C3)

(defconstant-exported MM_MIM_LONGDATA #x3C4)

(defconstant-exported MM_MIM_ERROR #x3C5)

(defconstant-exported MM_MIM_LONGERROR #x3C6)

(defconstant-exported MM_MOM_OPEN #x3C7)

(defconstant-exported MM_MOM_CLOSE #x3C8)

(defconstant-exported MM_MOM_DONE #x3C9)

(defconstant-exported MM_DRVM_OPEN #x3D0)

(defconstant-exported MM_DRVM_CLOSE #x3D1)

(defconstant-exported MM_DRVM_DATA #x3D2)

(defconstant-exported MM_DRVM_ERROR #x3D3)

(defconstant-exported MM_STREAM_OPEN #x3D4)

(defconstant-exported MM_STREAM_CLOSE #x3D5)

(defconstant-exported MM_STREAM_DONE #x3D6)

(defconstant-exported MM_STREAM_ERROR #x3D7)

(defconstant-exported MM_MOM_POSITIONCB #x3CA)

(defconstant-exported MM_MCISIGNAL #x3CB)

(defconstant-exported MM_MIM_MOREDATA #x3CC)

(defconstant-exported MM_MIXM_LINE_CHANGE #x3D0)

(defconstant-exported MM_MIXM_CONTROL_CHANGE #x3D1)

(defconstant-exported MMSYSERR_BASE 0)

(defconstant-exported WAVERR_BASE 32)

(defconstant-exported MIDIERR_BASE 64)

(defconstant-exported TIMERR_BASE 96)

(defconstant-exported JOYERR_BASE 160)

(defconstant-exported MCIERR_BASE 256)

(defconstant-exported MIXERR_BASE 1024)

(defconstant-exported MCI_STRING_OFFSET 512)

(defconstant-exported MCI_VD_OFFSET 1024)

(defconstant-exported MCI_CD_OFFSET 1088)

(defconstant-exported MCI_WAVE_OFFSET 1152)

(defconstant-exported MCI_SEQ_OFFSET 1216)

(defconstant-exported MMSYSERR_NOERROR 0)

(defconstant-exported MMSYSERR_ERROR (cl:+ 0 1))

(defconstant-exported MMSYSERR_BADDEVICEID (cl:+ 0 2))

(defconstant-exported MMSYSERR_NOTENABLED (cl:+ 0 3))

(defconstant-exported MMSYSERR_ALLOCATED (cl:+ 0 4))

(defconstant-exported MMSYSERR_INVALHANDLE (cl:+ 0 5))

(defconstant-exported MMSYSERR_NODRIVER (cl:+ 0 6))

(defconstant-exported MMSYSERR_NOMEM (cl:+ 0 7))

(defconstant-exported MMSYSERR_NOTSUPPORTED (cl:+ 0 8))

(defconstant-exported MMSYSERR_BADERRNUM (cl:+ 0 9))

(defconstant-exported MMSYSERR_INVALFLAG (cl:+ 0 10))

(defconstant-exported MMSYSERR_INVALPARAM (cl:+ 0 11))

(defconstant-exported MMSYSERR_HANDLEBUSY (cl:+ 0 12))

(defconstant-exported MMSYSERR_INVALIDALIAS (cl:+ 0 13))

(defconstant-exported MMSYSERR_BADDB (cl:+ 0 14))

(defconstant-exported MMSYSERR_KEYNOTFOUND (cl:+ 0 15))

(defconstant-exported MMSYSERR_READERROR (cl:+ 0 16))

(defconstant-exported MMSYSERR_WRITEERROR (cl:+ 0 17))

(defconstant-exported MMSYSERR_DELETEERROR (cl:+ 0 18))

(defconstant-exported MMSYSERR_VALNOTFOUND (cl:+ 0 19))

(defconstant-exported MMSYSERR_NODRIVERCB (cl:+ 0 20))

(defconstant-exported MMSYSERR_LASTERROR (cl:+ 0 20))

(defconstant-exported DRV_LOAD 1)

(defconstant-exported DRV_ENABLE 2)

(defconstant-exported DRV_OPEN 3)

(defconstant-exported DRV_CLOSE 4)

(defconstant-exported DRV_DISABLE 5)

(defconstant-exported DRV_FREE 6)

(defconstant-exported DRV_CONFIGURE 7)

(defconstant-exported DRV_QUERYCONFIGURE 8)

(defconstant-exported DRV_INSTALL 9)

(defconstant-exported DRV_REMOVE 10)

(defconstant-exported DRV_EXITSESSION 11)

(defconstant-exported DRV_POWER 15)

(defconstant-exported DRV_RESERVED #x800)

(defconstant-exported DRV_USER #x4000)

(defconstant-exported DRVCNF_CANCEL 0)

(defconstant-exported DRVCNF_OK 1)

(defconstant-exported DRVCNF_RESTART 2)

(defconstant-exported DRV_CANCEL 0)

(defconstant-exported DRV_OK 1)

(defconstant-exported DRV_RESTART 2)

(defconstant-exported DRV_MCI_FIRST #x800)

(defconstant-exported DRV_MCI_LAST (cl:+ #x800 #xFFF))

(defconstant-exported CALLBACK_TYPEMASK #x70000)

(defconstant-exported CALLBACK_NULL 0)

(defconstant-exported CALLBACK_WINDOW #x10000)

(defconstant-exported CALLBACK_TASK #x20000)

(defconstant-exported CALLBACK_FUNCTION #x30000)

(defconstant-exported CALLBACK_THREAD #x20000)

(defconstant-exported CALLBACK_EVENT #x50000)

(defconstant-exported SND_SYNC 0)

(defconstant-exported SND_ASYNC 1)

(defconstant-exported SND_NODEFAULT 2)

(defconstant-exported SND_MEMORY 4)

(defconstant-exported SND_LOOP 8)

(defconstant-exported SND_NOSTOP 16)

(defconstant-exported SND_NOWAIT #x2000)

(defconstant-exported SND_ALIAS #x10000)

(defconstant-exported SND_ALIAS_ID #x110000)

(defconstant-exported SND_FILENAME #x20000)

(defconstant-exported SND_RESOURCE #x40004)

(defconstant-exported SND_PURGE #x40)

(defconstant-exported SND_APPLICATION #x80)

(defconstant-exported SND_ALIAS_START 0)

(defconstant-exported WAVERR_BADFORMAT (cl:+ 32 0))

(defconstant-exported WAVERR_STILLPLAYING (cl:+ 32 1))

(defconstant-exported WAVERR_UNPREPARED (cl:+ 32 2))

(defconstant-exported WAVERR_SYNC (cl:+ 32 3))

(defconstant-exported WAVERR_LASTERROR (cl:+ 32 3))

(defconstant-exported WOM_OPEN #x3BB)

(defconstant-exported WOM_CLOSE #x3BC)

(defconstant-exported WOM_DONE #x3BD)

(defconstant-exported WIM_OPEN #x3BE)

(defconstant-exported WIM_CLOSE #x3BF)

(defconstant-exported WIM_DATA #x3C0)

(defconstant-exported WAVE_FORMAT_QUERY 1)

(defconstant-exported WAVE_ALLOWSYNC 2)

(defconstant-exported WAVE_MAPPED 4)

(defconstant-exported WAVE_FORMAT_DIRECT 8)

(defconstant-exported WAVE_FORMAT_DIRECT_QUERY (cl:logior 1 8))

(defconstant-exported WHDR_DONE 1)

(defconstant-exported WHDR_PREPARED 2)

(defconstant-exported WHDR_BEGINLOOP 4)

(defconstant-exported WHDR_ENDLOOP 8)

(defconstant-exported WHDR_INQUEUE 16)

(defconstant-exported WAVECAPS_PITCH 1)

(defconstant-exported WAVECAPS_PLAYBACKRATE 2)

(defconstant-exported WAVECAPS_VOLUME 4)

(defconstant-exported WAVECAPS_LRVOLUME 8)

(defconstant-exported WAVECAPS_SYNC 16)

(defconstant-exported WAVECAPS_SAMPLEACCURATE 32)

(defconstant-exported WAVECAPS_DIRECTSOUND 64)

(defconstant-exported WAVE_INVALIDFORMAT 0)

(defconstant-exported WAVE_FORMAT_1M08 1)

(defconstant-exported WAVE_FORMAT_1S08 2)

(defconstant-exported WAVE_FORMAT_1M16 4)

(defconstant-exported WAVE_FORMAT_1S16 8)

(defconstant-exported WAVE_FORMAT_2M08 16)

(defconstant-exported WAVE_FORMAT_2S08 32)

(defconstant-exported WAVE_FORMAT_2M16 64)

(defconstant-exported WAVE_FORMAT_2S16 128)

(defconstant-exported WAVE_FORMAT_4M08 256)

(defconstant-exported WAVE_FORMAT_4S08 512)

(defconstant-exported WAVE_FORMAT_4M16 1024)

(defconstant-exported WAVE_FORMAT_4S16 2048)

(defconstant-exported WAVE_FORMAT_PCM 1)

(defconstant-exported MIDIERR_UNPREPARED 64)

(defconstant-exported MIDIERR_STILLPLAYING (cl:+ 64 1))

(defconstant-exported MIDIERR_NOMAP (cl:+ 64 2))

(defconstant-exported MIDIERR_NOTREADY (cl:+ 64 3))

(defconstant-exported MIDIERR_NODEVICE (cl:+ 64 4))

(defconstant-exported MIDIERR_INVALIDSETUP (cl:+ 64 5))

(defconstant-exported MIDIERR_BADOPENMODE (cl:+ 64 6))

(defconstant-exported MIDIERR_DONT_CONTINUE (cl:+ 64 7))

(defconstant-exported MIDIERR_LASTERROR (cl:+ 64 7))

(defconstant-exported MIDIPATCHSIZE 128)

(defconstant-exported MIM_OPEN #x3C1)

(defconstant-exported MIM_CLOSE #x3C2)

(defconstant-exported MIM_DATA #x3C3)

(defconstant-exported MIM_LONGDATA #x3C4)

(defconstant-exported MIM_ERROR #x3C5)

(defconstant-exported MIM_LONGERROR #x3C6)

(defconstant-exported MOM_OPEN #x3C7)

(defconstant-exported MOM_CLOSE #x3C8)

(defconstant-exported MOM_DONE #x3C9)

(defconstant-exported MIM_MOREDATA #x3CC)

(defconstant-exported MOM_POSITIONCB #x3CA)

(defconstant-exported MIDI_IO_STATUS 32)

(defconstant-exported MIDI_CACHE_ALL 1)

(defconstant-exported MIDI_CACHE_BESTFIT 2)

(defconstant-exported MIDI_CACHE_QUERY 3)

(defconstant-exported MIDI_UNCACHE 4)

(defconstant-exported MOD_MIDIPORT 1)

(defconstant-exported MOD_SYNTH 2)

(defconstant-exported MOD_SQSYNTH 3)

(defconstant-exported MOD_FMSYNTH 4)

(defconstant-exported MOD_MAPPER 5)

(defconstant-exported MIDICAPS_VOLUME 1)

(defconstant-exported MIDICAPS_LRVOLUME 2)

(defconstant-exported MIDICAPS_CACHE 4)

(defconstant-exported MIDICAPS_STREAM 8)

(defconstant-exported MHDR_DONE 1)

(defconstant-exported MHDR_PREPARED 2)

(defconstant-exported MHDR_INQUEUE 4)

(defconstant-exported MHDR_ISSTRM 8)

(defconstant-exported MEVT_F_SHORT 0)

(defconstant-exported MEVT_F_LONG #x80000000)

(defconstant-exported MEVT_F_CALLBACK #x40000000)

(defconstant-exported MEVT_SHORTMSG 0)

(defconstant-exported MEVT_TEMPO 1)

(defconstant-exported MEVT_NOP 2)

(defconstant-exported MIDISTRM_ERROR -2)

(defconstant-exported MIDIPROP_SET #x80000000)

(defconstant-exported MIDIPROP_GET #x40000000)

(defconstant-exported MIDIPROP_TIMEDIV 1)

(defconstant-exported MIDIPROP_TEMPO 2)

(defconstant-exported AUXCAPS_CDAUDIO 1)

(defconstant-exported AUXCAPS_AUXIN 2)

(defconstant-exported AUXCAPS_VOLUME 1)

(defconstant-exported AUXCAPS_LRVOLUME 2)

(defconstant-exported MIXER_SHORT_NAME_CHARS 16)

(defconstant-exported MIXER_LONG_NAME_CHARS 64)

(defconstant-exported MIXERR_INVALLINE 1024)

(defconstant-exported MIXERR_INVALCONTROL (cl:+ 1024 1))

(defconstant-exported MIXERR_INVALVALUE (cl:+ 1024 2))

(defconstant-exported MIXERR_LASTERROR (cl:+ 1024 2))

(defconstant-exported MIXER_OBJECTF_HANDLE #x80000000)

(defconstant-exported MIXER_OBJECTF_MIXER 0)

(defconstant-exported MIXER_OBJECTF_HMIXER (cl:logior #x80000000 0))

(defconstant-exported MIXER_OBJECTF_WAVEOUT #x10000000)

(defconstant-exported MIXER_OBJECTF_HWAVEOUT (cl:logior #x80000000 #x10000000))

(defconstant-exported MIXER_OBJECTF_WAVEIN #x20000000)

(defconstant-exported MIXER_OBJECTF_HWAVEIN (cl:logior #x80000000 #x20000000))

(defconstant-exported MIXER_OBJECTF_MIDIOUT #x30000000)

(defconstant-exported MIXER_OBJECTF_HMIDIOUT (cl:logior #x80000000 #x30000000))

(defconstant-exported MIXER_OBJECTF_MIDIIN #x40000000)

(defconstant-exported MIXER_OBJECTF_HMIDIIN (cl:logior #x80000000 #x40000000))

(defconstant-exported MIXER_OBJECTF_AUX #x50000000)

(defconstant-exported MIXERLINE_LINEF_ACTIVE 1)

(defconstant-exported MIXERLINE_LINEF_DISCONNECTED #x8000)

(defconstant-exported MIXERLINE_LINEF_SOURCE #x80000000)

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_FIRST 0)

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_UNDEFINED 0)

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_DIGITAL (cl:+ 0 1))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_LINE (cl:+ 0 2))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_MONITOR (cl:+ 0 3))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_SPEAKERS (cl:+ 0 4))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_HEADPHONES (cl:+ 0 5))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_TELEPHONE (cl:+ 0 6))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_WAVEIN (cl:+ 0 7))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_VOICEIN (cl:+ 0 8))

(defconstant-exported MIXERLINE_COMPONENTTYPE_DST_LAST (cl:+ 0 8))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_FIRST #x1000)

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_UNDEFINED #x1000)

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_DIGITAL (cl:+ #x1000 1))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_LINE (cl:+ #x1000 2))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE (cl:+ #x1000 3))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_SYNTHESIZER (cl:+ #x1000 4))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_COMPACTDISC (cl:+ #x1000 5))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_TELEPHONE (cl:+ #x1000 6))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_PCSPEAKER (cl:+ #x1000 7))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_WAVEOUT (cl:+ #x1000 8))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_AUXILIARY (cl:+ #x1000 9))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_ANALOG (cl:+ #x1000 10))

(defconstant-exported MIXERLINE_COMPONENTTYPE_SRC_LAST (cl:+ #x1000 10))

(defconstant-exported MIXERLINE_TARGETTYPE_UNDEFINED 0)

(defconstant-exported MIXERLINE_TARGETTYPE_WAVEOUT 1)

(defconstant-exported MIXERLINE_TARGETTYPE_WAVEIN 2)

(defconstant-exported MIXERLINE_TARGETTYPE_MIDIOUT 3)

(defconstant-exported MIXERLINE_TARGETTYPE_MIDIIN 4)

(defconstant-exported MIXERLINE_TARGETTYPE_AUX 5)

(defconstant-exported MIXER_GETLINEINFOF_DESTINATION 0)

(defconstant-exported MIXER_GETLINEINFOF_SOURCE 1)

(defconstant-exported MIXER_GETLINEINFOF_LINEID 2)

(defconstant-exported MIXER_GETLINEINFOF_COMPONENTTYPE 3)

(defconstant-exported MIXER_GETLINEINFOF_TARGETTYPE 4)

(defconstant-exported MIXER_GETLINEINFOF_QUERYMASK 15)

(defconstant-exported MIXERCONTROL_CONTROLF_UNIFORM 1)

(defconstant-exported MIXERCONTROL_CONTROLF_MULTIPLE 2)

(defconstant-exported MIXERCONTROL_CONTROLF_DISABLED #x80000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_MASK #xF0000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_CUSTOM 0)

(defconstant-exported MIXERCONTROL_CT_CLASS_METER #x10000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_SWITCH #x20000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_NUMBER #x30000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_SLIDER #x40000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_FADER #x50000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_TIME #x60000000)

(defconstant-exported MIXERCONTROL_CT_CLASS_LIST #x70000000)

(defconstant-exported MIXERCONTROL_CT_SUBCLASS_MASK #xF000000)

(defconstant-exported MIXERCONTROL_CT_SC_SWITCH_BOOLEAN 0)

(defconstant-exported MIXERCONTROL_CT_SC_SWITCH_BUTTON #x1000000)

(defconstant-exported MIXERCONTROL_CT_SC_METER_POLLED 0)

(defconstant-exported MIXERCONTROL_CT_SC_TIME_MICROSECS 0)

(defconstant-exported MIXERCONTROL_CT_SC_TIME_MILLISECS #x1000000)

(defconstant-exported MIXERCONTROL_CT_SC_LIST_SINGLE 0)

(defconstant-exported MIXERCONTROL_CT_SC_LIST_MULTIPLE #x1000000)

(defconstant-exported MIXERCONTROL_CT_UNITS_MASK #xFF0000)

(defconstant-exported MIXERCONTROL_CT_UNITS_CUSTOM 0)

(defconstant-exported MIXERCONTROL_CT_UNITS_BOOLEAN #x10000)

(defconstant-exported MIXERCONTROL_CT_UNITS_SIGNED #x20000)

(defconstant-exported MIXERCONTROL_CT_UNITS_UNSIGNED #x30000)

(defconstant-exported MIXERCONTROL_CT_UNITS_DECIBELS #x40000)

(defconstant-exported MIXERCONTROL_CT_UNITS_PERCENT #x50000)

(defconstant-exported MIXERCONTROL_CONTROLTYPE_CUSTOM (cl:logior 0 0))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_BOOLEANMETER (cl:logior #x10000000 0 #x10000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_SIGNEDMETER (cl:logior #x10000000 0 #x20000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_PEAKMETER (cl:+ MIXERCONTROL_CONTROLTYPE_SIGNEDMETER 1))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_UNSIGNEDMETER (cl:logior #x10000000 0 #x30000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_BOOLEAN (cl:logior #x20000000 0 #x10000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_ONOFF (cl:+ MIXERCONTROL_CONTROLTYPE_BOOLEAN 1))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MUTE (cl:+ MIXERCONTROL_CONTROLTYPE_BOOLEAN 2))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MONO (cl:+ MIXERCONTROL_CONTROLTYPE_BOOLEAN 3))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_LOUDNESS (cl:+ MIXERCONTROL_CONTROLTYPE_BOOLEAN 4))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_STEREOENH (cl:+ MIXERCONTROL_CONTROLTYPE_BOOLEAN 5))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_BUTTON (cl:logior #x20000000 #x1000000 #x10000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_DECIBELS (cl:logior #x30000000 #x40000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_SIGNED (cl:logior #x30000000 #x20000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_UNSIGNED (cl:logior #x30000000 #x30000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_PERCENT (cl:logior #x30000000 #x50000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_SLIDER (cl:logior #x40000000 #x20000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_PAN (cl:+ MIXERCONTROL_CONTROLTYPE_SLIDER 1))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_QSOUNDPAN (cl:+ MIXERCONTROL_CONTROLTYPE_SLIDER 2))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_FADER (cl:logior #x50000000 #x30000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_VOLUME (cl:+ MIXERCONTROL_CONTROLTYPE_FADER 1))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_BASS (cl:+ MIXERCONTROL_CONTROLTYPE_FADER 2))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_TREBLE (cl:+ MIXERCONTROL_CONTROLTYPE_FADER 3))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_EQUALIZER (cl:+ MIXERCONTROL_CONTROLTYPE_FADER 4))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_SINGLESELECT (cl:logior #x70000000 0 #x10000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MUX (cl:+ MIXERCONTROL_CONTROLTYPE_SINGLESELECT 1))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MULTIPLESELECT (cl:logior #x70000000 #x1000000 #x10000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MIXER (cl:+ MIXERCONTROL_CONTROLTYPE_MULTIPLESELECT 1))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MICROTIME (cl:logior #x60000000 0 #x30000))

(defconstant-exported MIXERCONTROL_CONTROLTYPE_MILLITIME (cl:logior #x60000000 #x1000000 #x30000))

(defconstant-exported MIXER_GETLINECONTROLSF_ALL 0)

(defconstant-exported MIXER_GETLINECONTROLSF_ONEBYID 1)

(defconstant-exported MIXER_GETLINECONTROLSF_ONEBYTYPE 2)

(defconstant-exported MIXER_GETLINECONTROLSF_QUERYMASK 15)

(defconstant-exported MIXER_GETCONTROLDETAILSF_VALUE 0)

(defconstant-exported MIXER_GETCONTROLDETAILSF_LISTTEXT 1)

(defconstant-exported MIXER_GETCONTROLDETAILSF_QUERYMASK 15)

(defconstant-exported MIXER_SETCONTROLDETAILSF_VALUE 0)

(defconstant-exported MIXER_SETCONTROLDETAILSF_CUSTOM 1)

(defconstant-exported MIXER_SETCONTROLDETAILSF_QUERYMASK 15)

(defconstant-exported TIMERR_NOERROR 0)

(defconstant-exported TIMERR_NOCANDO (cl:+ 96 1))

(defconstant-exported TIMERR_STRUCT (cl:+ 96 33))

(defconstant-exported TIME_ONESHOT 0)

(defconstant-exported TIME_PERIODIC 1)

(defconstant-exported TIME_CALLBACK_FUNCTION 0)

(defconstant-exported TIME_CALLBACK_EVENT_SET 16)

(defconstant-exported TIME_CALLBACK_EVENT_PULSE 32)

(defconstant-exported JOYERR_NOERROR 0)

(defconstant-exported JOYERR_PARMS (cl:+ 160 5))

(defconstant-exported JOYERR_NOCANDO (cl:+ 160 6))

(defconstant-exported JOYERR_UNPLUGGED (cl:+ 160 7))

(defconstant-exported JOY_BUTTON1 1)

(defconstant-exported JOY_BUTTON2 2)

(defconstant-exported JOY_BUTTON3 4)

(defconstant-exported JOY_BUTTON4 8)

(defconstant-exported JOY_BUTTON1CHG 256)

(defconstant-exported JOY_BUTTON2CHG 512)

(defconstant-exported JOY_BUTTON3CHG 1024)

(defconstant-exported JOY_BUTTON4CHG 2048)

(defconstant-exported JOY_BUTTON5 257)

(defconstant-exported JOY_BUTTON6 513)

(defconstant-exported JOY_BUTTON7 1025)

(defconstant-exported JOY_BUTTON8 2049)

(defconstant-exported JOY_BUTTON9 256)

(defconstant-exported JOY_BUTTON10 512)

(defconstant-exported JOY_BUTTON11 1024)

(defconstant-exported JOY_BUTTON12 2048)

(defconstant-exported JOY_BUTTON13 4096)

(defconstant-exported JOY_BUTTON14 8192)

(defconstant-exported JOY_BUTTON15 16384)

(defconstant-exported JOY_BUTTON16 32768)

(defconstant-exported JOY_BUTTON17 65536)

(defconstant-exported JOY_BUTTON18 #x20000)

(defconstant-exported JOY_BUTTON19 #x40000)

(defconstant-exported JOY_BUTTON20 #x80000)

(defconstant-exported JOY_BUTTON21 #x100000)

(defconstant-exported JOY_BUTTON22 #x200000)

(defconstant-exported JOY_BUTTON23 #x400000)

(defconstant-exported JOY_BUTTON24 #x800000)

(defconstant-exported JOY_BUTTON25 #x1000000)

(defconstant-exported JOY_BUTTON26 #x2000000)

(defconstant-exported JOY_BUTTON27 #x4000000)

(defconstant-exported JOY_BUTTON28 #x8000000)

(defconstant-exported JOY_BUTTON29 #x10000000)

(defconstant-exported JOY_BUTTON30 #x20000000)

(defconstant-exported JOY_BUTTON31 #x40000000)

(defconstant-exported JOY_BUTTON32 #x80000000)

(defconstant-exported JOY_POVFORWARD 0)

(defconstant-exported JOY_POVRIGHT 9000)

(defconstant-exported JOY_POVBACKWARD 18000)

(defconstant-exported JOY_POVLEFT 27000)

(defconstant-exported JOY_RETURNX 1)

(defconstant-exported JOY_RETURNY 2)

(defconstant-exported JOY_RETURNZ 4)

(defconstant-exported JOY_RETURNR 8)

(defconstant-exported JOY_RETURNU 16)

(defconstant-exported JOY_RETURNV 32)

(defconstant-exported JOY_RETURNPOV 64)

(defconstant-exported JOY_RETURNBUTTONS 128)

(defconstant-exported JOY_RETURNRAWDATA 256)

(defconstant-exported JOY_RETURNPOVCTS 512)

(defconstant-exported JOY_RETURNCENTERED 1024)

(defconstant-exported JOY_USEDEADZONE 2048)

(defconstant-exported JOY_RETURNALL (cl:logior 1 2 4 8 16 32 64 128))

(defconstant-exported JOY_CAL_READALWAYS #x10000)

(defconstant-exported JOY_CAL_READXYONLY #x20000)

(defconstant-exported JOY_CAL_READ3 #x40000)

(defconstant-exported JOY_CAL_READ4 #x80000)

(defconstant-exported JOY_CAL_READXONLY #x100000)

(defconstant-exported JOY_CAL_READYONLY #x200000)

(defconstant-exported JOY_CAL_READ5 #x400000)

(defconstant-exported JOY_CAL_READ6 #x800000)

(defconstant-exported JOY_CAL_READZONLY #x1000000)

(defconstant-exported JOY_CAL_READRONLY #x2000000)

(defconstant-exported JOY_CAL_READUONLY #x4000000)

(defconstant-exported JOY_CAL_READVONLY #x8000000)

(defconstant-exported JOYSTICKID1 0)

(defconstant-exported JOYSTICKID2 1)

(defconstant-exported JOYCAPS_HASZ 1)

(defconstant-exported JOYCAPS_HASR 2)

(defconstant-exported JOYCAPS_HASU 4)

(defconstant-exported JOYCAPS_HASV 8)

(defconstant-exported JOYCAPS_HASPOV 16)

(defconstant-exported JOYCAPS_POV4DIR 32)

(defconstant-exported JOYCAPS_POVCTS 64)

(defconstant-exported MMIOERR_BASE 256)

(defconstant-exported MMIOERR_FILENOTFOUND (cl:+ 256 1))

(defconstant-exported MMIOERR_OUTOFMEMORY (cl:+ 256 2))

(defconstant-exported MMIOERR_CANNOTOPEN (cl:+ 256 3))

(defconstant-exported MMIOERR_CANNOTCLOSE (cl:+ 256 4))

(defconstant-exported MMIOERR_CANNOTREAD (cl:+ 256 5))

(defconstant-exported MMIOERR_CANNOTWRITE (cl:+ 256 6))

(defconstant-exported MMIOERR_CANNOTSEEK (cl:+ 256 7))

(defconstant-exported MMIOERR_CANNOTEXPAND (cl:+ 256 8))

(defconstant-exported MMIOERR_CHUNKNOTFOUND (cl:+ 256 9))

(defconstant-exported MMIOERR_UNBUFFERED (cl:+ 256 10))

(defconstant-exported MMIOERR_PATHNOTFOUND (cl:+ 256 11))

(defconstant-exported MMIOERR_ACCESSDENIED (cl:+ 256 12))

(defconstant-exported MMIOERR_SHARINGVIOLATION (cl:+ 256 13))

(defconstant-exported MMIOERR_NETWORKERROR (cl:+ 256 14))

(defconstant-exported MMIOERR_TOOMANYOPENFILES (cl:+ 256 15))

(defconstant-exported MMIOERR_INVALIDFILE (cl:+ 256 16))

(defconstant-exported CFSEPCHAR #\+)

(defconstant-exported MMIO_RWMODE 3)

(defconstant-exported MMIO_SHAREMODE #x70)

(defconstant-exported MMIO_CREATE #x1000)

(defconstant-exported MMIO_PARSE 256)

(defconstant-exported MMIO_DELETE 512)

(defconstant-exported MMIO_EXIST #x4000)

(defconstant-exported MMIO_ALLOCBUF #x10000)

(defconstant-exported MMIO_GETTEMP #x20000)

(defconstant-exported MMIO_DIRTY #x10000000)

(defconstant-exported MMIO_READ 0)

(defconstant-exported MMIO_WRITE 1)

(defconstant-exported MMIO_READWRITE 2)

(defconstant-exported MMIO_COMPAT 0)

(defconstant-exported MMIO_EXCLUSIVE 16)

(defconstant-exported MMIO_DENYWRITE 32)

(defconstant-exported MMIO_DENYREAD #x30)

(defconstant-exported MMIO_DENYNONE 64)

(defconstant-exported MMIO_FHOPEN 16)

(defconstant-exported MMIO_EMPTYBUF 16)

(defconstant-exported MMIO_TOUPPER 16)

(defconstant-exported MMIO_INSTALLPROC #x10000)

(defconstant-exported MMIO_GLOBALPROC #x10000000)

(defconstant-exported MMIO_REMOVEPROC #x20000)

(defconstant-exported MMIO_UNICODEPROC #x1000000)

(defconstant-exported MMIO_FINDPROC #x40000)

(defconstant-exported MMIO_FINDCHUNK 16)

(defconstant-exported MMIO_FINDRIFF 32)

(defconstant-exported MMIO_FINDLIST 64)

(defconstant-exported MMIO_CREATERIFF 32)

(defconstant-exported MMIO_CREATELIST 64)

(defconstant-exported MMIOM_READ 0)

(defconstant-exported MMIOM_WRITE 1)

(defconstant-exported MMIOM_SEEK 2)

(defconstant-exported MMIOM_OPEN 3)

(defconstant-exported MMIOM_CLOSE 4)

(defconstant-exported MMIOM_WRITEFLUSH 5)

(defconstant-exported MMIOM_RENAME 6)

(defconstant-exported MMIOM_USER #x8000)

(defconstant-exported MMIO_DEFAULTBUFFER 8192)

(defconstant-exported MCIERR_INVALID_DEVICE_ID (cl:+ 256 1))

(defconstant-exported MCIERR_UNRECOGNIZED_KEYWORD (cl:+ 256 3))

(defconstant-exported MCIERR_UNRECOGNIZED_COMMAND (cl:+ 256 5))

(defconstant-exported MCIERR_HARDWARE (cl:+ 256 6))

(defconstant-exported MCIERR_INVALID_DEVICE_NAME (cl:+ 256 7))

(defconstant-exported MCIERR_OUT_OF_MEMORY (cl:+ 256 8))

(defconstant-exported MCIERR_DEVICE_OPEN (cl:+ 256 9))

(defconstant-exported MCIERR_CANNOT_LOAD_DRIVER (cl:+ 256 10))

(defconstant-exported MCIERR_MISSING_COMMAND_STRING (cl:+ 256 11))

(defconstant-exported MCIERR_PARAM_OVERFLOW (cl:+ 256 12))

(defconstant-exported MCIERR_MISSING_STRING_ARGUMENT (cl:+ 256 13))

(defconstant-exported MCIERR_BAD_INTEGER (cl:+ 256 14))

(defconstant-exported MCIERR_PARSER_INTERNAL (cl:+ 256 15))

(defconstant-exported MCIERR_DRIVER_INTERNAL (cl:+ 256 16))

(defconstant-exported MCIERR_MISSING_PARAMETER (cl:+ 256 17))

(defconstant-exported MCIERR_UNSUPPORTED_FUNCTION (cl:+ 256 18))

(defconstant-exported MCIERR_FILE_NOT_FOUND (cl:+ 256 19))

(defconstant-exported MCIERR_DEVICE_NOT_READY (cl:+ 256 20))

(defconstant-exported MCIERR_INTERNAL (cl:+ 256 21))

(defconstant-exported MCIERR_DRIVER (cl:+ 256 22))

(defconstant-exported MCIERR_CANNOT_USE_ALL (cl:+ 256 23))

(defconstant-exported MCIERR_MULTIPLE (cl:+ 256 24))

(defconstant-exported MCIERR_EXTENSION_NOT_FOUND (cl:+ 256 25))

(defconstant-exported MCIERR_OUTOFRANGE (cl:+ 256 26))

(defconstant-exported MCIERR_FLAGS_NOT_COMPATIBLE (cl:+ 256 28))

(defconstant-exported MCIERR_FILE_NOT_SAVED (cl:+ 256 30))

(defconstant-exported MCIERR_DEVICE_TYPE_REQUIRED (cl:+ 256 31))

(defconstant-exported MCIERR_DEVICE_LOCKED (cl:+ 256 32))

(defconstant-exported MCIERR_DUPLICATE_ALIAS (cl:+ 256 33))

(defconstant-exported MCIERR_BAD_CONSTANT (cl:+ 256 34))

(defconstant-exported MCIERR_MUST_USE_SHAREABLE (cl:+ 256 35))

(defconstant-exported MCIERR_MISSING_DEVICE_NAME (cl:+ 256 36))

(defconstant-exported MCIERR_BAD_TIME_FORMAT (cl:+ 256 37))

(defconstant-exported MCIERR_NO_CLOSING_QUOTE (cl:+ 256 38))

(defconstant-exported MCIERR_DUPLICATE_FLAGS (cl:+ 256 39))

(defconstant-exported MCIERR_INVALID_FILE (cl:+ 256 40))

(defconstant-exported MCIERR_NULL_PARAMETER_BLOCK (cl:+ 256 41))

(defconstant-exported MCIERR_UNNAMED_RESOURCE (cl:+ 256 42))

(defconstant-exported MCIERR_NEW_REQUIRES_ALIAS (cl:+ 256 43))

(defconstant-exported MCIERR_NOTIFY_ON_AUTO_OPEN (cl:+ 256 44))

(defconstant-exported MCIERR_NO_ELEMENT_ALLOWED (cl:+ 256 45))

(defconstant-exported MCIERR_NONAPPLICABLE_FUNCTION (cl:+ 256 46))

(defconstant-exported MCIERR_ILLEGAL_FOR_AUTO_OPEN (cl:+ 256 47))

(defconstant-exported MCIERR_FILENAME_REQUIRED (cl:+ 256 48))

(defconstant-exported MCIERR_EXTRA_CHARACTERS (cl:+ 256 49))

(defconstant-exported MCIERR_DEVICE_NOT_INSTALLED (cl:+ 256 50))

(defconstant-exported MCIERR_GET_CD (cl:+ 256 51))

(defconstant-exported MCIERR_SET_CD (cl:+ 256 52))

(defconstant-exported MCIERR_SET_DRIVE (cl:+ 256 53))

(defconstant-exported MCIERR_DEVICE_LENGTH (cl:+ 256 54))

(defconstant-exported MCIERR_DEVICE_ORD_LENGTH (cl:+ 256 55))

(defconstant-exported MCIERR_NO_INTEGER (cl:+ 256 56))

(defconstant-exported MCIERR_WAVE_OUTPUTSINUSE (cl:+ 256 64))

(defconstant-exported MCIERR_WAVE_SETOUTPUTINUSE (cl:+ 256 65))

(defconstant-exported MCIERR_WAVE_INPUTSINUSE (cl:+ 256 66))

(defconstant-exported MCIERR_WAVE_SETINPUTINUSE (cl:+ 256 67))

(defconstant-exported MCIERR_WAVE_OUTPUTUNSPECIFIED (cl:+ 256 68))

(defconstant-exported MCIERR_WAVE_INPUTUNSPECIFIED (cl:+ 256 69))

(defconstant-exported MCIERR_WAVE_OUTPUTSUNSUITABLE (cl:+ 256 70))

(defconstant-exported MCIERR_WAVE_SETOUTPUTUNSUITABLE (cl:+ 256 71))

(defconstant-exported MCIERR_WAVE_INPUTSUNSUITABLE (cl:+ 256 72))

(defconstant-exported MCIERR_WAVE_SETINPUTUNSUITABLE (cl:+ 256 73))

(defconstant-exported MCIERR_SEQ_DIV_INCOMPATIBLE (cl:+ 256 80))

(defconstant-exported MCIERR_SEQ_PORT_INUSE (cl:+ 256 81))

(defconstant-exported MCIERR_SEQ_PORT_NONEXISTENT (cl:+ 256 82))

(defconstant-exported MCIERR_SEQ_PORT_MAPNODEVICE (cl:+ 256 83))

(defconstant-exported MCIERR_SEQ_PORT_MISCERROR (cl:+ 256 84))

(defconstant-exported MCIERR_SEQ_TIMER (cl:+ 256 85))

(defconstant-exported MCIERR_SEQ_PORTUNSPECIFIED (cl:+ 256 86))

(defconstant-exported MCIERR_SEQ_NOMIDIPRESENT (cl:+ 256 87))

(defconstant-exported MCIERR_NO_WINDOW (cl:+ 256 90))

(defconstant-exported MCIERR_CREATEWINDOW (cl:+ 256 91))

(defconstant-exported MCIERR_FILE_READ (cl:+ 256 92))

(defconstant-exported MCIERR_FILE_WRITE (cl:+ 256 93))

(defconstant-exported MCIERR_NO_IDENTITY (cl:+ 256 94))

(defconstant-exported MCIERR_CUSTOM_DRIVER_BASE (cl:+ 256 256))

(defconstant-exported MCI_FIRST #x800)

(defconstant-exported MCI_OPEN #x803)

(defconstant-exported MCI_CLOSE #x804)

(defconstant-exported MCI_ESCAPE #x805)

(defconstant-exported MCI_PLAY #x806)

(defconstant-exported MCI_SEEK #x807)

(defconstant-exported MCI_STOP #x808)

(defconstant-exported MCI_PAUSE #x809)

(defconstant-exported MCI_INFO #x80A)

(defconstant-exported MCI_GETDEVCAPS #x80B)

(defconstant-exported MCI_SPIN #x80C)

(defconstant-exported MCI_SET #x80D)

(defconstant-exported MCI_STEP #x80E)

(defconstant-exported MCI_RECORD #x80F)

(defconstant-exported MCI_SYSINFO #x810)

(defconstant-exported MCI_BREAK #x811)

(defconstant-exported MCI_SAVE #x813)

(defconstant-exported MCI_STATUS #x814)

(defconstant-exported MCI_CUE #x830)

(defconstant-exported MCI_REALIZE #x840)

(defconstant-exported MCI_WINDOW #x841)

(defconstant-exported MCI_PUT #x842)

(defconstant-exported MCI_WHERE #x843)

(defconstant-exported MCI_FREEZE #x844)

(defconstant-exported MCI_UNFREEZE #x845)

(defconstant-exported MCI_LOAD #x850)

(defconstant-exported MCI_CUT #x851)

(defconstant-exported MCI_COPY #x852)

(defconstant-exported MCI_PASTE #x853)

(defconstant-exported MCI_UPDATE #x854)

(defconstant-exported MCI_RESUME #x855)

(defconstant-exported MCI_DELETE #x856)

(defconstant-exported MCI_USER_MESSAGES (cl:+ #x800 #x400))

(defconstant-exported MCI_LAST #xFFF)

(defconstant-exported MCI_DEVTYPE_VCR 513)

(defconstant-exported MCI_DEVTYPE_VIDEODISC 514)

(defconstant-exported MCI_DEVTYPE_OVERLAY 515)

(defconstant-exported MCI_DEVTYPE_CD_AUDIO 516)

(defconstant-exported MCI_DEVTYPE_DAT 517)

(defconstant-exported MCI_DEVTYPE_SCANNER 518)

(defconstant-exported MCI_DEVTYPE_ANIMATION 519)

(defconstant-exported MCI_DEVTYPE_DIGITAL_VIDEO 520)

(defconstant-exported MCI_DEVTYPE_OTHER 521)

(defconstant-exported MCI_DEVTYPE_WAVEFORM_AUDIO 522)

(defconstant-exported MCI_DEVTYPE_SEQUENCER 523)

(defconstant-exported MCI_DEVTYPE_FIRST 513)

(defconstant-exported MCI_DEVTYPE_LAST 523)

(defconstant-exported MCI_DEVTYPE_FIRST_USER #x1000)

(defconstant-exported MCI_MODE_NOT_READY (cl:+ 512 12))

(defconstant-exported MCI_MODE_STOP (cl:+ 512 13))

(defconstant-exported MCI_MODE_PLAY (cl:+ 512 14))

(defconstant-exported MCI_MODE_RECORD (cl:+ 512 15))

(defconstant-exported MCI_MODE_SEEK (cl:+ 512 16))

(defconstant-exported MCI_MODE_PAUSE (cl:+ 512 17))

(defconstant-exported MCI_MODE_OPEN (cl:+ 512 18))

(defconstant-exported MCI_FORMAT_MILLISECONDS 0)

(defconstant-exported MCI_FORMAT_HMS 1)

(defconstant-exported MCI_FORMAT_MSF 2)

(defconstant-exported MCI_FORMAT_FRAMES 3)

(defconstant-exported MCI_FORMAT_SMPTE_24 4)

(defconstant-exported MCI_FORMAT_SMPTE_25 5)

(defconstant-exported MCI_FORMAT_SMPTE_30 6)

(defconstant-exported MCI_FORMAT_SMPTE_30DROP 7)

(defconstant-exported MCI_FORMAT_BYTES 8)

(defconstant-exported MCI_FORMAT_SAMPLES 9)

(defconstant-exported MCI_FORMAT_TMSF 10)

(defconstant-exported MCI_NOTIFY_SUCCESSFUL 1)

(defconstant-exported MCI_NOTIFY_SUPERSEDED 2)

(defconstant-exported MCI_NOTIFY_ABORTED 4)

(defconstant-exported MCI_NOTIFY_FAILURE 8)

(defconstant-exported MCI_NOTIFY 1)

(defconstant-exported MCI_WAIT 2)

(defconstant-exported MCI_FROM 4)

(defconstant-exported MCI_TO 8)

(defconstant-exported MCI_TRACK 16)

(defconstant-exported MCI_OPEN_SHAREABLE 256)

(defconstant-exported MCI_OPEN_ELEMENT 512)

(defconstant-exported MCI_OPEN_ALIAS 1024)

(defconstant-exported MCI_OPEN_ELEMENT_ID 2048)

(defconstant-exported MCI_OPEN_TYPE_ID #x1000)

(defconstant-exported MCI_OPEN_TYPE #x2000)

(defconstant-exported MCI_SEEK_TO_START 256)

(defconstant-exported MCI_SEEK_TO_END 512)

(defconstant-exported MCI_STATUS_ITEM 256)

(defconstant-exported MCI_STATUS_START 512)

(defconstant-exported MCI_STATUS_LENGTH 1)

(defconstant-exported MCI_STATUS_POSITION 2)

(defconstant-exported MCI_STATUS_NUMBER_OF_TRACKS 3)

(defconstant-exported MCI_STATUS_MODE 4)

(defconstant-exported MCI_STATUS_MEDIA_PRESENT 5)

(defconstant-exported MCI_STATUS_TIME_FORMAT 6)

(defconstant-exported MCI_STATUS_READY 7)

(defconstant-exported MCI_STATUS_CURRENT_TRACK 8)

(defconstant-exported MCI_INFO_PRODUCT 256)

(defconstant-exported MCI_INFO_FILE 512)

(defconstant-exported MCI_INFO_MEDIA_UPC 1024)

(defconstant-exported MCI_INFO_MEDIA_IDENTITY 2048)

(defconstant-exported MCI_INFO_NAME #x1000)

(defconstant-exported MCI_INFO_COPYRIGHT #x2000)

(defconstant-exported MCI_GETDEVCAPS_ITEM 256)

(defconstant-exported MCI_GETDEVCAPS_CAN_RECORD 1)

(defconstant-exported MCI_GETDEVCAPS_HAS_AUDIO 2)

(defconstant-exported MCI_GETDEVCAPS_HAS_VIDEO 3)

(defconstant-exported MCI_GETDEVCAPS_DEVICE_TYPE 4)

(defconstant-exported MCI_GETDEVCAPS_USES_FILES 5)

(defconstant-exported MCI_GETDEVCAPS_COMPOUND_DEVICE 6)

(defconstant-exported MCI_GETDEVCAPS_CAN_EJECT 7)

(defconstant-exported MCI_GETDEVCAPS_CAN_PLAY 8)

(defconstant-exported MCI_GETDEVCAPS_CAN_SAVE 9)

(defconstant-exported MCI_SYSINFO_QUANTITY 256)

(defconstant-exported MCI_SYSINFO_OPEN 512)

(defconstant-exported MCI_SYSINFO_NAME 1024)

(defconstant-exported MCI_SYSINFO_INSTALLNAME 2048)

(defconstant-exported MCI_SET_DOOR_OPEN 256)

(defconstant-exported MCI_SET_DOOR_CLOSED 512)

(defconstant-exported MCI_SET_TIME_FORMAT 1024)

(defconstant-exported MCI_SET_AUDIO 2048)

(defconstant-exported MCI_SET_VIDEO #x1000)

(defconstant-exported MCI_SET_ON #x2000)

(defconstant-exported MCI_SET_OFF #x4000)

(defconstant-exported MCI_SET_AUDIO_ALL 0)

(defconstant-exported MCI_SET_AUDIO_LEFT 1)

(defconstant-exported MCI_SET_AUDIO_RIGHT 2)

(defconstant-exported MCI_BREAK_KEY 256)

(defconstant-exported MCI_BREAK_HWND 512)

(defconstant-exported MCI_BREAK_OFF 1024)

(defconstant-exported MCI_RECORD_INSERT 256)

(defconstant-exported MCI_RECORD_OVERWRITE 512)

(defconstant-exported MCI_SAVE_FILE 256)

(defconstant-exported MCI_LOAD_FILE 256)

(defconstant-exported MCI_VD_MODE_PARK (cl:+ 1024 1))

(defconstant-exported MCI_VD_MEDIA_CLV (cl:+ 1024 2))

(defconstant-exported MCI_VD_MEDIA_CAV (cl:+ 1024 3))

(defconstant-exported MCI_VD_MEDIA_OTHER (cl:+ 1024 4))

(defconstant-exported MCI_VD_FORMAT_TRACK #x4001)

(defconstant-exported MCI_VD_PLAY_REVERSE #x10000)

(defconstant-exported MCI_VD_PLAY_FAST #x20000)

(defconstant-exported MCI_VD_PLAY_SPEED #x40000)

(defconstant-exported MCI_VD_PLAY_SCAN #x80000)

(defconstant-exported MCI_VD_PLAY_SLOW #x100000)

(defconstant-exported MCI_VD_SEEK_REVERSE #x10000)

(defconstant-exported MCI_VD_STATUS_SPEED #x4002)

(defconstant-exported MCI_VD_STATUS_FORWARD #x4003)

(defconstant-exported MCI_VD_STATUS_MEDIA_TYPE #x4004)

(defconstant-exported MCI_VD_STATUS_SIDE #x4005)

(defconstant-exported MCI_VD_STATUS_DISC_SIZE #x4006)

(defconstant-exported MCI_VD_GETDEVCAPS_CLV #x10000)

(defconstant-exported MCI_VD_GETDEVCAPS_CAV #x20000)

(defconstant-exported MCI_VD_SPIN_UP #x10000)

(defconstant-exported MCI_VD_SPIN_DOWN #x20000)

(defconstant-exported MCI_VD_GETDEVCAPS_CAN_REVERSE #x4002)

(defconstant-exported MCI_VD_GETDEVCAPS_FAST_RATE #x4003)

(defconstant-exported MCI_VD_GETDEVCAPS_SLOW_RATE #x4004)

(defconstant-exported MCI_VD_GETDEVCAPS_NORMAL_RATE #x4005)

(defconstant-exported MCI_VD_STEP_FRAMES #x10000)

(defconstant-exported MCI_VD_STEP_REVERSE #x20000)

(defconstant-exported MCI_VD_ESCAPE_STRING 256)

(defconstant-exported MCI_CDA_STATUS_TYPE_TRACK #x4001)

(defconstant-exported MCI_CDA_TRACK_AUDIO 1088)

(defconstant-exported MCI_CDA_TRACK_OTHER (cl:+ 1088 1))

(defconstant-exported MCI_WAVE_PCM 1152)

(defconstant-exported MCI_WAVE_MAPPER (cl:+ 1152 1))

(defconstant-exported MCI_WAVE_OPEN_BUFFER #x10000)

(defconstant-exported MCI_WAVE_SET_FORMATTAG #x10000)

(defconstant-exported MCI_WAVE_SET_CHANNELS #x20000)

(defconstant-exported MCI_WAVE_SET_SAMPLESPERSEC #x40000)

(defconstant-exported MCI_WAVE_SET_AVGBYTESPERSEC #x80000)

(defconstant-exported MCI_WAVE_SET_BLOCKALIGN #x100000)

(defconstant-exported MCI_WAVE_SET_BITSPERSAMPLE #x200000)

(defconstant-exported MCI_WAVE_INPUT #x400000)

(defconstant-exported MCI_WAVE_OUTPUT #x800000)

(defconstant-exported MCI_WAVE_STATUS_FORMATTAG #x4001)

(defconstant-exported MCI_WAVE_STATUS_CHANNELS #x4002)

(defconstant-exported MCI_WAVE_STATUS_SAMPLESPERSEC #x4003)

(defconstant-exported MCI_WAVE_STATUS_AVGBYTESPERSEC #x4004)

(defconstant-exported MCI_WAVE_STATUS_BLOCKALIGN #x4005)

(defconstant-exported MCI_WAVE_STATUS_BITSPERSAMPLE #x4006)

(defconstant-exported MCI_WAVE_STATUS_LEVEL #x4007)

(defconstant-exported MCI_WAVE_SET_ANYINPUT #x4000000)

(defconstant-exported MCI_WAVE_SET_ANYOUTPUT #x8000000)

(defconstant-exported MCI_WAVE_GETDEVCAPS_INPUTS #x4001)

(defconstant-exported MCI_WAVE_GETDEVCAPS_OUTPUTS #x4002)

(defconstant-exported MCI_SEQ_DIV_PPQN 1216)

(defconstant-exported MCI_SEQ_DIV_SMPTE_24 (cl:+ 1216 1))

(defconstant-exported MCI_SEQ_DIV_SMPTE_25 (cl:+ 1216 2))

(defconstant-exported MCI_SEQ_DIV_SMPTE_30DROP (cl:+ 1216 3))

(defconstant-exported MCI_SEQ_DIV_SMPTE_30 (cl:+ 1216 4))

(defconstant-exported MCI_SEQ_FORMAT_SONGPTR #x4001)

(defconstant-exported MCI_SEQ_FILE #x4002)

(defconstant-exported MCI_SEQ_MIDI #x4003)

(defconstant-exported MCI_SEQ_SMPTE #x4004)

(defconstant-exported MCI_SEQ_NONE 65533)

(defconstant-exported MCI_SEQ_MAPPER 65535)

(defconstant-exported MCI_SEQ_STATUS_TEMPO #x4002)

(defconstant-exported MCI_SEQ_STATUS_PORT #x4003)

(defconstant-exported MCI_SEQ_STATUS_SLAVE #x4007)

(defconstant-exported MCI_SEQ_STATUS_MASTER #x4008)

(defconstant-exported MCI_SEQ_STATUS_OFFSET #x4009)

(defconstant-exported MCI_SEQ_STATUS_DIVTYPE #x400A)

(defconstant-exported MCI_SEQ_STATUS_NAME #x400B)

(defconstant-exported MCI_SEQ_STATUS_COPYRIGHT #x400C)

(defconstant-exported MCI_SEQ_SET_TEMPO #x10000)

(defconstant-exported MCI_SEQ_SET_PORT #x20000)

(defconstant-exported MCI_SEQ_SET_SLAVE #x40000)

(defconstant-exported MCI_SEQ_SET_MASTER #x80000)

(defconstant-exported MCI_SEQ_SET_OFFSET #x1000000)

(defconstant-exported MCI_ANIM_OPEN_WS #x10000)

(defconstant-exported MCI_ANIM_OPEN_PARENT #x20000)

(defconstant-exported MCI_ANIM_OPEN_NOSTATIC #x40000)

(defconstant-exported MCI_ANIM_PLAY_SPEED #x10000)

(defconstant-exported MCI_ANIM_PLAY_REVERSE #x20000)

(defconstant-exported MCI_ANIM_PLAY_FAST #x40000)

(defconstant-exported MCI_ANIM_PLAY_SLOW #x80000)

(defconstant-exported MCI_ANIM_PLAY_SCAN #x100000)

(defconstant-exported MCI_ANIM_STEP_REVERSE #x10000)

(defconstant-exported MCI_ANIM_STEP_FRAMES #x20000)

(defconstant-exported MCI_ANIM_STATUS_SPEED #x4001)

(defconstant-exported MCI_ANIM_STATUS_FORWARD #x4002)

(defconstant-exported MCI_ANIM_STATUS_HWND #x4003)

(defconstant-exported MCI_ANIM_STATUS_HPAL #x4004)

(defconstant-exported MCI_ANIM_STATUS_STRETCH #x4005)

(defconstant-exported MCI_ANIM_INFO_TEXT #x10000)

(defconstant-exported MCI_ANIM_GETDEVCAPS_CAN_REVERSE #x4001)

(defconstant-exported MCI_ANIM_GETDEVCAPS_FAST_RATE #x4002)

(defconstant-exported MCI_ANIM_GETDEVCAPS_SLOW_RATE #x4003)

(defconstant-exported MCI_ANIM_GETDEVCAPS_NORMAL_RATE #x4004)

(defconstant-exported MCI_ANIM_GETDEVCAPS_PALETTES #x4006)

(defconstant-exported MCI_ANIM_GETDEVCAPS_CAN_STRETCH #x4007)

(defconstant-exported MCI_ANIM_GETDEVCAPS_MAX_WINDOWS #x4008)

(defconstant-exported MCI_ANIM_REALIZE_NORM #x10000)

(defconstant-exported MCI_ANIM_REALIZE_BKGD #x20000)

(defconstant-exported MCI_ANIM_WINDOW_HWND #x10000)

(defconstant-exported MCI_ANIM_WINDOW_STATE #x40000)

(defconstant-exported MCI_ANIM_WINDOW_TEXT #x80000)

(defconstant-exported MCI_ANIM_WINDOW_ENABLE_STRETCH #x100000)

(defconstant-exported MCI_ANIM_WINDOW_DISABLE_STRETCH #x200000)

(defconstant-exported MCI_ANIM_WINDOW_DEFAULT 0)

(defconstant-exported MCI_ANIM_RECT #x10000)

(defconstant-exported MCI_ANIM_PUT_SOURCE #x20000)

(defconstant-exported MCI_ANIM_PUT_DESTINATION #x40000)

(defconstant-exported MCI_ANIM_WHERE_SOURCE #x20000)

(defconstant-exported MCI_ANIM_WHERE_DESTINATION #x40000)

(defconstant-exported MCI_ANIM_UPDATE_HDC #x20000)

(defconstant-exported MCI_OVLY_OPEN_WS #x10000)

(defconstant-exported MCI_OVLY_OPEN_PARENT #x20000)

(defconstant-exported MCI_OVLY_STATUS_HWND #x4001)

(defconstant-exported MCI_OVLY_STATUS_STRETCH #x4002)

(defconstant-exported MCI_OVLY_INFO_TEXT #x10000)

(defconstant-exported MCI_OVLY_GETDEVCAPS_CAN_STRETCH #x4001)

(defconstant-exported MCI_OVLY_GETDEVCAPS_CAN_FREEZE #x4002)

(defconstant-exported MCI_OVLY_GETDEVCAPS_MAX_WINDOWS #x4003)

(defconstant-exported MCI_OVLY_WINDOW_HWND #x10000)

(defconstant-exported MCI_OVLY_WINDOW_STATE #x40000)

(defconstant-exported MCI_OVLY_WINDOW_TEXT #x80000)

(defconstant-exported MCI_OVLY_WINDOW_ENABLE_STRETCH #x100000)

(defconstant-exported MCI_OVLY_WINDOW_DISABLE_STRETCH #x200000)

(defconstant-exported MCI_OVLY_WINDOW_DEFAULT 0)

(defconstant-exported MCI_OVLY_RECT #x10000)

(defconstant-exported MCI_OVLY_PUT_SOURCE #x20000)

(defconstant-exported MCI_OVLY_PUT_DESTINATION #x40000)

(defconstant-exported MCI_OVLY_PUT_FRAME #x80000)

(defconstant-exported MCI_OVLY_PUT_VIDEO #x100000)

(defconstant-exported MCI_OVLY_WHERE_SOURCE #x20000)

(defconstant-exported MCI_OVLY_WHERE_DESTINATION #x40000)

(defconstant-exported MCI_OVLY_WHERE_FRAME #x80000)

(defconstant-exported MCI_OVLY_WHERE_VIDEO #x100000)

(defconstant-exported NEWTRANSPARENT 3)

(defconstant-exported QUERYROPSUPPORT 40)

(defconstant-exported SELECTDIB 41)

(defconstant-exported CAPS1 94)

(defconstant-exported C1_TRANSPARENT 1)

(defconstant-exported SEEK_SET 0)

(defconstant-exported SEEK_CUR 1)

(defconstant-exported SEEK_END 2)











(defcstructex-exported MMTIME
        (wType :unsigned-int)
        (u :pointer))







(cffi:defcunion MMTIME_u
        (ms :unsigned-long)
        (sample :unsigned-long)
        (cb :unsigned-long)
        (ticks :unsigned-long)
        (smpte :pointer)
        (midi :pointer))

(defcstructex-exported MMTIME_u_midi
        (songptrpos :unsigned-long))

(defcstructex-exported MMTIME_u_smpte
        (hour :unsigned-char)
        (min :unsigned-char)
        (sec :unsigned-char)
        (frame :unsigned-char)
        (fps :unsigned-char)
        (dummy :unsigned-char)
        (pad :pointer))

(defcstructex-exported HDRVR__
        (i :int))



(defcstructex-exported DRVCONFIGINFO
        (dwDCISize :unsigned-long)
        (lpszDCISectionName :pointer)
        (lpszDCIAliasName :pointer))







(defcstructex-exported DRVCONFIGINFOEX
        (dwDCISize :unsigned-long)
        (lpszDCISectionName :pointer)
        (lpszDCIAliasName :pointer)
        (dnDevNode :unsigned-long))















(defcstructex-exported HWAVE__
        (i :int))



(defcstructex-exported HWAVEIN__
        (i :int))



(defcstructex-exported HWAVEOUT__
        (i :int))











(defcstructex-exported WAVEHDR
        (lpData :string)
        (dwBufferLength :unsigned-long)
        (dwBytesRecorded :unsigned-long)
        (dwUser :unsigned-long)
        (dwFlags :unsigned-long)
        (dwLoops :unsigned-long)
        (lpNext :pointer)
        (reserved :unsigned-long))







(defcstructex-exported WAVEOUTCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (dwFormats :unsigned-long)
        (wChannels :unsigned-short)
        (wReserved1 :unsigned-short)
        (dwSupport :unsigned-long))







(defcstructex-exported WAVEOUTCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (dwFormats :unsigned-long)
        (wChannels :unsigned-short)
        (wReserved1 :unsigned-short)
        (dwSupport :unsigned-long))







(defcstructex-exported WAVEINCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (dwFormats :unsigned-long)
        (wChannels :unsigned-short)
        (wReserved1 :unsigned-short))







(defcstructex-exported WAVEINCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (dwFormats :unsigned-long)
        (wChannels :unsigned-short)
        (wReserved1 :unsigned-short))







(defcstructex-exported WAVEFORMAT
        (wFormatTag :unsigned-short)
        (nChannels :unsigned-short)
        (nSamplesPerSec :unsigned-long)
        (nAvgBytesPerSec :unsigned-long)
        (nBlockAlign :unsigned-short))







(defcstructex-exported PCMWAVEFORMAT
        (wf WAVEFORMAT)
        (wBitsPerSample :unsigned-short))







(defcstructex-exported WAVEFORMATEX
        (wFormatTag :unsigned-short)
        (nChannels :unsigned-short)
        (nSamplesPerSec :unsigned-long)
        (nAvgBytesPerSec :unsigned-long)
        (nBlockAlign :unsigned-short)
        (wBitsPerSample :unsigned-short)
        (cbSize :unsigned-short))









(defcstructex-exported HMIDI__
        (i :int))



(defcstructex-exported HMIDIIN__
        (i :int))



(defcstructex-exported HMIDIOUT__
        (i :int))



(defcstructex-exported HMIDISTRM__
        (i :int))























(defcstructex-exported MIDIOUTCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (wTechnology :unsigned-short)
        (wVoices :unsigned-short)
        (wNotes :unsigned-short)
        (wChannelMask :unsigned-short)
        (dwSupport :unsigned-long))







(defcstructex-exported MIDIOUTCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (wTechnology :unsigned-short)
        (wVoices :unsigned-short)
        (wNotes :unsigned-short)
        (wChannelMask :unsigned-short)
        (dwSupport :unsigned-long))







(defcstructex-exported MIDIINCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (dwSupport :unsigned-long))







(defcstructex-exported MIDIINCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (dwSupport :unsigned-long))









(defcstructex-exported MIDIHDR
        (lpData :string)
        (dwBufferLength :unsigned-long)
        (dwBytesRecorded :unsigned-long)
        (dwUser :unsigned-long)
        (dwFlags :unsigned-long)
        (lpNext :pointer)
        (reserved :unsigned-long)
        (dwOffset :unsigned-long)
        (dwReserved :pointer))







(defcstructex-exported MIDIEVENT
        (dwDeltaTime :unsigned-long)
        (dwStreamID :unsigned-long)
        (dwEvent :unsigned-long)
        (dwParms :pointer))



(defcstructex-exported MIDISTRMBUFFVER
        (dwVersion :unsigned-long)
        (dwMid :unsigned-long)
        (dwOEMVersion :unsigned-long))



(defcstructex-exported MIDIPROPTIMEDIV
        (cbStruct :unsigned-long)
        (dwTimeDiv :unsigned-long))





(defcstructex-exported MIDIPROPTEMPO
        (cbStruct :unsigned-long)
        (dwTempo :unsigned-long))





(defcstructex-exported AUXCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (wTechnology :unsigned-short)
        (wReserved1 :unsigned-short)
        (dwSupport :unsigned-long))







(defcstructex-exported AUXCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (wTechnology :unsigned-short)
        (wReserved1 :unsigned-short)
        (dwSupport :unsigned-long))







(defcstructex-exported HMIXEROBJ__
        (i :int))





(defcstructex-exported HMIXER__
        (i :int))





(defcstructex-exported MIXERCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (fdwSupport :unsigned-long)
        (cDestinations :unsigned-long))







(defcstructex-exported MIXERCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer)
        (fdwSupport :unsigned-long)
        (cDestinations :unsigned-long))







(defcstructex-exported MIXERLINEA
        (cbStruct :unsigned-long)
        (dwDestination :unsigned-long)
        (dwSource :unsigned-long)
        (dwLineID :unsigned-long)
        (fdwLine :unsigned-long)
        (dwUser :unsigned-long)
        (dwComponentType :unsigned-long)
        (cChannels :unsigned-long)
        (cConnections :unsigned-long)
        (cControls :unsigned-long)
        (szShortName :pointer)
        (szName :pointer)
        (Target :pointer))







(defcstructex-exported MIXERLINEA_Target
        (dwType :unsigned-long)
        (dwDeviceID :unsigned-long)
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer))

(defcstructex-exported MIXERLINEW
        (cbStruct :unsigned-long)
        (dwDestination :unsigned-long)
        (dwSource :unsigned-long)
        (dwLineID :unsigned-long)
        (fdwLine :unsigned-long)
        (dwUser :unsigned-long)
        (dwComponentType :unsigned-long)
        (cChannels :unsigned-long)
        (cConnections :unsigned-long)
        (cControls :unsigned-long)
        (szShortName :pointer)
        (szName :pointer)
        (Target :pointer))







(defcstructex-exported MIXERLINEW_Target
        (dwType :unsigned-long)
        (dwDeviceID :unsigned-long)
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (vDriverVersion :unsigned-int)
        (szPname :pointer))

(defcstructex-exported MIXERCONTROLA
        (cbStruct :unsigned-long)
        (dwControlID :unsigned-long)
        (dwControlType :unsigned-long)
        (fdwControl :unsigned-long)
        (cMultipleItems :unsigned-long)
        (szShortName :pointer)
        (szName :pointer)
        (Bounds :pointer)
        (Metrics :pointer))







(cffi:defcunion MIXERCONTROLA_Metrics
        (cSteps :unsigned-long)
        (cbCustomData :unsigned-long)
        (dwReserved :pointer))

(cffi:defcunion MIXERCONTROLA_Bounds
        (dwReserved :pointer)
        (s :pointer)
        (s1 :pointer))

(defcstructex-exported MIXERCONTROLA_Bounds_s1
        (dwMinimum :unsigned-long)
        (dwMaximum :unsigned-long))

(defcstructex-exported MIXERCONTROLA_Bounds_s
        (lMinimum :int32)
        (lMaximum :int32))

(defcstructex-exported MIXERCONTROLW
        (cbStruct :unsigned-long)
        (dwControlID :unsigned-long)
        (dwControlType :unsigned-long)
        (fdwControl :unsigned-long)
        (cMultipleItems :unsigned-long)
        (szShortName :pointer)
        (szName :pointer)
        (Bounds :pointer)
        (Metrics :pointer))







(cffi:defcunion MIXERCONTROLW_Metrics
        (cSteps :unsigned-long)
        (cbCustomData :unsigned-long)
        (dwReserved :pointer))

(cffi:defcunion MIXERCONTROLW_Bounds
        (dwReserved :pointer)
        (s :pointer)
        (s1 :pointer))

(defcstructex-exported MIXERCONTROLW_Bounds_s1
        (dwMinimum :unsigned-long)
        (dwMaximum :unsigned-long))

(defcstructex-exported MIXERCONTROLW_Bounds_s
        (lMinimum :int32)
        (lMaximum :int32))

(defcstructex-exported MIXERLINECONTROLSA
        (cbStruct :unsigned-long)
        (dwLineID :unsigned-long)
        (cControls :unsigned-long)
        (cbmxctrl :unsigned-long)
        (pamxctrl :pointer)
        (u :pointer))







(cffi:defcunion MIXERLINECONTROLSA_u
        (dwControlID :unsigned-long)
        (dwControlType :unsigned-long))

(defcstructex-exported MIXERLINECONTROLSW
        (cbStruct :unsigned-long)
        (dwLineID :unsigned-long)
        (cControls :unsigned-long)
        (cbmxctrl :unsigned-long)
        (pamxctrl :pointer)
        (u :pointer))







(cffi:defcunion MIXERLINECONTROLSW_u
        (dwControlID :unsigned-long)
        (dwControlType :unsigned-long))

(defcstructex-exported MIXERCONTROLDETAILS
        (cbStruct :unsigned-long)
        (dwControlID :unsigned-long)
        (cChannels :unsigned-long)
        (cbDetails :unsigned-long)
        (paDetails :pointer)
        (u :pointer))







(cffi:defcunion MIXERCONTROLDETAILS_u
        (hwndOwner :pointer)
        (cMultipleItems :unsigned-long))

(defcstructex-exported MIXERCONTROLDETAILS_LISTTEXTA
        (dwParam1 :unsigned-long)
        (dwParam2 :unsigned-long)
        (szName :pointer))







(defcstructex-exported MIXERCONTROLDETAILS_LISTTEXTW
        (dwParam1 :unsigned-long)
        (dwParam2 :unsigned-long)
        (szName :pointer))







(defcstructex-exported MIXERCONTROLDETAILS_BOOLEAN
        (fValue :int32))







(defcstructex-exported MIXERCONTROLDETAILS_SIGNED
        (lValue :int32))







(defcstructex-exported MIXERCONTROLDETAILS_UNSIGNED
        (dwValue :unsigned-long))











(defcstructex-exported TIMECAPS
        (wPeriodMin :unsigned-int)
        (wPeriodMax :unsigned-int))







(defcstructex-exported JOYCAPSA
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (szPname :pointer)
        (wXmin :unsigned-int)
        (wXmax :unsigned-int)
        (wYmin :unsigned-int)
        (wYmax :unsigned-int)
        (wZmin :unsigned-int)
        (wZmax :unsigned-int)
        (wNumButtons :unsigned-int)
        (wPeriodMin :unsigned-int)
        (wPeriodMax :unsigned-int)
        (wRmin :unsigned-int)
        (wRmax :unsigned-int)
        (wUmin :unsigned-int)
        (wUmax :unsigned-int)
        (wVmin :unsigned-int)
        (wVmax :unsigned-int)
        (wCaps :unsigned-int)
        (wMaxAxes :unsigned-int)
        (wNumAxes :unsigned-int)
        (wMaxButtons :unsigned-int)
        (szRegKey :pointer)
        (szOEMVxD :pointer))







(defcstructex-exported JOYCAPSW
        (wMid :unsigned-short)
        (wPid :unsigned-short)
        (szPname :pointer)
        (wXmin :unsigned-int)
        (wXmax :unsigned-int)
        (wYmin :unsigned-int)
        (wYmax :unsigned-int)
        (wZmin :unsigned-int)
        (wZmax :unsigned-int)
        (wNumButtons :unsigned-int)
        (wPeriodMin :unsigned-int)
        (wPeriodMax :unsigned-int)
        (wRmin :unsigned-int)
        (wRmax :unsigned-int)
        (wUmin :unsigned-int)
        (wUmax :unsigned-int)
        (wVmin :unsigned-int)
        (wVmax :unsigned-int)
        (wCaps :unsigned-int)
        (wMaxAxes :unsigned-int)
        (wNumAxes :unsigned-int)
        (wMaxButtons :unsigned-int)
        (szRegKey :pointer)
        (szOEMVxD :pointer))







(defcstructex-exported JOYINFO
        (wXpos :unsigned-int)
        (wYpos :unsigned-int)
        (wZpos :unsigned-int)
        (wButtons :unsigned-int))







(defcstructex-exported JOYINFOEX
        (dwSize :unsigned-long)
        (dwFlags :unsigned-long)
        (dwXpos :unsigned-long)
        (dwYpos :unsigned-long)
        (dwZpos :unsigned-long)
        (dwRpos :unsigned-long)
        (dwUpos :unsigned-long)
        (dwVpos :unsigned-long)
        (dwButtons :unsigned-long)
        (dwButtonNumber :unsigned-long)
        (dwPOV :unsigned-long)
        (dwReserved1 :unsigned-long)
        (dwReserved2 :unsigned-long))











(defcstructex-exported HMMIO__
        (i :int))







(defcstructex-exported MMIOINFO
        (dwFlags :unsigned-long)
        (fccIOProc :unsigned-long)
        (pIOProc :pointer)
        (wErrorRet :unsigned-int)
        (htask :pointer)
        (cchBuffer :int32)
        (pchBuffer :string)
        (pchNext :string)
        (pchEndRead :string)
        (pchEndWrite :string)
        (lBufOffset :int32)
        (lDiskOffset :int32)
        (adwInfo :pointer)
        (dwReserved1 :unsigned-long)
        (dwReserved2 :unsigned-long)
        (hmmio :pointer))









(defcstructex-exported MMCKINFO
        (ckid :unsigned-long)
        (cksize :unsigned-long)
        (fccType :unsigned-long)
        (dwDataOffset :unsigned-long)
        (dwFlags :unsigned-long))









(defcstructex-exported MCI_GENERIC_PARMS
        (dwCallback :unsigned-long))







(defcstructex-exported MCI_OPEN_PARMSA
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :string)
        (lpstrElementName :string)
        (lpstrAlias :string))







(defcstructex-exported MCI_OPEN_PARMSW
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :pointer)
        (lpstrElementName :pointer)
        (lpstrAlias :pointer))







(defcstructex-exported MCI_PLAY_PARMS
        (dwCallback :unsigned-long)
        (dwFrom :unsigned-long)
        (dwTo :unsigned-long))







(defcstructex-exported MCI_SEEK_PARMS
        (dwCallback :unsigned-long)
        (dwTo :unsigned-long))







(defcstructex-exported MCI_STATUS_PARMS
        (dwCallback :unsigned-long)
        (dwReturn :unsigned-long)
        (dwItem :unsigned-long)
        (dwTrack :unsigned-long))







(defcstructex-exported MCI_INFO_PARMSA
        (dwCallback :unsigned-long)
        (lpstrReturn :string)
        (dwRetSize :unsigned-long))





(defcstructex-exported MCI_INFO_PARMSW
        (dwCallback :unsigned-long)
        (lpstrReturn :pointer)
        (dwRetSize :unsigned-long))





(defcstructex-exported MCI_GETDEVCAPS_PARMS
        (dwCallback :unsigned-long)
        (dwReturn :unsigned-long)
        (dwItem :unsigned-long))







(defcstructex-exported MCI_SYSINFO_PARMSA
        (dwCallback :unsigned-long)
        (lpstrReturn :string)
        (dwRetSize :unsigned-long)
        (dwNumber :unsigned-long)
        (wDeviceType :unsigned-int))







(defcstructex-exported MCI_SYSINFO_PARMSW
        (dwCallback :unsigned-long)
        (lpstrReturn :pointer)
        (dwRetSize :unsigned-long)
        (dwNumber :unsigned-long)
        (wDeviceType :unsigned-int))







(defcstructex-exported MCI_SET_PARMS
        (dwCallback :unsigned-long)
        (dwTimeFormat :unsigned-long)
        (dwAudio :unsigned-long))







(defcstructex-exported MCI_BREAK_PARMS
        (dwCallback :unsigned-long)
        (nVirtKey :int)
        (hwndBreak :pointer))







(defcstructex-exported MCI_SAVE_PARMSA
        (dwCallback :unsigned-long)
        (lpfilename :string))







(defcstructex-exported MCI_SAVE_PARMSW
        (dwCallback :unsigned-long)
        (lpfilename :pointer))







(defcstructex-exported MCI_LOAD_PARMSA
        (dwCallback :unsigned-long)
        (lpfilename :string))







(defcstructex-exported MCI_LOAD_PARMSW
        (dwCallback :unsigned-long)
        (lpfilename :pointer))







(defcstructex-exported MCI_RECORD_PARMS
        (dwCallback :unsigned-long)
        (dwFrom :unsigned-long)
        (dwTo :unsigned-long))





(defcstructex-exported MCI_VD_PLAY_PARMS
        (dwCallback :unsigned-long)
        (dwFrom :unsigned-long)
        (dwTo :unsigned-long)
        (dwSpeed :unsigned-long))







(defcstructex-exported MCI_VD_STEP_PARMS
        (dwCallback :unsigned-long)
        (dwFrames :unsigned-long))







(defcstructex-exported MCI_VD_ESCAPE_PARMSA
        (dwCallback :unsigned-long)
        (lpstrCommand :string))







(defcstructex-exported MCI_VD_ESCAPE_PARMSW
        (dwCallback :unsigned-long)
        (lpstrCommand :pointer))







(defcstructex-exported MCI_WAVE_OPEN_PARMSA
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :string)
        (lpstrElementName :string)
        (lpstrAlias :string)
        (dwBufferSeconds :unsigned-long))







(defcstructex-exported MCI_WAVE_OPEN_PARMSW
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :pointer)
        (lpstrElementName :pointer)
        (lpstrAlias :pointer)
        (dwBufferSeconds :unsigned-long))







(defcstructex-exported MCI_WAVE_DELETE_PARMS
        (dwCallback :unsigned-long)
        (dwFrom :unsigned-long)
        (dwTo :unsigned-long))







(defcstructex-exported MCI_WAVE_SET_PARMS
        (dwCallback :unsigned-long)
        (dwTimeFormat :unsigned-long)
        (dwAudio :unsigned-long)
        (wInput :unsigned-int)
        (wOutput :unsigned-int)
        (wFormatTag :unsigned-short)
        (wReserved2 :unsigned-short)
        (nChannels :unsigned-short)
        (wReserved3 :unsigned-short)
        (nSamplesPerSec :unsigned-long)
        (nAvgBytesPerSec :unsigned-long)
        (nBlockAlign :unsigned-short)
        (wReserved4 :unsigned-short)
        (wBitsPerSample :unsigned-short)
        (wReserved5 :unsigned-short))







(defcfunex-exported ("CloseDriver" CloseDriver :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :int32))

(defcfunex-exported ("OpenDriver" OpenDriver :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("SendDriverMessage" SendDriverMessage :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int32)
  (arg3 :int32))

(defcfunex-exported ("DrvGetModuleHandle" DrvGetModuleHandle :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetDriverModuleHandle" GetDriverModuleHandle :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("DefDriverProc" DefDriverProc :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :int32)
  (arg4 :int32))

(defcfunex-exported ("mmsystemGetVersion" mmsystemGetVersion :convention :stdcall) :unsigned-int)

(defcfunex-exported ("sndPlaySoundA" sndPlaySoundA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-int))

(defcfunex-exported ("sndPlaySoundW" sndPlaySoundW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("PlaySoundA" PlaySoundA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("PlaySoundW" PlaySoundW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("waveOutGetNumDevs" waveOutGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("waveOutGetDevCapsA" waveOutGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutGetDevCapsW" waveOutGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutGetVolume" waveOutGetVolume :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("waveOutSetVolume" waveOutSetVolume :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("waveOutGetErrorTextA" waveOutGetErrorTextA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutGetErrorTextW" waveOutGetErrorTextW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutOpen" waveOutOpen :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long))

(defcfunex-exported ("waveOutClose" waveOutClose :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveOutPrepareHeader" waveOutPrepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutUnprepareHeader" waveOutUnprepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutWrite" waveOutWrite :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutPause" waveOutPause :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveOutRestart" waveOutRestart :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveOutReset" waveOutReset :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveOutBreakLoop" waveOutBreakLoop :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveOutGetPosition" waveOutGetPosition :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveOutGetPitch" waveOutGetPitch :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("waveOutSetPitch" waveOutSetPitch :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("waveOutGetPlaybackRate" waveOutGetPlaybackRate :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("waveOutSetPlaybackRate" waveOutSetPlaybackRate :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("waveOutGetID" waveOutGetID :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("waveOutMessage" waveOutMessage :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("waveInGetNumDevs" waveInGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("waveInGetDevCapsA" waveInGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInGetDevCapsW" waveInGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInGetErrorTextA" waveInGetErrorTextA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInGetErrorTextW" waveInGetErrorTextW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInOpen" waveInOpen :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long))

(defcfunex-exported ("waveInClose" waveInClose :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveInPrepareHeader" waveInPrepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInUnprepareHeader" waveInUnprepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInAddBuffer" waveInAddBuffer :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInStart" waveInStart :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveInStop" waveInStop :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveInReset" waveInReset :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("waveInGetPosition" waveInGetPosition :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("waveInGetID" waveInGetID :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("waveInMessage" waveInMessage :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("midiOutGetNumDevs" midiOutGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("midiStreamOpen" midiStreamOpen :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long))

(defcfunex-exported ("midiStreamClose" midiStreamClose :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiStreamProperty" midiStreamProperty :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("midiStreamPosition" midiStreamPosition :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiStreamOut" midiStreamOut :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiStreamPause" midiStreamPause :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiStreamRestart" midiStreamRestart :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiStreamStop" midiStreamStop :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiConnect" midiConnect :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("midiDisconnect" midiDisconnect :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("midiOutGetDevCapsA" midiOutGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutGetDevCapsW" midiOutGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutGetVolume" midiOutGetVolume :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("midiOutSetVolume" midiOutSetVolume :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("midiOutGetErrorTextA" midiOutGetErrorTextA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutGetErrorTextW" midiOutGetErrorTextW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutOpen" midiOutOpen :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("midiOutClose" midiOutClose :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiOutPrepareHeader" midiOutPrepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutUnprepareHeader" midiOutUnprepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutShortMsg" midiOutShortMsg :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("midiOutLongMsg" midiOutLongMsg :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiOutReset" midiOutReset :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiOutCachePatches" midiOutCachePatches :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("midiOutCacheDrumPatches" midiOutCacheDrumPatches :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("midiOutGetID" midiOutGetID :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("midiOutMessage" midiOutMessage :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("midiInGetNumDevs" midiInGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("midiInGetDevCapsA" midiInGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInGetDevCapsW" midiInGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInGetErrorTextA" midiInGetErrorTextA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInGetErrorTextW" midiInGetErrorTextW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInOpen" midiInOpen :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("midiInClose" midiInClose :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiInPrepareHeader" midiInPrepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInUnprepareHeader" midiInUnprepareHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInAddBuffer" midiInAddBuffer :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("midiInStart" midiInStart :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiInStop" midiInStop :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiInReset" midiInReset :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("midiInGetID" midiInGetID :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("midiInMessage" midiInMessage :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("auxGetNumDevs" auxGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("auxGetDevCapsA" auxGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("auxGetDevCapsW" auxGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("auxSetVolume" auxSetVolume :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-long))

(defcfunex-exported ("auxGetVolume" auxGetVolume :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("auxOutMessage" auxOutMessage :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("mixerGetNumDevs" mixerGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("mixerGetDevCapsA" mixerGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mixerGetDevCapsW" mixerGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mixerOpen" mixerOpen :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("mixerClose" mixerClose :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("mixerMessage" mixerMessage :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("mixerGetLineInfoA" mixerGetLineInfoA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerGetLineInfoW" mixerGetLineInfoW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerGetID" mixerGetID :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerGetLineControlsA" mixerGetLineControlsA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerGetLineControlsW" mixerGetLineControlsW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerGetControlDetailsA" mixerGetControlDetailsA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerGetControlDetailsW" mixerGetControlDetailsW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mixerSetControlDetails" mixerSetControlDetails :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("timeGetSystemTime" timeGetSystemTime :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("timeGetTime" timeGetTime :convention :stdcall) :unsigned-long)

(defcfunex-exported ("timeSetEvent" timeSetEvent :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-int))

(defcfunex-exported ("timeKillEvent" timeKillEvent :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int))

(defcfunex-exported ("timeGetDevCaps" timeGetDevCaps :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("timeBeginPeriod" timeBeginPeriod :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int))

(defcfunex-exported ("timeEndPeriod" timeEndPeriod :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int))

(defcfunex-exported ("joyGetNumDevs" joyGetNumDevs :convention :stdcall) :unsigned-int)

(defcfunex-exported ("joyGetDevCapsA" joyGetDevCapsA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("joyGetDevCapsW" joyGetDevCapsW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("joyGetPos" joyGetPos :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("joyGetPosEx" joyGetPosEx :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("joyGetThreshold" joyGetThreshold :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("joyReleaseCapture" joyReleaseCapture :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int))

(defcfunex-exported ("joySetCapture" joySetCapture :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int))

(defcfunex-exported ("joySetThreshold" joySetThreshold :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int))

(defcfunex-exported ("mmioStringToFOURCCA" mmioStringToFOURCCA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-int))

(defcfunex-exported ("mmioStringToFOURCCW" mmioStringToFOURCCW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("mmioInstallIOProcA" mmioInstallIOProcA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mmioInstallIOProcW" mmioInstallIOProcW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mmioOpenA" mmioOpenA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mmioOpenW" mmioOpenW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mmioRenameA" mmioRenameA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("mmioRenameW" mmioRenameW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("mmioClose" mmioClose :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("mmioRead" mmioRead :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int32))

(defcfunex-exported ("mmioWrite" mmioWrite :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int32))

(defcfunex-exported ("mmioSeek" mmioSeek :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :int))

(defcfunex-exported ("mmioGetInfo" mmioGetInfo :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mmioSetInfo" mmioSetInfo :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mmioSetBuffer" mmioSetBuffer :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int32)
  (arg3 :unsigned-int))

(defcfunex-exported ("mmioFlush" mmioFlush :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("mmioAdvance" mmioAdvance :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mmioSendMessage" mmioSendMessage :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int32)
  (arg3 :int32))

(defcfunex-exported ("mmioDescend" mmioDescend :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("mmioAscend" mmioAscend :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mmioCreateChunk" mmioCreateChunk :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mciSendCommandA" mciSendCommandA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("mciSendCommandW" mciSendCommandW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("mciSendStringA" mciSendStringA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("mciSendStringW" mciSendStringW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("mciGetDeviceIDA" mciGetDeviceIDA :convention :stdcall) :unsigned-int
  (arg0 :string))

(defcfunex-exported ("mciGetDeviceIDW" mciGetDeviceIDW :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("mciGetDeviceIDFromElementIDA" mciGetDeviceIDFromElementIDA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-long)
  (arg1 :string))

(defcfunex-exported ("mciGetDeviceIDFromElementIDW" mciGetDeviceIDFromElementIDW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("mciGetErrorStringA" mciGetErrorStringA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("mciGetErrorStringW" mciGetErrorStringW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("mciSetYieldProc" mciSetYieldProc :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("mciGetCreatorTask" mciGetCreatorTask :convention :stdcall) :pointer
  (arg0 :unsigned-int))

(defcfunex-exported ("mciGetYieldProc" mciGetYieldProc :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcstructex-exported MCI_SEQ_SET_PARMS
        (dwCallback :unsigned-long)
        (dwTimeFormat :unsigned-long)
        (dwAudio :unsigned-long)
        (dwTempo :unsigned-long)
        (dwPort :unsigned-long)
        (dwSlave :unsigned-long)
        (dwMaster :unsigned-long)
        (dwOffset :unsigned-long))







(defcstructex-exported MCI_ANIM_OPEN_PARMSA
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :string)
        (lpstrElementName :string)
        (lpstrAlias :string)
        (dwStyle :unsigned-long)
        (hWndParent :pointer))







(defcstructex-exported MCI_ANIM_OPEN_PARMSW
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :pointer)
        (lpstrElementName :pointer)
        (lpstrAlias :pointer)
        (dwStyle :unsigned-long)
        (hWndParent :pointer))







(defcstructex-exported MCI_ANIM_PLAY_PARMS
        (dwCallback :unsigned-long)
        (dwFrom :unsigned-long)
        (dwTo :unsigned-long)
        (dwSpeed :unsigned-long))







(defcstructex-exported MCI_ANIM_STEP_PARMS
        (dwCallback :unsigned-long)
        (dwFrames :unsigned-long))







(defcstructex-exported MCI_ANIM_WINDOW_PARMSA
        (dwCallback :unsigned-long)
        (hWnd :pointer)
        (nCmdShow :unsigned-int)
        (lpstrText :string))







(defcstructex-exported MCI_ANIM_WINDOW_PARMSW
        (dwCallback :unsigned-long)
        (hWnd :pointer)
        (nCmdShow :unsigned-int)
        (lpstrText :pointer))







(defcstructex-exported MCI_ANIM_RECT_PARMS
        (dwCallback :unsigned-long)
        (rc RECT))







(defcstructex-exported MCI_ANIM_UPDATE_PARMS
        (dwCallback :unsigned-long)
        (rc RECT)
        (hDC :pointer))







(defcstructex-exported MCI_OVLY_OPEN_PARMSA
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :string)
        (lpstrElementName :string)
        (lpstrAlias :string)
        (dwStyle :unsigned-long)
        (hWndParent :pointer))







(defcstructex-exported MCI_OVLY_OPEN_PARMSW
        (dwCallback :unsigned-long)
        (wDeviceID :unsigned-int)
        (lpstrDeviceType :pointer)
        (lpstrElementName :pointer)
        (lpstrAlias :pointer)
        (dwStyle :unsigned-long)
        (hWndParent :pointer))







(defcstructex-exported MCI_OVLY_WINDOW_PARMSA
        (dwCallback :unsigned-long)
        (hWnd :pointer)
        (nCmdShow :unsigned-int)
        (lpstrText :string))







(defcstructex-exported MCI_OVLY_WINDOW_PARMSW
        (dwCallback :unsigned-long)
        (hWnd :pointer)
        (nCmdShow :unsigned-int)
        (lpstrText :pointer))







(defcstructex-exported MCI_OVLY_RECT_PARMS
        (dwCallback :unsigned-long)
        (rc RECT))







(defcstructex-exported MCI_OVLY_SAVE_PARMSA
        (dwCallback :unsigned-long)
        (lpfilename :string)
        (rc RECT))







(defcstructex-exported MCI_OVLY_SAVE_PARMSW
        (dwCallback :unsigned-long)
        (lpfilename :pointer)
        (rc RECT))







(defcstructex-exported MCI_OVLY_LOAD_PARMSA
        (dwCallback :unsigned-long)
        (lpfilename :string)
        (rc RECT))


(defcstructex-exported MCI_OVLY_LOAD_PARMSW
        (dwCallback :unsigned-long)
        (lpfilename :pointer)
        (rc RECT))

