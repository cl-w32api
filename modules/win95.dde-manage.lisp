
(cl:in-package w32apimod)

(define-w32api-module win95.dde-manage :win95.dde-manage)

(cl:in-package cl-w32api.module.win95.dde-manage)

(defcfunex-exported ("DdeAbandonTransaction" DdeAbandonTransaction :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("DdeAccessData" DdeAccessData :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DdeAddData" DdeAddData :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("DdeClientTransaction" DdeClientTransaction :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-int)
  (arg5 :unsigned-int)
  (arg6 :unsigned-long)
  (arg7 :pointer))

(defcfunex-exported ("DdeCmpStringHandles" DdeCmpStringHandles :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DdeConnect" DdeConnect :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("DdeConnectList" DdeConnectList :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("DdeCreateDataHandle" DdeCreateDataHandle :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-int)
  (arg6 :unsigned-int))

(defcfunex-exported ("DdeCreateStringHandleA" DdeCreateStringHandleA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("DdeCreateStringHandleW" DdeCreateStringHandleW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("DdeDisconnect" DdeDisconnect :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DdeDisconnectList" DdeDisconnectList :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DdeEnableCallback" DdeEnableCallback :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("DdeFreeDataHandle" DdeFreeDataHandle :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DdeFreeStringHandle" DdeFreeStringHandle :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("DdeGetData" DdeGetData :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("DdeGetLastError" DdeGetLastError :convention :stdcall) :unsigned-int
  (arg0 :unsigned-long))

(defcfunex-exported ("DdeImpersonateClient" DdeImpersonateClient :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DdeInitializeA" DdeInitializeA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("DdeInitializeW" DdeInitializeW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("DdeKeepStringHandle" DdeKeepStringHandle :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("DdeNameService" DdeNameService :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("DdePostAdvise" DdePostAdvise :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DdeQueryConvInfo" DdeQueryConvInfo :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("DdeQueryNextServer" DdeQueryNextServer :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DdeQueryStringA" DdeQueryStringA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :unsigned-long)
  (arg4 :int))

(defcfunex-exported ("DdeQueryStringW" DdeQueryStringW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :int))

(defcfunex-exported ("DdeReconnect" DdeReconnect :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("DdeSetUserHandle" DdeSetUserHandle :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("DdeUnaccessData" DdeUnaccessData :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DdeUninitialize" DdeUninitialize :convention :stdcall) :int
  (arg0 :unsigned-long))
