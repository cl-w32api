
(cl:in-package w32apimod)

(define-w32api-module ime :ime)

(cl:in-package cl-w32api.module.ime)


(defconstant-exported WM_CONVERTREQUESTEX #x108)

(defconstant-exported WM_IME_STARTCOMPOSITION #x10D)

(defconstant-exported WM_IME_ENDCOMPOSITION #x10E)

(defconstant-exported WM_IME_COMPOSITION #x10F)

(defconstant-exported WM_IME_KEYLAST #x10F)

(defconstant-exported WM_IME_SETCONTEXT #x281)

(defconstant-exported WM_IME_NOTIFY #x282)

(defconstant-exported WM_IME_CONTROL #x283)

(defconstant-exported WM_IME_COMPOSITIONFULL #x284)

(defconstant-exported WM_IME_SELECT #x285)

(defconstant-exported WM_IME_CHAR #x286)

(defconstant-exported WM_IME_KEYDOWN #x290)

(defconstant-exported WM_IME_KEYUP #x291)

(defconstant-exported IMC_GETCANDIDATEPOS 7)

(defconstant-exported IMC_SETCANDIDATEPOS 8)

(defconstant-exported IMC_GETCOMPOSITIONFONT 9)

(defconstant-exported IMC_SETCOMPOSITIONFONT 10)

(defconstant-exported IMC_GETCOMPOSITIONWINDOW 11)

(defconstant-exported IMC_SETCOMPOSITIONWINDOW 12)

(defconstant-exported IMC_GETSTATUSWINDOWPOS 15)

(defconstant-exported IMC_SETSTATUSWINDOWPOS 16)

(defconstant-exported IMC_CLOSESTATUSWINDOW #x21)

(defconstant-exported IMC_OPENSTATUSWINDOW #x22)

(defconstant-exported IMN_CLOSESTATUSWINDOW 1)

(defconstant-exported IMN_OPENSTATUSWINDOW 2)

(defconstant-exported IMN_CHANGECANDIDATE 3)

(defconstant-exported IMN_CLOSECANDIDATE 4)

(defconstant-exported IMN_OPENCANDIDATE 5)

(defconstant-exported IMN_SETCONVERSIONMODE 6)

(defconstant-exported IMN_SETSENTENCEMODE 7)

(defconstant-exported IMN_SETOPENSTATUS 8)

(defconstant-exported IMN_SETCANDIDATEPOS 9)

(defconstant-exported IMN_SETCOMPOSITIONFONT 10)

(defconstant-exported IMN_SETCOMPOSITIONWINDOW 11)

(defconstant-exported IMN_SETSTATUSWINDOWPOS 12)

(defconstant-exported IMN_GUIDELINE 13)

(defconstant-exported IMN_PRIVATE 14)

(defconstant-exported NI_OPENCANDIDATE 16)

(defconstant-exported NI_CLOSECANDIDATE 17)

(defconstant-exported NI_SELECTCANDIDATESTR 18)

(defconstant-exported NI_CHANGECANDIDATELIST 19)

(defconstant-exported NI_FINALIZECONVERSIONRESULT 20)

(defconstant-exported NI_COMPOSITIONSTR 21)

(defconstant-exported NI_SETCANDIDATE_PAGESTART 22)

(defconstant-exported NI_SETCANDIDATE_PAGESIZE 23)

(defconstant-exported NI_IMEMENUSELECTED 24)

(defconstant-exported ISC_SHOWUICANDIDATEWINDOW 1)

(defconstant-exported ISC_SHOWUICOMPOSITIONWINDOW #x80000000)

(defconstant-exported ISC_SHOWUIGUIDELINE #x40000000)

(defconstant-exported ISC_SHOWUIALLCANDIDATEWINDOW 15)

(defconstant-exported ISC_SHOWUIALL #xC000000F)

(defconstant-exported CPS_COMPLETE 1)

(defconstant-exported CPS_CONVERT 2)

(defconstant-exported CPS_REVERT 3)

(defconstant-exported CPS_CANCEL 4)

(defconstant-exported IME_CHOTKEY_IME_NONIME_TOGGLE 16)

(defconstant-exported IME_CHOTKEY_SHAPE_TOGGLE 17)

(defconstant-exported IME_CHOTKEY_SYMBOL_TOGGLE 18)

(defconstant-exported IME_JHOTKEY_CLOSE_OPEN #x30)

(defconstant-exported IME_KHOTKEY_SHAPE_TOGGLE #x50)

(defconstant-exported IME_KHOTKEY_HANJACONVERT #x51)

(defconstant-exported IME_KHOTKEY_ENGLISH #x52)

(defconstant-exported IME_THOTKEY_IME_NONIME_TOGGLE #x70)

(defconstant-exported IME_THOTKEY_SHAPE_TOGGLE #x71)

(defconstant-exported IME_THOTKEY_SYMBOL_TOGGLE #x72)

(defconstant-exported IME_HOTKEY_DSWITCH_FIRST 256)

(defconstant-exported IME_HOTKEY_DSWITCH_LAST #x11F)

(defconstant-exported IME_ITHOTKEY_RESEND_RESULTSTR 512)

(defconstant-exported IME_ITHOTKEY_PREVIOUS_COMPOSITION 513)

(defconstant-exported IME_ITHOTKEY_UISTYLE_TOGGLE 514)

(defconstant-exported GCS_COMPREADSTR 1)

(defconstant-exported GCS_COMPREADATTR 2)

(defconstant-exported GCS_COMPREADCLAUSE 4)

(defconstant-exported GCS_COMPSTR 8)

(defconstant-exported GCS_COMPATTR 16)

(defconstant-exported GCS_COMPCLAUSE 32)

(defconstant-exported GCS_CURSORPOS 128)

(defconstant-exported GCS_DELTASTART 256)

(defconstant-exported GCS_RESULTREADSTR 512)

(defconstant-exported GCS_RESULTREADCLAUSE 1024)

(defconstant-exported GCS_RESULTSTR 2048)

(defconstant-exported GCS_RESULTCLAUSE 4096)

(defconstant-exported CS_INSERTCHAR #x2000)

(defconstant-exported CS_NOMOVECARET #x4000)

(defconstant-exported IMEVER_0310 #x3000A)

(defconstant-exported IMEVER_0400 #x40000)

(defconstant-exported IME_PROP_AT_CARET #x10000)

(defconstant-exported IME_PROP_SPECIAL_UI #x20000)

(defconstant-exported IME_PROP_CANDLIST_START_FROM_1 #x40000)

(defconstant-exported IME_PROP_UNICODE #x80000)

(defconstant-exported UI_CAP_2700 1)

(defconstant-exported UI_CAP_ROT90 2)

(defconstant-exported UI_CAP_ROTANY 4)

(defconstant-exported SCS_CAP_COMPSTR 1)

(defconstant-exported SCS_CAP_MAKEREAD 2)

(defconstant-exported SELECT_CAP_CONVERSION 1)

(defconstant-exported SELECT_CAP_SENTENCE 2)

(defconstant-exported GGL_LEVEL 1)

(defconstant-exported GGL_INDEX 2)

(defconstant-exported GGL_STRING 3)

(defconstant-exported GGL_PRIVATE 4)

(defconstant-exported GL_LEVEL_NOGUIDELINE 0)

(defconstant-exported GL_LEVEL_FATAL 1)

(defconstant-exported GL_LEVEL_ERROR 2)

(defconstant-exported GL_LEVEL_WARNING 3)

(defconstant-exported GL_LEVEL_INFORMATION 4)

(defconstant-exported GL_ID_UNKNOWN 0)

(defconstant-exported GL_ID_NOMODULE 1)

(defconstant-exported GL_ID_NODICTIONARY 16)

(defconstant-exported GL_ID_CANNOTSAVE 17)

(defconstant-exported GL_ID_NOCONVERT 32)

(defconstant-exported GL_ID_TYPINGERROR 33)

(defconstant-exported GL_ID_TOOMANYSTROKE 34)

(defconstant-exported GL_ID_READINGCONFLICT 35)

(defconstant-exported GL_ID_INPUTREADING 36)

(defconstant-exported GL_ID_INPUTRADICAL 37)

(defconstant-exported GL_ID_INPUTCODE 38)

(defconstant-exported GL_ID_INPUTSYMBOL 39)

(defconstant-exported GL_ID_CHOOSECANDIDATE 40)

(defconstant-exported GL_ID_REVERSECONVERSION 41)

(defconstant-exported GL_ID_PRIVATE_FIRST #x8000)

(defconstant-exported GL_ID_PRIVATE_LAST #xFFFF)

(defconstant-exported IGP_PROPERTY 4)

(defconstant-exported IGP_CONVERSION 8)

(defconstant-exported IGP_SENTENCE 12)

(defconstant-exported IGP_UI 16)

(defconstant-exported IGP_SETCOMPSTR #x14)

(defconstant-exported IGP_SELECT #x18)

(defconstant-exported SCS_SETSTR (cl:logior 1 8))

(defconstant-exported SCS_CHANGEATTR (cl:logior 2 16))

(defconstant-exported SCS_CHANGECLAUSE (cl:logior 4 32))

(defconstant-exported ATTR_INPUT 0)

(defconstant-exported ATTR_TARGET_CONVERTED 1)

(defconstant-exported ATTR_CONVERTED 2)

(defconstant-exported ATTR_TARGET_NOTCONVERTED 3)

(defconstant-exported ATTR_INPUT_ERROR 4)

(defconstant-exported ATTR_FIXEDCONVERTED 5)

(defconstant-exported CFS_DEFAULT 0)

(defconstant-exported CFS_RECT 1)

(defconstant-exported CFS_POINT 2)

(defconstant-exported CFS_SCREEN 4)

(defconstant-exported CFS_FORCE_POSITION 32)

(defconstant-exported CFS_CANDIDATEPOS 64)

(defconstant-exported CFS_EXCLUDE 128)

(defconstant-exported GCL_CONVERSION 1)

(defconstant-exported GCL_REVERSECONVERSION 2)

(defconstant-exported GCL_REVERSE_LENGTH 3)

(defconstant-exported IME_CMODE_ALPHANUMERIC 0)

(defconstant-exported IME_CMODE_NATIVE 1)

(defconstant-exported IME_CMODE_CHINESE 1)

(defconstant-exported IME_CMODE_HANGEUL 1)

(defconstant-exported IME_CMODE_HANGUL 1)

(defconstant-exported IME_CMODE_JAPANESE 1)

(defconstant-exported IME_CMODE_KATAKANA 2)

(defconstant-exported IME_CMODE_LANGUAGE 3)

(defconstant-exported IME_CMODE_FULLSHAPE 8)

(defconstant-exported IME_CMODE_ROMAN 16)

(defconstant-exported IME_CMODE_CHARCODE 32)

(defconstant-exported IME_CMODE_HANJACONVERT 64)

(defconstant-exported IME_CMODE_SOFTKBD 128)

(defconstant-exported IME_CMODE_NOCONVERSION 256)

(defconstant-exported IME_CMODE_EUDC 512)

(defconstant-exported IME_CMODE_SYMBOL 1024)

(defconstant-exported IME_CMODE_FIXED 2048)

(defconstant-exported IME_SMODE_NONE 0)

(defconstant-exported IME_SMODE_PLAURALCLAUSE 1)

(defconstant-exported IME_SMODE_SINGLECONVERT 2)

(defconstant-exported IME_SMODE_AUTOMATIC 4)

(defconstant-exported IME_SMODE_PHRASEPREDICT 8)

(defconstant-exported IME_CAND_UNKNOWN 0)

(defconstant-exported IME_CAND_READ 1)

(defconstant-exported IME_CAND_CODE 2)

(defconstant-exported IME_CAND_MEANING 3)

(defconstant-exported IME_CAND_RADICAL 4)

(defconstant-exported IME_CAND_STROKE 5)

(defconstant-exported IMM_ERROR_NODATA -1)

(defconstant-exported IMM_ERROR_GENERAL -2)

(defconstant-exported IME_CONFIG_GENERAL 1)

(defconstant-exported IME_CONFIG_REGISTERWORD 2)

(defconstant-exported IME_CONFIG_SELECTDICTIONARY 3)

(defconstant-exported IME_ESC_QUERY_SUPPORT 3)

(defconstant-exported IME_ESC_RESERVED_FIRST 4)

(defconstant-exported IME_ESC_RESERVED_LAST #x7FF)

(defconstant-exported IME_ESC_PRIVATE_FIRST #x800)

(defconstant-exported IME_ESC_PRIVATE_LAST #xFFF)

(defconstant-exported IME_ESC_SEQUENCE_TO_INTERNAL #x1001)

(defconstant-exported IME_ESC_GET_EUDC_DICTIONARY #x1003)

(defconstant-exported IME_ESC_SET_EUDC_DICTIONARY #x1004)

(defconstant-exported IME_ESC_MAX_KEY #x1005)

(defconstant-exported IME_ESC_IME_NAME #x1006)

(defconstant-exported IME_ESC_SYNC_HOTKEY #x1007)

(defconstant-exported IME_ESC_HANJA_MODE #x1008)

(defconstant-exported IME_ESC_AUTOMATA #x1009)

(defconstant-exported IME_REGWORD_STYLE_EUDC 1)

(defconstant-exported IME_REGWORD_STYLE_USER_FIRST #x80000000)

(defconstant-exported IME_REGWORD_STYLE_USER_LAST #xFFFFFFFF)

(defconstant-exported SOFTKEYBOARD_TYPE_T1 1)

(defconstant-exported SOFTKEYBOARD_TYPE_C1 2)

(defconstant-exported IMEMENUITEM_STRING_SIZE 80)

(defconstant-exported IACE_CHILDREN 1)

(defconstant-exported IACE_DEFAULT 16)

(defconstant-exported IACE_IGNORENOCONTEXT 32)

(defconstant-exported IGIMIF_RIGHTMENU 1)

(defconstant-exported IGIMII_CMODE 1)

(defconstant-exported IGIMII_SMODE 2)

(defconstant-exported IGIMII_CONFIGURE 4)

(defconstant-exported IGIMII_TOOLS 8)

(defconstant-exported IGIMII_HELP 16)

(defconstant-exported IGIMII_OTHER 32)

(defconstant-exported IGIMII_INPUTTOOLS 64)

(defconstant-exported IMFT_RADIOCHECK 1)

(defconstant-exported IMFT_SEPARATOR 2)

(defconstant-exported IMFT_SUBMENU 4)

(defconstant-exported IMFS_GRAYED 3)

(defconstant-exported IMFS_DISABLED 3)

(defconstant-exported IMFS_CHECKED 8)

(defconstant-exported IMFS_HILITE 128)

(defconstant-exported IMFS_ENABLED 0)

(defconstant-exported IMFS_UNCHECKED 0)

(defconstant-exported IMFS_UNHILITE 0)

(defconstant-exported IMFS_DEFAULT 4096)

(defconstant-exported STYLE_DESCRIPTION_SIZE 32)







(defcstructex-exported COMPOSITIONFORM
        (dwStyle :unsigned-long)
        (ptCurrentPos (:inline POINT))
        (rcArea (:inline RECT)))







(defcstructex-exported CANDIDATEFORM
        (dwIndex :unsigned-long)
        (dwStyle :unsigned-long)
        (ptCurrentPos (:inline POINT))
        (rcArea (:inline RECT)))







(defcstructex-exported CANDIDATELIST
        (dwSize :unsigned-long)
        (dwStyle :unsigned-long)
        (dwCount :unsigned-long)
        (dwSelection :unsigned-long)
        (dwPageStart :unsigned-long)
        (dwPageSize :unsigned-long)
        (dwOffset :pointer))







(defcstructex-exported REGISTERWORDA
        (lpReading :string)
        (lpWord :string))







(defcstructex-exported REGISTERWORDW
        (lpReading :pointer)
        (lpWord :pointer))







(defcstructex-exported STYLEBUFA
        (dwStyle :unsigned-long)
        (szDescription :pointer))







(defcstructex-exported STYLEBUFW
        (dwStyle :unsigned-long)
        (szDescription :pointer))







(defcstructex-exported IMEMENUITEMINFOA
        (cbSize :unsigned-int)
        (fType :unsigned-int)
        (fState :unsigned-int)
        (wID :unsigned-int)
        (hbmpChecked :pointer)
        (hbmpUnchecked :pointer)
        (dwItemData :unsigned-long)
        (szString :pointer)
        (hbmpItem :pointer))







(defcstructex-exported IMEMENUITEMINFOW
        (cbSize :unsigned-int)
        (fType :unsigned-int)
        (fState :unsigned-int)
        (wID :unsigned-int)
        (hbmpChecked :pointer)
        (hbmpUnchecked :pointer)
        (dwItemData :unsigned-long)
        (szString :pointer)
        (hbmpItem :pointer))





























(defcfunex-exported ("ImmInstallIMEA" ImmInstallIMEA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("ImmInstallIMEW" ImmInstallIMEW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ImmGetDefaultIMEWnd" ImmGetDefaultIMEWnd :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("ImmGetDescriptionA" ImmGetDescriptionA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("ImmGetDescriptionW" ImmGetDescriptionW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("ImmGetIMEFileNameA" ImmGetIMEFileNameA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("ImmGetIMEFileNameW" ImmGetIMEFileNameW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("ImmGetProperty" ImmGetProperty :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ImmIsIME" ImmIsIME :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ImmSimulateHotKey" ImmSimulateHotKey :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ImmCreateContext" ImmCreateContext :convention :stdcall) :unsigned-long)

(defcfunex-exported ("ImmDestroyContext" ImmDestroyContext :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("ImmGetContext" ImmGetContext :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("ImmReleaseContext" ImmReleaseContext :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ImmAssociateContext" ImmAssociateContext :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ImmGetCompositionStringA" ImmGetCompositionStringA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmGetCompositionStringW" ImmGetCompositionStringW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmSetCompositionStringA" ImmSetCompositionStringA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("ImmSetCompositionStringW" ImmSetCompositionStringW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("ImmGetCandidateListCountA" ImmGetCandidateListCountA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmGetCandidateListCountW" ImmGetCandidateListCountW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmGetCandidateListA" ImmGetCandidateListA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmGetCandidateListW" ImmGetCandidateListW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmGetGuideLineA" ImmGetGuideLineA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmGetGuideLineW" ImmGetGuideLineW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmGetConversionStatus" ImmGetConversionStatus :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ImmSetConversionStatus" ImmSetConversionStatus :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("ImmGetOpenStatus" ImmGetOpenStatus :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("ImmSetOpenStatus" ImmSetOpenStatus :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :int))

(defcfunex-exported ("ImmGetCompositionFontA" ImmGetCompositionFontA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmGetCompositionFontW" ImmGetCompositionFontW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmSetCompositionFontA" ImmSetCompositionFontA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmSetCompositionFontW" ImmSetCompositionFontW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmConfigureIMEA" ImmConfigureIMEA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ImmConfigureIMEW" ImmConfigureIMEW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ImmEscapeA" ImmEscapeA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("ImmEscapeW" ImmEscapeW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("ImmGetConversionListA" ImmGetConversionListA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-int))

(defcfunex-exported ("ImmGetConversionListW" ImmGetConversionListW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-int))

(defcfunex-exported ("ImmNotifyIME" ImmNotifyIME :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long))

(defcfunex-exported ("ImmGetStatusWindowPos" ImmGetStatusWindowPos :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmSetStatusWindowPos" ImmSetStatusWindowPos :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmGetCompositionWindow" ImmGetCompositionWindow :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmSetCompositionWindow" ImmSetCompositionWindow :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmGetCandidateWindow" ImmGetCandidateWindow :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("ImmSetCandidateWindow" ImmSetCandidateWindow :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("ImmIsUIMessageA" ImmIsUIMessageA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("ImmIsUIMessageW" ImmIsUIMessageW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("ImmGetVirtualKey" ImmGetVirtualKey :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("ImmRegisterWordA" ImmRegisterWordA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :string))

(defcfunex-exported ("ImmRegisterWordW" ImmRegisterWordW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ImmUnregisterWordA" ImmUnregisterWordA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :string))

(defcfunex-exported ("ImmUnregisterWordW" ImmUnregisterWordW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ImmGetRegisterWordStyleA" ImmGetRegisterWordStyleA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("ImmGetRegisterWordStyleW" ImmGetRegisterWordStyleW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("ImmEnumRegisterWordA" ImmEnumRegisterWordA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :unsigned-long)
  (arg4 :string)
  (arg5 :pointer))

(defcfunex-exported ("ImmEnumRegisterWordW" ImmEnumRegisterWordW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnableEUDC" EnableEUDC :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("ImmDisableIME" ImmDisableIME :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("ImmGetImeMenuItemsA" ImmGetImeMenuItemsA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("ImmGetImeMenuItemsW" ImmGetImeMenuItemsW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))
