
(cl:in-package w32apimod)

(define-w32api-module win95.console :win95.console)

(cl:in-package cl-w32api.module.win95.console)


(defcfunex-exported ("AllocConsole" AllocConsole :convention :stdcall) :int)

(defcfunex-exported ("CreateConsoleScreenBuffer" CreateConsoleScreenBuffer :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("FillConsoleOutputAttribute" FillConsoleOutputAttribute :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-short)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("FillConsoleOutputCharacterA" FillConsoleOutputCharacterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :char)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("FillConsoleOutputCharacterW" FillConsoleOutputCharacterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("FlushConsoleInputBuffer" FlushConsoleInputBuffer :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FreeConsole" FreeConsole :convention :stdcall) :int)

(defcfunex-exported ("GenerateConsoleCtrlEvent" GenerateConsoleCtrlEvent :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetConsoleCP" GetConsoleCP :convention :stdcall) :unsigned-int)

(defcfunex-exported ("GetConsoleCursorInfo" GetConsoleCursorInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetConsoleMode" GetConsoleMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetConsoleOutputCP" GetConsoleOutputCP :convention :stdcall) :unsigned-int)

(defcfunex-exported ("GetConsoleScreenBufferInfo" GetConsoleScreenBufferInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetConsoleTitleA" GetConsoleTitleA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetConsoleTitleW" GetConsoleTitleW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetLargestConsoleWindowSize" GetLargestConsoleWindowSize :convention :stdcall) COORD
  (arg0 :pointer))

(defcfunex-exported ("GetNumberOfConsoleInputEvents" GetNumberOfConsoleInputEvents :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetNumberOfConsoleMouseButtons" GetNumberOfConsoleMouseButtons :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetStdHandle" GetStdHandle :convention :stdcall) :pointer
  (arg0 :unsigned-long))

(defcfunex-exported ("PeekConsoleInputA" PeekConsoleInputA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("PeekConsoleInputW" PeekConsoleInputW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ReadConsoleA" ReadConsoleA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("ReadConsoleW" ReadConsoleW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("ReadConsoleInputA" ReadConsoleInputA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ReadConsoleInputW" ReadConsoleInputW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ReadConsoleOutputAttribute" ReadConsoleOutputAttribute :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("ReadConsoleOutputCharacterA" ReadConsoleOutputCharacterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("ReadConsoleOutputCharacterW" ReadConsoleOutputCharacterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("ReadConsoleOutputA" ReadConsoleOutputA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 COORD)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("ReadConsoleOutputW" ReadConsoleOutputW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 COORD)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("ScrollConsoleScreenBufferA" ScrollConsoleScreenBufferA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("ScrollConsoleScreenBufferW" ScrollConsoleScreenBufferW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("SetConsoleActiveScreenBuffer" SetConsoleActiveScreenBuffer :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetConsoleCP" SetConsoleCP :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("SetConsoleCtrlHandler" SetConsoleCtrlHandler :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetConsoleCursorInfo" SetConsoleCursorInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetConsoleCursorPosition" SetConsoleCursorPosition :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 COORD))

(defcfunex-exported ("SetConsoleMode" SetConsoleMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetConsoleOutputCP" SetConsoleOutputCP :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("SetConsoleScreenBufferSize" SetConsoleScreenBufferSize :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 COORD))

(defcfunex-exported ("SetConsoleTextAttribute" SetConsoleTextAttribute :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-short))

(defcfunex-exported ("SetConsoleTitleA" SetConsoleTitleA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("SetConsoleTitleW" SetConsoleTitleW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetConsoleWindowInfo" SetConsoleWindowInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("SetStdHandle" SetStdHandle :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("WriteConsoleA" WriteConsoleA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WriteConsoleW" WriteConsoleW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WriteConsoleInputA" WriteConsoleInputA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("WriteConsoleInputW" WriteConsoleInputW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("WriteConsoleOutputA" WriteConsoleOutputA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 COORD)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("WriteConsoleOutputW" WriteConsoleOutputW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 COORD)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("WriteConsoleOutputAttribute" WriteConsoleOutputAttribute :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("WriteConsoleOutputCharacterA" WriteConsoleOutputCharacterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

(defcfunex-exported ("WriteConsoleOutputCharacterW" WriteConsoleOutputCharacterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 COORD)
  (arg4 :pointer))

