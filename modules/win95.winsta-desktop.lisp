
(cl:in-package w32apimod)

(define-w32api-module win95.winsta-desktop :win95.winsta-desktop)

(cl:in-package cl-w32api.module.win95.winsta-desktop)

(defcfunex-exported ("CloseDesktop" CloseDesktop :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CloseWindowStation" CloseWindowStation :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CreateDesktopA" CreateDesktopA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("CreateDesktopW" CreateDesktopW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("CreateWindowStationA" CreateWindowStationA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("CreateWindowStationW" CreateWindowStationW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("EnumDesktopsA" EnumDesktopsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumDesktopsW" EnumDesktopsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumDesktopWindows" EnumDesktopWindows :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumWindowStationsA" EnumWindowStationsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int32))

(defcfunex-exported ("EnumWindowStationsW" EnumWindowStationsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int32))

(defcfunex-exported ("GetProcessWindowStation" GetProcessWindowStation :convention :stdcall) :pointer)

(defcfunex-exported ("GetThreadDesktop" GetThreadDesktop :convention :stdcall) :pointer
  (arg0 :unsigned-long))

(defcfunex-exported ("GetUserObjectInformationA" GetUserObjectInformationA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetUserObjectInformationW" GetUserObjectInformationW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetUserObjectSecurity" GetUserObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("OpenDesktopA" OpenDesktopA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("OpenDesktopW" OpenDesktopW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("OpenInputDesktop" OpenInputDesktop :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("OpenWindowStationA" OpenWindowStationA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("OpenWindowStationW" OpenWindowStationW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("SetProcessWindowStation" SetProcessWindowStation :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetThreadDesktop" SetThreadDesktop :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetUserObjectInformationA" SetUserObjectInformationA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetUserObjectInformationW" SetUserObjectInformationW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetUserObjectSecurity" SetUserObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("SwitchDesktop" SwitchDesktop :convention :stdcall) :int
  (arg0 :pointer))

