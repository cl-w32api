
(cl:in-package w32apimod)

(define-w32api-module win95.sysinfo :win95.sysinfo)

(cl:in-package cl-w32api.module.win95.sysinfo)

(defcfunex-exported ("ExpandEnvironmentStringsA" ExpandEnvironmentStringsA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("ExpandEnvironmentStringsW" ExpandEnvironmentStringsW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetComputerNameA" GetComputerNameA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("GetComputerNameW" GetComputerNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCurrentHwProfileA" GetCurrentHwProfileA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetCurrentHwProfileW" GetCurrentHwProfileW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetKeyboardType" GetKeyboardType :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("GetSysColor" GetSysColor :convention :stdcall) :unsigned-long
  (arg0 :int))

(defcfunex-exported ("GetSystemDirectoryA" GetSystemDirectoryA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetSystemDirectoryW" GetSystemDirectoryW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetSystemInfo" GetSystemInfo :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetSystemMetrics" GetSystemMetrics :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("GetUserNameA" GetUserNameA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("GetUserNameW" GetUserNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetVersionExA" GetVersionExA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetVersionExW" GetVersionExW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetWindowsDirectoryA" GetWindowsDirectoryA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetWindowsDirectoryW" GetWindowsDirectoryW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("IsProcessorFeaturePresent" IsProcessorFeaturePresent :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("SetComputerNameA" SetComputerNameA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("SetComputerNameW" SetComputerNameW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetSysColors" SetSysColors :convention :stdcall) :int
  (arg0 :int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("SystemParametersInfoA" SystemParametersInfoA :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("SystemParametersInfoW" SystemParametersInfoW :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int))


;;obsolete

(defcfunex-exported ("GetVersion" GetVersion :convention :stdcall) :unsigned-long)

