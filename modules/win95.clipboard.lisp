
(cl:in-package w32apimod)

(define-w32api-module win95.clipboard :win95.clipboard)

(cl:in-package cl-w32api.module.win95.clipboard)

(defcfunex-exported ("ChangeClipboardChain" ChangeClipboardChain :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CloseClipboard" CloseClipboard :convention :stdcall) :int)

(defcfunex-exported ("CountClipboardFormats" CountClipboardFormats :convention :stdcall) :int)

(defcfunex-exported ("EmptyClipboard" EmptyClipboard :convention :stdcall) :int)

(defcfunex-exported ("EnumClipboardFormats" EnumClipboardFormats :convention :stdcall) :unsigned-int
  (arg0 :unsigned-int))

(defcfunex-exported ("GetClipboardData" GetClipboardData :convention :stdcall) :pointer
  (arg0 :unsigned-int))

(defcfunex-exported ("GetClipboardFormatNameA" GetClipboardFormatNameA :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("GetClipboardFormatNameW" GetClipboardFormatNameW :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetClipboardOwner" GetClipboardOwner :convention :stdcall) :pointer)

(defcfunex-exported ("GetClipboardViewer" GetClipboardViewer :convention :stdcall) :pointer)

(defcfunex-exported ("GetOpenClipboardWindow" GetOpenClipboardWindow :convention :stdcall) :pointer)

(defcfunex-exported ("GetPriorityClipboardFormat" GetPriorityClipboardFormat :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("IsClipboardFormatAvailable" IsClipboardFormatAvailable :convention :stdcall) :int
  (arg0 :unsigned-int))

(defcfunex-exported ("OpenClipboard" OpenClipboard :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("RegisterClipboardFormatA" RegisterClipboardFormatA :convention :stdcall) :unsigned-int
  (arg0 :string))

(defcfunex-exported ("RegisterClipboardFormatW" RegisterClipboardFormatW :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("SetClipboardData" SetClipboardData :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("SetClipboardViewer" SetClipboardViewer :convention :stdcall) :pointer
  (arg0 :pointer))

