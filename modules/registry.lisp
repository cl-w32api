(cl:in-package w32apimod)

(define-w32api-module shutdown :shutdown)

(cl:in-package cl-w32api.module.shutdown)

(defconstant-exported REG_CREATED_NEW_KEY 1)

(defconstant-exported REG_OPENED_EXISTING_KEY 2)

(defconstant-exported REG_NONE 0)

(defconstant-exported REG_SZ 1)

(defconstant-exported REG_EXPAND_SZ 2)

(defconstant-exported REG_BINARY 3)

(defconstant-exported REG_DWORD_LITTLE_ENDIAN 4)

(defconstant-exported REG_DWORD 4)

(defconstant-exported REG_DWORD_BIG_ENDIAN 5)

(defconstant-exported REG_LINK 6)

(defconstant-exported REG_MULTI_SZ 7)

(defconstant-exported REG_RESOURCE_LIST 8)

(defconstant-exported REG_FULL_RESOURCE_DESCRIPTOR 9)

(defconstant-exported REG_RESOURCE_REQUIREMENTS_LIST 10)

(defconstant-exported REG_QWORD_LITTLE_ENDIAN 11)

(defconstant-exported REG_QWORD 11)

(defconstant-exported REG_NOTIFY_CHANGE_NAME 1)

(defconstant-exported REG_NOTIFY_CHANGE_ATTRIBUTES 2)

(defconstant-exported REG_NOTIFY_CHANGE_LAST_SET 4)

(defconstant-exported REG_NOTIFY_CHANGE_SECURITY 8)



(defcstructex-exported VALENTA
        (ve_valuename :string)
        (ve_valuelen :unsigned-long)
        (ve_valueptr :unsigned-long)
        (ve_type :unsigned-long))





(defcstructex-exported VALENTW
        (ve_valuename :pointer)
        (ve_valuelen :unsigned-long)
        (ve_valueptr :unsigned-long)
        (ve_type :unsigned-long))



(defcfunex-exported ("RegCloseKey" RegCloseKey :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RegConnectRegistryA" RegConnectRegistryA :convention :stdcall) :int32
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegConnectRegistryW" RegConnectRegistryW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegCreateKeyA" RegCreateKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("RegCreateKeyExA" RegCreateKeyExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :string)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer))

(defcfunex-exported ("RegCreateKeyExW" RegCreateKeyExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer))

(defcfunex-exported ("RegCreateKeyW" RegCreateKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegDeleteKeyA" RegDeleteKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("RegDeleteKeyW" RegDeleteKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RegDeleteValueA" RegDeleteValueA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("RegDeleteValueW" RegDeleteValueW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RegEnumKeyA" RegEnumKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :unsigned-long))

(defcfunex-exported ("RegEnumKeyW" RegEnumKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("RegEnumKeyExA" RegEnumKeyExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :string)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("RegEnumKeyExW" RegEnumKeyExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("RegEnumValueA" RegEnumValueA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("RegEnumValueW" RegEnumValueW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("RegFlushKey" RegFlushKey :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RegGetKeySecurity" RegGetKeySecurity :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RegLoadKeyA" RegLoadKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("RegLoadKeyW" RegLoadKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegNotifyChangeKeyValue" RegNotifyChangeKeyValue :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :int))

(defcfunex-exported ("RegOpenKeyA" RegOpenKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("RegOpenKeyExA" RegOpenKeyExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RegOpenKeyExW" RegOpenKeyExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RegOpenKeyW" RegOpenKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegQueryInfoKeyA" RegQueryInfoKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer)
  (arg11 :pointer))

(defcfunex-exported ("RegQueryInfoKeyW" RegQueryInfoKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer)
  (arg11 :pointer))

(defcfunex-exported ("RegQueryMultipleValuesA" RegQueryMultipleValuesA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :string)
  (arg4 :pointer))

(defcfunex-exported ("RegQueryMultipleValuesW" RegQueryMultipleValuesW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RegQueryValueA" RegQueryValueA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("RegQueryValueExA" RegQueryValueExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RegQueryValueExW" RegQueryValueExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RegQueryValueW" RegQueryValueW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RegReplaceKeyA" RegReplaceKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string))

(defcfunex-exported ("RegReplaceKeyW" RegReplaceKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RegRestoreKeyA" RegRestoreKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("RegRestoreKeyW" RegRestoreKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("RegSaveKeyA" RegSaveKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("RegSaveKeyW" RegSaveKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegSetKeySecurity" RegSetKeySecurity :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("RegSetValueA" RegSetValueA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :string)
  (arg4 :unsigned-long))

(defcfunex-exported ("RegSetValueExA" RegSetValueExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("RegSetValueExW" RegSetValueExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("RegSetValueW" RegSetValueW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("RegUnLoadKeyA" RegUnLoadKeyA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("RegUnLoadKeyW" RegUnLoadKeyW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))


