
(cl:in-package w32apimod)

(define-w32api-module win95.dialogbox :win95.dialogbox)

(cl:in-package cl-w32api.module.win95.dialogbox)

(require-and-inherit-module "win95.~")
(require-and-inherit-module "win95.window")

(defbitfield-extended (DIALOG-STYLES DWORD) WINDOW-STYLES
  (:3DLOOK 4)
  (:ABSALIGN 1)
  (:CENTER #x800)
  (:CENTERMOUSE 4096)
  (:CONTEXTHELP #x2000)
  (:CONTROL #x400)
  (:FIXEDSYS 8)
  (:LOCALEDIT 32)
  (:MODALFRAME 128)
  (:NOFAILCREATE 16)
  (:NOIDLEMSG 256)
  (:SETFONT 64)
  (:SETFOREGROUND 512)
  (:SYSMODAL 2)
  (:SHELLFONT #.(cl:logior 64 8)))

(defcstructex-exported DLGITEMTEMPLATE
        (style :unsigned-long)
        (dwExtendedStyle :unsigned-long)
        (x :short)
        (y :short)
        (cx :short)
        (cy :short)
        (id :unsigned-short))



(defcstructex-exported DLGTEMPLATE
        (style :unsigned-long)
        (dwExtendedStyle :unsigned-long)
        (cdit :unsigned-short)
        (x :short)
        (y :short)
        (cx :short)
        (cy :short))


(defcfunex-exported ("CreateDialogIndirectParamA" CreateDialogIndirectParamA :convention :stdcall) HWND
  (|hInstance|  HINSTANCE)
  (arg1 :pointer)
  (|hWndParent| HWND)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("CreateDialogIndirectParamW" CreateDialogIndirectParamW :convention :stdcall) HWND
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("CreateDialogParamA" CreateDialogParamA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("CreateDialogParamW" CreateDialogParamW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int32))

(defcfunex-exported ("GetDialogBaseUnits" GetDialogBaseUnits :convention :stdcall) :int32)

(defcfunex-exported ("GetDlgCtrlID" GetDlgCtrlID :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetDlgItem" GetDlgItem :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetDlgItemInt" GetDlgItemInt :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("GetDlgItemTextA" GetDlgItemTextA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :string)
  (arg3 :int))

(defcfunex-exported ("GetDlgItemTextW" GetDlgItemTextW :convention :stdcall) :unsigned-int
  (arg0 HWND)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))


(defbitfield (MESSAGEBOX-FLAGS UINT)
  (:USERICON 128) (:ICONASTERISK 64) (:ICONEXCLAMATION
  #x30) (:ICONWARNING #x30) (:ICONERROR 16) (:ICONHAND 16)
  (:ICONQUESTION 32) (:OK 0) (:ABORTRETRYIGNORE 2) (:APPLMODAL 0) (:DEFAULT_DESKTOP_ONLY #x20000) (:HELP #x4000)
  (:RIGHT #x80000) (:RTLREADING #x100000) (:TOPMOST #x40000) (:DEFBUTTON1 0) (:DEFBUTTON2 256) (:DEFBUTTON3 512)
  (:DEFBUTTON4 #x300) (:ICONINFORMATION 64) (:ICONSTOP 16) (:OKCANCEL 1) (:RETRYCANCEL 5) (:SERVICE_NOTIFICATION #x00200000)
  (:SERVICE_NOTIFICATION_NT3X #x00040000) (:SETFOREGROUND #x10000) (:SYSTEMMODAL 4096) (:TASKMODAL #x2000) (:YESNO 4)
  (:YESNOCANCEL 3) (:ICONMASK 240) (:DEFMASK 3840) (:MODEMASK #x00003000) (:MISCMASK #x0000C000) (:NOFOCUS #x00008000)
  (:TYPEMASK 15) )

(defcenum (DIALOGBOX-RETCODE :int)
  (:OK 1) (:CANCEL 2) (:ABORT 3) (:RETRY 4) (:IGNORE 5)
  (:YES 6) (:NO 7) (:CLOSE 8) (:HELP 9))

(defcfunex-exported ("MessageBoxA" MessageBoxA :convention :stdcall) DIALOGBOX-RETCODE
  (|hWnd|      HWND)
  (|lpText|    ASTRING)
  (|lpCaption| ASTRING)
  (|uType|     MESSAGEBOX-FLAGS))

(defcfunex-exported ("MessageBoxW" MessageBoxW :convention :stdcall) DIALOGBOX-RETCODE
  (|hWnd|      HWND)
  (|lpText|    WSTRING)
  (|lpCaption| WSTRING)
  (|uType|     MESSAGEBOX-FLAGS))

(define-abbrev-exported MessageBox MessageBoxW)

(defcfunex-exported ("MessageBoxExA" MessageBoxExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-int)
  (arg4 :unsigned-short))

(defcfunex-exported ("MessageBoxExW" MessageBoxExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :unsigned-short))

(defcfunex-exported ("MessageBoxIndirectA" MessageBoxIndirectA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("MessageBoxIndirectW" MessageBoxIndirectW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SendDlgItemMessageA" SendDlgItemMessageA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("SendDlgItemMessageW" SendDlgItemMessageW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("SetDlgItemInt" SetDlgItemInt :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :int))

(defcfunex-exported ("SetDlgItemTextA" SetDlgItemTextA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("SetDlgItemTextW" SetDlgItemTextW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))


