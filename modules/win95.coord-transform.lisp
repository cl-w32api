
(defcfunex-exported ("ClientToScreen" ClientToScreen :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CombineTransform" CombineTransform :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DPtoLP" DPtoLP :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetCurrentPositionEx" GetCurrentPositionEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetGraphicsMode" GetGraphicsMode :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetMapMode" GetMapMode :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetViewportExtEx" GetViewportExtEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetViewportOrgEx" GetViewportOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetWindowExtEx" GetWindowExtEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetWindowOrgEx" GetWindowOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetWorldTransform" GetWorldTransform :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("LPtoDP" LPtoDP :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("ModifyWorldTransform" ModifyWorldTransform :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("OffsetViewportOrgEx" OffsetViewportOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("OffsetWindowOrgEx" OffsetWindowOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("ScaleViewportExtEx" ScaleViewportExtEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :pointer))

(defcfunex-exported ("ScaleWindowExtEx" ScaleWindowExtEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :pointer))

(defcfunex-exported ("ScreenToClient" ScreenToClient :convention :stdcall) :int
  (hWnd :pointer)
  (lpPOINT :pointer))

(defcfunex-exported ("SetGraphicsMode" SetGraphicsMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetMapMode" SetMapMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetViewportExtEx" SetViewportExtEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("SetViewportOrgEx" SetViewportOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("SetWindowExtEx" SetWindowExtEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("SetWindowOrgEx" SetWindowOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("SetWorldTransform" SetWorldTransform :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

