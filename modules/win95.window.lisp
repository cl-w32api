
(cl:in-package w32apimod)

(define-w32api-module win95.window :win95.window)

(cl:in-package cl-w32api.module.win95.window)

(require-and-inherit-module "win95.~")

(defcfunex-exported ("AdjustWindowRect" AdjustWindowRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int))

(defcfunex-exported ("AdjustWindowRectEx" AdjustWindowRectEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("ArrangeIconicWindows" ArrangeIconicWindows :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("BeginDeferWindowPos" BeginDeferWindowPos :convention :stdcall) :pointer
  (arg0 :int))

(defcfunex-exported ("BringWindowToTop" BringWindowToTop :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CascadeWindows" CascadeWindows :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("ChildWindowFromPoint" ChildWindowFromPoint :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 (:inline POINT)))

(defcfunex-exported ("ChildWindowFromPointEx" ChildWindowFromPointEx :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 (:inline POINT))
  (arg2 :unsigned-int))

(defcfunex-exported ("CloseWindow" CloseWindow :convention :stdcall) BOOL
  (|hWnd| HWND))

(defbitfield (WINDOW-STYLES DWORD)
  (:BORDER #x800000)
  (:CAPTION #xc00000)
  (:CHILD #x40000000)
  (:CHILDWINDOW #x40000000)
  (:CLIPCHILDREN #x2000000)
  (:CLIPSIBLINGS #x4000000)
  (:DISABLED #x8000000)
  (:DLGFRAME #x400000)
  (:GROUP #x20000)
  (:HSCROLL #x100000)
  (:ICONIC #x20000000)
  (:MAXIMIZE #x1000000)
  (:MAXIMIZEBOX #x10000)
  (:MINIMIZE #x20000000)
  (:MINIMIZEBOX #x20000)
  (:OVERLAPPED 0)
  (:OVERLAPPEDWINDOW #xcf0000)
  (:POPUP #x80000000)
  (:POPUPWINDOW #x80880000)
  (:SIZEBOX #x40000)
  (:SYSMENU #x80000)
  (:TABSTOP #x10000)
  (:THICKFRAME #x40000)
  (:TILED 0)
  (:TILEDWINDOW #xcf0000)
  (:VISIBLE #x10000000)
  (:VSCROLL #x200000))
(export 'WINDOW-STYLES)

(defcfunex-exported ("CreateWindowExA" CreateWindowExA :convention :stdcall) :pointer
  (|dwExStyle|    DWORD)
  (|lpClassName|  ASTRING)
  (|lpWindowName| ASTRING)
  (|dwStyle|      WINDOW-STYLES)
  (|x|            :int)
  (|y|            :int)
  (|nWidth|       :int)
  (|nHeight|      :int)
  (|hWndParent|   HWND)
  (|hMenu|        HMENU)
  (|hInstance|    HINSTANCE)
  (|lpParam|      :pointer))


(defcfunex-exported ("CreateWindowExW" CreateWindowExW :convention :stdcall) HWND
  (|dwExStyle|    DWORD)
  (|lpClassName|  WSTRING)
  (|lpWindowName| WSTRING)
  (|dwStyle|      WINDOW-STYLES)
  (|x|            :int)
  (|y|            :int)
  (|nWidth|       :int)
  (|nHeight|      :int)
  (|hWndParent|   HWND)
  (|hMenu|        HMENU)
  (|hInstance|    HINSTANCE)
  (|lpParam|      :pointer))

(define-abbrev-exported CreateWindowEx CreateWindowExW)

(defmacro-exported CreateWindow
    (&whole form |lpClassName| |lpWindowName| |dwStyle| |x| |y| |nWidth| |nHeight|
	    |hWndParent| |hMenu| |hInstance| |lpParam|)
  (declare (ignore |lpClassName| |lpWindowName| |dwStyle| |x| |y| |nWidth| |nHeight|
		   |hWndParent| |hMenu| |hInstance| |lpParam| ))
  `(CreateWindowEx ,@(cdr form)))

(defcfunex-exported ("DeferWindowPos" DeferWindowPos :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :unsigned-int))

(defcfunex-exported ("DestroyWindow" DestroyWindow :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnableWindow" EnableWindow :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("EndDeferWindowPos" EndDeferWindowPos :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnumWindows" EnumWindows :convention :stdcall) :int
  (lpEnumFunc :pointer)
  (lParam :int32))

(defcfunex-exported ("FindWindowA" FindWindowA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("FindWindowW" FindWindowW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FindWindowExA" FindWindowExA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :string))

(defcfunex-exported ("FindWindowExW" FindWindowExW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetClientRect" GetClientRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetDesktopWindow" GetDesktopWindow :convention :stdcall) :pointer)

(defcfunex-exported ("GetForegroundWindow" GetForegroundWindow :convention :stdcall) :pointer)

(defcfunex-exported ("GetLastActivePopup" GetLastActivePopup :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetTopWindow" GetTopWindow :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetWindowTextA" GetWindowTextA :convention :stdcall) :int
  (hWnd :pointer)
  (lpString :string)
  (nMaxCount :int))

(defcfunex-exported ("GetWindowTextW" GetWindowTextW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetWindowTextLengthA" GetWindowTextLengthA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetWindowTextLengthW" GetWindowTextLengthW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetWindowThreadProcessId" GetWindowThreadProcessId :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IsWindow" IsWindow :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsWindowUnicode" IsWindowUnicode :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsWindowVisible" IsWindowVisible :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("MoveWindow" MoveWindow :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int))

(defcfunex-exported ("OpenIcon" OpenIcon :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetParent" SetParent :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetWindowLongA" SetWindowLongA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int32))

(defcfunex-exported ("SetWindowLongW" SetWindowLongW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int32))

(defcfunex-exported ("SetWindowPlacement" SetWindowPlacement :convention :stdcall) :int
  (hWnd :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetWindowPos" SetWindowPos :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :unsigned-int))

(defcfunex-exported ("SetWindowTextA" SetWindowTextA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("SetWindowTextW" SetWindowTextW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ShowOwnedPopups" ShowOwnedPopups :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcenum (SHOWWINDOW-FLAGS :int)
  (:HIDE 0)
  (:NORMAL 1)
  (:SHOWNORMAL 1)
  (:SHOWMINIMIZED 2)
  (:MAXIMIZE 3)
  (:SHOWMAXIMIZED 3)
  (:SHOWNOACTIVATE 4)
  (:SHOW 5)
  (:MINIMIZE 6)
  (:SHOWMINNOACTIVE 7)
  (:SHOWNA 8)
  (:RESTORE 9)
  (:SHOWDEFAULT 10)
  (:FORCEMINIMIZE 11)
  (:MAX 11))

(defcfunex-exported ("ShowWindow" ShowWindow :convention :stdcall) BOOL
  (|hWnd|     HWND)
  (|nCmdShow| SHOWWINDOW-FLAGS))

(defcfunex-exported ("ShowWindowAsync" ShowWindowAsync :convention :stdcall) BOOL
  (|hWnd|     HWND)
  (|nCmdShow| SHOWWINDOW-FLAGS))

(defcfunex-exported ("TileWindows" TileWindows :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("WindowFromPoint" WindowFromPoint :convention :stdcall) :pointer
              (point (:inline POINT)))

(defcfunex-exported ("WinMain" WinMain :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :int))

(defcfunex-exported ("wWinMain" wWinMain :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int))


;;obsolete
(defcfunex-exported ("AnyPopup" AnyPopup :convention :stdcall) :int)

