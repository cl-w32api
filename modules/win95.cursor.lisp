(cl:in-package w32apimod)

(define-w32api-module win95.cursor :win95.cursor)

(cl:in-package cl-w32api.module.win95.cursor)

(require-and-inherit-module "win95.~")

(defconstant-exported IDC_ARROW (MAKEINTRESOURCE 32512))
(defconstant-exported IDC_IBEAM (MAKEINTRESOURCE 32513))
(defconstant-exported IDC_WAIT (MAKEINTRESOURCE 32514))
(defconstant-exported IDC_CROSS (MAKEINTRESOURCE 32515))
(defconstant-exported IDC_UPARROW (MAKEINTRESOURCE 32516))
(defconstant-exported IDC_SIZENWSE (MAKEINTRESOURCE 32642))
(defconstant-exported IDC_SIZENESW (MAKEINTRESOURCE 32643))
(defconstant-exported IDC_SIZEWE (MAKEINTRESOURCE 32644))
(defconstant-exported IDC_SIZENS (MAKEINTRESOURCE 32645))
(defconstant-exported IDC_SIZEALL (MAKEINTRESOURCE 32646))
(defconstant-exported IDC_NO (MAKEINTRESOURCE 32648))
(defconstant-exported IDC_HAND (MAKEINTRESOURCE 32649))
(defconstant-exported IDC_APPSTARTING (MAKEINTRESOURCE 32650))
(defconstant-exported IDC_HELP (MAKEINTRESOURCE 32651))
(defconstant-exported IDC_ICON (MAKEINTRESOURCE 32641))
(defconstant-exported IDC_SIZE (MAKEINTRESOURCE 32640))

(defcfunex-exported ("ClipCursor" ClipCursor :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CreateCursor" CreateCursor :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("DestroyCursor" DestroyCursor :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetClipCursor" GetClipCursor :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetCursor" GetCursor :convention :stdcall) :pointer)

(defcfunex-exported ("GetCursorPos" GetCursorPos :convention :stdcall) :int
  (lpPOINT :pointer))

(defcfunex-exported ("LoadCursorA" LoadCursorA :convention :stdcall) HCURSOR
  (|hInstance| HINSTANCE)
  (|lpCursorName| ASTRING))

(defcfunex-exported ("LoadCursorW" LoadCursorW :convention :stdcall) HCURSOR
  (|hInstance| HINSTANCE)
  (|lpCursorName| WSTRING))

(define-abbrev-exported LoadCursor LoadCursorW)

(defcfunex-exported ("LoadCursorFromFileA" LoadCursorFromFileA :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("LoadCursorFromFileW" LoadCursorFromFileW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("SetCursor" SetCursor :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("SetCursorPos" SetCursorPos :convention :stdcall) :int
  (arg0 :int)
  (arg1 :int))

(defcfunex-exported ("SetSystemCursor" SetSystemCursor :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("ShowCursor" ShowCursor :convention :stdcall) :int
  (arg0 :int))

