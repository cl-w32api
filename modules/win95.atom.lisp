
(defcfunex-exported ("AddAtomA" AddAtomA :convention :stdcall) :unsigned-short
  (arg0 :string))

(defcfunex-exported ("AddAtomW" AddAtomW :convention :stdcall) :unsigned-short
  (arg0 :pointer))

(defcfunex-exported ("DeleteAtom" DeleteAtom :convention :stdcall) :unsigned-short
  (arg0 :unsigned-short))

(defcfunex-exported ("FindAtomA" FindAtomA :convention :stdcall) :unsigned-short
  (arg0 :string))

(defcfunex-exported ("FindAtomW" FindAtomW :convention :stdcall) :unsigned-short
  (arg0 :pointer))

(defcfunex-exported ("GetAtomNameA" GetAtomNameA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-short)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("GetAtomNameW" GetAtomNameW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-short)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GlobalAddAtomA" GlobalAddAtomA :convention :stdcall) :unsigned-short
  (arg0 :string))

(defcfunex-exported ("GlobalAddAtomW" GlobalAddAtomW :convention :stdcall) :unsigned-short
  (arg0 :pointer))

(defcfunex-exported ("GlobalDeleteAtom" GlobalDeleteAtom :convention :stdcall) :unsigned-short
  (arg0 :unsigned-short))

(defcfunex-exported ("GlobalFindAtomA" GlobalFindAtomA :convention :stdcall) :unsigned-short
  (arg0 :string))

(defcfunex-exported ("GlobalFindAtomW" GlobalFindAtomW :convention :stdcall) :unsigned-short
  (arg0 :pointer))

(defcfunex-exported ("GlobalGetAtomNameA" GlobalGetAtomNameA :convention :stdcall) :unsigned-int
  (arg0 :unsigned-short)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("GlobalGetAtomNameW" GlobalGetAtomNameW :convention :stdcall) :unsigned-int
  (arg0 :unsigned-short)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("InitAtomTable" InitAtomTable :convention :stdcall) :int
  (arg0 :unsigned-long))

