
(cl:in-package w32apimod)

(define-w32api-module win95.dll :win95.dll)

(cl:in-package cl-w32api.module.win95.dll)

(require-and-inherit-module "win95.~")

(defcfunex-exported ("DisableThreadLibraryCalls" DisableThreadLibraryCalls :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FreeLibrary" FreeLibrary :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FreeLibraryAndExitThread" FreeLibraryAndExitThread :convention :stdcall) :void
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetModuleFileNameA" GetModuleFileNameA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetModuleFileNameW" GetModuleFileNameW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetModuleHandleA" GetModuleHandleA :convention :stdcall) HMODULE
  (|lpModuleName| ASTRING))

(defcfunex-exported ("GetModuleHandleW" GetModuleHandleW :convention :stdcall) HMODULE
  (|lpModuleName| WSTRING))

(define-abbrev-exported GetModuleHandle GetModuleHandleW)

(defcfunex-exported ("GetProcAddress" GetProcAddress :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("LoadLibraryA" LoadLibraryA :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("LoadLibraryW" LoadLibraryW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("LoadLibraryExA" LoadLibraryExA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("LoadLibraryExW" LoadLibraryExW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("LoadModule" LoadModule :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :pointer))

