;;print

(defcfunex-exported ("AbortDoc" AbortDoc :convention :stdcall) :int
  (arg0 :pointer))


(defcfunex-exported ("DeviceCapabilitiesA" DeviceCapabilitiesA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-short)
  (arg3 :string)
  (arg4 :pointer))

(defcfunex-exported ("DeviceCapabilitiesW" DeviceCapabilitiesW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-short)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("EndDoc" EndDoc :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EndPage" EndPage :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("Escape" Escape :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :pointer))

(defcfunex-exported ("ExtEscape" ExtEscape :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int)
  (arg5 :string))

(defcfunex-exported ("SetAbortProc" SetAbortProc :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("StartDocA" StartDocA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("StartDocW" StartDocW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("StartPage" StartPage :convention :stdcall) :int
  (arg0 :pointer))


;;print spooler


(defcfunex-exported ("AbortPrinter" AbortPrinter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("AddFormA" AddFormA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddFormW" AddFormW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddJobA" AddJobA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("AddJobW" AddJobW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("AddMonitorA" AddMonitorA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddMonitorW" AddMonitorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddPortA" AddPortA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string))

(defcfunex-exported ("AddPortW" AddPortW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("AddPrinterA" AddPrinterA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddPrinterW" AddPrinterW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddPrinterConnectionA" AddPrinterConnectionA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("AddPrinterConnectionW" AddPrinterConnectionW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("AddPrinterDriverA" AddPrinterDriverA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddPrinterDriverW" AddPrinterDriverW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddPrintProcessorA" AddPrintProcessorA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string))

(defcfunex-exported ("AddPrintProcessorW" AddPrintProcessorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("AddPrintProvidorA" AddPrintProvidorA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AddPrintProvidorW" AddPrintProvidorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("AdvancedDocumentPropertiesA" AdvancedDocumentPropertiesA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("AdvancedDocumentPropertiesW" AdvancedDocumentPropertiesW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("ClosePrinter" ClosePrinter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ConfigurePortA" ConfigurePortA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string))

(defcfunex-exported ("ConfigurePortW" ConfigurePortW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ConnectToPrinterDlg" ConnectToPrinterDlg :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("DeleteFormA" DeleteFormA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("DeleteFormW" DeleteFormW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DeleteMonitorA" DeleteMonitorA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("DeleteMonitorW" DeleteMonitorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DeletePortA" DeletePortA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string))

(defcfunex-exported ("DeletePortW" DeletePortW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DeletePrinter" DeletePrinter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DeletePrinterConnectionA" DeletePrinterConnectionA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("DeletePrinterConnectionW" DeletePrinterConnectionW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DeletePrinterDataA" DeletePrinterDataA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("DeletePrinterDataW" DeletePrinterDataW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DeletePrinterDriverA" DeletePrinterDriverA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("DeletePrinterDriverW" DeletePrinterDriverW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DeletePrintProcessorA" DeletePrintProcessorA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("DeletePrintProcessorW" DeletePrintProcessorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DeletePrintProvidorA" DeletePrintProvidorA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("DeletePrintProvidorW" DeletePrintProvidorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DocumentPropertiesA" DocumentPropertiesA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("DocumentPropertiesW" DocumentPropertiesW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("EndDocPrinter" EndDocPrinter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EndPagePrinter" EndPagePrinter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnumFormsA" EnumFormsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumFormsW" EnumFormsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumJobsA" EnumJobsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("EnumJobsW" EnumJobsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("EnumMonitorsA" EnumMonitorsA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumMonitorsW" EnumMonitorsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumPortsA" EnumPortsA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumPortsW" EnumPortsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumPrinterDataA" EnumPrinterDataA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :unsigned-long)
  (arg8 :pointer))

(defcfunex-exported ("EnumPrinterDataW" EnumPrinterDataW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :unsigned-long)
  (arg8 :pointer))

(defcfunex-exported ("EnumPrinterDriversA" EnumPrinterDriversA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrinterDriversW" EnumPrinterDriversW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrintersA" EnumPrintersA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrintersW" EnumPrintersW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrintProcessorDatatypesA" EnumPrintProcessorDatatypesA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrintProcessorDatatypesW" EnumPrintProcessorDatatypesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrintProcessorsA" EnumPrintProcessorsA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("EnumPrintProcessorsW" EnumPrintProcessorsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("FindClosePrinterChangeNotification" FindClosePrinterChangeNotification :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FindFirstPrinterChangeNotification" FindFirstPrinterChangeNotification :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("FindNextPrinterChangeNotification" FindNextPrinterChangeNotification :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("FreePrinterNotifyInfo" FreePrinterNotifyInfo :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetFormA" GetFormA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetFormW" GetFormW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetJobA" GetJobA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetJobW" GetJobW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrinterA" GetPrinterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetPrinterW" GetPrinterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetPrinterDataA" GetPrinterDataA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrinterDataW" GetPrinterDataW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrinterDriverA" GetPrinterDriverA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrinterDriverW" GetPrinterDriverW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrinterDriverDirectoryA" GetPrinterDriverDirectoryA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrinterDriverDirectoryW" GetPrinterDriverDirectoryW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrintProcessorDirectoryA" GetPrintProcessorDirectoryA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrintProcessorDirectoryW" GetPrintProcessorDirectoryW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("OpenPrinterA" OpenPrinterA :convention :stdcall) :int
  (arg0 :string)
(arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("OpenPrinterW" OpenPrinterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("PrinterMessageBoxA" PrinterMessageBoxA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :string)
  (arg4 :string)
  (arg5 :unsigned-long))

(defcfunex-exported ("PrinterMessageBoxW" PrinterMessageBoxW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("PrinterProperties" PrinterProperties :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ReadPrinter" ReadPrinter :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("ResetPrinterA" ResetPrinterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ResetPrinterW" ResetPrinterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ScheduleJob" ScheduleJob :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetFormA" SetFormA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("SetFormW" SetFormW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("SetJobA" SetJobA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("SetJobW" SetJobW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("SetPrinterA" SetPrinterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetPrinterW" SetPrinterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetPrinterDataA" SetPrinterDataA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("SetPrinterDataW" SetPrinterDataW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("StartDocPrinterA" StartDocPrinterA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("StartDocPrinterW" StartDocPrinterW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("StartPagePrinter" StartPagePrinter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WaitForPrinterChange" WaitForPrinterChange :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("WritePrinter" WritePrinter :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))


