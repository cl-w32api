
(cl:in-package w32apimod)

(define-w32api-module rpc :rpc)

(cl:in-package cl-w32api.module.rpc)


(defconstant-exported GUID_SECTION ".text")

(defconstant-exported RPC_C_BINDING_INFINITE_TIMEOUT 10)

(defconstant-exported RPC_C_BINDING_MIN_TIMEOUT 0)

(defconstant-exported RPC_C_BINDING_DEFAULT_TIMEOUT 5)

(defconstant-exported RPC_C_BINDING_MAX_TIMEOUT 9)

(defconstant-exported RPC_C_CANCEL_INFINITE_TIMEOUT -1)

(defconstant-exported RPC_C_LISTEN_MAX_CALLS_DEFAULT 1234)

(defconstant-exported RPC_C_PROTSEQ_MAX_REQS_DEFAULT 10)

(defconstant-exported RPC_C_BIND_TO_ALL_NICS 1)

(defconstant-exported RPC_C_USE_INTERNET_PORT 1)

(defconstant-exported RPC_C_USE_INTRANET_PORT 2)

(defconstant-exported RPC_C_STATS_CALLS_IN 0)

(defconstant-exported RPC_C_STATS_CALLS_OUT 1)

(defconstant-exported RPC_C_STATS_PKTS_IN 2)

(defconstant-exported RPC_C_STATS_PKTS_OUT 3)

(defconstant-exported RPC_IF_AUTOLISTEN #x0001)

(defconstant-exported RPC_IF_OLE 2)

(defconstant-exported RPC_C_MGMT_INQ_IF_IDS 0)

(defconstant-exported RPC_C_MGMT_INQ_PRINC_NAME 1)

(defconstant-exported RPC_C_MGMT_INQ_STATS 2)

(defconstant-exported RPC_C_MGMT_IS_SERVER_LISTEN 3)

(defconstant-exported RPC_C_MGMT_STOP_SERVER_LISTEN 4)

(defconstant-exported RPC_C_EP_ALL_ELTS 0)

(defconstant-exported RPC_C_EP_MATCH_BY_IF 1)

(defconstant-exported RPC_C_EP_MATCH_BY_OBJ 2)

(defconstant-exported RPC_C_EP_MATCH_BY_BOTH 3)

(defconstant-exported RPC_C_VERS_ALL 1)

(defconstant-exported RPC_C_VERS_COMPATIBLE 2)

(defconstant-exported RPC_C_VERS_EXACT 3)

(defconstant-exported RPC_C_VERS_MAJOR_ONLY 4)

(defconstant-exported RPC_C_VERS_UPTO 5)

(defconstant-exported DCE_C_ERROR_STRING_LEN 256)

(defconstant-exported RPC_C_PARM_MAX_PACKET_LENGTH 1)

(defconstant-exported RPC_C_PARM_BUFFER_LENGTH 2)

(defconstant-exported RPC_C_AUTHN_LEVEL_DEFAULT 0)

(defconstant-exported RPC_C_AUTHN_LEVEL_NONE 1)

(defconstant-exported RPC_C_AUTHN_LEVEL_CONNECT 2)

(defconstant-exported RPC_C_AUTHN_LEVEL_CALL 3)

(defconstant-exported RPC_C_AUTHN_LEVEL_PKT 4)

(defconstant-exported RPC_C_AUTHN_LEVEL_PKT_INTEGRITY 5)

(defconstant-exported RPC_C_AUTHN_LEVEL_PKT_PRIVACY 6)

(defconstant-exported RPC_C_IMP_LEVEL_ANONYMOUS 1)

(defconstant-exported RPC_C_IMP_LEVEL_IDENTIFY 2)

(defconstant-exported RPC_C_IMP_LEVEL_IMPERSONATE 3)

(defconstant-exported RPC_C_IMP_LEVEL_DELEGATE 4)

(defconstant-exported RPC_C_QOS_IDENTITY_STATIC 0)

(defconstant-exported RPC_C_QOS_IDENTITY_DYNAMIC 1)

(defconstant-exported RPC_C_QOS_CAPABILITIES_DEFAULT 0)

(defconstant-exported RPC_C_QOS_CAPABILITIES_MUTUAL_AUTH 1)

(defconstant-exported RPC_C_AUTHN_NONE 0)

(defconstant-exported RPC_C_AUTHN_DCE_PRIVATE 1)

(defconstant-exported RPC_C_AUTHN_DCE_PUBLIC 2)

(defconstant-exported RPC_C_AUTHN_DEC_PUBLIC 4)

(defconstant-exported RPC_C_AUTHN_WINNT 10)

(defconstant-exported RPC_C_AUTHN_DEFAULT #xFFFFFFFF)

(defconstant-exported SEC_WINNT_AUTH_IDENTITY_ANSI #x1)

(defconstant-exported SEC_WINNT_AUTH_IDENTITY_UNICODE #x2)

(defconstant-exported RPC_C_AUTHZ_NONE 0)

(defconstant-exported RPC_C_AUTHZ_NAME 1)

(defconstant-exported RPC_C_AUTHZ_DCE 2)

(defconstant-exported RPC_C_AUTHZ_DEFAULT #xFFFFFFFF)





(defcstructex-exported RPC_BINDING_VECTOR
        (Count :unsigned-long)
        (BindingH :pointer))



(defcstructex-exported UUID_VECTOR
        (Count :unsigned-long)
        (Uuid :pointer))





(defcstructex-exported RPC_IF_ID
        (Uuid GUID)
        (VersMajor :unsigned-short)
        (VersMinor :unsigned-short))



(defcstructex-exported RPC_POLICY
        (Length :unsigned-int)
        (EndpointFlags :unsigned-long)
        (NICFlags :unsigned-long))









(defcstructex-exported RPC_STATS_VECTOR
        (Count :unsigned-int)
        (Stats :pointer))

(defcstructex-exported RPC_IF_ID_VECTOR
        (Count :unsigned-long)
        (IfId :pointer))





(defcstructex-exported RPC_SECURITY_QOS
        (Version :unsigned-long)
        (Capabilities :unsigned-long)
        (IdentityTracking :unsigned-long)
        (ImpersonationType :unsigned-long))





(defcstructex-exported SEC_WINNT_AUTH_IDENTITY_W
        (User :pointer)
        (UserLength :unsigned-long)
        (Domain :pointer)
        (DomainLength :unsigned-long)
        (Password :pointer)
        (PasswordLength :unsigned-long)
        (Flags :unsigned-long))





(defcstructex-exported SEC_WINNT_AUTH_IDENTITY_A
        (User :pointer)
        (UserLength :unsigned-long)
        (Domain :pointer)
        (DomainLength :unsigned-long)
        (Password :pointer)
        (PasswordLength :unsigned-long)
        (Flags :unsigned-long))





(defcstructex-exported RPC_CLIENT_INFORMATION1
        (UserName :pointer)
        (ComputerName :pointer)
        (Privilege :unsigned-short)
        (AuthFlags :unsigned-long))







(defcstructex-exported RPC_PROTSEQ_VECTORA
        (Count :unsigned-int)
        (Protseq :pointer))



(defcstructex-exported RPC_PROTSEQ_VECTORW
        (Count :unsigned-int)
        (Protseq :pointer))



(defcfunex-exported ("RpcBindingFromStringBindingA" RpcBindingFromStringBindingA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingFromStringBindingW" RpcBindingFromStringBindingW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingToStringBindingA" RpcBindingToStringBindingA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingToStringBindingW" RpcBindingToStringBindingW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcStringBindingComposeA" RpcStringBindingComposeA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcStringBindingComposeW" RpcStringBindingComposeW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcStringBindingParseA" RpcStringBindingParseA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcStringBindingParseW" RpcStringBindingParseW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcStringFreeA" RpcStringFreeA :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcStringFreeW" RpcStringFreeW :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNetworkIsProtseqValidA" RpcNetworkIsProtseqValidA :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNetworkIsProtseqValidW" RpcNetworkIsProtseqValidW :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNetworkInqProtseqsA" RpcNetworkInqProtseqsA :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNetworkInqProtseqsW" RpcNetworkInqProtseqsW :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcProtseqVectorFreeA" RpcProtseqVectorFreeA :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcProtseqVectorFreeW" RpcProtseqVectorFreeW :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcServerUseProtseqA" RpcServerUseProtseqA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerUseProtseqW" RpcServerUseProtseqW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerUseProtseqExA" RpcServerUseProtseqExA :convention :stdcall) :int32
  (arg0 :pointer)
  (MaxCalls :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerUseProtseqExW" RpcServerUseProtseqExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerUseProtseqEpA" RpcServerUseProtseqEpA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerUseProtseqEpExA" RpcServerUseProtseqEpExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcServerUseProtseqEpW" RpcServerUseProtseqEpW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerUseProtseqEpExW" RpcServerUseProtseqEpExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcServerUseProtseqIfA" RpcServerUseProtseqIfA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerUseProtseqIfExA" RpcServerUseProtseqIfExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcServerUseProtseqIfW" RpcServerUseProtseqIfW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerUseProtseqIfExW" RpcServerUseProtseqIfExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcMgmtInqServerPrincNameA" RpcMgmtInqServerPrincNameA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("RpcMgmtInqServerPrincNameW" RpcMgmtInqServerPrincNameW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerInqDefaultPrincNameA" RpcServerInqDefaultPrincNameA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcServerInqDefaultPrincNameW" RpcServerInqDefaultPrincNameW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsBindingInqEntryNameA" RpcNsBindingInqEntryNameA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsBindingInqEntryNameW" RpcNsBindingInqEntryNameW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("RpcBindingInqAuthClientA" RpcBindingInqAuthClientA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcBindingInqAuthClientW" RpcBindingInqAuthClientW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcBindingInqAuthInfoA" RpcBindingInqAuthInfoA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcBindingInqAuthInfoW" RpcBindingInqAuthInfoW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcBindingSetAuthInfoA" RpcBindingSetAuthInfoA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("RpcBindingSetAuthInfoExA" RpcBindingSetAuthInfoExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("RpcBindingSetAuthInfoW" RpcBindingSetAuthInfoW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("RpcBindingSetAuthInfoExW" RpcBindingSetAuthInfoExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("RpcBindingInqAuthInfoExA" RpcBindingInqAuthInfoExA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-long)
  (arg7 :pointer))

(defcfunex-exported ("RpcBindingInqAuthInfoExW" RpcBindingInqAuthInfoExW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-long)
  (arg7 :pointer))



(defcfunex-exported ("RpcServerRegisterAuthInfoA" RpcServerRegisterAuthInfoA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcServerRegisterAuthInfoW" RpcServerRegisterAuthInfoW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("UuidToStringA" UuidToStringA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UuidFromStringA" UuidFromStringA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UuidToStringW" UuidToStringW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UuidFromStringW" UuidFromStringW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcEpRegisterNoReplaceA" RpcEpRegisterNoReplaceA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcEpRegisterNoReplaceW" RpcEpRegisterNoReplaceW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcEpRegisterA" RpcEpRegisterA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcEpRegisterW" RpcEpRegisterW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("DceErrorInqTextA" DceErrorInqTextA :convention :stdcall) :int32
  (arg0 :int32)
  (arg1 :pointer))

(defcfunex-exported ("DceErrorInqTextW" DceErrorInqTextW :convention :stdcall) :int32
  (arg0 :int32)
  (arg1 :pointer))

(defcfunex-exported ("RpcMgmtEpEltInqNextA" RpcMgmtEpEltInqNextA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcMgmtEpEltInqNextW" RpcMgmtEpEltInqNextW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcBindingCopy" RpcBindingCopy :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingFree" RpcBindingFree :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcBindingInqObject" RpcBindingInqObject :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingReset" RpcBindingReset :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcBindingSetObject" RpcBindingSetObject :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcMgmtInqDefaultProtectLevel" RpcMgmtInqDefaultProtectLevel :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingVectorFree" RpcBindingVectorFree :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcIfInqId" RpcIfInqId :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcMgmtInqComTimeout" RpcMgmtInqComTimeout :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcMgmtSetComTimeout" RpcMgmtSetComTimeout :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("RpcMgmtSetCancelTimeout" RpcMgmtSetCancelTimeout :convention :stdcall) :int32
  (Timeout :int32))

(defcfunex-exported ("RpcObjectInqType" RpcObjectInqType :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcObjectSetInqFn" RpcObjectSetInqFn :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcObjectSetType" RpcObjectSetType :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcServerInqIf" RpcServerInqIf :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerListen" RpcServerListen :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("RpcServerRegisterIf" RpcServerRegisterIf :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerRegisterIfEx" RpcServerRegisterIfEx :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int)
  (arg5 :pointer))

(defcfunex-exported ("RpcServerRegisterIf2" RpcServerRegisterIf2 :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int)
  (arg5 :unsigned-int)
  (arg6 :pointer))

(defcfunex-exported ("RpcServerUnregisterIf" RpcServerUnregisterIf :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("RpcServerUseAllProtseqs" RpcServerUseAllProtseqs :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("RpcServerUseAllProtseqsEx" RpcServerUseAllProtseqsEx :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerUseAllProtseqsIf" RpcServerUseAllProtseqsIf :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcServerUseAllProtseqsIfEx" RpcServerUseAllProtseqsIfEx :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcMgmtStatsVectorFree" RpcMgmtStatsVectorFree :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcMgmtInqStats" RpcMgmtInqStats :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcMgmtIsServerListening" RpcMgmtIsServerListening :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcMgmtStopServerListening" RpcMgmtStopServerListening :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcMgmtWaitServerListen" RpcMgmtWaitServerListen :convention :stdcall) :int32)

(defcfunex-exported ("RpcMgmtSetServerStackSize" RpcMgmtSetServerStackSize :convention :stdcall) :int32
  (arg0 :unsigned-long))

(defcfunex-exported ("RpcSsDontSerializeContext" RpcSsDontSerializeContext :convention :stdcall) :void)

(defcfunex-exported ("RpcMgmtEnableIdleCleanup" RpcMgmtEnableIdleCleanup :convention :stdcall) :int32)

(defcfunex-exported ("RpcMgmtInqIfIds" RpcMgmtInqIfIds :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcIfIdVectorFree" RpcIfIdVectorFree :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcEpResolveBinding" RpcEpResolveBinding :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcBindingServerFromClient" RpcBindingServerFromClient :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcRaiseException" RpcRaiseException :convention :stdcall) :void
  (arg0 :int32))

(defcfunex-exported ("RpcTestCancel" RpcTestCancel :convention :stdcall) :int32)

(defcfunex-exported ("RpcCancelThread" RpcCancelThread :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("UuidCreate" UuidCreate :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("UuidCompare" UuidCompare :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("UuidCreateNil" UuidCreateNil :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("UuidEqual" UuidEqual :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("UuidHash" UuidHash :convention :stdcall) :unsigned-short
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UuidIsNil" UuidIsNil :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcEpUnregister" RpcEpUnregister :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcMgmtEpEltInqBegin" RpcMgmtEpEltInqBegin :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("RpcMgmtEpEltInqDone" RpcMgmtEpEltInqDone :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcMgmtEpUnregister" RpcMgmtEpUnregister :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcMgmtSetAuthorizationFn" RpcMgmtSetAuthorizationFn :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcMgmtInqParameter" RpcMgmtInqParameter :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("RpcMgmtSetParameter" RpcMgmtSetParameter :convention :stdcall) :int32
  (arg0 :unsigned-int)
  (arg1 :unsigned-long))

(defcfunex-exported ("RpcMgmtBindingInqParameter" RpcMgmtBindingInqParameter :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("RpcMgmtBindingSetParameter" RpcMgmtBindingSetParameter :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-long))

(defconstant-exported RPC_NCA_FLAGS_DEFAULT 0)

(defconstant-exported RPC_NCA_FLAGS_IDEMPOTENT 1)

(defconstant-exported RPC_NCA_FLAGS_BROADCAST 2)

(defconstant-exported RPC_NCA_FLAGS_MAYBE 4)

(defconstant-exported RPCFLG_ASYNCHRONOUS #x40000000)

(defconstant-exported RPCFLG_INPUT_SYNCHRONOUS #x20000000)

(defconstant-exported RPC_FLAGS_VALID_BIT #x8000)

(defconstant-exported TRANSPORT_TYPE_CN 1)

(defconstant-exported TRANSPORT_TYPE_DG 2)

(defconstant-exported TRANSPORT_TYPE_LPC 4)

(defconstant-exported TRANSPORT_TYPE_WMSG 8)

(defcstructex-exported RPC_VERSION
        (MajorVersion :unsigned-short)
        (MinorVersion :unsigned-short))



(defcstructex-exported RPC_SYNTAX_IDENTIFIER
        (SyntaxGUID GUID)
        (SyntaxVersion RPC_VERSION))





(defcstructex-exported RPC_MESSAGE
        (Handle :pointer)
        (DataRepresentation :unsigned-long)
        (Buffer :pointer)
        (BufferLength :unsigned-int)
        (ProcNum :unsigned-int)
        (TransferSyntax :pointer)
        (RpcInterfaceInformation :pointer)
        (ReservedForRuntime :pointer)
        (ManagerEpv :pointer)
        (ImportContext :pointer)
        (RpcFlags :unsigned-long))









(defcstructex-exported RPC_DISPATCH_TABLE
        (DispatchTableCount :unsigned-int)
        (DispatchTable :pointer)
        (Reserved :int))



(defcstructex-exported RPC_PROTSEQ_ENDPOINT
        (RpcProtocolSequence :pointer)
        (Endpoint :pointer))





(defcstructex-exported RPC_SERVER_INTERFACE
        (Length :unsigned-int)
        (InterfaceId RPC_SYNTAX_IDENTIFIER)
        (TransferSyntax RPC_SYNTAX_IDENTIFIER)
        (DispatchTable :pointer)
        (RpcProtseqEndpointCount :unsigned-int)
        (RpcProtseqEndpoint :pointer)
        (DefaultManagerEpv :pointer)
        (InterpreterInfo :pointer))





(defcstructex-exported RPC_CLIENT_INTERFACE
        (Length :unsigned-int)
        (InterfaceId RPC_SYNTAX_IDENTIFIER)
        (TransferSyntax RPC_SYNTAX_IDENTIFIER)
        (DispatchTable :pointer)
        (RpcProtseqEndpointCount :unsigned-int)
        (RpcProtseqEndpoint :pointer)
        (Reserved :unsigned-long)
        (InterpreterInfo :pointer))







(defcstructex-exported RPC_TRANSFER_SYNTAX
        (Uuid GUID)
        (VersMajor :unsigned-short)
        (VersMinor :unsigned-short))





(defcfunex-exported ("I_RpcGetBuffer" I_RpcGetBuffer :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcSendReceive" I_RpcSendReceive :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcSend" I_RpcSend :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcFreeBuffer" I_RpcFreeBuffer :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcRequestMutex" I_RpcRequestMutex :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("I_RpcClearMutex" I_RpcClearMutex :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("I_RpcDeleteMutex" I_RpcDeleteMutex :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("I_RpcAllocate" I_RpcAllocate :convention :stdcall) :pointer
  (arg0 :unsigned-int))

(defcfunex-exported ("I_RpcFree" I_RpcFree :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("I_RpcPauseExecution" I_RpcPauseExecution :convention :stdcall) :void
  (arg0 :unsigned-long))



(defcfunex-exported ("I_RpcMonitorAssociation" I_RpcMonitorAssociation :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("I_RpcStopMonitorAssociation" I_RpcStopMonitorAssociation :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcGetCurrentCallHandle" I_RpcGetCurrentCallHandle :convention :stdcall) :pointer)

(defcfunex-exported ("I_RpcGetAssociationContext" I_RpcGetAssociationContext :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcSetAssociationContext" I_RpcSetAssociationContext :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcNsBindingSetEntryName" I_RpcNsBindingSetEntryName :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("I_RpcBindingInqDynamicEndpoint" I_RpcBindingInqDynamicEndpoint :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcBindingInqTransportType" I_RpcBindingInqTransportType :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcIfInqTransferSyntaxes" I_RpcIfInqTransferSyntaxes :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("I_UuidCreate" I_UuidCreate :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcBindingCopy" I_RpcBindingCopy :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcBindingIsClientLocal" I_RpcBindingIsClientLocal :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcSsDontSerializeContext" I_RpcSsDontSerializeContext :convention :stdcall) :void)

(defcfunex-exported ("I_RpcServerRegisterForwardFunction" I_RpcServerRegisterForwardFunction :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcConnectionInqSockBuffSize" I_RpcConnectionInqSockBuffSize :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcConnectionSetSockBuffSize" I_RpcConnectionSetSockBuffSize :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :unsigned-long))

(defcfunex-exported ("I_RpcBindingSetAsync" I_RpcBindingSetAsync :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcAsyncSendReceive" I_RpcAsyncSendReceive :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcGetThreadWindowHandle" I_RpcGetThreadWindowHandle :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("I_RpcServerThreadPauseListening" I_RpcServerThreadPauseListening :convention :stdcall) :int32)

(defcfunex-exported ("I_RpcServerThreadContinueListening" I_RpcServerThreadContinueListening :convention :stdcall) :int32)

(defcfunex-exported ("I_RpcServerUnregisterEndpointA" I_RpcServerUnregisterEndpointA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("I_RpcServerUnregisterEndpointW" I_RpcServerUnregisterEndpointW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))



(defconstant-exported RPC_C_NS_SYNTAX_DEFAULT 0)

(defconstant-exported RPC_C_NS_SYNTAX_DCE 3)

(defconstant-exported RPC_C_PROFILE_DEFAULT_ELT 0)

(defconstant-exported RPC_C_PROFILE_ALL_ELT 1)

(defconstant-exported RPC_C_PROFILE_MATCH_BY_IF 2)

(defconstant-exported RPC_C_PROFILE_MATCH_BY_MBR 3)

(defconstant-exported RPC_C_PROFILE_MATCH_BY_BOTH 4)

(defconstant-exported RPC_C_NS_DEFAULT_EXP_AGE -1)

(defcfunex-exported ("RpcNsBindingExportA" RpcNsBindingExportA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsBindingUnexportA" RpcNsBindingUnexportA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsBindingLookupBeginA" RpcNsBindingLookupBeginA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("RpcNsBindingLookupNext" RpcNsBindingLookupNext :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsBindingLookupDone" RpcNsBindingLookupDone :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNsGroupDeleteA" RpcNsGroupDeleteA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsGroupMbrAddA" RpcNsGroupMbrAddA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsGroupMbrRemoveA" RpcNsGroupMbrRemoveA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsGroupMbrInqBeginA" RpcNsGroupMbrInqBeginA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsGroupMbrInqNextA" RpcNsGroupMbrInqNextA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsGroupMbrInqDone" RpcNsGroupMbrInqDone :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNsProfileDeleteA" RpcNsProfileDeleteA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsProfileEltAddA" RpcNsProfileEltAddA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("RpcNsProfileEltRemoveA" RpcNsProfileEltRemoveA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsProfileEltInqBeginA" RpcNsProfileEltInqBeginA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("RpcNsProfileEltInqNextA" RpcNsProfileEltInqNextA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsProfileEltInqDone" RpcNsProfileEltInqDone :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNsEntryObjectInqNext" RpcNsEntryObjectInqNext :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsEntryObjectInqDone" RpcNsEntryObjectInqDone :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNsEntryExpandNameA" RpcNsEntryExpandNameA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsMgmtBindingUnexportA" RpcNsMgmtBindingUnexportA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsMgmtEntryCreateA" RpcNsMgmtEntryCreateA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsMgmtEntryDeleteA" RpcNsMgmtEntryDeleteA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsMgmtEntryInqIfIdsA" RpcNsMgmtEntryInqIfIdsA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsMgmtHandleSetExpAge" RpcNsMgmtHandleSetExpAge :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("RpcNsMgmtInqExpAge" RpcNsMgmtInqExpAge :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNsMgmtSetExpAge" RpcNsMgmtSetExpAge :convention :stdcall) :int32
  (arg0 :unsigned-long))

(defcfunex-exported ("RpcNsBindingImportNext" RpcNsBindingImportNext :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsBindingImportDone" RpcNsBindingImportDone :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcNsBindingSelect" RpcNsBindingSelect :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsEntryObjectInqBeginA" RpcNsEntryObjectInqBeginA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsBindingImportBeginA" RpcNsBindingImportBeginA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsBindingExportW" RpcNsBindingExportW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsBindingUnexportW" RpcNsBindingUnexportW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsBindingLookupBeginW" RpcNsBindingLookupBeginW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("RpcNsGroupDeleteW" RpcNsGroupDeleteW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsGroupMbrAddW" RpcNsGroupMbrAddW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsGroupMbrRemoveW" RpcNsGroupMbrRemoveW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsGroupMbrInqBeginW" RpcNsGroupMbrInqBeginW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("RpcNsGroupMbrInqNextW" RpcNsGroupMbrInqNextW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsProfileDeleteW" RpcNsProfileDeleteW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsProfileEltAddW" RpcNsProfileEltAddW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("RpcNsProfileEltRemoveW" RpcNsProfileEltRemoveW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsProfileEltInqBeginW" RpcNsProfileEltInqBeginW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("RpcNsProfileEltInqNextW" RpcNsProfileEltInqNextW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsEntryObjectInqBeginW" RpcNsEntryObjectInqBeginW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsEntryExpandNameW" RpcNsEntryExpandNameW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsMgmtBindingUnexportW" RpcNsMgmtBindingUnexportW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RpcNsMgmtEntryCreateW" RpcNsMgmtEntryCreateW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsMgmtEntryDeleteW" RpcNsMgmtEntryDeleteW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("RpcNsMgmtEntryInqIfIdsW" RpcNsMgmtEntryInqIfIdsW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :unsigned-short)
  (arg2 :pointer))

(defcfunex-exported ("RpcNsBindingImportBeginW" RpcNsBindingImportBeginW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defconstant-exported RPC_S_OK 0)

(defconstant-exported RPC_S_INVALID_ARG 87)

(defconstant-exported RPC_S_OUT_OF_MEMORY 14)

(defconstant-exported RPC_S_OUT_OF_THREADS 164)

(defconstant-exported RPC_S_INVALID_LEVEL 87)

(defconstant-exported RPC_S_BUFFER_TOO_SMALL 122)

(defconstant-exported RPC_S_INVALID_SECURITY_DESC 1338)

(defconstant-exported RPC_S_ACCESS_DENIED 5)

(defconstant-exported RPC_S_SERVER_OUT_OF_MEMORY 1130)

(defconstant-exported RPC_X_NO_MEMORY 14)

(defconstant-exported RPC_X_INVALID_BOUND 1734)

(defconstant-exported RPC_X_INVALID_TAG 1733)

(defconstant-exported RPC_X_ENUM_VALUE_TOO_LARGE 1781)

(defconstant-exported RPC_X_SS_CONTEXT_MISMATCH 6)

(defconstant-exported RPC_X_INVALID_BUFFER 1784)

(defconstant-exported RPC_X_INVALID_PIPE_OPERATION 1831)

(defcfunex-exported ("RpcImpersonateClient" RpcImpersonateClient :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("RpcRevertToSelf" RpcRevertToSelf :convention :stdcall) :int32)

(defcfunex-exported ("I_RpcMapWin32Status" I_RpcMapWin32Status :convention :stdcall) :int32
  (arg0 :int32))
