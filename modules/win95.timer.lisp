(defcfunex-exported ("KillTimer" KillTimer :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("QueryPerformanceCounter" QueryPerformanceCounter :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("QueryPerformanceFrequency" QueryPerformanceFrequency :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetTimer" SetTimer :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

