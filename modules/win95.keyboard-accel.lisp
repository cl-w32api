(defcfunex-exported ("CopyAcceleratorTableA" CopyAcceleratorTableA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("CopyAcceleratorTableW" CopyAcceleratorTableW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("CreateAcceleratorTableA" CreateAcceleratorTableA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("CreateAcceleratorTableW" CreateAcceleratorTableW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("DestroyAcceleratorTable" DestroyAcceleratorTable :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("LoadAcceleratorsA" LoadAcceleratorsA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("LoadAcceleratorsW" LoadAcceleratorsW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("TranslateAcceleratorA" TranslateAcceleratorA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("TranslateAcceleratorW" TranslateAcceleratorW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

