(defcfunex-exported ("CreateFileMappingA" CreateFileMappingA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :string))

(defcfunex-exported ("CreateFileMappingW" CreateFileMappingW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("FlushViewOfFile" FlushViewOfFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("MapViewOfFile" MapViewOfFile :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("MapViewOfFileEx" MapViewOfFileEx :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("OpenFileMappingA" OpenFileMappingA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("OpenFileMappingW" OpenFileMappingW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("UnmapViewOfFile" UnmapViewOfFile :convention :stdcall) :int
  (arg0 :pointer))

