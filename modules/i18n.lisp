
(cl:in-package w32apimod)

(define-w32api-module i18n :i18n)

(cl:in-package cl-w32api.module.i18n)


(defconstant-exported MAX_LEADBYTES 12)

(defconstant-exported MAX_DEFAULTCHAR 2)

(defconstant-exported LOCALE_NOUSEROVERRIDE #x80000000)

(defconstant-exported LOCALE_USE_CP_ACP #x40000000)

(defconstant-exported LOCALE_RETURN_NUMBER #x20000000)

(defconstant-exported LOCALE_ILANGUAGE 1)

(defconstant-exported LOCALE_SLANGUAGE 2)

(defconstant-exported LOCALE_SENGLANGUAGE #x1001)

(defconstant-exported LOCALE_SABBREVLANGNAME 3)

(defconstant-exported LOCALE_SNATIVELANGNAME 4)

(defconstant-exported LOCALE_ICOUNTRY 5)

(defconstant-exported LOCALE_SCOUNTRY 6)

(defconstant-exported LOCALE_SENGCOUNTRY #x1002)

(defconstant-exported LOCALE_SABBREVCTRYNAME 7)

(defconstant-exported LOCALE_SNATIVECTRYNAME 8)

(defconstant-exported LOCALE_IDEFAULTLANGUAGE 9)

(defconstant-exported LOCALE_IDEFAULTCOUNTRY 10)

(defconstant-exported LOCALE_IDEFAULTCODEPAGE 11)

(defconstant-exported LOCALE_IDEFAULTANSICODEPAGE #x1004)

(defconstant-exported LOCALE_SLIST 12)

(defconstant-exported LOCALE_IMEASURE 13)

(defconstant-exported LOCALE_SDECIMAL 14)

(defconstant-exported LOCALE_STHOUSAND 15)

(defconstant-exported LOCALE_SGROUPING 16)

(defconstant-exported LOCALE_IDIGITS 17)

(defconstant-exported LOCALE_ILZERO 18)

(defconstant-exported LOCALE_INEGNUMBER #x1010)

(defconstant-exported LOCALE_SNATIVEDIGITS 19)

(defconstant-exported LOCALE_SCURRENCY 20)

(defconstant-exported LOCALE_SINTLSYMBOL 21)

(defconstant-exported LOCALE_SMONDECIMALSEP 22)

(defconstant-exported LOCALE_SMONTHOUSANDSEP 23)

(defconstant-exported LOCALE_SMONGROUPING 24)

(defconstant-exported LOCALE_ICURRDIGITS 25)

(defconstant-exported LOCALE_IINTLCURRDIGITS 26)

(defconstant-exported LOCALE_ICURRENCY 27)

(defconstant-exported LOCALE_INEGCURR 28)

(defconstant-exported LOCALE_SDATE 29)

(defconstant-exported LOCALE_STIME 30)

(defconstant-exported LOCALE_SSHORTDATE 31)

(defconstant-exported LOCALE_SLONGDATE 32)

(defconstant-exported LOCALE_STIMEFORMAT #x1003)

(defconstant-exported LOCALE_IDATE 33)

(defconstant-exported LOCALE_ILDATE 34)

(defconstant-exported LOCALE_ITIME 35)

(defconstant-exported LOCALE_ITIMEMARKPOSN #x1005)

(defconstant-exported LOCALE_ICENTURY 36)

(defconstant-exported LOCALE_ITLZERO 37)

(defconstant-exported LOCALE_IDAYLZERO 38)

(defconstant-exported LOCALE_IMONLZERO 39)

(defconstant-exported LOCALE_S1159 40)

(defconstant-exported LOCALE_S2359 41)

(defconstant-exported LOCALE_ICALENDARTYPE #x1009)

(defconstant-exported LOCALE_IOPTIONALCALENDAR #x100B)

(defconstant-exported LOCALE_IFIRSTDAYOFWEEK #x100C)

(defconstant-exported LOCALE_IFIRSTWEEKOFYEAR #x100D)

(defconstant-exported LOCALE_SDAYNAME1 42)

(defconstant-exported LOCALE_SDAYNAME2 43)

(defconstant-exported LOCALE_SDAYNAME3 44)

(defconstant-exported LOCALE_SDAYNAME4 45)

(defconstant-exported LOCALE_SDAYNAME5 46)

(defconstant-exported LOCALE_SDAYNAME6 47)

(defconstant-exported LOCALE_SDAYNAME7 48)

(defconstant-exported LOCALE_SABBREVDAYNAME1 49)

(defconstant-exported LOCALE_SABBREVDAYNAME2 50)

(defconstant-exported LOCALE_SABBREVDAYNAME3 51)

(defconstant-exported LOCALE_SABBREVDAYNAME4 52)

(defconstant-exported LOCALE_SABBREVDAYNAME5 53)

(defconstant-exported LOCALE_SABBREVDAYNAME6 54)

(defconstant-exported LOCALE_SABBREVDAYNAME7 55)

(defconstant-exported LOCALE_SMONTHNAME1 56)

(defconstant-exported LOCALE_SMONTHNAME2 57)

(defconstant-exported LOCALE_SMONTHNAME3 58)

(defconstant-exported LOCALE_SMONTHNAME4 59)

(defconstant-exported LOCALE_SMONTHNAME5 60)

(defconstant-exported LOCALE_SMONTHNAME6 61)

(defconstant-exported LOCALE_SMONTHNAME7 62)

(defconstant-exported LOCALE_SMONTHNAME8 63)

(defconstant-exported LOCALE_SMONTHNAME9 64)

(defconstant-exported LOCALE_SMONTHNAME10 65)

(defconstant-exported LOCALE_SMONTHNAME11 66)

(defconstant-exported LOCALE_SMONTHNAME12 67)

(defconstant-exported LOCALE_SMONTHNAME13 #x100E)

(defconstant-exported LOCALE_SABBREVMONTHNAME1 68)

(defconstant-exported LOCALE_SABBREVMONTHNAME2 69)

(defconstant-exported LOCALE_SABBREVMONTHNAME3 70)

(defconstant-exported LOCALE_SABBREVMONTHNAME4 71)

(defconstant-exported LOCALE_SABBREVMONTHNAME5 72)

(defconstant-exported LOCALE_SABBREVMONTHNAME6 73)

(defconstant-exported LOCALE_SABBREVMONTHNAME7 74)

(defconstant-exported LOCALE_SABBREVMONTHNAME8 75)

(defconstant-exported LOCALE_SABBREVMONTHNAME9 76)

(defconstant-exported LOCALE_SABBREVMONTHNAME10 77)

(defconstant-exported LOCALE_SABBREVMONTHNAME11 78)

(defconstant-exported LOCALE_SABBREVMONTHNAME12 79)

(defconstant-exported LOCALE_SABBREVMONTHNAME13 #x100F)

(defconstant-exported LOCALE_SPOSITIVESIGN 80)

(defconstant-exported LOCALE_SNEGATIVESIGN 81)

(defconstant-exported LOCALE_IPOSSIGNPOSN 82)

(defconstant-exported LOCALE_INEGSIGNPOSN 83)

(defconstant-exported LOCALE_IPOSSYMPRECEDES 84)

(defconstant-exported LOCALE_IPOSSEPBYSPACE 85)

(defconstant-exported LOCALE_INEGSYMPRECEDES 86)

(defconstant-exported LOCALE_INEGSEPBYSPACE 87)

(defconstant-exported LOCALE_FONTSIGNATURE 88)

(defconstant-exported LOCALE_SISO639LANGNAME 89)

(defconstant-exported LOCALE_SISO3166CTRYNAME 90)

(defconstant-exported LOCALE_SYSTEM_DEFAULT #x800)

(defconstant-exported LOCALE_USER_DEFAULT #x400)

(defconstant-exported NORM_IGNORECASE 1)

(defconstant-exported NORM_IGNOREKANATYPE 65536)

(defconstant-exported NORM_IGNORENONSPACE 2)

(defconstant-exported NORM_IGNORESYMBOLS 4)

(defconstant-exported NORM_IGNOREWIDTH 131072)

(defconstant-exported SORT_STRINGSORT 4096)

(defconstant-exported LCMAP_LOWERCASE #x00000100)

(defconstant-exported LCMAP_UPPERCASE #x00000200)

(defconstant-exported LCMAP_SORTKEY #x00000400)

(defconstant-exported LCMAP_BYTEREV #x00000800)

(defconstant-exported LCMAP_HIRAGANA #x00100000)

(defconstant-exported LCMAP_KATAKANA #x00200000)

(defconstant-exported LCMAP_HALFWIDTH #x00400000)

(defconstant-exported LCMAP_FULLWIDTH #x00800000)

(defconstant-exported LCMAP_LINGUISTIC_CASING #x01000000)

(defconstant-exported LCMAP_SIMPLIFIED_CHINESE #x02000000)

(defconstant-exported LCMAP_TRADITIONAL_CHINESE #x04000000)

(defconstant-exported ENUM_ALL_CALENDARS -1)

(defconstant-exported DATE_SHORTDATE 1)

(defconstant-exported DATE_LONGDATE 2)

(defconstant-exported DATE_USE_ALT_CALENDAR 4)

(defconstant-exported CP_INSTALLED 1)

(defconstant-exported CP_SUPPORTED 2)

(defconstant-exported LCID_INSTALLED 1)

(defconstant-exported LCID_SUPPORTED 2)

(defconstant-exported LCID_ALTERNATE_SORTS 4)

(defconstant-exported MAP_FOLDCZONE 16)

(defconstant-exported MAP_FOLDDIGITS 128)

(defconstant-exported MAP_PRECOMPOSED 32)

(defconstant-exported MAP_COMPOSITE 64)

(defconstant-exported CP_ACP 0)

(defconstant-exported CP_OEMCP 1)

(defconstant-exported CP_MACCP 2)

(defconstant-exported CP_THREAD_ACP 3)

(defconstant-exported CP_SYMBOL 42)

(defconstant-exported CP_UTF7 65000)

(defconstant-exported CP_UTF8 65001)

(defconstant-exported CT_CTYPE1 1)

(defconstant-exported CT_CTYPE2 2)

(defconstant-exported CT_CTYPE3 4)

(defconstant-exported C1_UPPER 1)

(defconstant-exported C1_LOWER 2)

(defconstant-exported C1_DIGIT 4)

(defconstant-exported C1_SPACE 8)

(defconstant-exported C1_PUNCT 16)

(defconstant-exported C1_CNTRL 32)

(defconstant-exported C1_BLANK 64)

(defconstant-exported C1_XDIGIT 128)

(defconstant-exported C1_ALPHA 256)

(defconstant-exported C2_LEFTTORIGHT 1)

(defconstant-exported C2_RIGHTTOLEFT 2)

(defconstant-exported C2_EUROPENUMBER 3)

(defconstant-exported C2_EUROPESEPARATOR 4)

(defconstant-exported C2_EUROPETERMINATOR 5)

(defconstant-exported C2_ARABICNUMBER 6)

(defconstant-exported C2_COMMONSEPARATOR 7)

(defconstant-exported C2_BLOCKSEPARATOR 8)

(defconstant-exported C2_SEGMENTSEPARATOR 9)

(defconstant-exported C2_WHITESPACE 10)

(defconstant-exported C2_OTHERNEUTRAL 11)

(defconstant-exported C2_NOTAPPLICABLE 0)

(defconstant-exported C3_NONSPACING 1)

(defconstant-exported C3_DIACRITIC 2)

(defconstant-exported C3_VOWELMARK 4)

(defconstant-exported C3_SYMBOL 8)

(defconstant-exported C3_KATAKANA 16)

(defconstant-exported C3_HIRAGANA 32)

(defconstant-exported C3_HALFWIDTH 64)

(defconstant-exported C3_FULLWIDTH 128)

(defconstant-exported C3_IDEOGRAPH 256)

(defconstant-exported C3_KASHIDA 512)

(defconstant-exported C3_LEXICAL 1024)

(defconstant-exported C3_ALPHA 32768)

(defconstant-exported C3_NOTAPPLICABLE 0)

(defconstant-exported TIME_NOMINUTESORSECONDS 1)

(defconstant-exported TIME_NOSECONDS 2)

(defconstant-exported TIME_NOTIMEMARKER 4)

(defconstant-exported TIME_FORCE24HOURFORMAT 8)

(defconstant-exported MB_PRECOMPOSED 1)

(defconstant-exported MB_COMPOSITE 2)

(defconstant-exported MB_ERR_INVALID_CHARS 8)

(defconstant-exported MB_USEGLYPHCHARS 4)

(defconstant-exported WC_COMPOSITECHECK 512)

(defconstant-exported WC_DISCARDNS 16)

(defconstant-exported WC_SEPCHARS 32)

(defconstant-exported WC_DEFAULTCHAR 64)

(defconstant-exported CTRY_DEFAULT 0)

(defconstant-exported CTRY_ALBANIA 355)

(defconstant-exported CTRY_ALGERIA 213)

(defconstant-exported CTRY_ARGENTINA 54)

(defconstant-exported CTRY_ARMENIA 374)

(defconstant-exported CTRY_AUSTRALIA 61)

(defconstant-exported CTRY_AUSTRIA 43)

(defconstant-exported CTRY_AZERBAIJAN 994)

(defconstant-exported CTRY_BAHRAIN 973)

(defconstant-exported CTRY_BELARUS 375)

(defconstant-exported CTRY_BELGIUM 32)

(defconstant-exported CTRY_BELIZE 501)

(defconstant-exported CTRY_BOLIVIA 591)

(defconstant-exported CTRY_BRAZIL 55)

(defconstant-exported CTRY_BRUNEI_DARUSSALAM 673)

(defconstant-exported CTRY_BULGARIA 359)

(defconstant-exported CTRY_CANADA 2)

(defconstant-exported CTRY_CARIBBEAN 1)

(defconstant-exported CTRY_CHILE 56)

(defconstant-exported CTRY_COLOMBIA 57)

(defconstant-exported CTRY_COSTA_RICA 506)

(defconstant-exported CTRY_CROATIA 385)

(defconstant-exported CTRY_CZECH 420)

(defconstant-exported CTRY_DENMARK 45)

(defconstant-exported CTRY_DOMINICAN_REPUBLIC 1)

(defconstant-exported CTRY_ECUADOR 593)

(defconstant-exported CTRY_EGYPT 20)

(defconstant-exported CTRY_EL_SALVADOR 503)

(defconstant-exported CTRY_ESTONIA 372)

(defconstant-exported CTRY_FAEROE_ISLANDS 298)

(defconstant-exported CTRY_FINLAND 358)

(defconstant-exported CTRY_FRANCE 33)

(defconstant-exported CTRY_GEORGIA 995)

(defconstant-exported CTRY_GERMANY 49)

(defconstant-exported CTRY_GREECE 30)

(defconstant-exported CTRY_GUATEMALA 502)

(defconstant-exported CTRY_HONDURAS 504)

(defconstant-exported CTRY_HONG_KONG 852)

(defconstant-exported CTRY_HUNGARY 36)

(defconstant-exported CTRY_ICELAND 354)

(defconstant-exported CTRY_INDIA 91)

(defconstant-exported CTRY_INDONESIA 62)

(defconstant-exported CTRY_IRAN 981)

(defconstant-exported CTRY_IRAQ 964)

(defconstant-exported CTRY_IRELAND 353)

(defconstant-exported CTRY_ISRAEL 972)

(defconstant-exported CTRY_ITALY 39)

(defconstant-exported CTRY_JAMAICA 1)

(defconstant-exported CTRY_JAPAN 81)

(defconstant-exported CTRY_JORDAN 962)

(defconstant-exported CTRY_KAZAKSTAN 7)

(defconstant-exported CTRY_KENYA 254)

(defconstant-exported CTRY_KUWAIT 965)

(defconstant-exported CTRY_KYRGYZSTAN 996)

(defconstant-exported CTRY_LATVIA 371)

(defconstant-exported CTRY_LEBANON 961)

(defconstant-exported CTRY_LIBYA 218)

(defconstant-exported CTRY_LIECHTENSTEIN 41)

(defconstant-exported CTRY_LITHUANIA 370)

(defconstant-exported CTRY_LUXEMBOURG 352)

(defconstant-exported CTRY_MACAU 853)

(defconstant-exported CTRY_MACEDONIA 389)

(defconstant-exported CTRY_MALAYSIA 60)

(defconstant-exported CTRY_MALDIVES 960)

(defconstant-exported CTRY_MEXICO 52)

(defconstant-exported CTRY_MONACO 33)

(defconstant-exported CTRY_MONGOLIA 976)

(defconstant-exported CTRY_MOROCCO 212)

(defconstant-exported CTRY_NETHERLANDS 31)

(defconstant-exported CTRY_NEW_ZEALAND 64)

(defconstant-exported CTRY_NICARAGUA 505)

(defconstant-exported CTRY_NORWAY 47)

(defconstant-exported CTRY_OMAN 968)

(defconstant-exported CTRY_PAKISTAN 92)

(defconstant-exported CTRY_PANAMA 507)

(defconstant-exported CTRY_PARAGUAY 595)

(defconstant-exported CTRY_PERU 51)

(defconstant-exported CTRY_PHILIPPINES 63)

(defconstant-exported CTRY_POLAND 48)

(defconstant-exported CTRY_PORTUGAL 351)

(defconstant-exported CTRY_PRCHINA 86)

(defconstant-exported CTRY_PUERTO_RICO 1)

(defconstant-exported CTRY_QATAR 974)

(defconstant-exported CTRY_ROMANIA 40)

(defconstant-exported CTRY_RUSSIA 7)

(defconstant-exported CTRY_SAUDI_ARABIA 966)

(defconstant-exported CTRY_SERBIA 381)

(defconstant-exported CTRY_SINGAPORE 65)

(defconstant-exported CTRY_SLOVAK 421)

(defconstant-exported CTRY_SLOVENIA 386)

(defconstant-exported CTRY_SOUTH_AFRICA 27)

(defconstant-exported CTRY_SOUTH_KOREA 82)

(defconstant-exported CTRY_SPAIN 34)

(defconstant-exported CTRY_SWEDEN 46)

(defconstant-exported CTRY_SWITZERLAND 41)

(defconstant-exported CTRY_SYRIA 963)

(defconstant-exported CTRY_TAIWAN 886)

(defconstant-exported CTRY_TATARSTAN 7)

(defconstant-exported CTRY_THAILAND 66)

(defconstant-exported CTRY_TRINIDAD_Y_TOBAGO 1)

(defconstant-exported CTRY_TUNISIA 216)

(defconstant-exported CTRY_TURKEY 90)

(defconstant-exported CTRY_UAE 971)

(defconstant-exported CTRY_UKRAINE 380)

(defconstant-exported CTRY_UNITED_KINGDOM 44)

(defconstant-exported CTRY_UNITED_STATES 1)

(defconstant-exported CTRY_URUGUAY 598)

(defconstant-exported CTRY_UZBEKISTAN 7)

(defconstant-exported CTRY_VENEZUELA 58)

(defconstant-exported CTRY_VIET_NAM 84)

(defconstant-exported CTRY_YEMEN 967)

(defconstant-exported CTRY_ZIMBABWE 263)

(defconstant-exported CAL_ICALINTVALUE 1)

(defconstant-exported CAL_SCALNAME 2)

(defconstant-exported CAL_IYEAROFFSETRANGE 3)

(defconstant-exported CAL_SERASTRING 4)

(defconstant-exported CAL_SSHORTDATE 5)

(defconstant-exported CAL_SLONGDATE 6)

(defconstant-exported CAL_SDAYNAME1 7)

(defconstant-exported CAL_SDAYNAME2 8)

(defconstant-exported CAL_SDAYNAME3 9)

(defconstant-exported CAL_SDAYNAME4 10)

(defconstant-exported CAL_SDAYNAME5 11)

(defconstant-exported CAL_SDAYNAME6 12)

(defconstant-exported CAL_SDAYNAME7 13)

(defconstant-exported CAL_SABBREVDAYNAME1 14)

(defconstant-exported CAL_SABBREVDAYNAME2 15)

(defconstant-exported CAL_SABBREVDAYNAME3 16)

(defconstant-exported CAL_SABBREVDAYNAME4 17)

(defconstant-exported CAL_SABBREVDAYNAME5 18)

(defconstant-exported CAL_SABBREVDAYNAME6 19)

(defconstant-exported CAL_SABBREVDAYNAME7 20)

(defconstant-exported CAL_SMONTHNAME1 21)

(defconstant-exported CAL_SMONTHNAME2 22)

(defconstant-exported CAL_SMONTHNAME3 23)

(defconstant-exported CAL_SMONTHNAME4 24)

(defconstant-exported CAL_SMONTHNAME5 25)

(defconstant-exported CAL_SMONTHNAME6 26)

(defconstant-exported CAL_SMONTHNAME7 27)

(defconstant-exported CAL_SMONTHNAME8 28)

(defconstant-exported CAL_SMONTHNAME9 29)

(defconstant-exported CAL_SMONTHNAME10 30)

(defconstant-exported CAL_SMONTHNAME11 31)

(defconstant-exported CAL_SMONTHNAME12 32)

(defconstant-exported CAL_SMONTHNAME13 33)

(defconstant-exported CAL_SABBREVMONTHNAME1 34)

(defconstant-exported CAL_SABBREVMONTHNAME2 35)

(defconstant-exported CAL_SABBREVMONTHNAME3 36)

(defconstant-exported CAL_SABBREVMONTHNAME4 37)

(defconstant-exported CAL_SABBREVMONTHNAME5 38)

(defconstant-exported CAL_SABBREVMONTHNAME6 39)

(defconstant-exported CAL_SABBREVMONTHNAME7 40)

(defconstant-exported CAL_SABBREVMONTHNAME8 41)

(defconstant-exported CAL_SABBREVMONTHNAME9 42)

(defconstant-exported CAL_SABBREVMONTHNAME10 43)

(defconstant-exported CAL_SABBREVMONTHNAME11 44)

(defconstant-exported CAL_SABBREVMONTHNAME12 45)

(defconstant-exported CAL_SABBREVMONTHNAME13 46)

(defconstant-exported CAL_GREGORIAN 1)

(defconstant-exported CAL_GREGORIAN_US 2)

(defconstant-exported CAL_JAPAN 3)

(defconstant-exported CAL_TAIWAN 4)

(defconstant-exported CAL_KOREA 5)

(defconstant-exported CAL_HIJRI 6)

(defconstant-exported CAL_THAI 7)

(defconstant-exported CAL_HEBREW 8)

(defconstant-exported CAL_GREGORIAN_ME_FRENCH 9)

(defconstant-exported CAL_GREGORIAN_ARABIC 10)

(defconstant-exported CAL_GREGORIAN_XLIT_ENGLISH 11)

(defconstant-exported CAL_GREGORIAN_XLIT_FRENCH 12)

(defconstant-exported CSTR_LESS_THAN 1)

(defconstant-exported CSTR_EQUAL 2)

(defconstant-exported CSTR_GREATER_THAN 3)

(defconstant-exported LGRPID_INSTALLED 1)

(defconstant-exported LGRPID_SUPPORTED 2)

(defconstant-exported LGRPID_WESTERN_EUROPE 1)

(defconstant-exported LGRPID_CENTRAL_EUROPE 2)

(defconstant-exported LGRPID_BALTIC 3)

(defconstant-exported LGRPID_GREEK 4)

(defconstant-exported LGRPID_CYRILLIC 5)

(defconstant-exported LGRPID_TURKISH 6)

(defconstant-exported LGRPID_JAPANESE 7)

(defconstant-exported LGRPID_KOREAN 8)

(defconstant-exported LGRPID_TRADITIONAL_CHINESE 9)

(defconstant-exported LGRPID_SIMPLIFIED_CHINESE 10)

(defconstant-exported LGRPID_THAI 11)

(defconstant-exported LGRPID_HEBREW 12)

(defconstant-exported LGRPID_ARABIC 13)

(defconstant-exported LGRPID_VIETNAMESE 14)

(defconstant-exported LGRPID_INDIC 15)

(defconstant-exported LGRPID_GEORGIAN 16)

(defconstant-exported LGRPID_ARMENIAN 17)

























































(cffi:defcenum NLS_FUNCTION
        (:COMPARE_STRING #.#x0001))

(cffi:defcenum SYSGEOCLASS
        (:GEOCLASS_NATION #.16)
        (:GEOCLASS_REGION #.14))

(cffi:defcenum SYSGEOTYPE
        (:GEO_NATION #.#x0001)
        (:GEO_LATITUDE #.#x0002)
        (:GEO_LONGITUDE #.#x0003)
        (:GEO_ISO2 #.#x0004)
        (:GEO_ISO3 #.#x0005)
        (:GEO_RFC1766 #.#x0006)
        (:GEO_LCID #.#x0007)
        (:GEO_FRIENDLYNAME #.#x0008)
        (:GEO_OFFICIALNAME #.#x0009)
        (:GEO_TIMEZONES #.#x000a)
        (:GEO_OFFICIALLANGUAGES #.#x000a))

(defcstructex-exported CPINFO
        (MaxCharSize :unsigned-int)
        (DefaultChar :pointer)
        (LeadByte :pointer))





(defcstructex-exported CPINFOEXA
        (MaxCharSize :unsigned-int)
        (DefaultChar :pointer)
        (LeadByte :pointer)
        (UnicodeDefaultChar :pointer)
        (CodePage :unsigned-int)
        (CodePageName :pointer))





(defcstructex-exported CPINFOEXW
        (MaxCharSize :unsigned-int)
        (DefaultChar :pointer)
        (LeadByte :pointer)
        (UnicodeDefaultChar :pointer)
        (CodePage :unsigned-int)
        (CodePageName :pointer))





(defcstructex-exported CURRENCYFMTA
        (NumDigits :unsigned-int)
        (LeadingZero :unsigned-int)
        (Grouping :unsigned-int)
        (lpDecimalSep :string)
        (lpThousandSep :string)
        (NegativeOrder :unsigned-int)
        (PositiveOrder :unsigned-int)
        (lpCurrencySymbol :string))





(defcstructex-exported CURRENCYFMTW
        (NumDigits :unsigned-int)
        (LeadingZero :unsigned-int)
        (Grouping :unsigned-int)
        (lpDecimalSep :pointer)
        (lpThousandSep :pointer)
        (NegativeOrder :unsigned-int)
        (PositiveOrder :unsigned-int)
        (lpCurrencySymbol :pointer))





(defcstructex-exported NLSVERSIONINFO
        (dwNLSVersionInfoSize :unsigned-long)
        (dwNLSVersion :unsigned-long)
        (dwDefinedVersion :unsigned-long))





(defcstructex-exported NUMBERFMTA
        (NumDigits :unsigned-int)
        (LeadingZero :unsigned-int)
        (Grouping :unsigned-int)
        (lpDecimalSep :string)
        (lpThousandSep :string)
        (NegativeOrder :unsigned-int))





(defcstructex-exported NUMBERFMTW
        (NumDigits :unsigned-int)
        (LeadingZero :unsigned-int)
        (Grouping :unsigned-int)
        (lpDecimalSep :pointer)
        (lpThousandSep :pointer)
        (NegativeOrder :unsigned-int))





(defcfunex-exported ("EnumSystemGeoID" EnumSystemGeoID :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetCalendarInfoA" GetCalendarInfoA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :string)
  (arg4 :int)
  (arg5 :pointer))

(defcfunex-exported ("GetCalendarInfoW" GetCalendarInfoW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :pointer))

(defcfunex-exported ("GetGeoInfoA" GetGeoInfoA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int)
  (arg4 :unsigned-short))

(defcfunex-exported ("GetGeoInfoW" GetGeoInfoW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :unsigned-short))

(defcfunex-exported ("GetLocaleInfoA" GetLocaleInfoA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :int))

(defcfunex-exported ("GetLocaleInfoW" GetLocaleInfoW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("GetNLSVersion" GetNLSVersion :convention :stdcall) :int
  (arg0 NLS_FUNCTION)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetSystemDefaultLangID" GetSystemDefaultLangID :convention :stdcall) :unsigned-short)

(defcfunex-exported ("GetSystemDefaultLCID" GetSystemDefaultLCID :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetThreadLocale" GetThreadLocale :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetTimeFormatA" GetTimeFormatA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :string)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("GetTimeFormatW" GetTimeFormatW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("GetUserDefaultLangID" GetUserDefaultLangID :convention :stdcall) :unsigned-short)

(defcfunex-exported ("GetUserDefaultLCID" GetUserDefaultLCID :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetUserGeoID" GetUserGeoID :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("IsDBCSLeadByte" IsDBCSLeadByte :convention :stdcall) :int
  (arg0 :unsigned-char))

(defcfunex-exported ("IsNLSDefinedString" IsNLSDefinedString :convention :stdcall) :int
  (arg0 NLS_FUNCTION)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int))

(defcfunex-exported ("SetCalendarInfoA" SetCalendarInfoA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :string))

(defcfunex-exported ("SetCalendarInfoW" SetCalendarInfoW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("SetLocaleInfoA" SetLocaleInfoA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string))

(defcfunex-exported ("SetLocaleInfoW" SetLocaleInfoW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetThreadLocale" SetThreadLocale :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("SetUserGeoID" SetUserGeoID :convention :stdcall) :int
  (arg0 :unsigned-long))




