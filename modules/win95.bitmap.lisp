
; TESTED
(defcfunex-exported ("BitBlt" BitBlt :convention :stdcall) :int
  (hdcDest :pointer)
  (nXDest :int)
  (nYDest :int)
  (nWidth :int)
  (nHeight :int)
  (hdcSrc :pointer)
  (nXSrc :int)
  (nYSrc :int)
  (dwRop :unsigned-long))

(defcfunex-exported ("CreateBitmap" CreateBitmap :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("CreateBitmapIndirect" CreateBitmapIndirect :convention :stdcall) :pointer
  (arg0 :pointer))

; TESTED
(defcfunex-exported ("CreateCompatibleBitmap" CreateCompatibleBitmap :convention :stdcall) :pointer
  (hdc :pointer)
  (nWidth :int)
  (nHeight :int))

(defcfunex-exported ("CreateDIBitmap" CreateDIBitmap :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-int))

(defcfunex-exported ("CreateDIBSection" CreateDIBSection :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("CreateDiscardableBitmap" CreateDiscardableBitmap :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("ExtFloodFill" ExtFloodFill :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :unsigned-int))

(defcfunex-exported ("FloodFill" FloodFill :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("GetBitmapBits" GetBitmapBits :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :pointer))

(defcfunex-exported ("GetBitmapDimensionEx" GetBitmapDimensionEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetDIBColorTable" GetDIBColorTable :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetDIBits" GetDIBits :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-int))

(defcfunex-exported ("GetPixel" GetPixel :convention :stdcall) :unsigned-long
  (hdc :pointer)
  (nXPos :int)
  (nYPos :int))

(defcfunex-exported ("GetStretchBltMode" GetStretchBltMode :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("LoadBitmapA" LoadBitmapA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("LoadBitmapW" LoadBitmapW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("MaskBlt" MaskBlt :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :pointer)
  (arg6 :int)
  (arg7 :int)
  (arg8 :pointer)
  (arg9 :int)
  (arg10 :int)
  (arg11 :unsigned-long))

(defcfunex-exported ("PatBlt" PatBlt :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :unsigned-long))

(defcfunex-exported ("PlgBlt" PlgBlt :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :pointer)
  (arg8 :int)
  (arg9 :int))

(defcfunex-exported ("SetBitmapBits" SetBitmapBits :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetBitmapDimensionEx" SetBitmapDimensionEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("SetDIBColorTable" SetDIBColorTable :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("SetDIBits" SetDIBits :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-int))

(defcfunex-exported ("SetDIBitsToDevice" SetDIBitsToDevice :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :int)
  (arg6 :int)
  (arg7 :unsigned-int)
  (arg8 :unsigned-int)
  (arg9 :pointer)
  (arg10 :pointer)
  (arg11 :unsigned-int))

(defcfunex-exported ("SetPixel" SetPixel :convention :stdcall) :unsigned-long
  (hdc :pointer)
  (X :int)
  (Y :int)
  (crColor :unsigned-long))

(defcfunex-exported ("SetPixelV" SetPixelV :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetStretchBltMode" SetStretchBltMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("StretchBlt" StretchBlt :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :pointer)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int)
  (arg9 :int)
  (arg10 :unsigned-long))

(defcfunex-exported ("StretchDIBits" StretchDIBits :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int)
  (arg9 :pointer)
  (arg10 :pointer)
  (arg11 :unsigned-int)
  (arg12 :unsigned-long))

