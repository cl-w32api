
(cl:in-package w32apimod)

(define-w32api-module lz :lz)

(cl:in-package cl-w32api.module.lz)



(defconstant-exported LZERROR_BADINHANDLE -1)

(defconstant-exported LZERROR_BADOUTHANDLE -2)

(defconstant-exported LZERROR_READ -3)

(defconstant-exported LZERROR_WRITE -4)

(defconstant-exported LZERROR_GLOBALLOC -5)

(defconstant-exported LZERROR_GLOBLOCK -6)

(defconstant-exported LZERROR_BADVALUE -7)

(defconstant-exported LZERROR_UNKNOWNALG -8)

(defcfunex-exported ("CopyLZFile" CopyLZFile :convention :stdcall) :int32
  (arg0 :int)
  (arg1 :int))

(defcfunex-exported ("GetExpandedNameA" GetExpandedNameA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("GetExpandedNameW" GetExpandedNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("LZClose" LZClose :convention :stdcall) :void
  (arg0 :int))

(defcfunex-exported ("LZCopy" LZCopy :convention :stdcall) :int32
  (arg0 :int)
  (arg1 :int))

(defcfunex-exported ("LZDone" LZDone :convention :stdcall) :void)

(defcfunex-exported ("LZInit" LZInit :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("LZOpenFileA" LZOpenFileA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-short))

(defcfunex-exported ("LZOpenFileW" LZOpenFileW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-short))

(defcfunex-exported ("LZRead" LZRead :convention :stdcall) :int
  (arg0 :int)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("LZSeek" LZSeek :convention :stdcall) :int32
  (arg0 :int)
  (arg1 :int32)
  (arg2 :int))

(defcfunex-exported ("LZStart" LZStart :convention :stdcall) :int)


