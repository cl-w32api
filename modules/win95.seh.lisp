

(defcfunex-exported ("RaiseException" RaiseException :convention :stdcall) :void
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("SetUnhandledExceptionFilter" SetUnhandledExceptionFilter :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("UnhandledExceptionFilter" UnhandledExceptionFilter :convention :stdcall) :int32
  (arg0 :pointer))

