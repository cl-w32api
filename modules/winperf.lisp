(cl:in-package w32apimod)

(define-w32api-module winperf :winperf)

(cl:in-package cl-w32api.module.winperf)



(defconstant-exported PERF_DATA_VERSION 1)

(defconstant-exported PERF_DATA_REVISION 1)

(defconstant-exported PERF_NO_INSTANCES -1)

(defconstant-exported PERF_SIZE_DWORD 0)

(defconstant-exported PERF_SIZE_LARGE 256)

(defconstant-exported PERF_SIZE_ZERO 512)

(defconstant-exported PERF_SIZE_VARIABLE_LEN 768)

(defconstant-exported PERF_TYPE_NUMBER 0)

(defconstant-exported PERF_TYPE_COUNTER 1024)

(defconstant-exported PERF_TYPE_TEXT 2048)

(defconstant-exported PERF_TYPE_ZERO #xC00)

(defconstant-exported PERF_NUMBER_HEX 0)

(defconstant-exported PERF_NUMBER_DECIMAL #x10000)

(defconstant-exported PERF_NUMBER_DEC_1000 #x20000)

(defconstant-exported PERF_COUNTER_VALUE 0)

(defconstant-exported PERF_COUNTER_RATE #x10000)

(defconstant-exported PERF_COUNTER_FRACTION #x20000)

(defconstant-exported PERF_COUNTER_BASE #x30000)

(defconstant-exported PERF_COUNTER_ELAPSED #x40000)

(defconstant-exported PERF_COUNTER_QUEUELEN #x50000)

(defconstant-exported PERF_COUNTER_HISTOGRAM #x60000)

(defconstant-exported PERF_TEXT_UNICODE 0)

(defconstant-exported PERF_TEXT_ASCII #x10000)

(defconstant-exported PERF_TIMER_TICK 0)

(defconstant-exported PERF_TIMER_100NS #x100000)

(defconstant-exported PERF_OBJECT_TIMER #x200000)

(defconstant-exported PERF_DELTA_COUNTER #x400000)

(defconstant-exported PERF_DELTA_BASE #x800000)

(defconstant-exported PERF_INVERSE_COUNTER #x1000000)

(defconstant-exported PERF_MULTI_COUNTER #x2000000)

(defconstant-exported PERF_DISPLAY_NO_SUFFIX 0)

(defconstant-exported PERF_DISPLAY_PER_SEC #x10000000)

(defconstant-exported PERF_DISPLAY_PERCENT #x20000000)

(defconstant-exported PERF_DISPLAY_SECONDS #x30000000)

(defconstant-exported PERF_DISPLAY_NOSHOW #x40000000)

(defconstant-exported PERF_COUNTER_HISTOGRAM_TYPE #x80000000)

(defconstant-exported PERF_NO_UNIQUE_ID -1)

(defconstant-exported PERF_DETAIL_NOVICE 100)

(defconstant-exported PERF_DETAIL_ADVANCED 200)

(defconstant-exported PERF_DETAIL_EXPERT 300)

(defconstant-exported PERF_DETAIL_WIZARD 400)

(defconstant-exported PERF_COUNTER_COUNTER (cl:logior 0 1024 #x10000 0 #x400000 #x10000000))

(defconstant-exported PERF_COUNTER_TIMER (cl:logior 256 1024 #x10000 0 #x400000 #x20000000))

(defconstant-exported PERF_COUNTER_QUEUELEN_TYPE (cl:logior 0 1024 #x50000 0 #x400000 0))

(defconstant-exported PERF_COUNTER_BULK_COUNT (cl:logior 256 1024 #x10000 0 #x400000 #x10000000))

(defconstant-exported PERF_COUNTER_TEXT (cl:logior 768 2048 0 0))

(defconstant-exported PERF_COUNTER_RAWCOUNT (cl:logior 0 0 #x10000 0))

(defconstant-exported PERF_COUNTER_LARGE_RAWCOUNT (cl:logior 256 0 #x10000 0))

(defconstant-exported PERF_COUNTER_RAWCOUNT_HEX (cl:logior 0 0 0 0))

(defconstant-exported PERF_COUNTER_LARGE_RAWCOUNT_HEX (cl:logior 256 0 0 0))

(defconstant-exported PERF_SAMPLE_FRACTION (cl:logior 0 1024 #x20000 #x400000 #x800000 #x20000000))

(defconstant-exported PERF_SAMPLE_COUNTER (cl:logior 0 1024 #x10000 0 #x400000 0))

(defconstant-exported PERF_COUNTER_NODATA (cl:logior 512 #x40000000))

(defconstant-exported PERF_COUNTER_TIMER_INV (cl:logior 256 1024 #x10000 0 #x400000 #x1000000 #x20000000))

(defconstant-exported PERF_SAMPLE_BASE (cl:logior 0 1024 #x30000 #x40000000 1))

(defconstant-exported PERF_AVERAGE_TIMER (cl:logior 0 1024 #x20000 #x30000000))

(defconstant-exported PERF_AVERAGE_BASE (cl:logior 0 1024 #x30000 #x40000000 2))

(defconstant-exported PERF_AVERAGE_BULK (cl:logior 256 1024 #x20000 #x40000000))

(defconstant-exported PERF_100NSEC_TIMER (cl:logior 256 1024 #x10000 #x100000 #x400000 #x20000000))

(defconstant-exported PERF_100NSEC_TIMER_INV (cl:logior 256 1024 #x10000 #x100000 #x400000 #x1000000 #x20000000))

(defconstant-exported PERF_COUNTER_MULTI_TIMER (cl:logior 256 1024 #x10000 #x400000 0 #x2000000 #x20000000))

(defconstant-exported PERF_COUNTER_MULTI_TIMER_INV (cl:logior 256 1024 #x10000 #x400000 #x2000000 0 #x1000000 #x20000000))

(defconstant-exported PERF_COUNTER_MULTI_BASE (cl:logior 256 1024 #x30000 #x2000000 #x40000000))

(defconstant-exported PERF_100NSEC_MULTI_TIMER (cl:logior 256 1024 #x400000 #x10000 #x100000 #x2000000 #x20000000))

(defconstant-exported PERF_100NSEC_MULTI_TIMER_INV (cl:logior 256 1024 #x400000 #x10000 #x100000 #x2000000 #x1000000 #x20000000))

(defconstant-exported PERF_RAW_FRACTION (cl:logior 0 1024 #x20000 #x20000000))

(defconstant-exported PERF_RAW_BASE (cl:logior 0 1024 #x30000 #x40000000 3))

(defconstant-exported PERF_ELAPSED_TIME (cl:logior 256 1024 #x40000 #x200000 #x30000000))

(defcstructex-exported PERF_DATA_BLOCK
        (Signature :pointer)
        (LittleEndian :unsigned-long)
        (Version :unsigned-long)
        (Revision :unsigned-long)
        (TotalByteLength :unsigned-long)
        (HeaderLength :unsigned-long)
        (NumObjectTypes :unsigned-long)
        (DefaultObject :int32)
        (SystemTime SYSTEMTIME)
        (PerfTime LARGE_INTEGER)
        (PerfFreq LARGE_INTEGER)
        (PerfTime100nSec LARGE_INTEGER)
        (SystemNameLength :unsigned-long)
        (SystemNameOffset :unsigned-long))





(defcstructex-exported PERF_OBJECT_TYPE
        (TotalByteLength :unsigned-long)
        (DefinitionLength :unsigned-long)
        (HeaderLength :unsigned-long)
        (ObjectNameTitleIndex :unsigned-long)
        (ObjectNameTitle :pointer)
        (ObjectHelpTitleIndex :unsigned-long)
        (ObjectHelpTitle :pointer)
        (DetailLevel :unsigned-long)
        (NumCounters :unsigned-long)
        (DefaultCounter :int32)
        (NumInstances :int32)
        (CodePage :unsigned-long)
        (PerfTime LARGE_INTEGER)
        (PerfFreq LARGE_INTEGER))





(defcstructex-exported PERF_COUNTER_DEFINITION
        (ByteLength :unsigned-long)
        (CounterNameTitleIndex :unsigned-long)
        (CounterNameTitle :pointer)
        (CounterHelpTitleIndex :unsigned-long)
        (CounterHelpTitle :pointer)
        (DefaultScale :int32)
        (DetailLevel :unsigned-long)
        (CounterType :unsigned-long)
        (CounterSize :unsigned-long)
        (CounterOffset :unsigned-long))





(defcstructex-exported PERF_INSTANCE_DEFINITION
        (ByteLength :unsigned-long)
        (ParentObjectTitleIndex :unsigned-long)
        (ParentObjectInstance :unsigned-long)
        (UniqueID :int32)
        (NameOffset :unsigned-long)
        (NameLength :unsigned-long))





(defcstructex-exported PERF_COUNTER_BLOCK
        (ByteLength :unsigned-long))





