
(cl:in-package w32apimod)

(define-w32api-module win95.~ :win95.~)

(cl:in-package cl-w32api.module.win95.~)

(defctype-exported VOID :void)

(defctype-exported BOOL :boolean)
(defctype-exported DWORD :unsigned-long)

(defctype-exported UINT :unsigned-int)
(defctype-exported LONG :int32)

(defctype-exported WPARAM  :unsigned-int)
(defctype-exported LPARAM  :int32)
(defctype-exported LRESULT :int32)
(defctype-exported HRESULT :int32)

(defctype-exported COLORREF :unsigned-long)
(defctype-exported COLOR16 :unsigned-short)

(defctype-exported SIZE_T  :unsigned-long)

(defctype-exported ATOM_T  :unsigned-short)


(defctype-exported HACCEL       HANDLE)
(defctype-exported HBITMAP      HANDLE)
(defctype-exported HBRUSH     HANDLE)
(defctype-exported HCOLORSPACE     HANDLE)
(defctype-exported HCURSOR     HANDLE)
(defctype-exported HDC     HANDLE)
(defctype-exported HGLRC     HANDLE)
(defctype-exported HDESK     HANDLE)
(defctype-exported HENHMETAFILE     HANDLE)
(defctype-exported HFILE     HANDLE)
(defctype-exported HFONT     HANDLE)
(defctype-exported HICON     HANDLE)
(defctype-exported HKEY     HANDLE)
(defctype-exported HMONITOR     HANDLE)
(defctype-exported HTERMINAL     HANDLE)
(defctype-exported HWINEVENTHOOK     HANDLE)
(defctype-exported HMENU     HANDLE)
(defctype-exported HMETAFILE     HANDLE)
(defctype-exported HMODULE     HANDLE)
(defctype-exported HINSTANCE HANDLE)
(defctype-exported HPALETTE     HANDLE)
(defctype-exported HPEN     HANDLE)
(defctype-exported HRGN     HANDLE)
(defctype-exported HRSRC     HANDLE)
(defctype-exported HSTR     HANDLE)
(defctype-exported HTASK     HANDLE)
(defctype-exported HWND      HANDLE)
(defctype-exported HWINSTA     HANDLE)
(defctype-exported HKL     HANDLE)

(defcstructex-exported POINT
        (x LONG)
        (y LONG))

(defcstructex-exported RECT
                 (left   LONG)
                 (top    LONG)
                 (right  LONG)
                 (bottom LONG))




(export '(cl-w32api.types::astring
	  cl-w32api.types::wstring
	  cl-w32api.types::handle
	  ))
;;(defctype-exported ASTRING :string)
;;(defctype-exported WSTRING :string)


;;(defconstant-exported %nullptr (null-pointer))

(defmacro-exported MAKEINTRESOURCE (x)
  (make-pointer x))
