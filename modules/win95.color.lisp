
(cl:in-package w32apimod)

(define-w32api-module win95.color :win95.color)

(cl:in-package cl-w32api.module.win95.color)

(defconstant-exported COLOR_3DDKSHADOW 21)
(defconstant-exported COLOR_3DFACE 15)
(defconstant-exported COLOR_3DHILIGHT 20)
(defconstant-exported COLOR_3DHIGHLIGHT 20)
(defconstant-exported COLOR_3DLIGHT 22)
(defconstant-exported COLOR_BTNHILIGHT 20)
(defconstant-exported COLOR_3DSHADOW 16)
(defconstant-exported COLOR_ACTIVEBORDER 10)
(defconstant-exported COLOR_ACTIVECAPTION 2)
(defconstant-exported COLOR_APPWORKSPACE 12)
(defconstant-exported COLOR_BACKGROUND 1)
(defconstant-exported COLOR_DESKTOP 1)
(defconstant-exported COLOR_BTNFACE 15)
(defconstant-exported COLOR_BTNHIGHLIGHT 20)
(defconstant-exported COLOR_BTNSHADOW 16)
(defconstant-exported COLOR_BTNTEXT 18)
(defconstant-exported COLOR_CAPTIONTEXT 9)
(defconstant-exported COLOR_GRAYTEXT 17)
(defconstant-exported COLOR_HIGHLIGHT 13)
(defconstant-exported COLOR_HIGHLIGHTTEXT 14)
(defconstant-exported COLOR_INACTIVEBORDER 11)
(defconstant-exported COLOR_INACTIVECAPTION 3)
(defconstant-exported COLOR_INACTIVECAPTIONTEXT 19)
(defconstant-exported COLOR_INFOBK 24)
(defconstant-exported COLOR_INFOTEXT 23)
(defconstant-exported COLOR_MENU 4)
(defconstant-exported COLOR_MENUTEXT 7)
(defconstant-exported COLOR_SCROLLBAR 0)
(defconstant-exported COLOR_WINDOW 5)
(defconstant-exported COLOR_WINDOWFRAME 6)
(defconstant-exported COLOR_WINDOWTEXT 8)
(defconstant-exported COLOR_HOTLIGHT 26)
(defconstant-exported COLOR_GRADIENTACTIVECAPTION 27)
(defconstant-exported COLOR_GRADIENTINACTIVECAPTION 28)

(defcfunex-exported ("AnimatePalette" AnimatePalette :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("CreateHalftonePalette" CreateHalftonePalette :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreatePalette" CreatePalette :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetColorAdjustment" GetColorAdjustment :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetNearestColor" GetNearestColor :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetNearestPaletteIndex" GetNearestPaletteIndex :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetPaletteEntries" GetPaletteEntries :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetSystemPaletteEntries" GetSystemPaletteEntries :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetSystemPaletteUse" GetSystemPaletteUse :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("RealizePalette" RealizePalette :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("ResizePalette" ResizePalette :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("SelectPalette" SelectPalette :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("SetColorAdjustment" SetColorAdjustment :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetPaletteEntries" SetPaletteEntries :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("SetSystemPaletteUse" SetSystemPaletteUse :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("UnrealizeObject" UnrealizeObject :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("UpdateColors" UpdateColors :convention :stdcall) :int
  (arg0 :pointer))



