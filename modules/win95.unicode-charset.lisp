(defcfunex-exported ("GetTextCharset" GetTextCharset :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetTextCharsetInfo" GetTextCharsetInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("IsDBCSLeadByteEx" IsDBCSLeadByteEx :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-char))

(defcfunex-exported ("TranslateCharsetInfo" TranslateCharsetInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

