(defcfunex-exported ("AbortPath" AbortPath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("BeginPath" BeginPath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CloseFigure" CloseFigure :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EndPath" EndPath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FillPath" FillPath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("FlattenPath" FlattenPath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetMiterLimit" GetMiterLimit :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetPath" GetPath :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("PathToRegion" PathToRegion :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("SetMiterLimit" SetMiterLimit :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :float)
  (arg2 :pointer))

(defcfunex-exported ("StrokeAndFillPath" StrokeAndFillPath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("StrokePath" StrokePath :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WidenPath" WidenPath :convention :stdcall) :int
  (arg0 :pointer))

