
(cl:in-package w32apimod)

(define-w32api-module win95.keyboard-input :win95.keyboard-input)

(cl:in-package cl-w32api.module.win95.keyboard-input)

(defcfunex-exported ("ActivateKeyboardLayout" ActivateKeyboardLayout :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetActiveWindow" GetActiveWindow :convention :stdcall) :pointer)

(defcfunex-exported ("GetAsyncKeyState" GetAsyncKeyState :convention :stdcall) :short
  (arg0 :int))

(defcfunex-exported ("GetKeyboardLayout" GetKeyboardLayout :convention :stdcall) :pointer
  (arg0 :unsigned-long))

(defcfunex-exported ("GetKeyboardLayoutList" GetKeyboardLayoutList :convention :stdcall) :unsigned-int
  (arg0 :int)
  (arg1 :pointer))

(defcfunex-exported ("GetKeyboardLayoutNameA" GetKeyboardLayoutNameA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("GetKeyboardLayoutNameW" GetKeyboardLayoutNameW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetKeyNameTextA" GetKeyNameTextA :convention :stdcall) :int
  (arg0 :int32)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("GetKeyNameTextW" GetKeyNameTextW :convention :stdcall) :int
  (arg0 :int32)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetKeyState" GetKeyState :convention :stdcall) :short
  (arg0 :int))

(defcfunex-exported ("IsWindowEnabled" IsWindowEnabled :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("RegisterHotKey" RegisterHotKey :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int))

(defcfunex-exported ("SetActiveWindow" SetActiveWindow :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("SetFocus" SetFocus :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("SetKeyboardState" SetKeyboardState :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ToAsciiEx" ToAsciiEx :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-int)
  (arg5 :pointer))

(defcfunex-exported ("ToUnicode" ToUnicode :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :unsigned-int))

(defcfunex-exported ("ToUnicodeEx" ToUnicodeEx :convention :stdcall) :int
  (arg0 :unsigned-int)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :unsigned-int)
  (arg6 :pointer))

(defcfunex-exported ("UnloadKeyboardLayout" UnloadKeyboardLayout :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("UnregisterHotKey" UnregisterHotKey :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("VkKeyScanA" VkKeyScanA :convention :stdcall) :short
  (arg0 :char))

(defcfunex-exported ("VkKeyScanW" VkKeyScanW :convention :stdcall) :short
  (arg0 :pointer))

(defcfunex-exported ("VkKeyScanExA" VkKeyScanExA :convention :stdcall) :short
  (arg0 :char)
  (arg1 :pointer))

(defcfunex-exported ("VkKeyScanExW" VkKeyScanExW :convention :stdcall) :short
  (arg0 :pointer)
  (arg1 :pointer))



;; obsolete

(defcfunex-exported ("GetKBCodePage" GetKBCodePage :convention :stdcall) :unsigned-int)

