
(defcfunex-exported ("AngleArc" AngleArc :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :float)
  (arg5 :float))

(defcfunex-exported ("Arc" Arc :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfunex-exported ("ArcTo" ArcTo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfunex-exported ("GetArcDirection" GetArcDirection :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("LineDDA" LineDDA :convention :stdcall) :int
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :int32))

(defcfunex-exported ("LineTo" LineTo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("MoveToEx" MoveToEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("PolyBezier" PolyBezier :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("PolyBezierTo" PolyBezierTo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("PolyDraw" PolyDraw :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("Polyline" Polyline :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("PolylineTo" PolylineTo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("PolyPolyline" PolyPolyline :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetArcDirection" SetArcDirection :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

