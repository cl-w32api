
(cl:in-package w32apimod)

(define-w32api-module win95.hook :win95.hook)

(cl:in-package cl-w32api.module.win95.hook)

(defcfunex-exported ("CallMsgFilterA" CallMsgFilterA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("CallMsgFilterW" CallMsgFilterW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("CallNextHookEx" CallNextHookEx :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("SetWindowsHookExA" SetWindowsHookExA :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("SetWindowsHookExW" SetWindowsHookExW :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("UnhookWindowsHookEx" UnhookWindowsHookEx :convention :stdcall) :int
  (arg0 :pointer))


;;obsolete

(defcfunex-exported ("SetWindowsHookA" SetWindowsHookA :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :pointer))

(defcfunex-exported ("SetWindowsHookW" SetWindowsHookW :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :pointer))

(defcfunex-exported ("UnhookWindowsHook" UnhookWindowsHook :convention :stdcall) :int
  (arg0 :int)
  (arg1 :pointer))

