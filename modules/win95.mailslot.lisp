
(defcfunex-exported ("CreateMailslotA" CreateMailslotA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("CreateMailslotW" CreateMailslotW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("GetMailslotInfo" GetMailslotInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("SetMailslotInfo" SetMailslotInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

