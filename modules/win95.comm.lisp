(cl:in-package w32apimod)

(define-w32api-module win95.comm :win95.comm)

(cl:in-package cl-w32api.module.win95.comm)

(defcfunex-exported ("BuildCommDCBA" BuildCommDCBA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("BuildCommDCBW" BuildCommDCBW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("BuildCommDCBAndTimeoutsA" BuildCommDCBAndTimeoutsA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("BuildCommDCBAndTimeoutsW" BuildCommDCBAndTimeoutsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ClearCommBreak" ClearCommBreak :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ClearCommError" ClearCommError :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("CommConfigDialogA" CommConfigDialogA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("CommConfigDialogW" CommConfigDialogW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("DeviceIoControl" DeviceIoControl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("EscapeCommFunction" EscapeCommFunction :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetCommConfig" GetCommConfig :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetCommMask" GetCommMask :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCommModemStatus" GetCommModemStatus :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCommProperties" GetCommProperties :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCommState" GetCommState :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetCommTimeouts" GetCommTimeouts :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetDefaultCommConfigA" GetDefaultCommConfigA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetDefaultCommConfigW" GetDefaultCommConfigW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("PurgeComm" PurgeComm :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetCommBreak" SetCommBreak :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetCommConfig" SetCommConfig :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("SetCommMask" SetCommMask :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetCommState" SetCommState :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetCommTimeouts" SetCommTimeouts :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetDefaultCommConfigA" SetDefaultCommConfigA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("SetDefaultCommConfigW" SetDefaultCommConfigW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("SetupComm" SetupComm :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("TransmitCommChar" TransmitCommChar :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :char))

(defcfunex-exported ("WaitCommEvent" WaitCommEvent :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

