
(cl:in-package w32apimod)

(define-w32api-module win95.scrollbar :win95.scrollbar)

(cl:in-package cl-w32api.module.win95.scrollbar)

(defcfunex-exported ("EnableScrollBar" EnableScrollBar :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("GetScrollInfo" GetScrollInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("GetScrollPos" GetScrollPos :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetScrollRange" GetScrollRange :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("ScrollDC" ScrollDC :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("ScrollWindow" ScrollWindow :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("ScrollWindowEx" ScrollWindowEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :unsigned-int))

(defcfunex-exported ("SetScrollInfo" SetScrollInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("SetScrollPos" SetScrollPos :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int))

(defcfunex-exported ("SetScrollRange" SetScrollRange :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("ShowScrollBar" ShowScrollBar :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))



