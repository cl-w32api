
(defcfunex-exported ("CreatePen" CreatePen :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("CreatePenIndirect" CreatePenIndirect :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("ExtCreatePen" ExtCreatePen :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

