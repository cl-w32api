
(cl:in-package w32apimod)

(define-w32api-module win95.painting-drawing :win95.painting-drawing)

(cl:in-package cl-w32api.module.win95.painting-drawing)

(require-and-inherit-module "win95.~")

(defcstructex-exported (PAINTSTRUCT :size 64)
        (hdc     HDC)
        (fErase  BOOL)
        (rcPaint (:inline RECT))
        (fRestore BOOL)
        (fIncUpdate BOOL)
        (rgbReserved :pointer))
#|
(defcstruct PAINTSTRUCT
        (a :pointer)
        (b :pointer)
        (c :pointer)
        (d :pointer)
        (e :pointer)
        (f :pointer)
        (g :pointer)
        (h :pointer)
        (i :pointer)
        (j :pointer)
        (k :pointer)
        (l :pointer)
        (m :pointer)
        (n :pointer))
|#
(export 'PAINTSTRUCT)

(defcfunex-exported ("BeginPaint" BeginPaint :convention :stdcall) HDC
  (|hWnd| HWND)
  (|lpPaint| PAINTSTRUCT))

(defcfunex-exported ("DrawAnimatedRects" DrawAnimatedRects :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("DrawCaption" DrawCaption :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("DrawEdge" DrawEdge :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int))

(defcfunex-exported ("DrawFocusRect" DrawFocusRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DrawFrameControl" DrawFrameControl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int))

(defcfunex-exported ("DrawStateA" DrawStateA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32)
  (arg4 :unsigned-int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int)
  (arg9 :unsigned-int))

(defcfunex-exported ("DrawStateW" DrawStateW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32)
  (arg4 :unsigned-int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int)
  (arg9 :unsigned-int))

(defcfunex-exported ("EndPaint" EndPaint :convention :stdcall) BOOL
  (|hWnd|    HWND)
  (|lpPaint| PAINTSTRUCT))

(defcfunex-exported ("ExcludeUpdateRgn" ExcludeUpdateRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GdiFlush" GdiFlush :convention :stdcall) :int)

(defcfunex-exported ("GdiGetBatchLimit" GdiGetBatchLimit :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GdiSetBatchLimit" GdiSetBatchLimit :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("GetBkColor" GetBkColor :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetBkMode" GetBkMode :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetBoundsRect" GetBoundsRect :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("GetROP2" GetROP2 :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetUpdateRect" GetUpdateRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetUpdateRgn" GetUpdateRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetWindowDC" GetWindowDC :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetWindowRgn" GetWindowRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GrayStringA" GrayStringA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfunex-exported ("GrayStringW" GrayStringW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfunex-exported ("InvalidateRect" InvalidateRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("InvalidateRgn" InvalidateRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("LockWindowUpdate" LockWindowUpdate :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("PaintDesktop" PaintDesktop :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("RedrawWindow" RedrawWindow :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("SetBkColor" SetBkColor :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetBkMode" SetBkMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetBoundsRect" SetBoundsRect :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("SetRectRgn" SetRectRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("SetROP2" SetROP2 :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetWindowRgn" SetWindowRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("UpdateWindow" UpdateWindow :convention :stdcall) BOOL
  (|hWnd| HWND))

(defcfunex-exported ("ValidateRect" ValidateRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ValidateRgn" ValidateRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("WindowFromDC" WindowFromDC :convention :stdcall) :pointer
  (hDC :pointer))

