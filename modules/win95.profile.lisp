(defcfunex-exported ("GetPrivateProfileIntA" GetPrivateProfileIntA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :string)
  (arg2 :int)
  (arg3 :string))

(defcfunex-exported ("GetPrivateProfileIntW" GetPrivateProfileIntW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetPrivateProfileSectionA" GetPrivateProfileSectionA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :string))

(defcfunex-exported ("GetPrivateProfileSectionW" GetPrivateProfileSectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("GetPrivateProfileSectionNamesA" GetPrivateProfileSectionNamesA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :string))

(defcfunex-exported ("GetPrivateProfileSectionNamesW" GetPrivateProfileSectionNamesW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetPrivateProfileStringA" GetPrivateProfileStringA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string)
  (arg4 :unsigned-long)
  (arg5 :string))

(defcfunex-exported ("GetPrivateProfileStringW" GetPrivateProfileStringW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("GetPrivateProfileStructA" GetPrivateProfileStructA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :string))

(defcfunex-exported ("GetPrivateProfileStructW" GetPrivateProfileStructW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("GetProfileIntA" GetProfileIntA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :string)
  (arg2 :int))

(defcfunex-exported ("GetProfileIntW" GetProfileIntW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetProfileSectionA" GetProfileSectionA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetProfileSectionW" GetProfileSectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetProfileStringA" GetProfileStringA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string)
  (arg4 :unsigned-long))

(defcfunex-exported ("GetProfileStringW" GetProfileStringW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("WritePrivateProfileSectionA" WritePrivateProfileSectionA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("WritePrivateProfileSectionW" WritePrivateProfileSectionW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WritePrivateProfileStringA" WritePrivateProfileStringA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string))

(defcfunex-exported ("WritePrivateProfileStringW" WritePrivateProfileStringW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WritePrivateProfileStructA" WritePrivateProfileStructA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :string))

(defcfunex-exported ("WritePrivateProfileStructW" WritePrivateProfileStructW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("WriteProfileSectionA" WriteProfileSectionA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("WriteProfileSectionW" WriteProfileSectionW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("WriteProfileStringA" WriteProfileStringA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("WriteProfileStringW" WriteProfileStringW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

