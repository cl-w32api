
(cl:in-package w32apimod)

(define-w32api-module version :version)

(cl:in-package cl-w32api.module.version)


(defconstant-exported VS_VERSION_INFO 1)

(defconstant-exported VS_USER_DEFINED 100)

(defconstant-exported VS_FFI_SIGNATURE #xFEEF04BD)

(defconstant-exported VS_FFI_STRUCVERSION #x10000)

(defconstant-exported VS_FFI_FILEFLAGSMASK #x3F)

(defconstant-exported VS_FF_DEBUG 1)

(defconstant-exported VS_FF_PRERELEASE 2)

(defconstant-exported VS_FF_PATCHED 4)

(defconstant-exported VS_FF_PRIVATEBUILD 8)

(defconstant-exported VS_FF_INFOINFERRED 16)

(defconstant-exported VS_FF_SPECIALBUILD 32)

(defconstant-exported VOS_UNKNOWN 0)

(defconstant-exported VOS_DOS #x10000)

(defconstant-exported VOS_OS216 #x20000)

(defconstant-exported VOS_OS232 #x30000)

(defconstant-exported VOS_NT #x40000)

(defconstant-exported VOS__BASE 0)

(defconstant-exported VOS__WINDOWS16 1)

(defconstant-exported VOS__PM16 2)

(defconstant-exported VOS__PM32 3)

(defconstant-exported VOS__WINDOWS32 4)

(defconstant-exported VOS_DOS_WINDOWS16 #x10001)

(defconstant-exported VOS_DOS_WINDOWS32 #x10004)

(defconstant-exported VOS_OS216_PM16 #x20002)

(defconstant-exported VOS_OS232_PM32 #x30003)

(defconstant-exported VOS_NT_WINDOWS32 #x40004)

(defconstant-exported VFT_UNKNOWN 0)

(defconstant-exported VFT_APP 1)

(defconstant-exported VFT_DLL 2)

(defconstant-exported VFT_DRV 3)

(defconstant-exported VFT_FONT 4)

(defconstant-exported VFT_VXD 5)

(defconstant-exported VFT_STATIC_LIB 7)

(defconstant-exported VFT2_UNKNOWN 0)

(defconstant-exported VFT2_DRV_PRINTER 1)

(defconstant-exported VFT2_DRV_KEYBOARD 2)

(defconstant-exported VFT2_DRV_LANGUAGE 3)

(defconstant-exported VFT2_DRV_DISPLAY 4)

(defconstant-exported VFT2_DRV_MOUSE 5)

(defconstant-exported VFT2_DRV_NETWORK 6)

(defconstant-exported VFT2_DRV_SYSTEM 7)

(defconstant-exported VFT2_DRV_INSTALLABLE 8)

(defconstant-exported VFT2_DRV_SOUND 9)

(defconstant-exported VFT2_DRV_COMM 10)

(defconstant-exported VFT2_DRV_INPUTMETHOD 11)

(defconstant-exported VFT2_FONT_RASTER 1)

(defconstant-exported VFT2_FONT_VECTOR 2)

(defconstant-exported VFT2_FONT_TRUETYPE 3)

(defconstant-exported VFFF_ISSHAREDFILE 1)

(defconstant-exported VFF_CURNEDEST 1)

(defconstant-exported VFF_FILEINUSE 2)

(defconstant-exported VFF_BUFFTOOSMALL 4)

(defconstant-exported VIFF_FORCEINSTALL 1)

(defconstant-exported VIFF_DONTDELETEOLD 2)

(defconstant-exported VIF_TEMPFILE 1)

(defconstant-exported VIF_MISMATCH 2)

(defconstant-exported VIF_SRCOLD 4)

(defconstant-exported VIF_DIFFLANG 8)

(defconstant-exported VIF_DIFFCODEPG 16)

(defconstant-exported VIF_DIFFTYPE 32)

(defconstant-exported VIF_WRITEPROT 64)

(defconstant-exported VIF_FILEINUSE 128)

(defconstant-exported VIF_OUTOFSPACE 256)

(defconstant-exported VIF_ACCESSVIOLATION 512)

(defconstant-exported VIF_SHARINGVIOLATION 1024)

(defconstant-exported VIF_CANNOTCREATE 2048)

(defconstant-exported VIF_CANNOTDELETE 4096)

(defconstant-exported VIF_CANNOTRENAME 8192)

(defconstant-exported VIF_CANNOTDELETECUR 16384)

(defconstant-exported VIF_OUTOFMEMORY 32768)

(defconstant-exported VIF_CANNOTREADSRC 65536)

(defconstant-exported VIF_CANNOTREADDST #x20000)

(defconstant-exported VIF_BUFFTOOSMALL #x40000)

(defcstructex-exported VS_FIXEDFILEINFO
        (dwSignature :unsigned-long)
        (dwStrucVersion :unsigned-long)
        (dwFileVersionMS :unsigned-long)
        (dwFileVersionLS :unsigned-long)
        (dwProductVersionMS :unsigned-long)
        (dwProductVersionLS :unsigned-long)
        (dwFileFlagsMask :unsigned-long)
        (dwFileFlags :unsigned-long)
        (dwFileOS :unsigned-long)
        (dwFileType :unsigned-long)
        (dwFileSubtype :unsigned-long)
        (dwFileDateMS :unsigned-long)
        (dwFileDateLS :unsigned-long))



(defcfunex-exported ("VerFindFileA" VerFindFileA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string)
  (arg4 :string)
  (arg5 :pointer)
  (arg6 :string)
  (arg7 :pointer))

(defcfunex-exported ("VerFindFileW" VerFindFileW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("VerInstallFileA" VerInstallFileA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string)
  (arg4 :string)
  (arg5 :string)
  (arg6 :string)
  (arg7 :pointer))

(defcfunex-exported ("VerInstallFileW" VerInstallFileW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("GetFileVersionInfoSizeA" GetFileVersionInfoSizeA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("GetFileVersionInfoSizeW" GetFileVersionInfoSizeW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetFileVersionInfoA" GetFileVersionInfoA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("GetFileVersionInfoW" GetFileVersionInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("VerLanguageNameA" VerLanguageNameA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("VerLanguageNameW" VerLanguageNameW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("VerQueryValueA" VerQueryValueA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("VerQueryValueW" VerQueryValueW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))
