

(defcfunex-exported ("GetSystemPowerStatus" GetSystemPowerStatus :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetSystemPowerState" SetSystemPowerState :convention :stdcall) :int
  (arg0 :int)
  (arg1 :int))
