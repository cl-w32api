(defcfunex-exported ("CreateBrushIndirect" CreateBrushIndirect :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateDIBPatternBrushPt" CreateDIBPatternBrushPt :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("CreateHatchBrush" CreateHatchBrush :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :unsigned-long))

(defcfunex-exported ("CreatePatternBrush" CreatePatternBrush :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateSolidBrush" CreateSolidBrush :convention :stdcall) :pointer
  (arg0 :unsigned-long))

(defcfunex-exported ("GetBrushOrgEx" GetBrushOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetBrushOrgEx" SetBrushOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))


;;obsolete

(defcfunex-exported ("CreateDIBPatternBrush" CreateDIBPatternBrush :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("FixBrushOrgEx" FixBrushOrgEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

