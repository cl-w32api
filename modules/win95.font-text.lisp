
(cl:in-package w32apimod)

(define-w32api-module win95.font-text :win95.font-text)

(cl:in-package cl-w32api.module.win95.font-text)

(require-and-inherit-module "win95.~")

(defcfunex-exported ("AddFontResourceA" AddFontResourceA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("AddFontResourceW" AddFontResourceW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CreateFontA" CreateFontA :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :unsigned-long)
  (arg8 :unsigned-long)
  (arg9 :unsigned-long)
  (arg10 :unsigned-long)
  (arg11 :unsigned-long)
  (arg12 :unsigned-long)
  (arg13 :string))

(defcfunex-exported ("CreateFontW" CreateFontW :convention :stdcall) :pointer
  (arg0 :int)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :unsigned-long)
  (arg8 :unsigned-long)
  (arg9 :unsigned-long)
  (arg10 :unsigned-long)
  (arg11 :unsigned-long)
  (arg12 :unsigned-long)
  (arg13 :pointer))

(defcfunex-exported ("CreateFontIndirectA" CreateFontIndirectA :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateFontIndirectW" CreateFontIndirectW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateScalableFontResourceA" CreateScalableFontResourceA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string))

(defcfunex-exported ("CreateScalableFontResourceW" CreateScalableFontResourceW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defbitfield (DRAWTEXT-FLAGS UINT)
  (:BOTTOM 8)
  (:CALCRECT 1024)
  (:CENTER 1)
  (:EDITCONTROL 8192)
  (:END_ELLIPSIS 32768)
  (:PATH_ELLIPSIS 16384)
  (:WORD_ELLIPSIS #x40000)
  (:EXPANDTABS 64)
  (:EXTERNALLEADING 512)
  (:LEFT 0)
  (:MODIFYSTRING 65536)
  (:NOCLIP 256)
  (:NOPREFIX 2048)
  (:RIGHT 2)
  (:RTLREADING 131072)
  (:SINGLELINE 32)
  (:TABSTOP 128)
  (:TOP 0)
  (:VCENTER 4)
  (:WORDBREAK 16)
  (:INTERNAL 4096))  

(defcfunex-exported ("DrawTextA" DrawTextA :convention :stdcall) :int
  (|hDC|      HDC)
  (|lpString| ASTRING)
  (|nCount|   :int)
  (|lpRect|   RECT)
  (|uFormat|  DRAWTEXT-FLAGS))

(defcfunex-exported ("DrawTextW" DrawTextW :convention :stdcall) :int
  (|hDC|      HDC)
  (|lpString| WSTRING)
  (|nCount|   :int)
  (|lpRect|   RECT)
  (|uFormat|  DRAWTEXT-FLAGS))

(define-abbrev-exported DrawText DrawTextW)

(defcfunex-exported ("DrawTextExA" DrawTextExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :unsigned-int)
  (arg5 :pointer))

(defcfunex-exported ("DrawTextExW" DrawTextExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :unsigned-int)
  (arg5 :pointer))

(define-abbrev-exported DrawTextEx DrawTextExW)

(defcfunex-exported ("EnumFontFamiliesA" EnumFontFamiliesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("EnumFontFamiliesW" EnumFontFamiliesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("EnumFontFamiliesExA" EnumFontFamiliesExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32)
  (arg4 :unsigned-long))

(defcfunex-exported ("EnumFontFamiliesExW" EnumFontFamiliesExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32)
  (arg4 :unsigned-long))

(defcfunex-exported ("EnumFontsA" EnumFontsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("EnumFontsW" EnumFontsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("ExtTextOutA" ExtTextOutA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-int)
  (arg4 :pointer)
  (arg5 :string)
  (arg6 :unsigned-int)
  (arg7 :pointer))

(defcfunex-exported ("ExtTextOutW" ExtTextOutW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-int)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-int)
  (arg7 :pointer))

(defcfunex-exported ("GetCharABCWidthsA" GetCharABCWidthsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharABCWidthsW" GetCharABCWidthsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharABCWidthsFloatA" GetCharABCWidthsFloatA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharABCWidthsFloatW" GetCharABCWidthsFloatW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharacterPlacementA" GetCharacterPlacementA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("GetCharacterPlacementW" GetCharacterPlacementW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("GetCharWidth32A" GetCharWidth32A :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharWidth32W" GetCharWidth32W :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharWidthA" GetCharWidthA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharWidthW" GetCharWidthW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharWidthFloatA" GetCharWidthFloatA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetCharWidthFloatW" GetCharWidthFloatW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("GetFontData" GetFontData :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("GetFontLanguageInfo" GetFontLanguageInfo :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetGlyphOutlineA" GetGlyphOutlineA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("GetGlyphOutlineW" GetGlyphOutlineW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("GetKerningPairsA" GetKerningPairsA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetKerningPairsW" GetKerningPairsW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))


(defcfunex-exported ("GetOutlineTextMetricsA" GetOutlineTextMetricsA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetOutlineTextMetricsW" GetOutlineTextMetricsW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetRasterizerCaps" GetRasterizerCaps :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("GetTabbedTextExtentA" GetTabbedTextExtentA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("GetTabbedTextExtentW" GetTabbedTextExtentW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("GetTextAlign" GetTextAlign :convention :stdcall) :unsigned-int
  (arg0 :pointer))

(defcfunex-exported ("GetTextCharacterExtra" GetTextCharacterExtra :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetTextColor" GetTextColor :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetTextExtentExPointA" GetTextExtentExPointA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("GetTextExtentExPointW" GetTextExtentExPointW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("GetTextExtentPointA" GetTextExtentPointA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetTextExtentPointW" GetTextExtentPointW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetTextExtentPoint32A" GetTextExtentPoint32A :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetTextExtentPoint32W" GetTextExtentPoint32W :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetTextFaceA" GetTextFaceA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("GetTextFaceW" GetTextFaceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))


(defcfunex-exported ("GetTextMetricsA" GetTextMetricsA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetTextMetricsW" GetTextMetricsW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("PolyTextOutA" PolyTextOutA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("PolyTextOutW" PolyTextOutW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("RemoveFontResourceA" RemoveFontResourceA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("RemoveFontResourceW" RemoveFontResourceW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetMapperFlags" SetMapperFlags :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetTextAlign" SetTextAlign :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("SetTextCharacterExtra" SetTextCharacterExtra :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetTextColor" SetTextColor :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetTextJustification" SetTextJustification :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("TabbedTextOutA" TabbedTextOutA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string)
  (arg4 :int)
  (arg5 :int)
  (arg6 :pointer)
  (arg7 :int))

(defcfunex-exported ("TabbedTextOutW" TabbedTextOutW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :int)
  (arg6 :pointer)
  (arg7 :int))

(defcfunex-exported ("TextOutA" TextOutA :convention :stdcall) :int
  (arg0 HDC)
  (arg1 :int)
  (arg2 :int)
  (arg3 ASTRING)
  (arg4 :int))

(defcfunex-exported ("TextOutW" TextOutW :convention :stdcall) :int
  (arg0 HDC)
  (arg1 :int)
  (arg2 :int)
  (arg3 WSTRING)
  (arg4 :int))

(define-abbrev-exported TextOut TextOutW)
