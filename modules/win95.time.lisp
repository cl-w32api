
(cl:in-package w32apimod)

(define-w32api-module win95.time :win95.time)

(cl:in-package cl-w32api.module.win95.time)

(defcfunex-exported ("CompareFileTime" CompareFileTime :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("DosDateTimeToFileTime" DosDateTimeToFileTime :convention :stdcall) :int
  (arg0 :unsigned-short)
  (arg1 :unsigned-short)
  (arg2 :pointer))

(defcfunex-exported ("FileTimeToDosDateTime" FileTimeToDosDateTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("FileTimeToLocalFileTime" FileTimeToLocalFileTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FileTimeToSystemTime" FileTimeToSystemTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetFileTime" GetFileTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetLocalTime" GetLocalTime :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetSystemTime" GetSystemTime :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetSystemTimeAdjustment" GetSystemTimeAdjustment :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetSystemTimeAsFileTime" GetSystemTimeAsFileTime :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetTickCount" GetTickCount :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetTimeZoneInformation" GetTimeZoneInformation :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("LocalFileTimeToFileTime" LocalFileTimeToFileTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetFileTime" SetFileTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("SetLocalTime" SetLocalTime :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetSystemTime" SetSystemTime :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetSystemTimeAdjustment" SetSystemTimeAdjustment :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :int))

(defcfunex-exported ("SetTimeZoneInformation" SetTimeZoneInformation :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SystemTimeToFileTime" SystemTimeToFileTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SystemTimeToTzSpecificLocalTime" SystemTimeToTzSpecificLocalTime :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

;;obsolete

