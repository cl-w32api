;;emf

(defcfunex-exported ("CloseEnhMetaFile" CloseEnhMetaFile :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CopyEnhMetaFileA" CopyEnhMetaFileA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("CopyEnhMetaFileW" CopyEnhMetaFileW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CreateEnhMetaFileA" CreateEnhMetaFileA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :string))

(defcfunex-exported ("CreateEnhMetaFileW" CreateEnhMetaFileW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("DeleteEnhMetaFile" DeleteEnhMetaFile :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnumEnhMetaFile" EnumEnhMetaFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("GdiComment" GdiComment :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetEnhMetaFileA" GetEnhMetaFileA :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("GetEnhMetaFileW" GetEnhMetaFileW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetEnhMetaFileBits" GetEnhMetaFileBits :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetEnhMetaFileDescriptionA" GetEnhMetaFileDescriptionA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string))

(defcfunex-exported ("GetEnhMetaFileDescriptionW" GetEnhMetaFileDescriptionW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetEnhMetaFileHeader" GetEnhMetaFileHeader :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetEnhMetaFilePaletteEntries" GetEnhMetaFilePaletteEntries :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("GetWinMetaFileBits" GetWinMetaFileBits :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :pointer))

(defcfunex-exported ("PlayEnhMetaFile" PlayEnhMetaFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("PlayEnhMetaFileRecord" PlayEnhMetaFileRecord :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("SetEnhMetaFileBits" SetEnhMetaFileBits :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :pointer))

(defcfunex-exported ("SetWinMetaFileBits" SetWinMetaFileBits :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))


;;wmf

(defcfunex-exported ("CloseMetaFile" CloseMetaFile :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CopyMetaFileA" CopyMetaFileA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("CopyMetaFileW" CopyMetaFileW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CreateMetaFileA" CreateMetaFileA :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("CreateMetaFileW" CreateMetaFileW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("DeleteMetaFile" DeleteMetaFile :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnumMetaFile" EnumMetaFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int32))

(defcfunex-exported ("GetMetaFileA" GetMetaFileA :convention :stdcall) :pointer
  (arg0 :string))

(defcfunex-exported ("GetMetaFileW" GetMetaFileW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetMetaFileBitsEx" GetMetaFileBitsEx :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer))

(defcfunex-exported ("PlayMetaFile" PlayMetaFile :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("PlayMetaFileRecord" PlayMetaFileRecord :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("SetMetaFileBitsEx" SetMetaFileBitsEx :convention :stdcall) :pointer
  (arg0 :unsigned-int)
  (arg1 :pointer))

