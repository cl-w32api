(defcfunex-exported ("BackupEventLogA" BackupEventLogA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("BackupEventLogW" BackupEventLogW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ClearEventLogA" ClearEventLogA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("ClearEventLogW" ClearEventLogW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CloseEventLog" CloseEventLog :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DeregisterEventSource" DeregisterEventSource :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetNumberOfEventLogRecords" GetNumberOfEventLogRecords :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetOldestEventLogRecord" GetOldestEventLogRecord :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("NotifyChangeEventLog" NotifyChangeEventLog :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("OpenBackupEventLogA" OpenBackupEventLogA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("OpenBackupEventLogW" OpenBackupEventLogW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("OpenEventLogA" OpenEventLogA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("OpenEventLogW" OpenEventLogW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ReadEventLogA" ReadEventLogA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("ReadEventLogW" ReadEventLogW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("RegisterEventSourceA" RegisterEventSourceA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("RegisterEventSourceW" RegisterEventSourceW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ReportEventA" ReportEventA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-short)
  (arg2 :unsigned-short)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-short)
  (arg6 :unsigned-long)
  (arg7 :pointer)
  (arg8 :pointer))

(defcfunex-exported ("ReportEventW" ReportEventW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-short)
  (arg2 :unsigned-short)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-short)
  (arg6 :unsigned-long)
  (arg7 :pointer)
  (arg8 :pointer))

