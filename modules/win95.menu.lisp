
(cl:in-package w32apimod)

(define-w32api-module win95.menu :win95.menu)

(cl:in-package cl-w32api.module.win95.menu)

(defcfunex-exported ("GetMenu" GetMenu :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetMenuCheckMarkDimensions" GetMenuCheckMarkDimensions :convention :stdcall) :int32)


(defcfunex-exported ("GetMenuDefaultItem" GetMenuDefaultItem :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("GetMenuItemCount" GetMenuItemCount :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetMenuItemID" GetMenuItemID :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("GetMenuItemInfoA" GetMenuItemInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetMenuItemInfoW" GetMenuItemInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("GetMenuItemRect" GetMenuItemRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("SetMenu" SetMenu :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetMenuDefaultItem" SetMenuDefaultItem :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

(defcfunex-exported ("SetMenuItemBitmaps" SetMenuItemBitmaps :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("SetMenuItemInfoA" SetMenuItemInfoA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("SetMenuItemInfoW" SetMenuItemInfoW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("TrackPopupMenu" TrackPopupMenu :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("TrackPopupMenuEx" TrackPopupMenuEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :pointer))

;;obsolete

(defcfunex-exported ("AppendMenuA" AppendMenuA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :string))

(defcfunex-exported ("AppendMenuW" AppendMenuW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :pointer))

(defcfunex-exported ("ChangeMenuA" ChangeMenuA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int))

(defcfunex-exported ("ChangeMenuW" ChangeMenuW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int)
  (arg4 :unsigned-int))


(defcfunex-exported ("InsertMenuA" InsertMenuA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :string))

(defcfunex-exported ("InsertMenuW" InsertMenuW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("ModifyMenuA" ModifyMenuA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :string))

(defcfunex-exported ("ModifyMenuW" ModifyMenuW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :pointer))

(defcfunex-exported ("RemoveMenu" RemoveMenu :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int))

