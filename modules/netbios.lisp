
(cl:in-package w32apimod)

(define-w32api-module netbios :netbios)

(cl:in-package cl-w32api.module.netbios)














(defconstant-exported NCBNAMSZ 16)

(defconstant-exported MAX_LANA 254)

(defconstant-exported NAME_FLAGS_MASK #x87)

(defconstant-exported GROUP_NAME #x80)

(defconstant-exported UNIQUE_NAME #x00)

(defconstant-exported REGISTERING #x00)

(defconstant-exported REGISTERED #x04)

(defconstant-exported DEREGISTERED #x05)

(defconstant-exported DUPLICATE #x06)

(defconstant-exported DUPLICATE_DEREG #x07)

(defconstant-exported LISTEN_OUTSTANDING #x01)

(defconstant-exported CALL_PENDING #x02)

(defconstant-exported SESSION_ESTABLISHED #x03)

(defconstant-exported HANGUP_PENDING #x04)

(defconstant-exported HANGUP_COMPLETE #x05)

(defconstant-exported SESSION_ABORTED #x06)

(defconstant-exported ALL_TRANSPORTS "M")

(defconstant-exported MS_NBF "MNBF")

(defconstant-exported NCBCALL #x10)

(defconstant-exported NCBLISTEN #x11)

(defconstant-exported NCBHANGUP #x12)

(defconstant-exported NCBSEND #x14)

(defconstant-exported NCBRECV #x15)

(defconstant-exported NCBRECVANY #x16)

(defconstant-exported NCBCHAINSEND #x17)

(defconstant-exported NCBDGSEND #x20)

(defconstant-exported NCBDGRECV #x21)

(defconstant-exported NCBDGSENDBC #x22)

(defconstant-exported NCBDGRECVBC #x23)

(defconstant-exported NCBADDNAME #x30)

(defconstant-exported NCBDELNAME #x31)

(defconstant-exported NCBRESET #x32)

(defconstant-exported NCBASTAT #x33)

(defconstant-exported NCBSSTAT #x34)

(defconstant-exported NCBCANCEL #x35)

(defconstant-exported NCBADDGRNAME #x36)

(defconstant-exported NCBENUM #x37)

(defconstant-exported NCBUNLINK #x70)

(defconstant-exported NCBSENDNA #x71)

(defconstant-exported NCBCHAINSENDNA #x72)

(defconstant-exported NCBLANSTALERT #x73)

(defconstant-exported NCBACTION #x77)

(defconstant-exported NCBFINDNAME #x78)

(defconstant-exported NCBTRACE #x79)

(defconstant-exported ASYNCH #x80)

(defconstant-exported NRC_GOODRET #x00)

(defconstant-exported NRC_BUFLEN #x01)

(defconstant-exported NRC_ILLCMD #x03)

(defconstant-exported NRC_CMDTMO #x05)

(defconstant-exported NRC_INCOMP #x06)

(defconstant-exported NRC_BADDR #x07)

(defconstant-exported NRC_SNUMOUT #x08)

(defconstant-exported NRC_NORES #x09)

(defconstant-exported NRC_SCLOSED #x0a)

(defconstant-exported NRC_CMDCAN #x0b)

(defconstant-exported NRC_DUPNAME #x0d)

(defconstant-exported NRC_NAMTFUL #x0e)

(defconstant-exported NRC_ACTSES #x0f)

(defconstant-exported NRC_LOCTFUL #x11)

(defconstant-exported NRC_REMTFUL #x12)

(defconstant-exported NRC_ILLNN #x13)

(defconstant-exported NRC_NOCALL #x14)

(defconstant-exported NRC_NOWILD #x15)

(defconstant-exported NRC_INUSE #x16)

(defconstant-exported NRC_NAMERR #x17)

(defconstant-exported NRC_SABORT #x18)

(defconstant-exported NRC_NAMCONF #x19)

(defconstant-exported NRC_IFBUSY #x21)

(defconstant-exported NRC_TOOMANY #x22)

(defconstant-exported NRC_BRIDGE #x23)

(defconstant-exported NRC_CANOCCR #x24)

(defconstant-exported NRC_CANCEL #x26)

(defconstant-exported NRC_DUPENV #x30)

(defconstant-exported NRC_ENVNOTDEF #x34)

(defconstant-exported NRC_OSRESNOTAV #x35)

(defconstant-exported NRC_MAXAPPS #x36)

(defconstant-exported NRC_NOSAPS #x37)

(defconstant-exported NRC_NORESOURCES #x38)

(defconstant-exported NRC_INVADDRESS #x39)

(defconstant-exported NRC_INVDDID #x3B)

(defconstant-exported NRC_LOCKFAIL #x3C)

(defconstant-exported NRC_OPENERR #x3f)

(defconstant-exported NRC_SYSTEM #x40)

(defconstant-exported NRC_PENDING #xff)

(defcstructex-exported ACTION_HEADER
        (transport_id :unsigned-long)
        (action_code :unsigned-short)
        (reserved :unsigned-short))





(defcstructex-exported ADAPTER_STATUS
        (adapter_address :pointer)
        (rev_major :unsigned-char)
        (reserved0 :unsigned-char)
        (adapter_type :unsigned-char)
        (rev_minor :unsigned-char)
        (duration :unsigned-short)
        (frmr_recv :unsigned-short)
        (frmr_xmit :unsigned-short)
        (iframe_recv_err :unsigned-short)
        (xmit_aborts :unsigned-short)
        (xmit_success :unsigned-long)
        (recv_success :unsigned-long)
        (iframe_xmit_err :unsigned-short)
        (recv_buff_unavail :unsigned-short)
        (t1_timeouts :unsigned-short)
        (ti_timeouts :unsigned-short)
        (reserved1 :unsigned-long)
        (free_ncbs :unsigned-short)
        (max_cfg_ncbs :unsigned-short)
        (max_ncbs :unsigned-short)
        (xmit_buf_unavail :unsigned-short)
        (max_dgram_size :unsigned-short)
        (pending_sess :unsigned-short)
        (max_cfg_sess :unsigned-short)
        (max_sess :unsigned-short)
        (max_sess_pkt_size :unsigned-short)
        (name_count :unsigned-short))





(defcstructex-exported FIND_NAME_BUFFER
        (length :unsigned-char)
        (access_control :unsigned-char)
        (frame_control :unsigned-char)
        (destination_addr :pointer)
        (source_addr :pointer)
        (routing_info :pointer))





(defcstructex-exported FIND_NAME_HEADER
        (node_count :unsigned-short)
        (reserved :unsigned-char)
        (unique_group :unsigned-char))





(defcstructex-exported LANA_ENUM
        (length :unsigned-char)
        (lana :pointer))





(defcstructex-exported NAME_BUFFER
        (name :pointer)
        (name_num :unsigned-char)
        (name_flags :unsigned-char))





(defcstructex-exported NCB
        (ncb_command :unsigned-char)
        (ncb_retcode :unsigned-char)
        (ncb_lsn :unsigned-char)
        (ncb_num :unsigned-char)
        (ncb_buffer :pointer)
        (ncb_length :unsigned-short)
        (ncb_callname :pointer)
        (ncb_name :pointer)
        (ncb_rto :unsigned-char)
        (ncb_sto :unsigned-char)
        (ncb_post :pointer)
        (ncb_lana_num :unsigned-char)
        (ncb_cmd_cplt :unsigned-char)
        (ncb_reserve :pointer)
        (ncb_event :pointer))





(defcstructex-exported SESSION_BUFFER
        (lsn :unsigned-char)
        (state :unsigned-char)
        (local_name :pointer)
        (remote_name :pointer)
        (rcvs_outstanding :unsigned-char)
        (sends_outstanding :unsigned-char))





(defcstructex-exported SESSION_HEADER
        (sess_name :unsigned-char)
        (num_sess :unsigned-char)
        (rcv_dg_outstanding :unsigned-char)
        (rcv_any_outstanding :unsigned-char))





(defcfunex-exported ("Netbios" Netbios :convention :stdcall) :unsigned-char
  (arg0 :pointer))

