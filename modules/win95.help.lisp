
(cl:in-package w32apimod)

(define-w32api-module win95.help :win95.help)

(cl:in-package cl-w32api.module.win95.help)

(defcfunex-exported ("GetMenuContextHelpId" GetMenuContextHelpId :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetWindowContextHelpId" GetWindowContextHelpId :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("SetMenuContextHelpId" SetMenuContextHelpId :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetWindowContextHelpId" SetWindowContextHelpId :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))


(defcfunex-exported ("WinHelpA" WinHelpA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int)
  (arg3 :unsigned-long))

(defcfunex-exported ("WinHelpW" WinHelpW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-long))

