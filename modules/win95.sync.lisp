
(cl:in-package w32apimod)

(define-w32api-module win95.sync :win95.sync)

(cl:in-package cl-w32api.module.win95.sync)

(defcfunex-exported ("CancelWaitableTimer" CancelWaitableTimer :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CreateEventA" CreateEventA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :string))

(defcfunex-exported ("CreateEventW" CreateEventW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("CreateMutexA" CreateMutexA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("CreateMutexW" CreateMutexW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("CreateSemaphoreA" CreateSemaphoreA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :int32)
  (arg3 :string))

(defcfunex-exported ("CreateSemaphoreW" CreateSemaphoreW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :int32)
  (arg3 :pointer))

(defcfunex-exported ("CreateWaitableTimerA" CreateWaitableTimerA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("CreateWaitableTimerW" CreateWaitableTimerW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("DeleteCriticalSection" DeleteCriticalSection :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("EnterCriticalSection" EnterCriticalSection :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetOverlappedResult" GetOverlappedResult :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("InitializeCriticalSection" InitializeCriticalSection :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("InterlockedCompareExchange" InterlockedCompareExchange :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :int32))

(defcfunex-exported ("InterlockedDecrement" InterlockedDecrement :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("InterlockedExchange" InterlockedExchange :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int32))

(defcfunex-exported ("InterlockedExchangeAdd" InterlockedExchangeAdd :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :int32))

(defcfunex-exported ("InterlockedIncrement" InterlockedIncrement :convention :stdcall) :int32
  (arg0 :pointer))

(defcfunex-exported ("LeaveCriticalSection" LeaveCriticalSection :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("MsgWaitForMultipleObjects" MsgWaitForMultipleObjects :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("MsgWaitForMultipleObjectsEx" MsgWaitForMultipleObjectsEx :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long))

(defcfunex-exported ("OpenEventA" OpenEventA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("OpenEventW" OpenEventW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("OpenMutexA" OpenMutexA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("OpenMutexW" OpenMutexW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("OpenSemaphoreA" OpenSemaphoreA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("OpenSemaphoreW" OpenSemaphoreW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("OpenWaitableTimerA" OpenWaitableTimerA :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :string))

(defcfunex-exported ("OpenWaitableTimerW" OpenWaitableTimerW :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :pointer))

(defcfunex-exported ("PulseEvent" PulseEvent :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("QueueUserAPC" QueueUserAPC :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("ReleaseMutex" ReleaseMutex :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ReleaseSemaphore" ReleaseSemaphore :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int32)
  (arg2 :pointer))

(defcfunex-exported ("ResetEvent" ResetEvent :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetEvent" SetEvent :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("SetWaitableTimer" SetWaitableTimer :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("SignalObjectAndWait" SignalObjectAndWait :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :int))

(defcfunex-exported ("TryEnterCriticalSection" TryEnterCriticalSection :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("WaitForMultipleObjects" WaitForMultipleObjects :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("WaitForMultipleObjectsEx" WaitForMultipleObjectsEx :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :int))

(defcfunex-exported ("WaitForSingleObject" WaitForSingleObject :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("WaitForSingleObjectEx" WaitForSingleObjectEx :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int))

