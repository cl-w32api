(cl:in-package w32apimod)

(define-w32api-module shutdown :shutdown)

(cl:in-package cl-w32api.module.shutdown)


(defcfunex-exported ("AbortSystemShutdownA" AbortSystemShutdownA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("AbortSystemShutdownW" AbortSystemShutdownW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("InitiateSystemShutdownA" InitiateSystemShutdownA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("InitiateSystemShutdownW" InitiateSystemShutdownW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :int)
  (arg4 :int))

