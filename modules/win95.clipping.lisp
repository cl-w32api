
(cl:in-package w32apimod)

(define-w32api-module win95.clipping :win95.clipping)

(cl:in-package cl-w32api.module.win95.clipping)

(defcfunex-exported ("ExcludeClipRect" ExcludeClipRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("ExtSelectClipRgn" ExtSelectClipRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("GetClipBox" GetClipBox :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetClipRgn" GetClipRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetMetaRgn" GetMetaRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("IntersectClipRect" IntersectClipRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("OffsetClipRgn" OffsetClipRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("PtVisible" PtVisible :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int))

(defcfunex-exported ("RectVisible" RectVisible :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SelectClipPath" SelectClipPath :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SelectClipRgn" SelectClipRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetMetaRgn" SetMetaRgn :convention :stdcall) :int
  (arg0 :pointer))

