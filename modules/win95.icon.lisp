
(cl:in-package w32apimod)

(define-w32api-module win95.icon :win95.icon)

(cl:in-package cl-w32api.module.win95.icon)

(defcfunex-exported ("CopyIcon" CopyIcon :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateIcon" CreateIcon :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :unsigned-char)
  (arg4 :unsigned-char)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("CreateIconFromResource" CreateIconFromResource :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int)
  (arg3 :unsigned-long))

(defcfunex-exported ("CreateIconFromResourceEx" CreateIconFromResourceEx :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int)
  (arg3 :unsigned-long)
  (arg4 :int)
  (arg5 :int)
  (arg6 :unsigned-int))

(defcfunex-exported ("CreateIconIndirect" CreateIconIndirect :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("DestroyIcon" DestroyIcon :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DrawIcon" DrawIcon :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("DrawIconEx" DrawIconEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :int)
  (arg6 :unsigned-int)
  (arg7 :pointer)
  (arg8 :unsigned-int))

(defcfunex-exported ("GetIconInfo" GetIconInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("LoadIconA" LoadIconA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("LoadIconW" LoadIconW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("LookupIconIdFromDirectory" LookupIconIdFromDirectory :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("LookupIconIdFromDirectoryEx" LookupIconIdFromDirectoryEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :unsigned-int))


