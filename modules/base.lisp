
(in-package w32apimod)

(define-w32api-module base :base :system.base)

(in-package cl-w32api.module.base)

(define-foreign-library kernel32
    (t (:default "kernel32")))
(define-foreign-library user32
    (t (:default "user32")))
(define-foreign-library gdi32
    (t (:default "gdi32")))

(define-w32api-module-ctor ()
    (progn
      (use-foreign-library kernel32)
      (use-foreign-library user32)
      (use-foreign-library gdi32)

      (require-and-inherit-module "win95.dialogbox")
      (require-and-inherit-module "win95.wndclass")
      (require-and-inherit-module "win95.window")
      (require-and-inherit-module "win95.message")
      (require-and-inherit-module "win95.color")
      (require-and-inherit-module "win95.painting-drawing")
      (require-and-inherit-module "win95.dll")
      (require-and-inherit-module "win95.dc")
      (require-and-inherit-module "win95.font-text")
      (require-and-inherit-module "win95.cursor")      
      )
  )


