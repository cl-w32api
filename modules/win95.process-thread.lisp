
(cl:in-package w32apimod)

(define-w32api-module win95.process-thread :win95.process-thread)

(cl:in-package cl-w32api.module.win95.process-thread)

(defcfunex-exported ("AttachThreadInput" AttachThreadInput :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :int))

(defcfunex-exported ("CommandLineToArgvW" CommandLineToArgvW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CreateProcessA" CreateProcessA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :string)
  (arg8 :pointer)
  (arg9 :pointer))

(defcfunex-exported ("CreateProcessW" CreateProcessW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer))

(defcfunex-exported ("CreateProcessAsUserA" CreateProcessAsUserA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int)
  (arg6 :unsigned-long)
  (arg7 :pointer)
  (arg8 :string)
  (arg9 :pointer)
  (arg10 :pointer))

(defcfunex-exported ("CreateProcessAsUserW" CreateProcessAsUserW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int)
  (arg6 :unsigned-long)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer))

(defcfunex-exported ("CreateRemoteThread" CreateRemoteThread :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer))

(defcfunex-exported ("CreateThread" CreateThread :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("ExitProcess" ExitProcess :convention :stdcall) :void
  (arg0 :unsigned-int))

(defcfunex-exported ("ExitThread" ExitThread :convention :stdcall) :void
  (arg0 :unsigned-long))

(defcfunex-exported ("FreeEnvironmentStringsA" FreeEnvironmentStringsA :convention :stdcall) :int
  (arg0 :string))

(defcfunex-exported ("FreeEnvironmentStringsW" FreeEnvironmentStringsW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetCommandLineA" GetCommandLineA :convention :stdcall) :string)

(defcfunex-exported ("GetCommandLineW" GetCommandLineW :convention :stdcall) :pointer)

(defcfunex-exported ("GetCurrentProcess" GetCurrentProcess :convention :stdcall) :pointer)

(defcfunex-exported ("GetCurrentProcessId" GetCurrentProcessId :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetCurrentThread" GetCurrentThread :convention :stdcall) :pointer)

(defcfunex-exported ("GetCurrentThreadId" GetCurrentThreadId :convention :stdcall) :unsigned-long)

(defcfunex-exported ("GetEnvironmentStrings" GetEnvironmentStrings :convention :stdcall) :string)

(defcfunex-exported ("GetEnvironmentStringsA" GetEnvironmentStringsA :convention :stdcall) :string)

(defcfunex-exported ("GetEnvironmentStringsW" GetEnvironmentStringsW :convention :stdcall) :pointer)

(defcfunex-exported ("GetEnvironmentVariableA" GetEnvironmentVariableA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetEnvironmentVariableW" GetEnvironmentVariableW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetExitCodeProcess" GetExitCodeProcess :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetExitCodeThread" GetExitCodeThread :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetPriorityClass" GetPriorityClass :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetProcessAffinityMask" GetProcessAffinityMask :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetProcessShutdownParameters" GetProcessShutdownParameters :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetProcessTimes" GetProcessTimes :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("GetProcessVersion" GetProcessVersion :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long))

(defcfunex-exported ("GetProcessWorkingSetSize" GetProcessWorkingSetSize :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetStartupInfoA" GetStartupInfoA :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetStartupInfoW" GetStartupInfoW :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetThreadPriority" GetThreadPriority :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetThreadPriorityBoost" GetThreadPriorityBoost :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetThreadTimes" GetThreadTimes :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("OpenProcess" OpenProcess :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :int)
  (arg2 :unsigned-long))

(defcfunex-exported ("ResumeThread" ResumeThread :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("SetEnvironmentVariableA" SetEnvironmentVariableA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string))

(defcfunex-exported ("SetEnvironmentVariableW" SetEnvironmentVariableW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetPriorityClass" SetPriorityClass :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetProcessAffinityMask" SetProcessAffinityMask :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetProcessShutdownParameters" SetProcessShutdownParameters :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetProcessPriorityBoost" SetProcessPriorityBoost :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetProcessWorkingSetSize" SetProcessWorkingSetSize :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("SetThreadAffinityMask" SetThreadAffinityMask :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetThreadIdealProcessor" SetThreadIdealProcessor :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("SetThreadPriority" SetThreadPriority :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetThreadPriorityBoost" SetThreadPriorityBoost :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("Sleep" Sleep :convention :stdcall) :void
  (arg0 :unsigned-long))

(defcfunex-exported ("SleepEx" SleepEx :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :int))

(defcfunex-exported ("SuspendThread" SuspendThread :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("SwitchToThread" SwitchToThread :convention :stdcall) :int)

(defcfunex-exported ("TerminateProcess" TerminateProcess :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int))

(defcfunex-exported ("TerminateThread" TerminateThread :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("TlsAlloc" TlsAlloc :convention :stdcall) :unsigned-long)

(defcfunex-exported ("TlsFree" TlsFree :convention :stdcall) :int
  (arg0 :unsigned-long))

(defcfunex-exported ("TlsGetValue" TlsGetValue :convention :stdcall) :pointer
  (arg0 :unsigned-long))

(defcfunex-exported ("TlsSetValue" TlsSetValue :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("WaitForInputIdle" WaitForInputIdle :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

;;fiber

(defcfunex-exported ("ConvertThreadToFiber" ConvertThreadToFiber :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateFiber" CreateFiber :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("CreateFiberEx" CreateFiberEx :convention :stdcall) :pointer
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("DeleteFiber" DeleteFiber :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("GetCurrentFiber" GetCurrentFiber :convention :stdcall) :pointer)

(defcfunex-exported ("GetFiberData" GetFiberData :convention :stdcall) :pointer)

(defcfunex-exported ("SwitchToFiber" SwitchToFiber :convention :stdcall) :void
  (arg0 :pointer))

;;obsolete

(defcfunex-exported ("WinExec" WinExec :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :unsigned-int))

