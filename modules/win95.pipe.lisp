
(defcfunex-exported ("CallNamedPipeA" CallNamedPipeA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :unsigned-long))

(defcfunex-exported ("CallNamedPipeW" CallNamedPipeW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :unsigned-long))

(defcfunex-exported ("ConnectNamedPipe" ConnectNamedPipe :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("CreateNamedPipeA" CreateNamedPipeA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :pointer))

(defcfunex-exported ("CreateNamedPipeW" CreateNamedPipeW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :pointer))

(defcfunex-exported ("CreatePipe" CreatePipe :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("DisconnectNamedPipe" DisconnectNamedPipe :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetNamedPipeHandleStateA" GetNamedPipeHandleStateA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :string)
  (arg6 :unsigned-long))

(defcfunex-exported ("GetNamedPipeHandleStateW" GetNamedPipeHandleStateW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-long))

(defcfunex-exported ("GetNamedPipeInfo" GetNamedPipeInfo :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("PeekNamedPipe" PeekNamedPipe :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("SetNamedPipeHandleState" SetNamedPipeHandleState :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("TransactNamedPipe" TransactNamedPipe :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("WaitNamedPipeA" WaitNamedPipeA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long))

(defcfunex-exported ("WaitNamedPipeW" WaitNamedPipeW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

