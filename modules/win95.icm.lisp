
(defcfunex-exported ("CheckColorsInGamut" CheckColorsInGamut :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("ColorMatchToTarget" ColorMatchToTarget :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))


(defcfunex-exported ("CreateColorSpaceA" CreateColorSpaceA :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("CreateColorSpaceW" CreateColorSpaceW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("DeleteColorSpace" DeleteColorSpace :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnumICMProfilesA" EnumICMProfilesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("EnumICMProfilesW" EnumICMProfilesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int32))

(defcfunex-exported ("GetColorSpace" GetColorSpace :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetDeviceGammaRamp" GetDeviceGammaRamp :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("GetICMProfileA" GetICMProfileA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :string))

(defcfunex-exported ("GetICMProfileW" GetICMProfileW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetLogColorSpaceA" GetLogColorSpaceA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("GetLogColorSpaceW" GetLogColorSpaceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("SetColorSpace" SetColorSpace :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetDeviceGammaRamp" SetDeviceGammaRamp :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetICMMode" SetICMMode :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("SetICMProfileA" SetICMProfileA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string))

(defcfunex-exported ("SetICMProfileW" SetICMProfileW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("UpdateICMRegKeyA" UpdateICMRegKeyA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :string)
  (arg3 :unsigned-int))

(defcfunex-exported ("UpdateICMRegKeyW" UpdateICMRegKeyW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-int))

