
(cl:in-package w32apimod)

(define-w32api-module win95.comdlg :win95.comdlg)

(cl:in-package cl-w32api.module.win95.comdlg)

(defcfunex-exported ("ChooseColorA" ChooseColorA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ChooseColorW" ChooseColorW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ChooseFontA" ChooseFontA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ChooseFontW" ChooseFontW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("CommDlgExtendedError" CommDlgExtendedError :convention :stdcall) :unsigned-long)

(defcfunex-exported ("FindTextA" FindTextA :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("FindTextW" FindTextW :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetFileTitleA" GetFileTitleA :convention :stdcall) :short
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-short))

(defcfunex-exported ("GetFileTitleW" GetFileTitleW :convention :stdcall) :short
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-short))

(defcfunex-exported ("GetOpenFileNameA" GetOpenFileNameA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetOpenFileNameW" GetOpenFileNameW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetSaveFileNameA" GetSaveFileNameA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("GetSaveFileNameW" GetSaveFileNameW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("PageSetupDlgA" PageSetupDlgA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("PageSetupDlgW" PageSetupDlgW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("PrintDlgA" PrintDlgA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("PrintDlgW" PrintDlgW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ReplaceTextA" ReplaceTextA :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("ReplaceTextW" ReplaceTextW :convention :stdcall) :pointer
  (arg0 :pointer))

