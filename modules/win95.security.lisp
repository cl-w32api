
(cl:in-package w32apimod)

(define-w32api-module win95.security :win95.security)

(cl:in-package cl-w32api.module.win95.security)

(defcfunex-exported ("AccessCheck" AccessCheck :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("AccessCheckAndAuditAlarmA" AccessCheckAndAuditAlarmA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :string)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :int)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer))

(defcfunex-exported ("AccessCheckAndAuditAlarmW" AccessCheckAndAuditAlarmW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :int)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer))

(defcfunex-exported ("AddAccessAllowedAce" AddAccessAllowedAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("AddAccessDeniedAce" AddAccessDeniedAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("AddAce" AddAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("AddAuditAccessAce" AddAuditAccessAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :int)
  (arg5 :int))

(defcfunex-exported ("AdjustTokenGroups" AdjustTokenGroups :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("AdjustTokenPrivileges" AdjustTokenPrivileges :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("AllocateAndInitializeSid" AllocateAndInitializeSid :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-char)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :unsigned-long)
  (arg8 :unsigned-long)
  (arg9 :unsigned-long)
  (arg10 :pointer))

(defcfunex-exported ("AllocateLocallyUniqueId" AllocateLocallyUniqueId :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("AreAllAccessesGranted" AreAllAccessesGranted :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long))

(defcfunex-exported ("AreAnyAccessesGranted" AreAnyAccessesGranted :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-long))

(defcfunex-exported ("CopySid" CopySid :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("CreatePrivateObjectSecurity" CreatePrivateObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("DeleteAce" DeleteAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("DestroyPrivateObjectSecurity" DestroyPrivateObjectSecurity :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("DuplicateToken" DuplicateToken :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 SECURITY_IMPERSONATION_LEVEL)
  (arg2 :pointer))

(defcfunex-exported ("DuplicateTokenEx" DuplicateTokenEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 SECURITY_IMPERSONATION_LEVEL)
  (arg4 TOKEN_TYPE)
  (arg5 :pointer))

(defcfunex-exported ("EqualPrefixSid" EqualPrefixSid :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("EqualSid" EqualSid :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FindFirstFreeAce" FindFirstFreeAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("FreeSid" FreeSid :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetAce" GetAce :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("GetAclInformation" GetAclInformation :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 ACL_INFORMATION_CLASS))

(defcfunex-exported ("GetFileSecurityA" GetFileSecurityA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetFileSecurityW" GetFileSecurityW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetKernelObjectSecurity" GetKernelObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetLengthSid" GetLengthSid :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetPrivateObjectSecurity" GetPrivateObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("GetSecurityDescriptorControl" GetSecurityDescriptorControl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetSecurityDescriptorDacl" GetSecurityDescriptorDacl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetSecurityDescriptorGroup" GetSecurityDescriptorGroup :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetSecurityDescriptorLength" GetSecurityDescriptorLength :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("GetSecurityDescriptorOwner" GetSecurityDescriptorOwner :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("GetSecurityDescriptorSacl" GetSecurityDescriptorSacl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetSidIdentifierAuthority" GetSidIdentifierAuthority :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetSidLengthRequired" GetSidLengthRequired :convention :stdcall) :unsigned-long
  (arg0 :unsigned-char))

(defcfunex-exported ("GetSidSubAuthority" GetSidSubAuthority :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("GetSidSubAuthorityCount" GetSidSubAuthorityCount :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("GetTokenInformation" GetTokenInformation :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 TOKEN_INFORMATION_CLASS)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("ImpersonateLoggedOnUser" ImpersonateLoggedOnUser :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ImpersonateNamedPipeClient" ImpersonateNamedPipeClient :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ImpersonateSelf" ImpersonateSelf :convention :stdcall) :int
  (arg0 SECURITY_IMPERSONATION_LEVEL))

(defcfunex-exported ("InitializeAcl" InitializeAcl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long))

(defcfunex-exported ("InitializeSecurityDescriptor" InitializeSecurityDescriptor :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("InitializeSid" InitializeSid :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-char))

(defcfunex-exported ("IsValidAcl" IsValidAcl :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsValidSecurityDescriptor" IsValidSecurityDescriptor :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("IsValidSid" IsValidSid :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("LogonUserA" LogonUserA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("LogonUserW" LogonUserW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :pointer))

(defcfunex-exported ("LookupAccountNameA" LookupAccountNameA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :string)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("LookupAccountNameW" LookupAccountNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("LookupAccountSidA" LookupAccountSidA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :string)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("LookupAccountSidW" LookupAccountSidW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer))

(defcfunex-exported ("LookupPrivilegeDisplayNameA" LookupPrivilegeDisplayNameA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("LookupPrivilegeDisplayNameW" LookupPrivilegeDisplayNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("LookupPrivilegeNameA" LookupPrivilegeNameA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("LookupPrivilegeNameW" LookupPrivilegeNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("LookupPrivilegeValueA" LookupPrivilegeValueA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("LookupPrivilegeValueW" LookupPrivilegeValueW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("MakeAbsoluteSD" MakeAbsoluteSD :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer))

(defcfunex-exported ("MakeSelfRelativeSD" MakeSelfRelativeSD :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("MapGenericMask" MapGenericMask :convention :stdcall) :void
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ObjectCloseAuditAlarmA" ObjectCloseAuditAlarmA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("ObjectCloseAuditAlarmW" ObjectCloseAuditAlarmW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("ObjectDeleteAuditAlarmA" ObjectDeleteAuditAlarmA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("ObjectDeleteAuditAlarmW" ObjectDeleteAuditAlarmW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("ObjectOpenAuditAlarmA" ObjectOpenAuditAlarmA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :string)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-long)
  (arg7 :unsigned-long)
  (arg8 :pointer)
  (arg9 :int)
  (arg10 :int)
  (arg11 :pointer))

(defcfunex-exported ("ObjectOpenAuditAlarmW" ObjectOpenAuditAlarmW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :unsigned-long)
  (arg7 :unsigned-long)
  (arg8 :pointer)
  (arg9 :int)
  (arg10 :int)
  (arg11 :pointer))

(defcfunex-exported ("ObjectPrivilegeAuditAlarmA" ObjectPrivilegeAuditAlarmA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("ObjectPrivilegeAuditAlarmW" ObjectPrivilegeAuditAlarmW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("OpenProcessToken" OpenProcessToken :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("OpenThreadToken" OpenThreadToken :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int)
  (arg3 :pointer))

(defcfunex-exported ("PrivilegeCheck" PrivilegeCheck :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("PrivilegedServiceAuditAlarmA" PrivilegedServiceAuditAlarmA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int))

(defcfunex-exported ("PrivilegedServiceAuditAlarmW" PrivilegedServiceAuditAlarmW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :int))

(defcfunex-exported ("RevertToSelf" RevertToSelf :convention :stdcall) :int)

(defcfunex-exported ("SetAclInformation" SetAclInformation :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 ACL_INFORMATION_CLASS))

(defcfunex-exported ("SetFileSecurityA" SetFileSecurityA :convention :stdcall) :int
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetFileSecurityW" SetFileSecurityW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetKernelObjectSecurity" SetKernelObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetPrivateObjectSecurity" SetPrivateObjectSecurity :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("SetSecurityDescriptorDacl" SetSecurityDescriptorDacl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("SetSecurityDescriptorGroup" SetSecurityDescriptorGroup :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("SetSecurityDescriptorOwner" SetSecurityDescriptorOwner :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("SetSecurityDescriptorSacl" SetSecurityDescriptorSacl :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("SetThreadToken" SetThreadToken :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("SetTokenInformation" SetTokenInformation :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 TOKEN_INFORMATION_CLASS)
  (arg2 :pointer)
  (arg3 :unsigned-long))


