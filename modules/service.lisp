
(cl:in-package w32apimod)

(define-w32api-module service :service)

(cl:in-package cl-w32api.module.service)



(defconstant-exported SERVICES_ACTIVE_DATABASEA "ServicesActive")

(defconstant-exported SERVICES_FAILED_DATABASEA "ServicesFailed")

(defconstant-exported SC_GROUP_IDENTIFIERA #\+)

(defconstant-exported SC_MANAGER_ALL_ACCESS #xf003f)

(defconstant-exported SC_MANAGER_CONNECT 1)

(defconstant-exported SC_MANAGER_CREATE_SERVICE 2)

(defconstant-exported SC_MANAGER_ENUMERATE_SERVICE 4)

(defconstant-exported SC_MANAGER_LOCK 8)

(defconstant-exported SC_MANAGER_QUERY_LOCK_STATUS 16)

(defconstant-exported SC_MANAGER_MODIFY_BOOT_CONFIG 32)

(defconstant-exported SERVICE_NO_CHANGE #xffffffff)

(defconstant-exported SERVICE_STOPPED 1)

(defconstant-exported SERVICE_START_PENDING 2)

(defconstant-exported SERVICE_STOP_PENDING 3)

(defconstant-exported SERVICE_RUNNING 4)

(defconstant-exported SERVICE_CONTINUE_PENDING 5)

(defconstant-exported SERVICE_PAUSE_PENDING 6)

(defconstant-exported SERVICE_PAUSED 7)

(defconstant-exported SERVICE_ACCEPT_STOP 1)

(defconstant-exported SERVICE_ACCEPT_PAUSE_CONTINUE 2)

(defconstant-exported SERVICE_ACCEPT_SHUTDOWN 4)

(defconstant-exported SERVICE_ACCEPT_PARAMCHANGE 8)

(defconstant-exported SERVICE_ACCEPT_NETBINDCHANGE 16)

(defconstant-exported SERVICE_ACCEPT_HARDWAREPROFILECHANGE 32)

(defconstant-exported SERVICE_ACCEPT_POWEREVENT 64)

(defconstant-exported SERVICE_ACCEPT_SESSIONCHANGE 128)

(defconstant-exported SERVICE_CONTROL_STOP 1)

(defconstant-exported SERVICE_CONTROL_PAUSE 2)

(defconstant-exported SERVICE_CONTROL_CONTINUE 3)

(defconstant-exported SERVICE_CONTROL_INTERROGATE 4)

(defconstant-exported SERVICE_CONTROL_SHUTDOWN 5)

(defconstant-exported SERVICE_CONTROL_PARAMCHANGE 6)

(defconstant-exported SERVICE_CONTROL_NETBINDADD 7)

(defconstant-exported SERVICE_CONTROL_NETBINDREMOVE 8)

(defconstant-exported SERVICE_CONTROL_NETBINDENABLE 9)

(defconstant-exported SERVICE_CONTROL_NETBINDDISABLE 10)

(defconstant-exported SERVICE_CONTROL_DEVICEEVENT 11)

(defconstant-exported SERVICE_CONTROL_HARDWAREPROFILECHANGE 12)

(defconstant-exported SERVICE_CONTROL_POWEREVENT 13)

(defconstant-exported SERVICE_CONTROL_SESSIONCHANGE 14)

(defconstant-exported SERVICE_ACTIVE 1)

(defconstant-exported SERVICE_INACTIVE 2)

(defconstant-exported SERVICE_STATE_ALL 3)

(defconstant-exported SERVICE_QUERY_CONFIG 1)

(defconstant-exported SERVICE_CHANGE_CONFIG 2)

(defconstant-exported SERVICE_QUERY_STATUS 4)

(defconstant-exported SERVICE_ENUMERATE_DEPENDENTS 8)

(defconstant-exported SERVICE_START 16)

(defconstant-exported SERVICE_STOP 32)

(defconstant-exported SERVICE_PAUSE_CONTINUE 64)

(defconstant-exported SERVICE_INTERROGATE 128)

(defconstant-exported SERVICE_USER_DEFINED_CONTROL 256)

(defconstant-exported SERVICE_ALL_ACCESS (cl:logior #xF0000 1 2 4 8 16 32 64 128 256))

(defconstant-exported SERVICE_RUNS_IN_SYSTEM_PROCESS 1)

(defconstant-exported SERVICE_CONFIG_DESCRIPTION 1)

(defconstant-exported SERVICE_CONFIG_FAILURE_ACTIONS 2)

(defcstructex-exported SERVICE_STATUS
        (dwServiceType :unsigned-long)
        (dwCurrentState :unsigned-long)
        (dwControlsAccepted :unsigned-long)
        (dwWin32ExitCode :unsigned-long)
        (dwServiceSpecificExitCode :unsigned-long)
        (dwCheckPoint :unsigned-long)
        (dwWaitHint :unsigned-long))





(defcstructex-exported SERVICE_STATUS_PROCESS
        (dwServiceType :unsigned-long)
        (dwCurrentState :unsigned-long)
        (dwControlsAccepted :unsigned-long)
        (dwWin32ExitCode :unsigned-long)
        (dwServiceSpecificExitCode :unsigned-long)
        (dwCheckPoint :unsigned-long)
        (dwWaitHint :unsigned-long)
        (dwProcessId :unsigned-long)
        (dwServiceFlags :unsigned-long))





(cffi:defcenum SC_STATUS_TYPE
        (:SC_STATUS_PROCESS_INFO #.0))

(cffi:defcenum SC_ENUM_TYPE
        (:SC_ENUM_PROCESS_INFO #.0))

(defcstructex-exported ENUM_SERVICE_STATUSA
        (lpServiceName :string)
        (lpDisplayName :string)
        (ServiceStatus SERVICE_STATUS))





(defcstructex-exported ENUM_SERVICE_STATUSW
        (lpServiceName :pointer)
        (lpDisplayName :pointer)
        (ServiceStatus SERVICE_STATUS))





(defcstructex-exported ENUM_SERVICE_STATUS_PROCESSA
        (lpServiceName :string)
        (lpDisplayName :string)
        (ServiceStatusProcess SERVICE_STATUS_PROCESS))





(defcstructex-exported ENUM_SERVICE_STATUS_PROCESSW
        (lpServiceName :pointer)
        (lpDisplayName :pointer)
        (ServiceStatusProcess SERVICE_STATUS_PROCESS))





(defcstructex-exported QUERY_SERVICE_CONFIGA
        (dwServiceType :unsigned-long)
        (dwStartType :unsigned-long)
        (dwErrorControl :unsigned-long)
        (lpBinaryPathName :string)
        (lpLoadOrderGroup :string)
        (dwTagId :unsigned-long)
        (lpDependencies :string)
        (lpServiceStartName :string)
        (lpDisplayName :string))





(defcstructex-exported QUERY_SERVICE_CONFIGW
        (dwServiceType :unsigned-long)
        (dwStartType :unsigned-long)
        (dwErrorControl :unsigned-long)
        (lpBinaryPathName :pointer)
        (lpLoadOrderGroup :pointer)
        (dwTagId :unsigned-long)
        (lpDependencies :pointer)
        (lpServiceStartName :pointer)
        (lpDisplayName :pointer))





(defcstructex-exported QUERY_SERVICE_LOCK_STATUSA
        (fIsLocked :unsigned-long)
        (lpLockOwner :string)
        (dwLockDuration :unsigned-long))





(defcstructex-exported QUERY_SERVICE_LOCK_STATUSW
        (fIsLocked :unsigned-long)
        (lpLockOwner :pointer)
        (dwLockDuration :unsigned-long))









(defcstructex-exported SERVICE_TABLE_ENTRYA
        (lpServiceName :string)
        (lpServiceProc :pointer))





(defcstructex-exported SERVICE_TABLE_ENTRYW
        (lpServiceName :pointer)
        (lpServiceProc :pointer))





(defcstructex-exported SC_HANDLE__
        (i :int))













(defcstructex-exported SERVICE_DESCRIPTIONA
        (lpDescription :string))





(defcstructex-exported SERVICE_DESCRIPTIONW
        (lpDescription :pointer))





(cffi:defcenum SC_ACTION_TYPE
        (:SC_ACTION_NONE #.0)
        (:SC_ACTION_RESTART #.1)
        (:SC_ACTION_REBOOT #.2)
        (:SC_ACTION_RUN_COMMAND #.3))

(defcstructex-exported SC_ACTION
        (Type SC_ACTION_TYPE)
        (Delay :unsigned-long))





(defcstructex-exported SERVICE_FAILURE_ACTIONSA
        (dwResetPeriod :unsigned-long)
        (lpRebootMsg :string)
        (lpCommand :string)
        (cActions :unsigned-long)
        (lpsaActions :pointer))





(defcstructex-exported SERVICE_FAILURE_ACTIONSW
        (dwResetPeriod :unsigned-long)
        (lpRebootMsg :pointer)
        (lpCommand :pointer)
        (cActions :unsigned-long)
        (lpsaActions :pointer))





(defcfunex-exported ("ChangeServiceConfigA" ChangeServiceConfigA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :string)
  (arg5 :string)
  (arg6 :pointer)
  (arg7 :string)
  (arg8 :string)
  (arg9 :string)
  (arg10 :string))

(defcfunex-exported ("ChangeServiceConfigW" ChangeServiceConfigW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer))

(defcfunex-exported ("ChangeServiceConfig2A" ChangeServiceConfig2A :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("ChangeServiceConfig2W" ChangeServiceConfig2W :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("CloseServiceHandle" CloseServiceHandle :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ControlService" ControlService :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("CreateServiceA" CreateServiceA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :string)
  (arg8 :string)
  (arg9 :pointer)
  (arg10 :string)
  (arg11 :string)
  (arg12 :string))

(defcfunex-exported ("CreateServiceW" CreateServiceW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :unsigned-long)
  (arg5 :unsigned-long)
  (arg6 :unsigned-long)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer)
  (arg10 :pointer)
  (arg11 :pointer)
  (arg12 :pointer))

(defcfunex-exported ("DeleteService" DeleteService :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("EnumDependentServicesA" EnumDependentServicesA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumDependentServicesW" EnumDependentServicesW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :pointer))

(defcfunex-exported ("EnumServicesStatusA" EnumServicesStatusA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("EnumServicesStatusW" EnumServicesStatusW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("EnumServicesStatusExA" EnumServicesStatusExA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 SC_ENUM_TYPE)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :string))

(defcfunex-exported ("EnumServicesStatusExW" EnumServicesStatusExW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 SC_ENUM_TYPE)
  (arg2 :unsigned-long)
  (arg3 :unsigned-long)
  (arg4 :pointer)
  (arg5 :unsigned-long)
  (arg6 :pointer)
  (arg7 :pointer)
  (arg8 :pointer)
  (arg9 :pointer))

(defcfunex-exported ("GetServiceDisplayNameA" GetServiceDisplayNameA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("GetServiceDisplayNameW" GetServiceDisplayNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("GetServiceKeyNameA" GetServiceKeyNameA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("GetServiceKeyNameW" GetServiceKeyNameW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("LockServiceDatabase" LockServiceDatabase :convention :stdcall) :pointer
  (arg0 :pointer))

(defcfunex-exported ("NotifyBootConfigStatus" NotifyBootConfigStatus :convention :stdcall) :int
  (arg0 :int))

(defcfunex-exported ("OpenSCManagerA" OpenSCManagerA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("OpenSCManagerW" OpenSCManagerW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("OpenServiceA" OpenServiceA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long))

(defcfunex-exported ("OpenServiceW" OpenServiceW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long))

(defcfunex-exported ("QueryServiceConfigA" QueryServiceConfigA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("QueryServiceConfigW" QueryServiceConfigW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("QueryServiceConfig2A" QueryServiceConfig2A :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("QueryServiceConfig2W" QueryServiceConfig2W :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("QueryServiceLockStatusA" QueryServiceLockStatusA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("QueryServiceLockStatusW" QueryServiceLockStatusW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer))

(defcfunex-exported ("QueryServiceObjectSecurity" QueryServiceObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("QueryServiceStatus" QueryServiceStatus :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("QueryServiceStatusEx" QueryServiceStatusEx :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 SC_STATUS_TYPE)
  (arg2 :pointer)
  (arg3 :unsigned-long)
  (arg4 :pointer))

(defcfunex-exported ("RegisterServiceCtrlHandlerA" RegisterServiceCtrlHandlerA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("RegisterServiceCtrlHandlerW" RegisterServiceCtrlHandlerW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("RegisterServiceCtrlHandlerExA" RegisterServiceCtrlHandlerExA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("RegisterServiceCtrlHandlerExW" RegisterServiceCtrlHandlerExW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("SetServiceObjectSecurity" SetServiceObjectSecurity :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("SetServiceStatus" SetServiceStatus :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :pointer))

(defcfunex-exported ("StartServiceA" StartServiceA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("StartServiceCtrlDispatcherA" StartServiceCtrlDispatcherA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("StartServiceCtrlDispatcherW" StartServiceCtrlDispatcherW :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("StartServiceW" StartServiceW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("UnlockServiceDatabase" UnlockServiceDatabase :convention :stdcall) :int
  (arg0 :pointer))




(defconstant-exported SERVICES_ACTIVE_DATABASE "ServicesActive")

(defconstant-exported SERVICES_FAILED_DATABASE "ServicesFailed")

(defconstant-exported SC_GROUP_IDENTIFIER SC_GROUP_IDENTIFIERA)


