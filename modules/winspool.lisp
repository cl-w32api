(cl:in-package w32apimod)

(define-w32api-module winspool :winspool)

(cl:in-package cl-w32api.module.winspool)



(defconstant-exported DI_CHANNEL 1)

(defconstant-exported DI_CHANNEL_WRITE 2)

(defconstant-exported DI_READ_SPOOL_JOB 3)

(defconstant-exported FORM_BUILTIN 1)

(defconstant-exported JOB_CONTROL_PAUSE 1)

(defconstant-exported JOB_CONTROL_RESUME 2)

(defconstant-exported JOB_CONTROL_CANCEL 3)

(defconstant-exported JOB_CONTROL_RESTART 4)

(defconstant-exported JOB_CONTROL_DELETE 5)

(defconstant-exported JOB_STATUS_PAUSED 1)

(defconstant-exported JOB_STATUS_ERROR 2)

(defconstant-exported JOB_STATUS_DELETING 4)

(defconstant-exported JOB_STATUS_SPOOLING 8)

(defconstant-exported JOB_STATUS_PRINTING 16)

(defconstant-exported JOB_STATUS_OFFLINE 32)

(defconstant-exported JOB_STATUS_PAPEROUT #x40)

(defconstant-exported JOB_STATUS_PRINTED #x80)

(defconstant-exported JOB_STATUS_DELETED #x100)

(defconstant-exported JOB_STATUS_BLOCKED_DEVQ #x200)

(defconstant-exported JOB_STATUS_USER_INTERVENTION #x400)

(defconstant-exported JOB_POSITION_UNSPECIFIED 0)

(defconstant-exported JOB_NOTIFY_TYPE 1)

(defconstant-exported JOB_NOTIFY_FIELD_PRINTER_NAME 0)

(defconstant-exported JOB_NOTIFY_FIELD_MACHINE_NAME 1)

(defconstant-exported JOB_NOTIFY_FIELD_PORT_NAME 2)

(defconstant-exported JOB_NOTIFY_FIELD_USER_NAME 3)

(defconstant-exported JOB_NOTIFY_FIELD_NOTIFY_NAME 4)

(defconstant-exported JOB_NOTIFY_FIELD_DATATYPE 5)

(defconstant-exported JOB_NOTIFY_FIELD_PRINT_PROCESSOR 6)

(defconstant-exported JOB_NOTIFY_FIELD_PARAMETERS 7)

(defconstant-exported JOB_NOTIFY_FIELD_DRIVER_NAME 8)

(defconstant-exported JOB_NOTIFY_FIELD_DEVMODE 9)

(defconstant-exported JOB_NOTIFY_FIELD_STATUS 10)

(defconstant-exported JOB_NOTIFY_FIELD_STATUS_STRING 11)

(defconstant-exported JOB_NOTIFY_FIELD_SECURITY_DESCRIPTOR 12)

(defconstant-exported JOB_NOTIFY_FIELD_DOCUMENT 13)

(defconstant-exported JOB_NOTIFY_FIELD_PRIORITY 14)

(defconstant-exported JOB_NOTIFY_FIELD_POSITION 15)

(defconstant-exported JOB_NOTIFY_FIELD_SUBMITTED 16)

(defconstant-exported JOB_NOTIFY_FIELD_START_TIME 17)

(defconstant-exported JOB_NOTIFY_FIELD_UNTIL_TIME 18)

(defconstant-exported JOB_NOTIFY_FIELD_TIME 19)

(defconstant-exported JOB_NOTIFY_FIELD_TOTAL_PAGES 20)

(defconstant-exported JOB_NOTIFY_FIELD_PAGES_PRINTED 21)

(defconstant-exported JOB_NOTIFY_FIELD_TOTAL_BYTES 22)

(defconstant-exported JOB_NOTIFY_FIELD_BYTES_PRINTED 23)

(defconstant-exported JOB_ACCESS_ADMINISTER 16)

(defconstant-exported JOB_ALL_ACCESS (cl:logior #xF0000 16))

(defconstant-exported JOB_READ (cl:logior #x20000 16))

(defconstant-exported JOB_WRITE (cl:logior #x20000 16))

(defconstant-exported JOB_EXECUTE (cl:logior #x20000 16))

(defconstant-exported PRINTER_NOTIFY_OPTIONS_REFRESH 1)

(defconstant-exported PRINTER_ACCESS_ADMINISTER 4)

(defconstant-exported PRINTER_ACCESS_USE 8)

(defconstant-exported PRINTER_ERROR_INFORMATION #x80000000)

(defconstant-exported PRINTER_ERROR_WARNING #x40000000)

(defconstant-exported PRINTER_ERROR_SEVERE #x20000000)

(defconstant-exported PRINTER_ERROR_OUTOFPAPER 1)

(defconstant-exported PRINTER_ERROR_JAM 2)

(defconstant-exported PRINTER_ERROR_OUTOFTONER 4)

(defconstant-exported PRINTER_CONTROL_PAUSE 1)

(defconstant-exported PRINTER_CONTROL_RESUME 2)

(defconstant-exported PRINTER_CONTROL_PURGE 3)

(defconstant-exported PRINTER_CONTROL_SET_STATUS 4)

(defconstant-exported PRINTER_STATUS_PAUSED 1)

(defconstant-exported PRINTER_STATUS_ERROR 2)

(defconstant-exported PRINTER_STATUS_PENDING_DELETION 4)

(defconstant-exported PRINTER_STATUS_PAPER_JAM 8)

(defconstant-exported PRINTER_STATUS_PAPER_OUT #x10)

(defconstant-exported PRINTER_STATUS_MANUAL_FEED #x20)

(defconstant-exported PRINTER_STATUS_PAPER_PROBLEM #x40)

(defconstant-exported PRINTER_STATUS_OFFLINE #x80)

(defconstant-exported PRINTER_STATUS_IO_ACTIVE #x100)

(defconstant-exported PRINTER_STATUS_BUSY #x200)

(defconstant-exported PRINTER_STATUS_PRINTING #x400)

(defconstant-exported PRINTER_STATUS_OUTPUT_BIN_FULL #x800)

(defconstant-exported PRINTER_STATUS_NOT_AVAILABLE #x1000)

(defconstant-exported PRINTER_STATUS_WAITING #x2000)

(defconstant-exported PRINTER_STATUS_PROCESSING #x4000)

(defconstant-exported PRINTER_STATUS_INITIALIZING #x8000)

(defconstant-exported PRINTER_STATUS_WARMING_UP #x10000)

(defconstant-exported PRINTER_STATUS_TONER_LOW #x20000)

(defconstant-exported PRINTER_STATUS_NO_TONER #x40000)

(defconstant-exported PRINTER_STATUS_PAGE_PUNT #x80000)

(defconstant-exported PRINTER_STATUS_USER_INTERVENTION #x100000)

(defconstant-exported PRINTER_STATUS_OUT_OF_MEMORY #x200000)

(defconstant-exported PRINTER_STATUS_DOOR_OPEN #x400000)

(defconstant-exported PRINTER_STATUS_SERVER_UNKNOWN #x800000)

(defconstant-exported PRINTER_STATUS_POWER_SAVE #x1000000)

(defconstant-exported PRINTER_ATTRIBUTE_QUEUED 1)

(defconstant-exported PRINTER_ATTRIBUTE_DIRECT 2)

(defconstant-exported PRINTER_ATTRIBUTE_DEFAULT 4)

(defconstant-exported PRINTER_ATTRIBUTE_SHARED 8)

(defconstant-exported PRINTER_ATTRIBUTE_NETWORK #x10)

(defconstant-exported PRINTER_ATTRIBUTE_HIDDEN #x20)

(defconstant-exported PRINTER_ATTRIBUTE_LOCAL #x40)

(defconstant-exported PRINTER_ATTRIBUTE_ENABLE_DEVQ #x80)

(defconstant-exported PRINTER_ATTRIBUTE_KEEPPRINTEDJOBS #x100)

(defconstant-exported PRINTER_ATTRIBUTE_DO_COMPLETE_FIRST #x200)

(defconstant-exported PRINTER_ATTRIBUTE_WORK_OFFLINE #x400)

(defconstant-exported PRINTER_ATTRIBUTE_ENABLE_BIDI #x800)

(defconstant-exported PRINTER_ATTRIBUTE_RAW_ONLY #x1000)

(defconstant-exported PRINTER_ATTRIBUTE_PUBLISHED #x2000)

(defconstant-exported PRINTER_ENUM_DEFAULT 1)

(defconstant-exported PRINTER_ENUM_LOCAL 2)

(defconstant-exported PRINTER_ENUM_CONNECTIONS 4)

(defconstant-exported PRINTER_ENUM_FAVORITE 4)

(defconstant-exported PRINTER_ENUM_NAME 8)

(defconstant-exported PRINTER_ENUM_REMOTE 16)

(defconstant-exported PRINTER_ENUM_SHARED 32)

(defconstant-exported PRINTER_ENUM_NETWORK #x40)

(defconstant-exported PRINTER_ENUM_EXPAND #x4000)

(defconstant-exported PRINTER_ENUM_CONTAINER #x8000)

(defconstant-exported PRINTER_ENUM_ICONMASK #xff0000)

(defconstant-exported PRINTER_ENUM_ICON1 #x10000)

(defconstant-exported PRINTER_ENUM_ICON2 #x20000)

(defconstant-exported PRINTER_ENUM_ICON3 #x40000)

(defconstant-exported PRINTER_ENUM_ICON4 #x80000)

(defconstant-exported PRINTER_ENUM_ICON5 #x100000)

(defconstant-exported PRINTER_ENUM_ICON6 #x200000)

(defconstant-exported PRINTER_ENUM_ICON7 #x400000)

(defconstant-exported PRINTER_ENUM_ICON8 #x800000)

(defconstant-exported PRINTER_NOTIFY_TYPE 0)

(defconstant-exported PRINTER_NOTIFY_FIELD_SERVER_NAME 0)

(defconstant-exported PRINTER_NOTIFY_FIELD_PRINTER_NAME 1)

(defconstant-exported PRINTER_NOTIFY_FIELD_SHARE_NAME 2)

(defconstant-exported PRINTER_NOTIFY_FIELD_PORT_NAME 3)

(defconstant-exported PRINTER_NOTIFY_FIELD_DRIVER_NAME 4)

(defconstant-exported PRINTER_NOTIFY_FIELD_COMMENT 5)

(defconstant-exported PRINTER_NOTIFY_FIELD_LOCATION 6)

(defconstant-exported PRINTER_NOTIFY_FIELD_DEVMODE 7)

(defconstant-exported PRINTER_NOTIFY_FIELD_SEPFILE 8)

(defconstant-exported PRINTER_NOTIFY_FIELD_PRINT_PROCESSOR 9)

(defconstant-exported PRINTER_NOTIFY_FIELD_PARAMETERS 10)

(defconstant-exported PRINTER_NOTIFY_FIELD_DATATYPE 11)

(defconstant-exported PRINTER_NOTIFY_FIELD_SECURITY_DESCRIPTOR 12)

(defconstant-exported PRINTER_NOTIFY_FIELD_ATTRIBUTES 13)

(defconstant-exported PRINTER_NOTIFY_FIELD_PRIORITY 14)

(defconstant-exported PRINTER_NOTIFY_FIELD_DEFAULT_PRIORITY 15)

(defconstant-exported PRINTER_NOTIFY_FIELD_START_TIME 16)

(defconstant-exported PRINTER_NOTIFY_FIELD_UNTIL_TIME 17)

(defconstant-exported PRINTER_NOTIFY_FIELD_STATUS 18)

(defconstant-exported PRINTER_NOTIFY_FIELD_STATUS_STRING 19)

(defconstant-exported PRINTER_NOTIFY_FIELD_CJOBS 20)

(defconstant-exported PRINTER_NOTIFY_FIELD_AVERAGE_PPM 21)

(defconstant-exported PRINTER_NOTIFY_FIELD_TOTAL_PAGES 22)

(defconstant-exported PRINTER_NOTIFY_FIELD_PAGES_PRINTED 23)

(defconstant-exported PRINTER_NOTIFY_FIELD_TOTAL_BYTES 24)

(defconstant-exported PRINTER_NOTIFY_FIELD_BYTES_PRINTED 25)

(defconstant-exported PRINTER_CHANGE_ADD_PRINTER 1)

(defconstant-exported PRINTER_CHANGE_SET_PRINTER 2)

(defconstant-exported PRINTER_CHANGE_DELETE_PRINTER 4)

(defconstant-exported PRINTER_CHANGE_FAILED_CONNECTION_PRINTER 8)

(defconstant-exported PRINTER_CHANGE_PRINTER #xFF)

(defconstant-exported PRINTER_CHANGE_ADD_JOB #x100)

(defconstant-exported PRINTER_CHANGE_SET_JOB #x200)

(defconstant-exported PRINTER_CHANGE_DELETE_JOB #x400)

(defconstant-exported PRINTER_CHANGE_WRITE_JOB #x800)

(defconstant-exported PRINTER_CHANGE_JOB #xFF00)

(defconstant-exported PRINTER_CHANGE_ADD_FORM #x10000)

(defconstant-exported PRINTER_CHANGE_SET_FORM #x20000)

(defconstant-exported PRINTER_CHANGE_DELETE_FORM #x40000)

(defconstant-exported PRINTER_CHANGE_FORM #x70000)

(defconstant-exported PRINTER_CHANGE_ADD_PORT #x100000)

(defconstant-exported PRINTER_CHANGE_CONFIGURE_PORT #x200000)

(defconstant-exported PRINTER_CHANGE_DELETE_PORT #x400000)

(defconstant-exported PRINTER_CHANGE_PORT #x700000)

(defconstant-exported PRINTER_CHANGE_ADD_PRINT_PROCESSOR #x1000000)

(defconstant-exported PRINTER_CHANGE_DELETE_PRINT_PROCESSOR #x4000000)

(defconstant-exported PRINTER_CHANGE_PRINT_PROCESSOR #x7000000)

(defconstant-exported PRINTER_CHANGE_ADD_PRINTER_DRIVER #x10000000)

(defconstant-exported PRINTER_CHANGE_SET_PRINTER_DRIVER #x20000000)

(defconstant-exported PRINTER_CHANGE_DELETE_PRINTER_DRIVER #x40000000)

(defconstant-exported PRINTER_CHANGE_PRINTER_DRIVER #x70000000)

(defconstant-exported PRINTER_CHANGE_TIMEOUT #x80000000)

(defconstant-exported PRINTER_CHANGE_ALL #x7777FFFF)

(defconstant-exported PRINTER_NOTIFY_INFO_DISCARDED 1)

(defconstant-exported PRINTER_ALL_ACCESS (cl:logior #xF0000 4 8))

(defconstant-exported PRINTER_READ (cl:logior #x20000 8))

(defconstant-exported PRINTER_WRITE (cl:logior #x20000 8))

(defconstant-exported PRINTER_EXECUTE (cl:logior #x20000 8))

(defconstant-exported NO_PRIORITY 0)

(defconstant-exported MAX_PRIORITY 99)

(defconstant-exported MIN_PRIORITY 1)

(defconstant-exported DEF_PRIORITY 1)

(defconstant-exported PORT_TYPE_WRITE 1)

(defconstant-exported PORT_TYPE_READ 2)

(defconstant-exported PORT_TYPE_REDIRECTED 4)

(defconstant-exported PORT_TYPE_NET_ATTACHED 8)

(defconstant-exported SERVER_ACCESS_ADMINISTER 1)

(defconstant-exported SERVER_ACCESS_ENUMERATE 2)

(defconstant-exported SERVER_ALL_ACCESS (cl:logior #xF0000 1 2))

(defconstant-exported SERVER_READ (cl:logior #x20000 2))

(defconstant-exported SERVER_WRITE (cl:logior #x20000 1 2))

(defconstant-exported SERVER_EXECUTE (cl:logior #x20000 2))

(defconstant-exported PORT_STATUS_TYPE_ERROR 1)

(defconstant-exported PORT_STATUS_TYPE_WARNING 2)

(defconstant-exported PORT_STATUS_TYPE_INFO 3)

(defconstant-exported PORT_STATUS_OFFLINE 1)

(defconstant-exported PORT_STATUS_PAPER_JAM 2)

(defconstant-exported PORT_STATUS_PAPER_OUT 3)

(defconstant-exported PORT_STATUS_OUTPUT_BIN_FULL 4)

(defconstant-exported PORT_STATUS_PAPER_PROBLEM 5)

(defconstant-exported PORT_STATUS_NO_TONER 6)

(defconstant-exported PORT_STATUS_DOOR_OPEN 7)

(defconstant-exported PORT_STATUS_USER_INTERVENTION 8)

(defconstant-exported PORT_STATUS_OUT_OF_MEMORY 9)

(defconstant-exported PORT_STATUS_TONER_LOW 10)

(defconstant-exported PORT_STATUS_WARMING_UP 11)

(defconstant-exported PORT_STATUS_POWER_SAVE 12)

(defcstructex-exported ADDJOB_INFO_1A
        (Path :string)
        (JobId :unsigned-long))







(defcstructex-exported ADDJOB_INFO_1W
        (Path :pointer)
        (JobId :unsigned-long))







(defcstructex-exported DATATYPES_INFO_1A
        (pName :string))







(defcstructex-exported DATATYPES_INFO_1W
        (pName :pointer))







(defcstructex-exported JOB_INFO_1A
        (JobId :unsigned-long)
        (pPrinterName :string)
        (pMachineName :string)
        (pUserName :string)
        (pDocument :string)
        (pDatatype :string)
        (pStatus :string)
        (Status :unsigned-long)
        (Priority :unsigned-long)
        (Position :unsigned-long)
        (TotalPages :unsigned-long)
        (PagesPrinted :unsigned-long)
        (Submitted SYSTEMTIME))







(defcstructex-exported JOB_INFO_1W
        (JobId :unsigned-long)
        (pPrinterName :pointer)
        (pMachineName :pointer)
        (pUserName :pointer)
        (pDocument :pointer)
        (pDatatype :pointer)
        (pStatus :pointer)
        (Status :unsigned-long)
        (Priority :unsigned-long)
        (Position :unsigned-long)
        (TotalPages :unsigned-long)
        (PagesPrinted :unsigned-long)
        (Submitted SYSTEMTIME))







(defcstructex-exported JOB_INFO_2A
        (JobId :unsigned-long)
        (pPrinterName :string)
        (pMachineName :string)
        (pUserName :string)
        (pDocument :string)
        (pNotifyName :string)
        (pDatatype :string)
        (pPrintProcessor :string)
        (pParameters :string)
        (pDriverName :string)
        (pDevMode :pointer)
        (pStatus :string)
        (pSecurityDescriptor :pointer)
        (Status :unsigned-long)
        (Priority :unsigned-long)
        (Position :unsigned-long)
        (StartTime :unsigned-long)
        (UntilTime :unsigned-long)
        (TotalPages :unsigned-long)
        (Size :unsigned-long)
        (Submitted SYSTEMTIME)
        (Time :unsigned-long)
        (PagesPrinted :unsigned-long))







(defcstructex-exported JOB_INFO_2W
        (JobId :unsigned-long)
        (pPrinterName :pointer)
        (pMachineName :pointer)
        (pUserName :pointer)
        (pDocument :pointer)
        (pNotifyName :pointer)
        (pDatatype :pointer)
        (pPrintProcessor :pointer)
        (pParameters :pointer)
        (pDriverName :pointer)
        (pDevMode :pointer)
        (pStatus :pointer)
        (pSecurityDescriptor :pointer)
        (Status :unsigned-long)
        (Priority :unsigned-long)
        (Position :unsigned-long)
        (StartTime :unsigned-long)
        (UntilTime :unsigned-long)
        (TotalPages :unsigned-long)
        (Size :unsigned-long)
        (Submitted SYSTEMTIME)
        (Time :unsigned-long)
        (PagesPrinted :unsigned-long))







(defcstructex-exported DOC_INFO_1A
        (pDocName :string)
        (pOutputFile :string)
        (pDatatype :string))







(defcstructex-exported DOC_INFO_1W
        (pDocName :pointer)
        (pOutputFile :pointer)
        (pDatatype :pointer))







(defcstructex-exported DOC_INFO_2A
        (pDocName :string)
        (pOutputFile :string)
        (pDatatype :string)
        (dwMode :unsigned-long)
        (JobId :unsigned-long))







(defcstructex-exported DOC_INFO_2W
        (pDocName :pointer)
        (pOutputFile :pointer)
        (pDatatype :pointer)
        (dwMode :unsigned-long)
        (JobId :unsigned-long))







(defcstructex-exported DRIVER_INFO_1A
        (pName :string))







(defcstructex-exported DRIVER_INFO_1W
        (pName :pointer))







(defcstructex-exported DRIVER_INFO_2A
        (cVersion :unsigned-long)
        (pName :string)
        (pEnvironment :string)
        (pDriverPath :string)
        (pDataFile :string)
        (pConfigFile :string))







(defcstructex-exported DRIVER_INFO_2W
        (cVersion :unsigned-long)
        (pName :pointer)
        (pEnvironment :pointer)
        (pDriverPath :pointer)
        (pDataFile :pointer)
        (pConfigFile :pointer))







(defcstructex-exported DRIVER_INFO_3A
        (cVersion :unsigned-long)
        (pName :string)
        (pEnvironment :string)
        (pDriverPath :string)
        (pDataFile :string)
        (pConfigFile :string)
        (pHelpFile :string)
        (pDependentFiles :string)
        (pMonitorName :string)
        (pDefaultDataType :string))







(defcstructex-exported DRIVER_INFO_3W
        (cVersion :unsigned-long)
        (pName :pointer)
        (pEnvironment :pointer)
        (pDriverPath :pointer)
        (pDataFile :pointer)
        (pConfigFile :pointer)
        (pHelpFile :pointer)
        (pDependentFiles :pointer)
        (pMonitorName :pointer)
        (pDefaultDataType :pointer))







(defcstructex-exported MONITOR_INFO_1A
        (pName :string))







(defcstructex-exported MONITOR_INFO_1W
        (pName :pointer))







(defcstructex-exported PORT_INFO_1A
        (pName :string))







(defcstructex-exported PORT_INFO_1W
        (pName :pointer))







(defcstructex-exported MONITOR_INFO_2A
        (pName :string)
        (pEnvironment :string)
        (pDLLName :string))







(defcstructex-exported MONITOR_INFO_2W
        (pName :pointer)
        (pEnvironment :pointer)
        (pDLLName :pointer))







(defcstructex-exported PORT_INFO_2A
        (pPortName :string)
        (pMonitorName :string)
        (pDescription :string)
        (fPortType :unsigned-long)
        (Reserved :unsigned-long))







(defcstructex-exported PORT_INFO_2W
        (pPortName :pointer)
        (pMonitorName :pointer)
        (pDescription :pointer)
        (fPortType :unsigned-long)
        (Reserved :unsigned-long))







(defcstructex-exported PORT_INFO_3A
        (dwStatus :unsigned-long)
        (pszStatus :string)
        (dwSeverity :unsigned-long))







(defcstructex-exported PORT_INFO_3W
        (dwStatus :unsigned-long)
        (pszStatus :pointer)
        (dwSeverity :unsigned-long))







(defcstructex-exported PRINTER_INFO_1A
        (Flags :unsigned-long)
        (pDescription :string)
        (pName :string)
        (pComment :string))







(defcstructex-exported PRINTER_INFO_1W
        (Flags :unsigned-long)
        (pDescription :pointer)
        (pName :pointer)
        (pComment :pointer))







(defcstructex-exported PRINTER_INFO_2A
        (pServerName :string)
        (pPrinterName :string)
        (pShareName :string)
        (pPortName :string)
        (pDriverName :string)
        (pComment :string)
        (pLocation :string)
        (pDevMode :pointer)
        (pSepFile :string)
        (pPrintProcessor :string)
        (pDatatype :string)
        (pParameters :string)
        (pSecurityDescriptor :pointer)
        (Attributes :unsigned-long)
        (Priority :unsigned-long)
        (DefaultPriority :unsigned-long)
        (StartTime :unsigned-long)
        (UntilTime :unsigned-long)
        (Status :unsigned-long)
        (cJobs :unsigned-long)
        (AveragePPM :unsigned-long))







(defcstructex-exported PRINTER_INFO_2W
        (pServerName :pointer)
        (pPrinterName :pointer)
        (pShareName :pointer)
        (pPortName :pointer)
        (pDriverName :pointer)
        (pComment :pointer)
        (pLocation :pointer)
        (pDevMode :pointer)
        (pSepFile :pointer)
        (pPrintProcessor :pointer)
        (pDatatype :pointer)
        (pParameters :pointer)
        (pSecurityDescriptor :pointer)
        (Attributes :unsigned-long)
        (Priority :unsigned-long)
        (DefaultPriority :unsigned-long)
        (StartTime :unsigned-long)
        (UntilTime :unsigned-long)
        (Status :unsigned-long)
        (cJobs :unsigned-long)
        (AveragePPM :unsigned-long))







(defcstructex-exported PRINTER_INFO_3
        (pSecurityDescriptor :pointer))







(defcstructex-exported PRINTER_INFO_4A
        (pPrinterName :string)
        (pServerName :string)
        (Attributes :unsigned-long))







(defcstructex-exported PRINTER_INFO_4W
        (pPrinterName :pointer)
        (pServerName :pointer)
        (Attributes :unsigned-long))







(defcstructex-exported PRINTER_INFO_5A
        (pPrinterName :string)
        (pPortName :string)
        (Attributes :unsigned-long)
        (DeviceNotSelectedTimeout :unsigned-long)
        (TransmissionRetryTimeout :unsigned-long))







(defcstructex-exported PRINTER_INFO_5W
        (pPrinterName :pointer)
        (pPortName :pointer)
        (Attributes :unsigned-long)
        (DeviceNotSelectedTimeout :unsigned-long)
        (TransmissionRetryTimeout :unsigned-long))







(defcstructex-exported PRINTER_INFO_6
        (dwStatus :unsigned-long))







(defcstructex-exported PRINTPROCESSOR_INFO_1A
        (pName :string))







(defcstructex-exported PRINTPROCESSOR_INFO_1W
        (pName :pointer))







(defcstructex-exported PRINTER_NOTIFY_INFO_DATA
        (Type :unsigned-short)
        (Field :unsigned-short)
        (Reserved :unsigned-long)
        (Id :unsigned-long)
        (NotifyData :pointer))







(cffi:defcunion PRINTER_NOTIFY_INFO_DATA_NotifyData
        (adwData :pointer)
        (Data :pointer))

(defcstructex-exported PRINTER_NOTIFY_INFO_DATA_NotifyData_Data
        (cbBuf :unsigned-long)
        (pBuf :pointer))

(defcstructex-exported PRINTER_NOTIFY_INFO
        (Version :unsigned-long)
        (Flags :unsigned-long)
        (Count :unsigned-long)
        (aData :pointer))







(defcstructex-exported FORM_INFO_1A
        (Flags :unsigned-long)
        (pName :string)
        (Size :pointer)
        (ImageableArea RECTL))







(defcstructex-exported FORM_INFO_1W
        (Flags :unsigned-long)
        (pName :pointer)
        (Size :pointer)
        (ImageableArea RECTL))







(defcstructex-exported PRINTER_DEFAULTSA
        (pDatatype :string)
        (pDevMode :pointer)
        (DesiredAccess :unsigned-long))







(defcstructex-exported PRINTER_DEFAULTSW
        (pDatatype :pointer)
        (pDevMode :pointer)
        (DesiredAccess :unsigned-long))


