
(cl:in-package w32apimod)

(define-w32api-module win95.message :win95.message)

(cl:in-package cl-w32api.module.win95.message)

(require-and-inherit-module "win95.~")

(defcstructex-exported MSG
  (hwnd    HWND)
  (message UINT)
  (wParam  WPARAM)
  (lParam  LPARAM)
  (time    DWORD)
  (pt      (:inline POINT)))

(defconstant-exported WM_APP 32768)

(defconstant-exported WM_ACTIVATE 6)

(defconstant-exported WM_ACTIVATEAPP 28)

(defconstant-exported WM_AFXFIRST 864)

(defconstant-exported WM_AFXLAST 895)

(defconstant-exported WM_ASKCBFORMATNAME 780)

(defconstant-exported WM_CANCELJOURNAL 75)

(defconstant-exported WM_CANCELMODE 31)

(defconstant-exported WM_CAPTURECHANGED 533)

(defconstant-exported WM_CHANGECBCHAIN 781)

(defconstant-exported WM_CHAR 258)

(defconstant-exported WM_CHARTOITEM 47)

(defconstant-exported WM_CHILDACTIVATE 34)

(defconstant-exported WM_CLEAR 771)

(defconstant-exported WM_CLOSE 16)

(defconstant-exported WM_COMMAND 273)

(defconstant-exported WM_COMMNOTIFY 68)

(defconstant-exported WM_COMPACTING 65)

(defconstant-exported WM_COMPAREITEM 57)

(defconstant-exported WM_CONTEXTMENU 123)

(defconstant-exported WM_COPY 769)

(defconstant-exported WM_COPYDATA 74)

(defconstant-exported WM_CREATE 1)

(defconstant-exported WM_CTLCOLORBTN 309)

(defconstant-exported WM_CTLCOLORDLG 310)

(defconstant-exported WM_CTLCOLOREDIT 307)

(defconstant-exported WM_CTLCOLORLISTBOX 308)

(defconstant-exported WM_CTLCOLORMSGBOX 306)

(defconstant-exported WM_CTLCOLORSCROLLBAR 311)

(defconstant-exported WM_CTLCOLORSTATIC 312)

(defconstant-exported WM_CUT 768)

(defconstant-exported WM_DEADCHAR 259)

(defconstant-exported WM_DELETEITEM 45)

(defconstant-exported WM_DESTROY 2)

(defconstant-exported WM_DESTROYCLIPBOARD 775)

(defconstant-exported WM_DEVICECHANGE 537)

(defconstant-exported WM_DEVMODECHANGE 27)

(defconstant-exported WM_DISPLAYCHANGE 126)

(defconstant-exported WM_DRAWCLIPBOARD 776)

(defconstant-exported WM_DRAWITEM 43)

(defconstant-exported WM_DROPFILES 563)

(defconstant-exported WM_ENABLE 10)

(defconstant-exported WM_ENDSESSION 22)

(defconstant-exported WM_ENTERIDLE 289)

(defconstant-exported WM_ENTERMENULOOP 529)

(defconstant-exported WM_ENTERSIZEMOVE 561)

(defconstant-exported WM_ERASEBKGND 20)

(defconstant-exported WM_EXITMENULOOP 530)

(defconstant-exported WM_EXITSIZEMOVE 562)

(defconstant-exported WM_FONTCHANGE 29)

(defconstant-exported WM_GETDLGCODE 135)

(defconstant-exported WM_GETFONT 49)

(defconstant-exported WM_GETHOTKEY 51)

(defconstant-exported WM_GETICON 127)

(defconstant-exported WM_GETMINMAXINFO 36)

(defconstant-exported WM_GETTEXT 13)

(defconstant-exported WM_GETTEXTLENGTH 14)

(defconstant-exported WM_HANDHELDFIRST 856)

(defconstant-exported WM_HANDHELDLAST 863)

(defconstant-exported WM_HELP 83)

(defconstant-exported WM_HOTKEY 786)

(defconstant-exported WM_HSCROLL 276)

(defconstant-exported WM_HSCROLLCLIPBOARD 782)

(defconstant-exported WM_ICONERASEBKGND 39)

(defconstant-exported WM_INITDIALOG 272)

(defconstant-exported WM_INITMENU 278)

(defconstant-exported WM_INITMENUPOPUP 279)

(defconstant-exported WM_INPUTLANGCHANGE 81)

(defconstant-exported WM_INPUTLANGCHANGEREQUEST 80)

(defconstant-exported WM_KEYDOWN 256)

(defconstant-exported WM_KEYUP 257)

(defconstant-exported WM_KILLFOCUS 8)

(defconstant-exported WM_MDIACTIVATE 546)

(defconstant-exported WM_MDICASCADE 551)

(defconstant-exported WM_MDICREATE 544)

(defconstant-exported WM_MDIDESTROY 545)

(defconstant-exported WM_MDIGETACTIVE 553)

(defconstant-exported WM_MDIICONARRANGE 552)

(defconstant-exported WM_MDIMAXIMIZE 549)

(defconstant-exported WM_MDINEXT 548)

(defconstant-exported WM_MDIREFRESHMENU 564)

(defconstant-exported WM_MDIRESTORE 547)

(defconstant-exported WM_MDISETMENU 560)

(defconstant-exported WM_MDITILE 550)

(defconstant-exported WM_MEASUREITEM 44)

(defconstant-exported WM_MENUCHAR 288)

(defconstant-exported WM_MENUSELECT 287)

(defconstant-exported WM_NEXTMENU 531)

(defconstant-exported WM_MOVE 3)

(defconstant-exported WM_MOVING 534)

(defconstant-exported WM_NCACTIVATE 134)

(defconstant-exported WM_NCCALCSIZE 131)

(defconstant-exported WM_NCCREATE 129)

(defconstant-exported WM_NCDESTROY 130)

(defconstant-exported WM_NCHITTEST 132)

(defconstant-exported WM_NCLBUTTONDBLCLK 163)

(defconstant-exported WM_NCLBUTTONDOWN 161)

(defconstant-exported WM_NCLBUTTONUP 162)

(defconstant-exported WM_NCMBUTTONDBLCLK 169)

(defconstant-exported WM_NCMBUTTONDOWN 167)

(defconstant-exported WM_NCMBUTTONUP 168)

(defconstant-exported WM_NCMOUSEMOVE 160)

(defconstant-exported WM_NCPAINT 133)

(defconstant-exported WM_NCRBUTTONDBLCLK 166)

(defconstant-exported WM_NCRBUTTONDOWN 164)

(defconstant-exported WM_NCRBUTTONUP 165)

(defconstant-exported WM_NEXTDLGCTL 40)

(defconstant-exported WM_NOTIFY 78)

(defconstant-exported WM_NOTIFYFORMAT 85)

(defconstant-exported WM_NULL 0)

(defconstant-exported WM_PAINT 15)

(defconstant-exported WM_PAINTCLIPBOARD 777)

(defconstant-exported WM_PAINTICON 38)

(defconstant-exported WM_PALETTECHANGED 785)

(defconstant-exported WM_PALETTEISCHANGING 784)

(defconstant-exported WM_PARENTNOTIFY 528)

(defconstant-exported WM_PASTE 770)

(defconstant-exported WM_PENWINFIRST 896)

(defconstant-exported WM_PENWINLAST 911)

(defconstant-exported WM_POWER 72)

(defconstant-exported WM_POWERBROADCAST 536)

(defconstant-exported WM_PRINT 791)

(defconstant-exported WM_PRINTCLIENT 792)

(defconstant-exported WM_QUERYDRAGICON 55)

(defconstant-exported WM_QUERYENDSESSION 17)

(defconstant-exported WM_QUERYNEWPALETTE 783)

(defconstant-exported WM_QUERYOPEN 19)

(defconstant-exported WM_QUEUESYNC 35)

(defconstant-exported WM_QUIT 18)

(defconstant-exported WM_RENDERALLFORMATS 774)

(defconstant-exported WM_RENDERFORMAT 773)

(defconstant-exported WM_SETCURSOR 32)

(defconstant-exported WM_SETFOCUS 7)

(defconstant-exported WM_SETFONT 48)

(defconstant-exported WM_SETHOTKEY 50)

(defconstant-exported WM_SETICON 128)

(defconstant-exported WM_SETREDRAW 11)

(defconstant-exported WM_SETTEXT 12)

(defconstant-exported WM_SETTINGCHANGE 26)

(defconstant-exported WM_SHOWWINDOW 24)

(defconstant-exported WM_SIZE 5)

(defconstant-exported WM_SIZECLIPBOARD 779)

(defconstant-exported WM_SIZING 532)

(defconstant-exported WM_SPOOLERSTATUS 42)

(defconstant-exported WM_STYLECHANGED 125)

(defconstant-exported WM_STYLECHANGING 124)

(defconstant-exported WM_SYSCHAR 262)

(defconstant-exported WM_SYSCOLORCHANGE 21)

(defconstant-exported WM_SYSCOMMAND 274)

(defconstant-exported WM_SYSDEADCHAR 263)

(defconstant-exported WM_SYSKEYDOWN 260)

(defconstant-exported WM_SYSKEYUP 261)

(defconstant-exported WM_TCARD 82)

(defconstant-exported WM_THEMECHANGED 794)

(defconstant-exported WM_TIMECHANGE 30)

(defconstant-exported WM_TIMER 275)

(defconstant-exported WM_UNDO 772)

(defconstant-exported WM_USER 1024)

(defconstant-exported WM_USERCHANGED 84)

(defconstant-exported WM_VKEYTOITEM 46)

(defconstant-exported WM_VSCROLL 277)

(defconstant-exported WM_VSCROLLCLIPBOARD 778)

(defconstant-exported WM_WINDOWPOSCHANGED 71)

(defconstant-exported WM_WINDOWPOSCHANGING 70)

(defconstant-exported WM_WININICHANGE 26)

(defconstant-exported WM_KEYFIRST 256)

(defconstant-exported WM_KEYLAST 264)

(defconstant-exported WM_SYNCPAINT 136)

(defconstant-exported WM_MOUSEACTIVATE 33)

(defconstant-exported WM_MOUSEMOVE 512)

(defconstant-exported WM_LBUTTONDOWN 513)

(defconstant-exported WM_LBUTTONUP 514)

(defconstant-exported WM_LBUTTONDBLCLK 515)

(defconstant-exported WM_RBUTTONDOWN 516)

(defconstant-exported WM_RBUTTONUP 517)

(defconstant-exported WM_RBUTTONDBLCLK 518)

(defconstant-exported WM_MBUTTONDOWN 519)

(defconstant-exported WM_MBUTTONUP 520)

(defconstant-exported WM_MBUTTONDBLCLK 521)

(defconstant-exported WM_MOUSEWHEEL 522)

(defconstant-exported WM_MOUSEFIRST 512)

(defconstant-exported WM_MOUSELAST 522)

(defconstant-exported WM_MOUSEHOVER #x2A1)

(defconstant-exported WM_MOUSELEAVE #x2A3)

(defcfunex-exported ("BroadcastSystemMessage" BroadcastSystemMessage :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("BroadcastSystemMessageA" BroadcastSystemMessageA :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("BroadcastSystemMessageW" BroadcastSystemMessageW :convention :stdcall) :int32
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("DefWindowProcA" DefWindowProcA :convention :stdcall) :int32
  (|hWnd|   HWND)
  (|Msg|    UINT)
  (|wParam| WPARAM)
  (|lParam| LPARAM))

(defcfunex-exported ("DefWindowProcW" DefWindowProcW :convention :stdcall) :int32
  (|hWnd|   HWND)
  (|Msg|    UINT)
  (|wParam| WPARAM)
  (|lParam| LPARAM))

(define-abbrev-exported DefWindowProc DefWindowProcW)

(defcfunex-exported ("DispatchMessageA" DispatchMessageA :convention :stdcall) LONG
  (|lpMsg| MSG))

(defcfunex-exported ("DispatchMessageW" DispatchMessageW :convention :stdcall) LONG
  (|lpMsg| MSG))

(define-abbrev-exported DispatchMessage DispatchMessageW)

(defcfunex-exported ("GetMessageA" GetMessageA :convention :stdcall) BOOL
  (|lpMsg|         MSG)
  (|hWnd|          HWND)
  (|wMsgFilterMin| UINT)
  (|wMsgFilterMax| UINT))

(defcfunex-exported ("GetMessageW" GetMessageW :convention :stdcall) BOOL
  (|lpMsg|         MSG)
  (|hWnd|          HWND)
  (|wMsgFilterMin| UINT)
  (|wMsgFilterMax| UINT))

(define-abbrev-exported GetMessage GetMessageW)

(defcfunex-exported ("GetMessageExtraInfo" GetMessageExtraInfo :convention :stdcall) LONG)

(defcfunex-exported ("GetMessagePos" GetMessagePos :convention :stdcall) DWORD)

(defcfunex-exported ("GetMessageTime" GetMessageTime :convention :stdcall) LONG)

(defcfunex-exported ("InSendMessage" InSendMessage :convention :stdcall) BOOL)

(defcfunex-exported ("PeekMessageA" PeekMessageA :convention :stdcall) BOOL
  (|lpMsg|         MSG)
  (|hWnd|          HWND)
  (|wMsgFilterMin| UINT)
  (|wMsgFilterMax| UINT)
  (|wRemoveMsg|    UINT))

(defcfunex-exported ("PeekMessageW" PeekMessageW :convention :stdcall) BOOL
  (|lpMsg|         MSG)
  (|hWnd|          HWND)
  (|wMsgFilterMin| UINT)
  (|wMsgFilterMax| UINT)
  (|wRemoveMsg|    UINT))

(define-abbrev-exported PeekMessage PeekMessageW)

(defcfunex-exported ("PostMessageA" PostMessageA :convention :stdcall) BOOL
  (|hWnd| HWND)
  (|Msg|  UINT)
  (|wParam| WPARAM)
  (|lParam| LPARAM))

(defcfunex-exported ("PostMessageW" PostMessageW :convention :stdcall) BOOL
  (|hWnd| HWND)
  (|Msg|  UINT)
  (|wParam| WPARAM)
  (|lParam| LPARAM))

(define-abbrev-exported PostMessage PostMessageW)

(defcfunex-exported ("PostQuitMessage" PostQuitMessage :convention :stdcall) VOID
  (|nExitCode| :int))

(defcfunex-exported ("PostThreadMessageA" PostThreadMessageA :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("PostThreadMessageW" PostThreadMessageW :convention :stdcall) :int
  (arg0 :unsigned-long)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("SendMessageA" SendMessageA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("SendMessageW" SendMessageW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32))

(defcfunex-exported ("SendMessageCallbackA" SendMessageCallbackA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("SendMessageCallbackW" SendMessageCallbackW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32)
  (arg4 :pointer)
  (arg5 :unsigned-long))

(defcfunex-exported ("SendMessageTimeoutA" SendMessageTimeoutA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32)
  (arg4 :unsigned-int)
  (arg5 :unsigned-int)
  (arg6 :pointer))

(defcfunex-exported ("SendMessageTimeoutW" SendMessageTimeoutW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :unsigned-int)
  (arg3 :int32)
  (arg4 :unsigned-int)
  (arg5 :unsigned-int)
  (arg6 :pointer))

(defcfunex-exported ("SetMessageExtraInfo" SetMessageExtraInfo :convention :stdcall) LPARAM
  (|lParam| LPARAM))

(defcfunex-exported ("TranslateMessage" TranslateMessage :convention :stdcall) BOOL
  (|lpMsg| MSG))

(defcfunex-exported ("WaitMessage" WaitMessage :convention :stdcall) BOOL)

