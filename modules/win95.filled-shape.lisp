
(defcfunex-exported ("Chord" Chord :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfunex-exported ("Ellipse" Ellipse :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("FillRect" FillRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("InvertRgn" InvertRgn :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("Pie" Pie :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int)
  (arg7 :int)
  (arg8 :int))

(defcfunex-exported ("Polygon" Polygon :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :int))

(defcfunex-exported ("PolyPolygon" PolyPolygon :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :int))

(defcfunex-exported ("Rectangle" Rectangle :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int))

(defcfunex-exported ("RoundRect" RoundRect :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :int)
  (arg3 :int)
  (arg4 :int)
  (arg5 :int)
  (arg6 :int))

