(defcfunex-exported ("CallWindowProcA" CallWindowProcA :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

(defcfunex-exported ("CallWindowProcW" CallWindowProcW :convention :stdcall) :int32
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int)
  (arg3 :unsigned-int)
  (arg4 :int32))

