
(cl:in-package w32apimod)

(define-w32api-module networking :networking)

(cl:in-package cl-w32api.module.networking)


(defconstant-exported WNNC_NET_MSNET #x00010000)

(defconstant-exported WNNC_NET_LANMAN #x00020000)

(defconstant-exported WNNC_NET_NETWARE #x00030000)

(defconstant-exported WNNC_NET_VINES #x00040000)

(defconstant-exported WNNC_NET_10NET #x00050000)

(defconstant-exported WNNC_NET_LOCUS #x00060000)

(defconstant-exported WNNC_NET_SUN_PC_NFS #x00070000)

(defconstant-exported WNNC_NET_LANSTEP #x00080000)

(defconstant-exported WNNC_NET_9TILES #x00090000)

(defconstant-exported WNNC_NET_LANTASTIC #x000A0000)

(defconstant-exported WNNC_NET_AS400 #x000B0000)

(defconstant-exported WNNC_NET_FTP_NFS #x000C0000)

(defconstant-exported WNNC_NET_PATHWORKS #x000D0000)

(defconstant-exported WNNC_NET_LIFENET #x000E0000)

(defconstant-exported WNNC_NET_POWERLAN #x000F0000)

(defconstant-exported WNNC_NET_BWNFS #x00100000)

(defconstant-exported WNNC_NET_COGENT #x00110000)

(defconstant-exported WNNC_NET_FARALLON #x00120000)

(defconstant-exported WNNC_NET_APPLETALK #x00130000)

(defconstant-exported WNNC_NET_INTERGRAPH #x00140000)

(defconstant-exported WNNC_NET_SYMFONET #x00150000)

(defconstant-exported WNNC_NET_CLEARCASE #x00160000)

(defconstant-exported WNNC_NET_FRONTIER #x00170000)

(defconstant-exported WNNC_NET_BMC #x00180000)

(defconstant-exported WNNC_NET_DCE #x00190000)

(defconstant-exported WNNC_NET_AVID #x001A0000)

(defconstant-exported WNNC_NET_DOCUSPACE #x001B0000)

(defconstant-exported WNNC_NET_MANGOSOFT #x001C0000)

(defconstant-exported WNNC_NET_SERNET #x001D0000)

(defconstant-exported WNNC_NET_DECORB #x00200000)

(defconstant-exported WNNC_NET_PROTSTOR #x00210000)

(defconstant-exported WNNC_NET_FJ_REDIR #x00220000)

(defconstant-exported WNNC_NET_DISTINCT #x00230000)

(defconstant-exported WNNC_NET_TWINS #x00240000)

(defconstant-exported WNNC_NET_RDR2SAMPLE #x00250000)

(defconstant-exported WNNC_NET_CSC #x00260000)

(defconstant-exported WNNC_NET_3IN1 #x00270000)

(defconstant-exported WNNC_NET_EXTENDNET #x00290000)

(defconstant-exported WNNC_NET_OBJECT_DIRE #x00300000)

(defconstant-exported WNNC_NET_MASFAX #x00310000)

(defconstant-exported WNNC_NET_HOB_NFS #x00320000)

(defconstant-exported WNNC_NET_SHIVA #x00330000)

(defconstant-exported WNNC_NET_IBMAL #x00340000)

(defconstant-exported WNNC_CRED_MANAGER #xFFFF0000)

(defconstant-exported RESOURCE_CONNECTED 1)

(defconstant-exported RESOURCE_GLOBALNET 2)

(defconstant-exported RESOURCE_REMEMBERED 3)

(defconstant-exported RESOURCE_RECENT 4)

(defconstant-exported RESOURCE_CONTEXT 5)

(defconstant-exported RESOURCETYPE_ANY 0)

(defconstant-exported RESOURCETYPE_DISK 1)

(defconstant-exported RESOURCETYPE_PRINT 2)

(defconstant-exported RESOURCETYPE_RESERVED 8)

(defconstant-exported RESOURCETYPE_UNKNOWN #xFFFFFFFF)

(defconstant-exported RESOURCEUSAGE_CONNECTABLE #x00000001)

(defconstant-exported RESOURCEUSAGE_CONTAINER #x00000002)

(defconstant-exported RESOURCEUSAGE_NOLOCALDEVICE #x00000004)

(defconstant-exported RESOURCEUSAGE_SIBLING #x00000008)

(defconstant-exported RESOURCEUSAGE_ATTACHED #x00000010)

(defconstant-exported RESOURCEUSAGE_ALL (cl:logior #x00000001 #x00000002 #x00000010))

(defconstant-exported RESOURCEUSAGE_RESERVED #x80000000)

(defconstant-exported RESOURCEDISPLAYTYPE_GENERIC 0)

(defconstant-exported RESOURCEDISPLAYTYPE_DOMAIN 1)

(defconstant-exported RESOURCEDISPLAYTYPE_SERVER 2)

(defconstant-exported RESOURCEDISPLAYTYPE_SHARE 3)

(defconstant-exported RESOURCEDISPLAYTYPE_FILE 4)

(defconstant-exported RESOURCEDISPLAYTYPE_GROUP 5)

(defconstant-exported RESOURCEDISPLAYTYPE_NETWORK 6)

(defconstant-exported RESOURCEDISPLAYTYPE_ROOT 7)

(defconstant-exported RESOURCEDISPLAYTYPE_SHAREADMIN 8)

(defconstant-exported RESOURCEDISPLAYTYPE_DIRECTORY 9)

(defconstant-exported RESOURCEDISPLAYTYPE_TREE 10)

(defconstant-exported NETPROPERTY_PERSISTENT 1)

(defconstant-exported CONNECT_UPDATE_PROFILE 1)

(defconstant-exported CONNECT_UPDATE_RECENT 2)

(defconstant-exported CONNECT_TEMPORARY 4)

(defconstant-exported CONNECT_INTERACTIVE 8)

(defconstant-exported CONNECT_PROMPT 16)

(defconstant-exported CONNECT_NEED_DRIVE 32)

(defconstant-exported CONNECT_REFCOUNT 64)

(defconstant-exported CONNECT_REDIRECT 128)

(defconstant-exported CONNECT_LOCALDRIVE 256)

(defconstant-exported CONNECT_CURRENT_MEDIA 512)

(defconstant-exported CONNDLG_RO_PATH 1)

(defconstant-exported CONNDLG_CONN_POINT 2)

(defconstant-exported CONNDLG_USE_MRU 4)

(defconstant-exported CONNDLG_HIDE_BOX 8)

(defconstant-exported CONNDLG_PERSIST 16)

(defconstant-exported CONNDLG_NOT_PERSIST 32)

(defconstant-exported DISC_UPDATE_PROFILE 1)

(defconstant-exported DISC_NO_FORCE 64)

(defconstant-exported WNFMT_MULTILINE 1)

(defconstant-exported WNFMT_ABBREVIATED 2)

(defconstant-exported WNFMT_INENUM 16)

(defconstant-exported WNFMT_CONNECTION 32)

(defconstant-exported WN_SUCCESS 0)

(defconstant-exported WN_NO_ERROR 0)

(defconstant-exported WN_NOT_SUPPORTED 50)

(defconstant-exported WN_CANCEL 1223)

(defconstant-exported WN_RETRY 1237)

(defconstant-exported WN_NET_ERROR 59)

(defconstant-exported WN_MORE_DATA 234)

(defconstant-exported WN_BAD_POINTER 487)

(defconstant-exported WN_BAD_VALUE 87)

(defconstant-exported WN_BAD_USER 2202)

(defconstant-exported WN_BAD_PASSWORD 86)

(defconstant-exported WN_ACCESS_DENIED 5)

(defconstant-exported WN_FUNCTION_BUSY 170)

(defconstant-exported WN_WINDOWS_ERROR 59)

(defconstant-exported WN_OUT_OF_MEMORY 8)

(defconstant-exported WN_NO_NETWORK 1222)

(defconstant-exported WN_EXTENDED_ERROR 1208)

(defconstant-exported WN_BAD_LEVEL 124)

(defconstant-exported WN_BAD_HANDLE 6)

(defconstant-exported WN_NOT_INITIALIZING 1247)

(defconstant-exported WN_NO_MORE_DEVICES 1248)

(defconstant-exported WN_NOT_CONNECTED 2250)

(defconstant-exported WN_OPEN_FILES 2401)

(defconstant-exported WN_DEVICE_IN_USE 2404)

(defconstant-exported WN_BAD_NETNAME 67)

(defconstant-exported WN_BAD_LOCALNAME 1200)

(defconstant-exported WN_ALREADY_CONNECTED 85)

(defconstant-exported WN_DEVICE_ERROR 31)

(defconstant-exported WN_CONNECTION_CLOSED 1201)

(defconstant-exported WN_NO_NET_OR_BAD_PATH 1203)

(defconstant-exported WN_BAD_PROVIDER 1204)

(defconstant-exported WN_CANNOT_OPEN_PROFILE 1205)

(defconstant-exported WN_BAD_PROFILE 1206)

(defconstant-exported WN_BAD_DEV_TYPE 66)

(defconstant-exported WN_DEVICE_ALREADY_REMEMBERED 1202)

(defconstant-exported WN_NO_MORE_ENTRIES 259)

(defconstant-exported WN_NOT_CONTAINER 1207)

(defconstant-exported WN_NOT_AUTHENTICATED 1244)

(defconstant-exported WN_NOT_LOGGED_ON 1245)

(defconstant-exported WN_NOT_VALIDATED 1311)

(defconstant-exported UNIVERSAL_NAME_INFO_LEVEL 1)

(defconstant-exported REMOTE_NAME_INFO_LEVEL 2)

(defconstant-exported NETINFO_DLL16 1)

(defconstant-exported NETINFO_DISKRED 4)

(defconstant-exported NETINFO_PRINTERRED 8)

(defconstant-exported RP_LOGON 1)

(defconstant-exported RP_INIFILE 2)

(defconstant-exported PP_DISPLAYERRORS 1)

(defconstant-exported WNCON_FORNETCARD 1)

(defconstant-exported WNCON_NOTROUTED 2)

(defconstant-exported WNCON_SLOWLINK 4)

(defconstant-exported WNCON_DYNAMIC 8)

(defcstructex-exported NETRESOURCEA
        (dwScope :unsigned-long)
        (dwType :unsigned-long)
        (dwDisplayType :unsigned-long)
        (dwUsage :unsigned-long)
        (lpLocalName :string)
        (lpRemoteName :string)
        (lpComment :string)
        (lpProvider :string))





(defcstructex-exported NETRESOURCEW
        (dwScope :unsigned-long)
        (dwType :unsigned-long)
        (dwDisplayType :unsigned-long)
        (dwUsage :unsigned-long)
        (lpLocalName :pointer)
        (lpRemoteName :pointer)
        (lpComment :pointer)
        (lpProvider :pointer))





(defcstructex-exported CONNECTDLGSTRUCTA
        (cbStructure :unsigned-long)
        (hwndOwner :pointer)
        (lpConnRes :pointer)
        (dwFlags :unsigned-long)
        (dwDevNum :unsigned-long))





(defcstructex-exported CONNECTDLGSTRUCTW
        (cbStructure :unsigned-long)
        (hwndOwner :pointer)
        (lpConnRes :pointer)
        (dwFlags :unsigned-long)
        (dwDevNum :unsigned-long))





(defcstructex-exported DISCDLGSTRUCTA
        (cbStructure :unsigned-long)
        (hwndOwner :pointer)
        (lpLocalName :string)
        (lpRemoteName :string)
        (dwFlags :unsigned-long))





(defcstructex-exported DISCDLGSTRUCTW
        (cbStructure :unsigned-long)
        (hwndOwner :pointer)
        (lpLocalName :pointer)
        (lpRemoteName :pointer)
        (dwFlags :unsigned-long))





(defcstructex-exported UNIVERSAL_NAME_INFOA
        (lpUniversalName :string))





(defcstructex-exported UNIVERSAL_NAME_INFOW
        (lpUniversalName :pointer))





(defcstructex-exported REMOTE_NAME_INFOA
        (lpUniversalName :string)
        (lpConnectionName :string)
        (lpRemainingPath :string))





(defcstructex-exported REMOTE_NAME_INFOW
        (lpUniversalName :pointer)
        (lpConnectionName :pointer)
        (lpRemainingPath :pointer))





(defcstructex-exported NETINFOSTRUCT
        (cbStructure :unsigned-long)
        (dwProviderVersion :unsigned-long)
        (dwStatus :unsigned-long)
        (dwCharacteristics :unsigned-long)
        (dwHandle :unsigned-long)
        (wNetType :unsigned-short)
        (dwPrinters :unsigned-long)
        (dwDrives :unsigned-long))

















(defcstructex-exported NETCONNECTINFOSTRUCT
        (cbStructure :unsigned-long)
        (dwFlags :unsigned-long)
        (dwSpeed :unsigned-long)
        (dwDelay :unsigned-long)
        (dwOptDataSize :unsigned-long))





(defcfunex-exported ("WNetAddConnectionA" WNetAddConnectionA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("WNetAddConnectionW" WNetAddConnectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WNetAddConnection2A" WNetAddConnection2A :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :unsigned-long))

(defcfunex-exported ("WNetAddConnection2W" WNetAddConnection2W :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :unsigned-long))

(defcfunex-exported ("WNetAddConnection3A" WNetAddConnection3A :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :string)
  (arg4 :unsigned-long))

(defcfunex-exported ("WNetAddConnection3W" WNetAddConnection3W :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("WNetCancelConnectionA" WNetCancelConnectionA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :int))

(defcfunex-exported ("WNetCancelConnectionW" WNetCancelConnectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("WNetCancelConnection2A" WNetCancelConnection2A :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :int))

(defcfunex-exported ("WNetCancelConnection2W" WNetCancelConnection2W :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :int))

(defcfunex-exported ("WNetGetConnectionA" WNetGetConnectionA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetConnectionW" WNetGetConnectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WNetUseConnectionA" WNetUseConnectionA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :string)
  (arg3 :string)
  (arg4 :unsigned-long)
  (arg5 :string)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("WNetUseConnectionW" WNetUseConnectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-long)
  (arg5 :pointer)
  (arg6 :pointer)
  (arg7 :pointer))

(defcfunex-exported ("WNetSetConnectionA" WNetSetConnectionA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WNetSetConnectionW" WNetSetConnectionW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer))

(defcfunex-exported ("WNetConnectionDialog" WNetConnectionDialog :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("WNetDisconnectDialog" WNetDisconnectDialog :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long))

(defcfunex-exported ("WNetConnectionDialog1A" WNetConnectionDialog1A :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("WNetConnectionDialog1W" WNetConnectionDialog1W :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("WNetDisconnectDialog1A" WNetDisconnectDialog1A :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("WNetDisconnectDialog1W" WNetDisconnectDialog1W :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("WNetOpenEnumA" WNetOpenEnumA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WNetOpenEnumW" WNetOpenEnumW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :unsigned-long)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :pointer))

(defcfunex-exported ("WNetEnumResourceA" WNetEnumResourceA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WNetEnumResourceW" WNetEnumResourceW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WNetCloseEnum" WNetCloseEnum :convention :stdcall) :unsigned-long
  (arg0 :pointer))

(defcfunex-exported ("WNetGetUniversalNameA" WNetGetUniversalNameA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WNetGetUniversalNameW" WNetGetUniversalNameW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :unsigned-long)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WNetGetUserA" WNetGetUserA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetUserW" WNetGetUserW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetProviderNameA" WNetGetProviderNameA :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetProviderNameW" WNetGetProviderNameW :convention :stdcall) :unsigned-long
  (arg0 :unsigned-long)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetNetworkInformationA" WNetGetNetworkInformationA :convention :stdcall) :unsigned-long
  (arg0 :string)
  (arg1 :pointer))

(defcfunex-exported ("WNetGetNetworkInformationW" WNetGetNetworkInformationW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("WNetGetResourceInformationA" WNetGetResourceInformationA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WNetGetResourceInformationW" WNetGetResourceInformationW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("WNetGetResourceParentA" WNetGetResourceParentA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetResourceParentW" WNetGetResourceParentW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("WNetGetLastErrorA" WNetGetLastErrorA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-long)
  (arg3 :string)
  (arg4 :unsigned-long))

(defcfunex-exported ("WNetGetLastErrorW" WNetGetLastErrorW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-long)
  (arg3 :pointer)
  (arg4 :unsigned-long))

(defcfunex-exported ("MultinetGetConnectionPerformanceA" MultinetGetConnectionPerformanceA :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("MultinetGetConnectionPerformanceW" MultinetGetConnectionPerformanceW :convention :stdcall) :unsigned-long
  (arg0 :pointer)
  (arg1 :pointer))

