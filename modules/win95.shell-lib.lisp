
(cl:in-package w32apimod)

(define-w32api-module win95.shell-lib :win95.shell-lib)

(cl:in-package cl-w32api.module.win95.shell-lib)

(defcfunex-exported ("DragAcceptFiles" DragAcceptFiles :convention :stdcall) :void
  (arg0 :pointer)
  (arg1 :int))

(defcfunex-exported ("DragFinish" DragFinish :convention :stdcall) :void
  (arg0 :pointer))

(defcfunex-exported ("DragQueryFileA" DragQueryFileA :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :string)
  (arg3 :unsigned-int))

(defcfunex-exported ("DragQueryFileW" DragQueryFileW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :unsigned-int)
  (arg2 :pointer)
  (arg3 :unsigned-int))

(defcfunex-exported ("DragQueryPoint" DragQueryPoint :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer))

(defcfunex-exported ("ExtractAssociatedIconA" ExtractAssociatedIconA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :pointer))

(defcfunex-exported ("ExtractAssociatedIconW" ExtractAssociatedIconW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ExtractIconA" ExtractIconA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :unsigned-int))

(defcfunex-exported ("ExtractIconW" ExtractIconW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :unsigned-int))

(defcfunex-exported ("ExtractIconExA" ExtractIconExA :convention :stdcall) :unsigned-int
  (arg0 :string)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-int))

(defcfunex-exported ("ExtractIconExW" ExtractIconExW :convention :stdcall) :unsigned-int
  (arg0 :pointer)
  (arg1 :int)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :unsigned-int))

(defcfunex-exported ("FindExecutableA" FindExecutableA :convention :stdcall) :pointer
  (arg0 :string)
  (arg1 :string)
  (arg2 :string))

(defcfunex-exported ("FindExecutableW" FindExecutableW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer))

(defcfunex-exported ("ShellAboutA" ShellAboutA :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :pointer))

(defcfunex-exported ("ShellAboutW" ShellAboutW :convention :stdcall) :int
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer))

(defcfunex-exported ("ShellExecuteA" ShellExecuteA :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :string)
  (arg2 :string)
  (arg3 :string)
  (arg4 :string)
  (arg5 :int))

(defcfunex-exported ("ShellExecuteW" ShellExecuteW :convention :stdcall) :pointer
  (arg0 :pointer)
  (arg1 :pointer)
  (arg2 :pointer)
  (arg3 :pointer)
  (arg4 :pointer)
  (arg5 :int))

(defcfunex-exported ("ShellExecuteExA" ShellExecuteExA :convention :stdcall) :int
  (arg0 :pointer))

(defcfunex-exported ("ShellExecuteExW" ShellExecuteExW :convention :stdcall) :int
  (arg0 :pointer))

