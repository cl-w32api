
(in-package w32api)

(lutilities:defmacro-exported %require-module (module-file-title &optional module-symbol)
  (when (not module-symbol)
      (setf module-symbol (intern (string-upcase module-file-title) "KEYWORD") ))
  `(progn (w32apimod::load-module-from-file ,module-file-title)
	  (w32apimod::enable-module ,module-symbol)))

(lutilities:defmacro-exported %require-all-modules ()
  `(w32apimod::enable-all-modules t))

;;(progn (cl:import 'luciffi::defcallback)
;;       (cl:export 'luciffi::defcallback ))

(%require-module "base")
