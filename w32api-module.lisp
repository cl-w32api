(in-package cl-user)

(require 'lucifer-lutilities)
(require 'lucifer-luciffi)

(defpackage cl-w32api.module
  (:nicknames w32apimod)
  (:use :cl :lutilities :luciffi :cl-w32api.utils)
  )

(in-package w32apimod)

(defvar *w32api-modules* (make-hash-table :test 'equal))
(defvar *w32api-module-path* (or #.*compile-file-truename*
				 #.*load-truename*))

(defun make-module-package-name (name)
  (intern (concatenate 'string
		       (string-upcase "cl-w32api.module.")
		       (symbol-name name)) (symbol-package name)))

(defmacro-exported define-w32api-module (name &rest module-names)
  (let ((new-package-name (make-module-package-name name)))
    `(progn (defpackage ,new-package-name
	      (:use :cl :lutilities :luciffi :cl-w32api.types
		    :cl-w32api.module :cl-w32api.utils))
	    (eval-when (:load-toplevel :execute)
	      (let ((new-package (find-package ',new-package-name)))
		,@(loop for module-name in module-names
		       collecting `(setf (gethash ,module-name *w32api-modules*)
					 new-package))))
	    )))

(defmacro without-package-variance-warnings (&body body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (handler-bind (#+sbcl(sb-int:package-at-variance #'muffle-warning))
       ,@body)))

(defmacro-exported w32api-sync-exported-names ()
  `(eval-when (:load-toplevel :execute)
     (let ((w32api-package (find-package 'w32api)))
       (when w32api-package
	 (without-package-variance-warnings 
	     (do-external-symbols (s (find-package *package*))
	       (import s w32api-package)
	       (export s w32api-package)))))))

(defun-exported load-module-from-file (filename)
  (let ((path-of-this-file *w32api-module-path*)
	(filename-string (format nil "~a" filename)))
    (load (make-pathname :directory (append (pathname-directory path-of-this-file)
				       (list "modules"))
			  :name filename-string :defaults path-of-this-file))))

(defun-exported load-all-modules ()
  (let* ((path-of-this-file *w32api-module-path*)
	 (filelist (directory
		    (make-pathname :directory
				   (append (pathname-directory path-of-this-file)
					   (list "modules"))
				   :name "*"
				   :defaults path-of-this-file))))
    (dolist (filespec filelist t)
      (load-module-from-file (pathname-name filespec))
    )))
  

(defmacro-exported define-w32api-module-ctor (params &body code)
  (let* ((module-package-name (package-name *package*))
	 (module-constructor (intern module-package-name *package*)))
    `(defun ,module-constructor ,params
       ,@code)))

(defun enable-module-by-package (module-package)
  (assert module-package)
  (let* ((module-package-name (package-name module-package))
	 (module-constructor (intern module-package-name module-package)))
    (when (fboundp module-constructor)
      (funcall module-constructor))
    (let ((*package* module-package))
      (w32api-sync-exported-names))
    t
    ))

(defun-exported enable-module (module)
  (let ((module-package (gethash module *w32api-modules*)))
    (when module-package
      (enable-module-by-package module-package))))

(defun-exported enable-all-modules (&optional (load-first nil))
  (when load-first (load-all-modules))
  (dolist (module-package (remove-duplicates
			   (loop for module-package being the hash-values
			      in *w32api-modules*
			      collecting module-package)) t)
    (enable-module-by-package module-package)))

(defun-exported finish-loading-modules ()
  (fmakunbound 'load-module-from-file)
  (fmakunbound 'load-all-modules)
  )


(defmacro-exported require-and-inherit-module (module-file-title &optional module-symbol)
  (when (not module-symbol)
      (setf module-symbol (intern (string-upcase module-file-title) "KEYWORD") ))
  `(progn
     (w32apimod::load-module-from-file ,module-file-title)
     (w32apimod::enable-module ,module-symbol)
     (use-package ',(make-module-package-name module-symbol))))


