          README
         cl-w32api
--------------------------------------------

This library provides an interface for invoking Win32 API calls. 

This library is based on earlier efforts of stlxv<n.akr.akiiya _at_ gmail.com>, who successfully used swig to convert the public domain w32api c headers to CFFI-definitions. 

However, with some limitations of CFFI-itself, the definitions are kind of untidy and unmaintainable. I have did some hacking of CFFI, created a library called luciffi with is an extension to cffi, and luciffi supports directly passing structures (instead of pointers) on stack and embedding sub-structures in another structure. (Luciffi is part of anotherproject of mine called lucifer and the main goal is irrevelant).

So now i'm able to clean and modularize the code. Technically, this library is a fork of http://sourceforge.net/project/cl-w32api, which is started by stlxv himself, but the code is quite different. Actually, i'm rewriting almost everything.

This library is under MIT license.

                                                Charles Lew
						<crlf0710 _at_ gmail.com>

