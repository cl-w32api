
(defpackage cl-w32api.types
  (:use :cl :lucifer.luciffi))
(in-package cl-w32api.types)


;;string types

(defvar *win32-astring-encoding* :cp1251)
(defvar *win32-wstring-encoding* :ucs-2le)

(define-foreign-type w32api-astring-type  (cffi::foreign-string-type)
  ()
  (:simple-parser ASTRING))
(export 'astring)

(defmethod translate-to-foreign :around (s (type w32api-astring-type))
  (let ((luciffi::*default-foreign-encoding* *win32-astring-encoding*))
    (if (null s)
	(null-pointer)
	(call-next-method))))

(defmethod translate-from-foreign :around (ptr (type w32api-astring-type))
  (let ((luciffi::*default-foreign-encoding* *win32-astring-encoding*))
    (if (null-pointer-p ptr)
	nil
	(call-next-method))))

(define-foreign-type w32api-wstring-type (cffi::foreign-string-type)
  ()
  (:simple-parser WSTRING))
(export 'wstring)

(defmethod translate-to-foreign :around (s (type w32api-wstring-type))
  (let ((luciffi::*default-foreign-encoding* *win32-wstring-encoding*))
    (if (null s)
	(null-pointer)
	(call-next-method))))

(defmethod translate-from-foreign :around (ptr (type w32api-wstring-type))
  (let ((luciffi::*default-foreign-encoding* *win32-wstring-encoding*))
    (if (null-pointer-p ptr)
	nil
	(call-next-method))))

;;handle types

(define-foreign-type w32api-handle-type ()
  ()
  (:actual-type :pointer)
  (:simple-parser handle))
(export 'handle)

(defmethod translate-to-foreign (s (type w32api-handle-type))
  (cond
    ((null s) (luciffi:null-pointer))
    ((integerp s) (luciffi:make-pointer s))
    ((luciffi:pointerp s) s)
    (t (error "Not a pointer: ~a" s))))

(defmethod translate-from-foreign :around (ptr (type w32api-handle-type))
  (let ((luciffi::*default-foreign-encoding* *win32-wstring-encoding*))
    (if (null-pointer-p ptr)
	nil
	(call-next-method))))

;;flag types
#|
(define-foreign-type w32api-flag-type ()
  ((available-flags :initarg :flag :reader available-flags))
  (:actual-type :unsigned-int))

(defmethod translate-to-foreign (s (type w32api-flag-type))
  (let ((available-flags (available-flags type)))
    (cond
      ((integerp s) s)
      ((listp s)
       (apply #'logior (mapcar (lambda (x)
				 (cond
				   ((symbolp x)
				    (or (cadr (assoc x available-flags :test #'eql))
					(error "Unrecognizable-flag: ~a ." x)))
				   (t x))) s))))))
(defmethod translate-from-foreign (v (type w32api-flag-type))
  (let ((available-flags (available-flags type)))
    (loop for (flag-symbol flag-value) in available-flags
	 if (equal (logior v flag-value) flag-value)
	 collecting flag-symbol into total-symbol-list
	 and collecting flag-value into total-value-list
	 end
	 finally (let ((rest-values (logxor v (apply #'logior total-value-list))))
		   (when (not (zerop rest-values))
		     (setf total-symbol-list (nconc total-symbol-list
						    (list rest-values))))
		   (return total-symbol-list)))))

(lutilities:defmacro-exported define-flags-type
    (name available-flags &optional documentation)
  (declare (ignore documentation))
  (cffi::warn-if-kw-or-belongs-to-cl name)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (cffi::notice-foreign-type
      ',name (make-instance 'w32api-flag-type :flag ',available-flags))))
  
|#
;;(lutilities:defexport define
