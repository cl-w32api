(in-package cl-user)

(require 'lucifer-luciffi)

(defpackage cl-w32api.utils
  (:nicknames w32apiutils)
  (:use :cl :lutilities :luciffi))

(in-package cl-w32api.utils)

(defvar-exported +win32-string-encoding+ :cp1251)
(defvar-exported +win32-wstring-encoding+ :ucs-2le)

;;export macros for cffi macros.
(defmacro-exported defctype-exported (&whole form name base-type &optional documentation)
  (declare (ignore name base-type documentation))
  `(defexport defctype ,@(cdr form)))

;;wrong -- TODO: process struct name from arg.
(defmacro-exported defcstructex-exported (&whole form name-and-options &body fields)
  (declare (ignorable))
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',(if (atom name-and-options)
		     name-and-options
		     (car name-and-options))))
     (defcstruct-extended ,@(cdr form))))
  
;;wrong  -- TODO: process lisp-name from arg.
(defmacro-exported defcfunex-exported
    (&whole form (c-name lisp-name &rest other-options) return-type &body args)
  (declare (ignorable c-name other-options return-type args))
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',lisp-name))
     (defcfun-extended ,@(cdr form))))

(defmacro-exported define-abbrev-exported (short long)
  `(progn (eval-when (:compile-toplevel :load-toplevel :execute)
	    (export ',short))
	  (eval-when (:load-toplevel :execute)
	    (cond
	      ((macro-function ',long) (setf (macro-function ',short)
					     (macro-function ',long)))
	      ((fdefinition    ',long) (setf (fdefinition ',short)
					     (fdefinition    ',long)))
	      (t (error "~S is not defined!" ',long))))))
