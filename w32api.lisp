(in-package cl-user)

(defpackage cl-w32api
  (:nicknames w32api)
  (:use :cl))


(defpackage cl-w32api-user
  (:nicknames w32api-user)
  (:use :cl :cl-w32api :luciffi))
