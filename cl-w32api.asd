
(defpackage cl-w32api-asd
  (:use :cl :asdf))

(in-package cl-w32api-asd)

(defsystem cl-w32api
  :name "cl-w32api"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :description "A library for invoking W32api calls"
  :depends-on (:lucifer-luciffi)
  :components ((:file "w32api")
	       (:file "w32api-utils")
	       (:file "w32api-types")
	       (:file "w32api-module"
		      :depends-on ("w32api" "w32api-utils" "w32api-types"))
	       (:file "w32api-aux" :depends-on ("w32api" "w32api-module"))))

